<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\ShippingChargeController;
use App\Http\Controllers\ShippingAreaController;
use App\Http\Controllers\FrontImageController;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ExcelImportController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    return "Cache is cleared";
});

Route::get('/', function () {
    return view('front.wellcome');
});
// routes for mobile application
Route::get('/privacy-policy/app', function () {
	return view('privacy-policy');
});
Route::get('/terms-condition/app', function () {
	return view('termCondition');
});
		Route::post('login', [AuthController::class, 'login'])->name('custom.login');
		Route::post('register', [AuthController::class, 'register'])->name('user.register');
	
	
    Route::get('return-cashfree', [HomeController::class,'returnCashFree'])->name('return.cashefree');
	Route::post('notify-cashfree', [HomeController::class,'notifyPayment'])->name('notify.cashefree');
    Route::get('refresh/captcha', [HomeController::class, 'refreshCaptcha'])->name('refresh.captcha');
    Route::get('checkout', [HomeController::class, 'checkout'])->name('checkout');
   // Route::get('self-pickup', [HomeController::class, 'selfPickup'])->name('self.pickup');
    Route::get('time', [ShippingChargeController::class, 'timeCheck'])->name('get.time');
	Route::get('razorpay-create', [HomeController::class,'getRazorpayGateway'])->name('razorpay.payment.get');
    Route::middleware(['auth:sanctum', 'verified'])->group(function (){
	Route::get('dashboard', [HomeController::class, 'mainDashboard'])->name('dashboard');
	Route::get('address-data', [OrderController::class, 'addressData'])->name('address.data');
	Route::post('order-tracking', [OrderController::class, 'orderCreateTracking'])->name('create.order.tracking');
	Route::post('cancel', [OrderController::class, 'CancelOrder'])->name('order.status');
	Route::post('accept', [OrderController::class, 'acceptOrder'])->name('accept.order');
	Route::post('Shipping/vendor', [OrderController::class, 'shippingUpdate'])->name('shipping.update');
	Route::post('Shipping-address-update', [OrderController::class, 'shippingStatusUpdate'])->name('shipping-address-update');
	Route::get('/find-user/{id}/', [UserController::class, 'userName']);
	Route::resource('address', AddressController::class);
	//Route::get('address-edit', [AddressController::class, 'addressEdit'])->name('address.edit');
	Route::post('new-address-store', [AddressController::class, 'addressAdd'])->name('new.address.store');
	Route::get('user/edit/address/{id}', [AddressController::class, 'Editaddress'])->name('editadr');
	Route::get('user/delete/address/{id}', [AddressController::class, 'DeleteAddress'])->name('deleteadr');
	Route::post('add-delete', [HomeController::class, 'addDelete'])->name('add.delete');
     
});
Route::group(['middleware' => ['auth']], function () {
    Route::get('order-item-{id}', [HomeController::class,'viewItems'])->name('views.items');
	Route::get('my-order/list', [OrderController::class, 'userOderList'])->name('user.order.list');
	Route::get('myprofile', [UserController::class, 'MyProfile'])->name('myprofile.index');
});
Route::middleware('admin')->group(function (){
		Route::get('export-product', [HomeController::class, 'exportProduct'])->name('export.product');
		Route::put('selfprofile/{id}', [HomeController::class, 'selfProfile'])->name('self.update');
	    Route::post('file-uplaod', [HomeController::class, 'uplaodFile'])->name('file.upload');
	    Route::delete('queries-destroy{id}', [HomeController::class, 'queryDestroy'])->name('query.destroy');
	    Route::get('update-status{id}', [HomeController::class, 'updateStatus'])->name('status.update.query');
	    
	    Route::get('query', [HomeController::class, 'queryIndex'])->name('query.index');
		Route::resource('area', ShippingAreaController::class);
		Route::resource('faqs', FaqController::class);
		Route::resource('frontimage', FrontImageController::class);
		Route::resource('charge', ShippingChargeController::class);
		Route::resource('user', UserController::class);
		Route::get('user.dataTable', [UserController::class, 'dataTable'])->name('user.dataTable');
		Route::get('faqs.dataTable', [FaqController::class, 'faqdataTable'])->name('faqs.dataTable');
		Route::resource('brand', BrandController::class);
		Route::resource('category', CategoryController::class);
		Route::get('cat.dataTable', [CategoryController::class, 'dataTable'])->name('cat.dataTable');
		Route::resource('blog', BlogController::class);
		Route::get('user.dataTable', [UserController::class, 'dataTable'])->name('user.dataTable');
		Route::get('blog-datatable', [BlogController::class, 'dataTable'])->name('blog.dataTable');
		Route::resource('product', ProductController::class);
		Route::get('product.dataTable', [ProductController::class, 'dataTable'])->name('product.dataTable');
		Route::get('get-variant', [ProductController::class, 'variantData'])->name('variant.get.data');
		Route::resource('settings', SettingsController::class);
		Route::post('settings/update', [SettingsController::class, 'update'])->name('setting.update');
		Route::resource('order', OrderController::class);
		Route::get('/product-variant/{pvt}/', [OrderController::class, 'variantName']);
		Route::resource('page', PageController::class);    
		Route::get('page-datatable', [PageController::class,'dataTable'])->name('page.dataTable');
		Route::post('images-file/{foldername}', [PageController::class,'uploadImages'])->name('page.imageupload');
		Route::get('variant.found', [OrderController::class, 'variantFound'])->name('variant.found');
        Route::get('order-invoice/{id}', [OrderController::class, 'orderinvoice'])->name('order.invoice');
		Route::get('order-dataTable', [OrderController::class, 'dataTable'])->name('order.dataTable');
		Route::get('allorder-dataTable', [OrderController::class, 'allOrderdataTable'])->name('allorder.dataTable');
		Route::get('variant-dataTable', [ProductController::class, 'varianDataTable'])->name('variant.dataTable');
		Route::get('get-variant-data', [OrderController::class, 'getVariant'])->name('variant.data');
		Route::get('specific-user}', [UserController::class, 'specificUser'])->name('specific.user');
		Route::resource('notification', NotificationController::class);
		//Route::get('admin-seen-notification', [NotificationController::class, 'adminSeenNotification'])->name('adminSeenNoti');
	Route::get('all-notification', [NotificationController::class, 'allNoti'])->name('allNoti');
	Route::get('notification-datatable', [NotificationController::class, 'dataTable'])->name('notification.dataTable');
	//Route::get('get-notification', [NotificationController::class, 'getNotification'])->name('get.notification');
	Route::get('/import-excel', [ExcelImportController::class,'index']);
	Route::post('/import-excel', [ExcelImportController::class,'import'])->name('import.excel');
});
		
         Route::get('test', [HomeController::class, 'test'])->name('test');
         Route::get('shop', [HomeController::class, 'shop'])->name('shop');
		 Route::get('favorite-product', [HomeController::class, 'favoriteProduct'])->name('favorite.product');
         Route::get('offer-product', [HomeController::class, 'offer'])->name('offer');
		 Route::get('contact/{url}', [HomeController::class, 'contactUser'])->name('contact.user');
		 Route::get('check-coupon', [CouponController::class, 'checkCoupon'])->name('cart.checkcoupon');
		 Route::get('delete-coupon', [CouponController::class, 'deleteCoupon'])->name('delete.order.coupon');
		 Route::get('cart', [HomeController::class, 'cart'])->name('cart');
		 Route::get('products/category/{url}', [HomeController::class, 'categoryCat'])->name('catCat');
		 Route::get('faq', [HomeController::class, 'faq'])->name('faq');
		 Route::get('{url}', [HomeController::class,'staticPage'])->name('static');
		 Route::post('subscribe', [HomeController::class, 'subscribe'])->name('subscribe');
		 Route::post('add-cart', [HomeController::class, 'addToCart'])->name('add.cart');
		 Route::get('product/detail/{slu}', [HomeController::class, 'ProductDetail'])->name('detail.product');
		 Route::get('brands/{name}/', [HomeController::class, 'brandProducts'])->name('brand.product');
		 Route::post('/', [HomeController::class,'inquiryVP'])->name('inquiry.product');
		 Route::post('createOrder/order', [HomeController::class, 'createOrder'])->name('createOrder');
		 Route::post('remove/cart', [HomeController::class, 'removeCart'])->name('remove.cart');
		 Route::post('add-favorite', [HomeController::class, 'addFavorite'])->name('add.favorite');
		 
		 Route::post('sendmail', [HomeController::class, 'sendEmail'])->name('sendmail.user');
		Route::post('shipping-charge', [HomeController::class, 'getShiping'])->name('shippingCharge');
