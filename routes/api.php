<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\BrandController;
use App\Http\Controllers\Api\CheckoutController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);
Route::post('logout', [AuthController::class, 'logout']);
Route::middleware(['auth:sanctum'])->group(function(){
	Route::get('get-user-info', [AuthController::class, 'getUserInfo']);
	Route::get('categories', [CategoryController::class, 'categories']);
	Route::get('products', [CategoryController::class, 'getproducts']);
	Route::get('top-selling-products', [CategoryController::class, 'getTopSellingProducts']);
	Route::get('new-products', [CategoryController::class, 'getNewpProducts']);
	Route::get('get-product-by-type-id', [CategoryController::class, 'getProductByTypeId']);
	
	//old routes
	Route::post('check-login', [AuthController::class, 'checkLogin']);
    Route::put('update-user', [AuthController::class, 'updateUser']);
    Route::post('user-address', [AuthController::class, 'getUserAddress']);
    Route::delete('delete-user-address', [AuthController::class, 'deleteUserAddress']);
    Route::post('add-address', [AuthController::class, 'addAddress']);
    Route::post('setLastAdd', [AuthController::class, 'setLastAdd']);
	Route::post('set-token', [AuthController::class, 'SetDeviceToken']);
	Route::post('setting-value', [CategoryController::class, 'getSettingValue']);
	Route::post('/setting-value-amount', [CategoryController::class, 'getSettingValueAmount']);
	Route::post('brands', [BrandController::class, 'brands']);
	Route::post('offers-list', [CategoryController::class, 'getOffers']);
	Route::post('get-special-product', [CategoryController::class, 'getSpecialproducts']);
	Route::post('single-product', [CategoryController::class, 'singleProduct']);
	Route::post('add-to-cart', [CategoryController::class, 'addToCart']);
	 Route::post('get-all-notification', [CategoryController::class, 'getAllNotification']);
	Route::post('cart-products', [CategoryController::class, 'getCartProducts']);
	Route::post('get-area', [AuthController::class, 'getShippingArea']);
	Route::post('shipping-charge', [AuthController::class, 'getShippingCharge']);
	Route::post('getShiping-charge', [AuthController::class, 'getShiping']);
	Route::post('cart-product-quantity', [CategoryController::class, 'cartProductQty']);
	Route::delete('remove-cart-products', [CategoryController::class, 'removeCartProducts']);
	Route::post('checkout', [CheckoutController::class, 'checkout']);
	Route::post('orders-list', [CategoryController::class, 'getOrders']);
	Route::post('order-items', [CategoryController::class, 'getOrdersItems']);
	Route::post('search', [BrandController::class, 'search']);
	Route::post('faqs', [BrandController::class, 'faqs']);
	Route::post('submit-query', [BrandController::class, 'submitQuery']);
	Route::post('add-favorite', [BrandController::class, 'addFavorite']);
	Route::get('only-favorite', [BrandController::class, 'onlyFavorite']);
});
