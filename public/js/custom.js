$(document).ready(function () {
  // lightbox.option({
    // resizeDuration: 200,
    // wrapAround: true,
  // });

  $(".shop-view a").click(function () {
    if (!$(this).hasClass("current")) {
      $(".shop-view a").removeClass("current");
      $(this).toggleClass("current");
      $(this).parents(".productFilterHead").toggleClass("listVw");
    }
  });

  // $(".priceFilter").slider({
  //     min: 0,
  //     max: 100,
  //     step: 1,
  //     values: [10, 90],
  //     slide: function(event, ui) {
  //         for (var i = 0; i < ui.values.length; ++i) {
  //             $("input.sliderValue[data-index=" + i + "]").val(ui.values[i]);
  //         }
  //     }
  // });
function datatableRender(num,url)
{
var table=$('#listtable'+num).DataTable({
responsive: false,
processing: false,
searching: true,
serverSide: false,
stateSave: false,
bLengthChange: false,
bInfo: false,
ordering: false,
bPaginate: false,
bFilter: false,

} );

$('#myInputTextField').keyup(function(){
table.search($(this).val()).draw() ;
});

return table;
}
function datatableRenderWithoutAjax(num)
{
var table=$('#listtable'+num).DataTable();

$('#myInputTextField').keyup(function(){
table.search($(this).val()).draw() ;
});

return table;
}
  $(".sliderValue").change(function () {
    var $this = $(this);
    $(".priceFilter").slider("values", $this.data("index"), $this.val());
  });

  $(".suggest-items").slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 6,
  });

  $(".homBannerWrap").slick({
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
  });

  $(".homeBestSeller").slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ],
  });
  $(".clentSec .clientInr").slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 9,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1000,
    infinite: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ],
  });
});
//tiny code

function loadTinyMCe(id,url)
{
	
tinymce.init({
  selector: '#'+id,
  branding: false,
  statusbar: false,
  images_upload_url: url,
  //images_upload_base_path:'/',
  plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons',
  imagetools_cors_hosts: ['picsum.photos'],
  menubar: 'file edit view insert format tools table help',
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | preview | image media link codesample | ltr rtl | code',
  fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 36pt",
  //toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
  toolbar_sticky: true,
   relative_urls : false,
  // autosave_ask_before_unload: true,
  // autosave_interval: "30s",
  // autosave_prefix: "{path}{query}-{id}-",
  // autosave_restore_when_empty: false,
 // autosave_retention: "2m",
  image_advtab: true,
 
  content_css: [
    
  ],
  // link_list: [
    // { title: 'My page 1', value: 'http://www.tinymce.com' },
    // { title: 'My page 2', value: 'http://www.moxiecode.com' }
  // ],
  // image_list: [
    // { title: 'My page 1', value: 'http://www.tinymce.com' },
    // { title: 'My page 2', value: 'http://www.moxiecode.com' }
  // ],
  // image_class_list: [
    // { title: 'None', value: '' },
    // { title: 'Some class', value: 'class-name' }
  // ],
  importcss_append: true,
  height: 400
  // file_picker_callback: function (callback, value, meta) {
    // /* Provide file and text for the link dialog */
    // if (meta.filetype === 'file') {
      // callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
    // }

    // /* Provide image and alt text for the image dialog */
    // if (meta.filetype === 'image') {
      // callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
    // }

    // /* Provide alternative source and posted for the media dialog */
    // if (meta.filetype === 'media') {
      // callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
    // }
  // },
  // templates: [
        // { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
    // { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
    // { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
  // ],
  // template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  // template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  // height: 600,
  // image_caption: true,
  // quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  // noneditable_noneditable_class: "mceNonEditable",
  // toolbar_drawer: 'sliding',
  // contextmenu: "link image imagetools table",
 });
}
function renderTinymce(id)
{
	tinymce.init({
	selector: '#'+id,
	branding: false,
	statusbar: false,
	menubar:false,
	plugins: 'link codesample lists code',
	fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 36pt",
	toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | code',
	toolbar_sticky: true,
	relative_urls : false,
	//content_style: "body { color:#fff; }"
	});
}
function renderBasicTiny(id)
{
	tinymce.init({
		selector: '#'+id,
		themes: 'modern',
			branding: false,
	statusbar: false,
	menubar:false,
	plugins: 'link codesample lists code',
	fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 36pt",
	toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | code',
	toolbar_sticky: true,
	relative_urls : false,
	});
}
