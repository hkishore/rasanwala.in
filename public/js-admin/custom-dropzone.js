Dropzone.autoDiscover = false;
function addDropZone(id,url)
{
	//if(ftype && ftype=)		
	//var ftype="image";
   var ftype=".jpeg,.jpg,.png";
    // if(fileType=="image")
	// {
		 // 
	// }
	// else if(fileType=="artwork")
	// {
		// ftype=".pdf,.ai";
	// }
   $("#"+id).dropzone({ 
	url: url,
	addRemoveLinks: false,
	uploadMultiple:false,
	queueLimit:1,
	maxFiles: 1,
	maxFilesize: 50,  //1 mb is here the max file upload size constraint
    acceptedFiles: ftype,
	init: function() {
		
		var dzone=this;
		this.on("addedfile", function(file) {
			var extArray=file.name.split('.');
			var ftypeArray=ftype.split(',');
			var ext='.'+extArray[extArray.length-1];
			var size=file.size/1024/1024;
			if(ftype!="")
			{ 
				
				if(!ftypeArray.includes(ext) )
				{
				dzone.removeFile(file);
				toastr["error"]("Invalid file format");
				}
				else if(size>30)
				{
				dzone.removeFile(file);
				toastr["error"]("Fiel size should be 30 MB.");
				}
			}
			
		  });
	   },
	removedfile: function(file) {
	   handleRemoveFile(id);
	 return true;
	},
	success: function (file, response) {
	//	console.log(response.success);
	//var imgName = JSON.parse(response);
	if(typeof(response)=="string")
	{
	response = JSON.parse($.trim(response));
	}
	file.previewElement.classList.add("dz-success");
	$("#"+id).find('.uploaded_file').val(response.success);
	addPreview(id,response.success,'iframe',this,file);	
	},
   maxfilesexceeded: function(file) {
        this.removeAllFiles();
        this.addFile(file);
		handleFile(id);
    },
	error: function (file, response) {
	console.log(response);
	file.previewElement.classList.add("dz-error");
	this.removeAllFiles();
	var erMs="Please upload valid file";
	if(ftype!="")
	{
		erMs=erMs+" ( "+ftype+")";
	}
	 toastr["error"](erMs);
	 handleFile(id);
	},
	sending: function(file, xhr, formData){
	formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
	}
	});
}

function removeImage(obj,id){
    $("#"+id).val("");
	$(obj).parent().remove();
}

function getObj(id)
{
	var parentObj=$("#"+id).parent().parent();
	     if(parentObj.find('.newfileSec').length==0)
		 {
			  parentObj=$("#"+id).parent().parent().parent();
		 }
		 return parentObj;
}
function handleFile(id)
{
	   var parentObj=getObj(id);
		parentObj.find('.exitfile').show();
		parentObj.find('.newfileSec').hide();
}
function addPreview(id,file,type,dzone,fileObj)
{
	     var parentObj=getObj(id); 
		parentObj.find('.dz-message').hide();
		 parentObj.find('.exitfile').hide();
		
		parentObj.find('.newfileSec').html('<a href="javascript:void(0)" class="slink btn me-2 dataActionBtn btnDelete" id="'+id+'btnd"><img src="/img/datatable-delete.svg" alt=""></a>').show();
			

        $('#'+id+'btnd').click(function (e) {
     
         dzone.removeFile(fileObj);
         });
        
		
}
function handleRemoveFile(id)
{
	 var parentObj=getObj(id);
		parentObj.find('.exitfile').show();
		parentObj.find('.newfileSec').hide();
		$("#"+id).find('.uploaded_file').val('');
		$("#"+id).find('.dz-preview').remove();
		$("#"+id).find('.dz-message').show();
}

// function showFile(filename)
// {
	// //var path="https://docs.google.com/gview?url="++"&embedded=true"
	// $.fancybox.open({
		// type : 'iframe',
		// src  : "",
		// buttons : [
		// 'fb',
		// 'close'
		// ]
		// });
// }
function deleteFile(obj,id)
{
	$('#'+id).find('.dz-message').html('Drag and Drop File Here<br>');
	$('#remove_'+id).val('y');
	$(obj).parent().remove();
}