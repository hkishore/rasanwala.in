var host = "ws://beta.finewrapind.com:8090";
//var host = "wss://www.ikoowi.com/wss";
var conn = new WebSocket(host);
var sound1 = new Audio("../sound1.mp3");


// ***************************** Functions related to websockets **********************************

// webscoket handler for performing action after socket connect
conn.onopen = function (e) {
    console.log("Websockets connection established!");
    conn.send(JSON.stringify({ command: "register", userId: userId}));
	if(itemCnt!=null && itemCnt.length>0)
	 {
			$.each(itemCnt, function( index, value ) {
				//console.log(value.toString(),'rrrr');
			 chat(value.toString(),'message',action_type);
			});
	 }

};

// websocket handler for receiving messages from websocket
conn.onmessage = function (e) {
    data = JSON.parse(e.data);
	//console.log(data);
    if (data.command === "message") {
        if (data.to === userId) {
			var msg="Item Approved";
            if(data.type=="item")
			{
				$('.itemcnt').html(parseInt($('.itemcnt').html())+1);
				msg="New Item has been added";
			}
			else if(data.type=="order")
			{
				$('.ordercnt').html(parseInt($('.ordercnt').html())+1);
				msg="New Order has been added";
			}
			toastr.success(msg);
			playSound(sound1);
        }
       
    }    
};

// function for sending chat messages to websocket
const chat = (to, msg,type) => {
    conn.send(JSON.stringify({ command: "message", from: userId, to: to, message: msg,type:type }));
}

const playSound = (soundClip) => {
    soundClip.pause();
    soundClip.currentTime = 0;
    soundClip.play();
}
