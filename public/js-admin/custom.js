

$(window).resize(function(){
	
	if($('body').width() > 768){
	}else{
		menuChecker();
	}

});

$(document).ready( function () {
	//older code
	$('.menutoggler').click(function(){
		$('.dashboard').toggleClass('closeMenu');
	});	
	
	if($('body').width() < 768){
		menuChecker();
	}
	
	
	//navItemShow();

     $('.sidenav .navbar-nav li.nav-item.readMore > a').click(function(){
         $(this).toggleClass('less');
         $('.sidenav .navbar-nav .nav-item').toggleClass('showAll');
     })
	
} );


    
window.onload = function(){

    $("#menudiv").mCustomScrollbar();
   // navItemShow();
}

function menuChecker(){
	console.log('woking');
	$('.dashboard').toggleClass('closeMenu');
	
}
function datatableRender(num,url)
{
var table=$('#listtable'+num).DataTable({
responsive: true,
processing: true,
serverSide: true,		
ajax:  url,
stateSave: true,
bLengthChange: false,
bInfo: true,
ordering: false

} );

$('#myInputTextField').keyup(function(){
table.search($(this).val()).draw() ;
});

return table;
}

function datatableRenderWithoutAjax(num)
{
var table=$('#listtable'+num).DataTable();

$('#myInputTextField').keyup(function(){
table.search($(this).val()).draw() ;
});

return table;
}

function loadTinyMCe(id,url)
{
	
tinymce.init({
  selector: '#'+id,
  branding: false,
  statusbar: false,
  images_upload_url: url,
  //images_upload_base_path:'/',
  plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists imagetools textpattern noneditable help charmap quickbars emoticons',
  imagetools_cors_hosts: ['picsum.photos'],
  menubar: 'file edit view insert format tools table help',
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | preview | image media link codesample | ltr rtl | code',
  fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 36pt",
  //toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
  toolbar_sticky: true,
   relative_urls : false,
  // autosave_ask_before_unload: true,
  // autosave_interval: "30s",
  // autosave_prefix: "{path}{query}-{id}-",
  // autosave_restore_when_empty: false,
 // autosave_retention: "2m",
  image_advtab: true,
 
  content_css: [
    
  ],
  // link_list: [
    // { title: 'My page 1', value: 'http://www.tinymce.com' },
    // { title: 'My page 2', value: 'http://www.moxiecode.com' }
  // ],
  // image_list: [
    // { title: 'My page 1', value: 'http://www.tinymce.com' },
    // { title: 'My page 2', value: 'http://www.moxiecode.com' }
  // ],
  // image_class_list: [
    // { title: 'None', value: '' },
    // { title: 'Some class', value: 'class-name' }
  // ],
  importcss_append: true,
  height: 400
  // file_picker_callback: function (callback, value, meta) {
    // /* Provide file and text for the link dialog */
    // if (meta.filetype === 'file') {
      // callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
    // }

    // /* Provide image and alt text for the image dialog */
    // if (meta.filetype === 'image') {
      // callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
    // }

    // /* Provide alternative source and posted for the media dialog */
    // if (meta.filetype === 'media') {
      // callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
    // }
  // },
  // templates: [
        // { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
    // { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
    // { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
  // ],
  // template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  // template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  // height: 600,
  // image_caption: true,
  // quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  // noneditable_noneditable_class: "mceNonEditable",
  // toolbar_drawer: 'sliding',
  // contextmenu: "link image imagetools table",
 });
}
function renderTinymce(id)
{
	tinymce.init({
	selector: '#'+id,
	branding: false,
	statusbar: false,
	menubar:false,
	plugins: 'link codesample lists code',
	fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 36pt",
	toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | code',
	toolbar_sticky: true,
	relative_urls : false,
	//content_style: "body { color:#fff; }"
	});
}
function renderBasicTiny(id)
{
	tinymce.init({
		selector: '#'+id,
		themes: 'modern',
			branding: false,
	statusbar: false,
	menubar:false,
	plugins: 'link codesample lists code',
	fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt 24pt 26pt 28pt 30pt 36pt",
	toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | code',
	toolbar_sticky: true,
	relative_urls : false,
	});
}
function navItemShow(){
    var navLiHeight = 0; 
    var itemCounter = 0; 
    var navItemOutrHight = $('.sidenav .navbar-nav .nav-item').outerHeight();
   $('.sidenav .navbar-nav .nav-item').each(function(){
       navLiHeight = navLiHeight + $(this).outerHeight()
       itemCounter = itemCounter + 1
   });
   if(navLiHeight > $('#menudiv').outerHeight()){

       var finalMinus =  $('#menudiv').outerHeight() - 150;
       finalMinus = finalMinus / navItemOutrHight;
       console.log(finalMinus)
       var showValue = 1;
       $('.sidenav .navbar-nav .nav-item').each(function(){
           if(showValue < finalMinus - 1){
               $('.sidenav .navbar-nav .nav-item:nth-child('+ showValue +')').addClass('shouldshow')
               showValue = showValue + 1;
           }
       })
       // setTimeout(function(){
           // $('.sidenav .navbar-nav .nav-item:not(.shouldshow)').addClass('hide');
       // },500);
   }
}
  