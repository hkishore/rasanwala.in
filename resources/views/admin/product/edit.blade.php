@extends('layouts.admin')

@section('content')

     <div class="pageTtl">
            <h1>Edit Product</h1>
			</div>
		  <div class="greyBx">
            <div class="innerWrap">	
              {{ Form::model($model,['route' => ['product.update', $model->id],'enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }}
                    @method('PUT')
                    @include('admin.product.form')            

                {{ Form::close() }}	
    </div>
    </div>

@endsection