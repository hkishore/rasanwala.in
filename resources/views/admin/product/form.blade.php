<?php

use App\Models\Common;
use App\Models\Brand;
use App\Models\User;
use App\Models\Category;

$userRole = Auth::user();
$role = $userRole->role;
$oldfile = "";
$cat_id = '';
$status = '';
if (isset($model)) {
	$cat_id = $model->categeory_id;
	if ($model->image != '') {
		$oldfile = $model->image;
	}
	$status = 1;
}
$cat_name = Common::getSubCategoryList($status);
$catSelect = '<option value="">Select Categeory</option>';
$categeory = [];
foreach ($cat_name as $key => $cats) {

	$subcats = '';
	foreach ($cats as $kc => $cat) {
		$subcats .= '<option value="' . $kc . '">' . $cat . '</option>';
	}
	$catSelect .= '<optgroup label="' . $key . '">' . $subcats . '</optgroup>';
}
$brand = Brand::pluck('name', 'id');
$visible = config('custom.visible');
$deleteImage = asset('img/datatable-delete.svg');

?>

<div class="row">
	<div class="col-md-12">

		<div class="row">
			<div class="mb-2rem col-md-6 {{ $errors->has('name') ? 'has-error' : '' }}">
				<label for="name" class="form-label">Name <span class="red"> *</span></label>
				{{ Form::text('name',  null, ['class' =>'form-control','placeholder'=>'Name','id'=>'name','required'=>'required', 'value' => old('name')]) }}
				@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>
			<div class="mb-2rem col-md-6 status {{ $errors->has('status') ? 'has-error' : '' }}">
				<label for="status" class="form-label">Status</label>
				{{ Form::select('status', ['1'=>'Active','0'=>'InActive'],null, ['class' => 'form-select', 'value' => old('status')]) }}
				@if ($errors->has('status'))
				<span class="help-block">
					<strong>{{ $errors->first('status') }}</strong>
				</span>
				@endif
			</div>
		</div>

		<div class="gift_b col-md-12">
			<div class="row">
				<div class="mb-2rem col-md-6 {{ $errors->has('brand_id') ? 'has-error' : '' }}">
					<label for="brand_id" class="form-label">Brand Name</label>
					{!! Form::select('brand_id', $brand, null, ['class' => 'form-select','placeholder' => 'Select Brand', 'value' => old('brand_id')]) !!}
					@if($errors->has('brand_id'))
					<span class="help-block">
						<strong>{{ $errors->first('brand_id') }}</strong>
					</span>
					@endif
				</div>
				<div class="mb-2rem col-md-6 {{ $errors->has('category_id') ? 'has-error' : '' }}">
					<label for="category_id" class="form-label">Categeory <span class="red"> *</span></label>
					<select class="form-control categeory_id" name="categeory_id" id="categeory_id"></select>
					@if ($errors->has('category_id'))
					<span class="help-block">
						<strong>{{ $errors->first('category_id') }}</strong>
					</span>
					@endif
				</div>

			</div>
		</div>
		<div class="gift_b col-md-12">
			<div class="row">
				<div class="mb-2rem col-md-6 {{ $errors->has('brand_id') ? 'has-error' : '' }}">
					<label for="brand_id" class="form-label">GST %</label>
					{{ Form::text('gst_percent',  null, ['class' =>'form-control','placeholder'=>'GST','id'=>'gst','required'=>'required', 'value' => old('gst')]) }}
					@if ($errors->has('gst'))
					<span class="help-block">
						<strong>{{ $errors->first('gst') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">

				<div class="mb-2rem col-md-6 {{ $errors->has('checkbox') ? 'has-error' : '' }}">
					<input type="checkbox" id="is_api_use" name="best_sell" value="1" class="form-check-input" @if(!empty($model) && $model->best_sell=='1') checked @endif>
					<label class="form-check-label" for="exampleCheck1">Best Seller</label>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="mb-2rem col-md-12 {{ $errors->has('description') ? 'has-error' : '' }}">
					<label for="description" class="form-label">Description</label>
					{{ Form::textarea('description',null, ['class' => 'form-control','placeholder'=>'Write somthing here............','id'=>'description', 'value' => old('description')]) }}
					@if ($errors->has('description'))
					<span class="help-block">
						<strong>{{ $errors->first('description') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="mb-2rem col-md-6 {{ $errors->has('image') ? 'has-error' : '' }}">
					@include('admin.image-uploader',['label'=>'Main Image','key'=>'image','oldfile'=>$oldfile])

					@if ($errors->has('images'))
					<span class="help-block">
						<strong>{{ $errors->first('image') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>


		<div class="col-md-12" id="variable_product">
			<div class="mb-2rem col-md-3">
				<a onclick="AddMore()" class="btn btn-primary themeBtn themeFill iconLeft">
					Add Variable &nbsp;
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
						<path fill-rule="evenodd" clip-rule="evenodd" d="M0 8.69814H6.56325V15.261H8.69814V8.69814H15.261V6.56322H8.69814V0H6.56325V6.56322H0V8.69814Z" fill="white" />
					</svg>
				</a>
			</div>
			<table class="display table table-striped">
				<thead>
					<tr>
						<th>Size</th>
						<th>Regular Price</th>
						<th>Sale Price</th>
						<th>Visible</th>
						<th>Order Limit</th>
						<th>Stock Limit</th>
						<th>Shipping Charge</th>
						<th>Action:</th>
					</tr>
				</thead>
				<tbody id="VariantId">
					@if(isset($variant) && count($variant)>0)
					@foreach($variant as $key=>$variantData)

					<tr>

						<td>
							<input type="hidden" class='form-control ' name="vids[]" value="{{$variantData->id}}" />
							<input type="text" class='form-control ' name="size[]" value="{{$variantData->size}}" placeholder="Size" required />
						</td>

						<td><input type="text" class='form-control ' name="regular_price[]" value="{{$variantData->regular_price}}" placeholder="Regular Price" required /> </td>

						<td><input type="text" class='form-control ' name="sale_price[]" value="{{$variantData->sale_price}}" placeholder="Sale Price" required /> </td>
						<td>
							<!--<select class = 'form-select ' name="visible[]">
									  @foreach($visible as $key=>$vb)
									  <option value="{{$key}}">{{$vb}}</option>
									@endforeach
									</select>-->
							{{ Form::select('visible[]', $visible, $variantData->visible, ['class' => 'form-select' ,'value' => old('visible')]) }}
						</td>
						<td><input type="text" class='form-control ' name="order_limit[]" value="{{$variantData->	order_limit}}" placeholder="Order Limit"  /> </td>
						<td><input type="text" class='form-control ' name="stock_limit[]" value="{{$variantData->stock_limit}}" placeholder="Stock Limit"  /> </td>
						<td><input type="text" class='form-control ' name="charge[]" value="{{$variantData->charge}}" placeholder="Shipping Charge"  /> </td>
						<td>
							<a href="javascript:void(0)" title="Delete" onclick="$(this).parent().parent().remove();" style=""><i class="fa fa-trash-o"></i></a>
						</td>

						@endforeach
						@else

						<td>
							<input type="hidden" class='form-control ' name="vids[]" value="" />
							<input type="text" class='form-control ' name="size[]" value="" placeholder="Size" required />
						</td>

						<td><input type="text" class='form-control ' name="regular_price[]" value="" placeholder="Regular Price" required /> </td>

						<td><input type="text" class='form-control ' name="sale_price[]" value="" placeholder="Sale Price" required /> </td>

						<td>
							<select class='form-select ' name="visible[]">
								@foreach($visible as $key=>$vb)
								<option value="{{$key}}">{{$vb}}</option>
								@endforeach
							</select>
						</td>

						<td><input type="text" class='form-control ' name="order_limit[]" value="" placeholder="Order Limit"  /> </td>

						<td><input type="text" class='form-control ' name="stock_limit[]" value="" placeholder="Stock Limit"  /> </td>

						<td><input type="text" class='form-control ' name="charge[]" value="" placeholder="Shipping Charge"  /> </td>

						<td>
							<a id="deleteId" href="javascript:void(0)" title="Delete" onclick="$(this).parent().parent().remove();" style="display:none"><i class="fa fa-trash-o"></i></a>
						</td>

						@endif

					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="mb-2rem col-md-6 {{ $errors->has('meta_title') ? 'has-error' : '' }}">
					<label for="meta_title" class="form-label">Meta Title</label>
					{{ Form::text('meta_title',  null, ['class' => 'form-control','placeholder'=>'Meta Title','id'=>'meta_title','value' => old('meta_title')]) }}
					@if ($errors->has('meta_title'))
					<span class="help-block">
						<strong>{{ $errors->first('meta_title') }}</strong>
					</span>
					@endif
				</div>
				<div class="mb-2rem col-md-6 {{ $errors->has('meta_keyword') ? 'has-error' : '' }}">
					<label for="meta_keyword" class="form-label">Meta Keyword</label>
					{{ Form::text('meta_keyword',null, ['class' => 'form-control','value' => old('meta_keyword'),'id'=>'meta_keyword']) }}
					@if ($errors->has('meta_keyword'))
					<span class="help-block">
						<strong>{{ $errors->first('meta_keyword') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="mb-2rem col-md-12 {{ $errors->has('meta_description') ? 'has-error' : '' }}">
					<label for="meta_description" class="form-label">Meta Description</label>
					{{ Form::textarea('meta_description',null, ['class' => 'form-control','placeholder'=>'Write somthing here............','value' => old('meta_description'),'id'=>'meta_description']) }}
					@if ($errors->has('meta_description'))
					<span class="help-block">
						<strong>{{ $errors->first('meta_description') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>

		<div class="d-flex justify-content-start ">
			<button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
			<a href="{{ url()->previous() }}" class="btn btn-primary themeBtn themeUnFill">Cancel</a>
			<!--<a type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</a>-->
		</div>




		@push('scripts')
		<script type="text/javascript">
			var catSelect = {!!json_encode($catSelect)!!};
			var filepath = "{{route('file.upload')}}";
			var cat_id = "{{$cat_id}}";
			$('document').ready(function() {
				if ($('#image').length > 0) {
					addDropZone('image', filepath);
				}
				renderTinymce('description');
				$('#categeory_id').html(catSelect);
				if (cat_id != "") {
					$('#categeory_id').val(cat_id);
				}
				
			});

			function AddMore() {
				var cln = $('#VariantId').find('tr').eq(0).clone();
				cln.find("input").val("").end();
				$('#VariantId').append(cln);
				$('#deleteId').show();
			}
		</script>
		@endpush