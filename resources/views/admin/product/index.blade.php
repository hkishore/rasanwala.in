
<?php
use App\Models\User;
$status=config('custom.status');
$user=Auth::user();
?>
@extends('layouts.admin')
@section('content')
<?php
use App\Models\Category;
$mCat=Category::where('parent_id',null)->pluck('id')->toArray();
$category=Category::whereIn('parent_id',$mCat)->get();
?>
<div class="themeTable">
          <div class="table_id DatatableHeader" >
            <div class="pgTitle">
              <h1>Products</h1>
            </div>
            <div class="searchBx">
              <input type="text" id="myInputTextField" placeholder="Search">
            </div>
			&nbsp;&nbsp;
			<div class="col-md-2 pull-right searchBx" >
					<select id='cat_id'class="form-control searchBx">
					    <option value="0">Select Catgeory</option>
					 @foreach($category as $key=>$cat)
							<option value="{{$cat->id}}" >{{$cat->name}}</option>
					@endforeach
					</select>
            </div>
			&nbsp;&nbsp;
			<div class="col-md-2 pull-right searchBx" >
					<select id='status_id'class="form-control searchBx">
					 <option value="">Select Status</option>
					    <option value="1">Active</option>
					    <option value="0">InActive</option>
					</select>
            </div>
			&nbsp;&nbsp;
			<div class="col-md-2 pull-right searchBx" >
					<a href="{{route('export.product')}}" class="btn btn-primary themeBtn themeFill iconLeft">
                &nbsp; Export Product</a>
            </div>
            <div class="tableLength">
			
              <a href="{{route('product.create')}}" class="btn btn-primary themeBtn themeFill iconLeft">
                
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M0 8.69814H6.56325V15.261H8.69814V8.69814H15.261V6.56322H8.69814V0H6.56325V6.56322H0V8.69814Z" fill="white"/>
                </svg>
                &nbsp; Add new</a>
            </div>
          </div>
	<div class="container">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
 
    <form action="{{ route('import.excel') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group col-md-6">
            <label for="file">Choose Excel File</label>
            <input type="file" name="file" id="file" class="form-control">
			<button type="submit" class="btn btn-primary mt-1">Import Product List</button>
        </div>
    </form>
</div>
	<div class="table-responsive">
		<table id="listtable1" class="display table table-striped">
			<thead>
				<tr>
					<th>Image</th>
					<th>Name</th>
					<th>Brand</th>
                    <th>Categeory</th> 	
                    <th>Variant</th> 	
                    <th>GST%</th>	
                    <th>Status</th>                  
                    <th>Action</th>    
                </tr>
            </thead>
            <tbody>
			 </tbody>
            </table>
          </div>
      </div>
@endsection
@push('modals')
<div class="modal fade themeModal" id="variant-pop-up" >
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content" id="tableId">
            <!--<table id="listtable2" class="display table table-striped" style="width: -2px;">-->
			<table id="listtable2" class="display table table-striped dataTable" style="width: -2px;" role="grid">
			 <thead>
                <tr>
				<th>Size</th>	
                <th>Regular Price</th>
			    <th>Sale Price</th>
				<th>Order Limit</th>
				<th>Stock Limit</th>
				<th>Shiping Charge</th>
                <th>Visible</th>   
                </tr>
            </thead>
            <tbody class="settable">
			 </tbody>
            </table>
    </div>
  </div>
</div>
@include('admin.orders.pop')
<!--@include('variant-popup')-->
@endpush
@push('scripts')
<script type="text/javascript">
	var productTable;
	var variantTable;
	var pr_id;
	$( document ).ready(function() {
	productTable=$('#listtable1').DataTable({
			responsive: true,
			processing: true,
			serverSide: true,
			// searching: true,
			ajax:  {
				"url":"{{route('product.dataTable')}}",
				"data": function(data){
					data.cat_id = $('#cat_id').val();
					data.status_id = $('#status_id').val();
				}
			},
			
			stateSave: false,
			bLengthChange: true,
			bInfo: true,

		});
       $('#myInputTextField').keyup(function(){
       productTable.search($(this).val()).draw() ;
         });
		 $('#cat_id').change(function(){
			productTable.draw();
		}); 
		$('#status_id').change(function(){
			productTable.draw();
		});

		 // variantTable=$('#listtable2').DataTable({
			// responsive: false,
			// processing: true,
			// serverSide: true,
			// searching: false,
			// ajax:  {
				// "url":"{{route('variant.dataTable')}}",
				// "data": function(data){
					// data.product_id = pr_id;
				// }
			// },
			
			// stateSave: true,
			// bLengthChange: false,
			// bInfo: false,
			// //bPaginate: false,

		// });
	});
function variantShow(pId){
	$('#variant-pop-up').modal('show');
	$('.settable').html('');
				 $.ajax({
           type: "get",
           url: "{{route('variant.get.data')}}",
           dataType: "json",
           data: {	product_id: pId },
		   success: function (data) {
			   console.log(data);
			if(data.length>0 && data!="")
			{
			  $.each(data, function(key,value) {
				var visible="Customer";
				if(value.visible==3)
				{
					visible="Store Keeper";
				}
				if(value.visible==4)
				{
				visible="Both";
				}
				 $('.settable').append('<tr><td>'+value.size+'</td><td>₹ '+value.regular_price+'</td><td>₹ '+value.sale_price+'</td><td>₹ '+value.order_limit+'</td><td>₹ '+value.stock_limit+'</td><td>₹ '+value.shiping_charge+'</td><td>'+visible+'</td></tr>');
				 });
			}else{
				$('.settable').html('Not found any data');
			}
		   }
		   });
	
}
function deleteProduct(obj)
{
	var url=$(obj).data('href');
	$('#confirm-delete .modal-title').html('Delete Product');
    $('#confirm-delete form').attr('action',url);
	$('#confirm-delete .text-center').html('Are you sure want to delete this product?');
	$('#confirm-delete').modal('show');
}
function variantDetails(id)
{
}
</script>
@endpush