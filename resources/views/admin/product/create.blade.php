@extends('layouts.admin')

@section('content')

    <div class="pageTtl">
            <h1>Add Product</h1>
        </div>       
    <div class="greyBx">
            <div class="innerWrap"> 
                {{ Form::open(['route' => 'product.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }} 
                                      
                    @include('admin.product.form') 

                {{ Form::close() }}
           </div>
    </div>

@endsection
