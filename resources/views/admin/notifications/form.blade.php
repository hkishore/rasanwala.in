<?php

use App\Models\User;
use App\Models\Notification;

$userType = config('constant.userTypeN');
//dd($userType);
//dd($users);
$id = [];
if (isset($model) && !empty($model)) {
	$id = Notification::where('notification_id', $model->id)->pluck('user_id')->toArray();
	$users = User::where('status', 1)->get();
}

?>
<div class="row">
	<div class="col-4 col-md-4 mb-3">
		<label>Subject</label>
		{{ Form::text('subject',null, ['id' => 'subject', 'class' => 'form-control', 'value' => old('subject')]) }}
		@if ($errors->has('subject'))
		<span class="help-block">
			<strong>{{ $errors->first('subject') }}</strong>
		</span>
		@endif
	</div>
	<!--<div class="col-4 col-md-4 mb-3">
		<label>STATUS</label>
		{{ Form::select('status', ['1'=>'Active','0'=>'InActive'],null, ['class' => 'form-select']) }}
		@if ($errors->has('status'))
		<span class="help-block">
			<strong>{{ $errors->first('status') }}</strong>
		</span>
		@endif
	</div>--->
	<div class="col-4 col-md-4 mb-3">
		<label>User Type</label>
		{{ Form::select('user_type',$userType,null, ['id' => 'user_type', 'class' => 'form-select', 'value' => old('user_type'),'onchange'=>'userTypeN()']) }}
		@if ($errors->has('user_type'))
		<span class="help-block">
			<strong>{{ $errors->first('user_type') }}</strong>
		</span>
		@endif
	</div>
	
	
	<div class="col-4 col-md-4 mb-3" id="select_user" style="display: none;">
	<label>Select User</label>
			<select class="form-control" id="multiple_user" name="multiple_user[]" multiple="multiple">
			@if(isset($model) && !empty($model) && count($users)> 0)
			@foreach($users as $key=>$user)
			<option value="{{$user->id}}" @if(isset($model) && !empty($model)) {{ in_array($user->id, $id) ? 'selected' : '' }} @endif>{{$user->email}}</option>
			
			@endforeach
			@endif
			
			</select>
			<span class="">
                    <small>(Enter user mobile phone number)</small>
                </span>
	</div>
</div>
<div class="form-row">
	<div class="col-12 col-md-12">

		
							<div class="col-4 col-md-4 mb-3">
								<div class="dragFileWrap">
									<label  class="form-label">Notification icon</label>
									<div class="dropzone fileType" id="noti_icon">
										<input type="hidden" class="uploaded_file" name="noti_icon">
										<input type="hidden" class="remove_uploaded_file" name="remove_profile_photo" id="remove_profile_photo_path">
										<div class="dz-message">
											
											<img src="{{asset('img/ico-drag-n-drop.svg')}}" alt="" style="max-height: 80px;">
											jpeg,jpg,png
											
										</div>
									</div>
								</div>
								<div class="exitfile">
									
									<a href="javascript:void(0)" class="btn dataActionBtn " onclick="deleteFile(this,'profile_photo_path')"><i class="fa fa-trash-o"></i></a>
									
								</div>
								<div class="newfileSec">
								</div>
							</div>
						
	</div>
</div>
<div class="form-row">
	<div class="col-12 col-md-12">
		<label>Notification</label>
		{!! Form::textarea('notification', null, ['id' => 'notification', 'class' => 'form-control', 'placeholder' => 'Write somthing here................', 'value' => old('description')]) !!}
		@if ($errors->has('notification'))
		<span class="help-block">
			<strong>{{ $errors->first('notification') }}</strong>
		</span>
		@endif
	</div>
</div>


<div class="d-flex justify-content-start" style="margin-top:20px;">
                        <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
                        <a  href="{{ url()->previous() }}" class="btn btn-primary themeBtn themeUnFill">Cancel</a>
                      </div>
@push('scripts')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link type="text/css" href="{{asset('css/vendor-dropzone.css')}}" rel="stylesheet" />
<script src="{{ asset('js/dropzone.min.js')}}"></script>
<script src="{{asset('js/custom-dropzone.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.js-example-basic-multiple').select2();
		$("#multiple_user").select2({
			minimumInputLength:2,
			width: '100%',
			allowClear: true,
			ajax: {
				url: "{{route('specific.user')}}",
				dataType: 'json',
				type: "GET",
				quietMillis: 50,
				data: function(term) {
					return {
						term: term
					};
				},
				results: function(data) {
					console.log(data,89018);
					return {
						results: $.map(data, function(item) {
							return {
								id: item.id,
								text: item.phone
							}
						})
					};
				}
			}
		});

		$('#user_type').trigger("change");

	});

	function userTypeN() {
		var userType = $('#user_type').val();
		//console.log(userType);
		if (userType == 'specify') {

			$('#select_user').show();

		} else {
			$('#select_user').hide();

		}
	}

	var filepath = "{{route('file.upload')}}";
	$(document).ready(function() {

		addDropZone('noti_icon', filepath);

	});
</script>
@endpush