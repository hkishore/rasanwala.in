@extends('layouts.admin')

@section('content')
   <div class="card-form">
		<div class="row no-gutters">
		<div class="col-lg-12 card-form__body card-body">
              {{ Form::model($model,['route' => ['notification.update', $model->id],'enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }}
                    @method('PUT')
                    @include('admin.notifications.form')            

                {{ Form::close() }}
              </div>
              </div>
    </div>

@endsection