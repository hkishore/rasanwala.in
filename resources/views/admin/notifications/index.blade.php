<?php
// use App\Models\Permision;
// $userId=Auth::user();
// $permision=Permision::where('user_id',$userId->id)->first();
?>
@extends('layouts.admin')
@section('content')
<div class="themeTable">
	<div class="table_id DatatableHeader">
		<div class="pgTitle">
			<h1>Notifications</h1>
		</div>
		<div class="searchBx">
			<input type="text" id="myInputTextField" placeholder="Search">
		</div>

		<div class="tableLength" style="margin-bottom:10px;">
			<a href="{{route('notification.create')}}" class="btn btn-primary themeBtn themeFill iconLeft">Add New</a>
		</div>
	</div>
	<div class="table-responsive">
		<table class="display table table-striped" id="listtable1">
			<thead>
				<tr>
					<th>Notification</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody class="list">

			</tbody>
		</table>
	</div>
</div>
@endsection
@push('scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		datatableRender = $('#listtable1').DataTable({
			responsive: true,
			processing: true,
			serverSide: true,
			//searching: false,
			ajax: {
				"url": "{{route('notification.dataTable')}}",
				"data": function(data) {}
			},
			aoColumnDefs: [{
				bSortable: false,
				aTargets: ['_all']
			}],
			stateSave: false,
			bSort: false,
			bLengthChange: true,
			//bInfo: false,

		});
		$('#myInputTextField').keyup(function() {
			datatableRender.search($(this).val()).draw();
		});
	});

	function deleteNotfication(obj) {
		var url = $(obj).data('href');
		$('#confirm-delete .modal-title').html('Delete Notification');
		$('#confirm-delete form').attr('action', url);
		$('#confirm-delete .text-left').html('Are you sure, you want to delete this Notification?');
		$('#confirm-delete').modal('show');
	}
</script>
@endpush