@extends('layouts.admin')

@section('content')
<div class="pageTtl">
    <h1>Notification</h1>
</div>
<div class="greyBx">
    <div class="innerWrap">
        {{ Form::open(['route' => 'notification.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}
        {{ csrf_field() }}
        @include('admin.notifications.form')
        {{ Form::close() }}
    </div>
</div>

@endsection