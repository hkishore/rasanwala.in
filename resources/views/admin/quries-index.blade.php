
@extends('layouts.admin')
@section('content')
<div class="themeTable">
          <div class="table_id DatatableHeader" >
            <div class="pgTitle">
              <h1>Query</h1>
            </div>
          </div>
          
          <div class="table-responsive">
            <table id="listtable1" class="display table table-striped">
			 <thead>
                <tr>
					<th>Name</th>
                    <th>Email</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Status</th>                 
                    <th>Action</th>    
                </tr>
            </thead>
            <tbody>
			@if(count($model)>0)
               @foreach($model as $qr)
               <tr>
			   <?php
			   $status="Un Seen";
			   if($qr->status==1)
			   {
				   $status="Seen";
			   }
			   ?>
                 <td>{{$qr->name}}</td>
                 
                 <td>{{$qr->email}}</td>
                 <td>{{$qr->subject}}</td>
                 <td>{{$qr->message}}</td>
                 <td>{{$status}}</td>
                <td>
				<div class="d-flex justify-content-start">
				@if($qr->status!=1)
				<a  href="{{route('status.update.query',$qr->id) }}" class="btn me-2 dataActionBtn btnEdit"><i class="fa fa-eye" aria-hidden="true"></i></a>
				@endif
                <a  onclick="deletequery('{{route('query.destroy',$qr->id) }}')" href="javascript:void(0);" class="btn me-2 dataActionBtn btnDelete"><img src="{{asset('img/datatable-delete.svg')}}"/></a>
                </div> 				
				 </td>
               </tr>
               @endforeach
             @endif
			 </tbody>
            </table>
          </div>
      </div>
@endsection
@push('scripts')
<script type="text/javascript">
$( document ).ready(function() {
 datatableRenderWithoutAjax(1)
	});
function deletequery(url)
{
	$('#confirm-delete .modal-title').html('Delete Query');
    $('#confirm-delete form').attr('action',url);
	$('#confirm-delete .text-center').html('Are you sure want to delete this Query?');
	$('#confirm-delete').modal('show');
}
</script>
@endpush