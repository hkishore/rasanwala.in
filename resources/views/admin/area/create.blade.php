@extends('layouts.admin')

@section('content')

    <div class="tableWrap sectionDash">
            <h1 class="title">Add Area</h1>
			</div>
			<div class="greyBx">
            <div class="innerWrap">
              {{ Form::open(['route' => 'area.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }} 
                                      
                    @include('admin.area.form') 

                {{ Form::close() }}
            

                
   
	</div>
	</div>

@endsection
