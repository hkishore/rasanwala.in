@extends('layouts.admin')

@section('content')

    <div class="tableWrap sectionDash">
            <h1 class="title">Edit Area</h1>
			</div>
			<div class="greyBx">
            <div class="innerWrap">
              {{ Form::model($model,['route' => ['area.update', $model->id],'enctype'=>"multipart/form-data"]) }}
                    {{ csrf_field() }}
                    @method('PUT')
                    @include('admin.area.form')            

                {{ Form::close() }}
           
   </div>
	</div>

@endsection