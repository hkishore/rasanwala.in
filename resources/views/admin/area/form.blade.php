
                  <div class="row">
                    <div class="mb-2rem col-md-6">
                       <label for="" class="form-label">Area <span class="red">*</span></label>
                      {!! Form::text('area', null, ['id' =>'area', 'class' => 'form-control', 'placeholder' => 'Area Name', 'value' => old('area')]) !!}
					  @if ($errors->has('area'))
						<span class="help-block">
						<strong>{{ $errors->first('area') }}</strong>
						</span>
						@endif
                    </div>
                    <div class="mb-2rem col-md-6">
                      <label for="" class="form-label">Charge Amount</label>
                      {!! Form::text('charge', null, ['id' => 'charge', 'class' => 'form-control', 'placeholder' => 'Amount', 'value' => old('charge')]) !!}

			          @if ($errors->has('charge'))
			          <span class="help-block">
			          <strong>{{ $errors->first('charge') }}</strong>
			          </span>
			          @endif
                    </div>
                    </div>
					 <div class="row">
                    <div class="mb-2rem col-md-6">
                       <label for="" class="form-label">Start Time AM/PM</label>
                      {!! Form::text('start_time', null, ['id' =>'start_time', 'class' => 'form-control', 'placeholder' => 'Start Time', 'value' => old('start_time')]) !!}
					  @if ($errors->has('start_time'))
						<span class="help-block">
						<strong>{{ $errors->first('start_time') }}</strong>
						</span>
						@endif
                    </div>
                    <div class="mb-2rem col-md-6">
                      <label for="" class="form-label">End Time AM/PM</label>
                      {!! Form::text('end_time', null, ['id' => 'end_time', 'class' => 'form-control', 'placeholder' => 'Amount', 'value' => old('end_time')]) !!}

			          @if ($errors->has('end_time'))
			          <span class="help-block">
			          <strong>{{ $errors->first('end_time') }}</strong>
			          </span>
			          @endif
                    </div>
                    </div>
					<div class="row">
                    <div class="mb-2rem col-md-6">
					<label for="status" class="form-label">Status</label>
			 {{ Form::select('status', ['1'=>'Active','0'=>'InActive'],null, ['class' => 'form-select', 'value' => old('status')]) }}
			@if ($errors->has('status'))
				<span class="help-block">
					<strong>{{ $errors->first('status') }}</strong>
				</span>
			@endif
                       
                    </div>
                   
                    </div>
				        <div class="d-flex justify-content-start">
                        <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
                        <a  href="{{ url()->previous() }}" class="btn btn-primary themeBtn themeUnFill">Cancel</a>
                      </div>