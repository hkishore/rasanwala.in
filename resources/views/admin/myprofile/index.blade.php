
<?php
use App\Models\State;
$user=Auth::user();
$state=State::pluck('name','id');
$editimage=asset('img/datatable-edit-plus.svg');
$deleteImage=asset('img/delete-button.svg');
use App\Models\Common;
$meta=Common::getMetaInfo('homepage');
?>
@extends('layouts.front',$meta)
@section('content')
<div class="container">
<h1>Edit Profile</h1>
<div class="greyBx">
<div class="innerWrap">
            {{ Form::model($user,['route' => ['self.update', $user->id],'enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }}
                    @method('PUT')
              <div class="row">
                <div class="col-12 col-lg-7">
                 	<div class="secu">		  
                   <div class="mb-3">
                       <label for="" class="form-label">NAME</label>
                      {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name', 'value' => old('name')]) !!}
					  @if ($errors->has('name'))
						<span class="help-block">
						<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
                    </div>
					 <div class="mb-3">
                      <label for="" class="form-label">Phone</label>
                      {!! Form::text('phone', null, ['id' => 'phone', 'class' => 'form-control', 'placeholder' => 'Phone', 'value' => old('phone')]) !!}

			          @if ($errors->has('phone'))
			          <span class="help-block">
			          <strong>{{ $errors->first('phone') }}</strong>
			          </span>
			          @endif
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Email</label>
                      {!! Form::text('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Email', 'value' => old('email')]) !!}

			          @if ($errors->has('email'))
			          <span class="help-block">
			          <strong>{{ $errors->first('email') }}</strong>
			          </span>
			          @endif
                    </div>
					 <div class="mb-3">
                      <label for="" class="form-label">Password</label>
                     <input type="password" name="password" id="password" class='form-control'>

			          @if ($errors->has('password'))
			          <span class="help-block">
			          <strong>{{ $errors->first('password') }}</strong>
			          </span>
			          @endif
                    </div>
					                
                      </div>
                      </div>
					   
					 
					  </div>
					 
                      <div class="col-md-4 d-flex justify-content-stat">
                        <button type="submit" class="btn btn-outline-success">Submit</button>
						
                  
					  </div> 
					  </div>
					 {{ Form::close() }}
					  &nbsp;
					  <div class="row">
                      <div class="mb-3 col-md-6">    
					  <!--<div class="col-md-8">
                      <div class="col-12 col-md-4 mb-4 d-flex justify-content-end">-->
                      <h2>Address</h2>
					  </div>
					  @if(count($model)<5)
					  <div class="mb-4 col-md-6 d-flex justify-content-end">
                     <!-- <a href="" class="btn btn-outline-success">Add New Address</a>-->
                      </div>
					  @endif
					  </div>
					   @if(count($model)>0)
					  <div class="table-responsive">
                      <table id="listtable1" class="display table table-striped">
			          <thead>
                      <tr>
					  <th>Name</th>
                      <th>Contact</th>
                      <th>Address</th>
                      <th>Location</th>
                      <th>Action</th>
                      </tr>
                      </thead>					  
                      <tbody>
			         
                      @foreach($model as $adrs)
                      <tr>
					  @if($adrs->status!=3)
					  <td>{{$adrs->first_name}}</td>
					  <td>{{$adrs->phone_no}}</td>
					  <td>{{$adrs->address_1}}</td>
					  @if($adrs->area_id != 0)
                      <td>{{$adrs->location->area}}</td>
					@else
						<td>{{$adrs->other}}</td>
					@endif
					  <td><a href="{{route('editadr',$adrs->id)}}" class="btn me-2 dataActionBtn btnEdit"  title="Edit"><img src="{{$editimage}}" alt="" /></a>|<a href="{{route('deleteadr',$adrs->id)}}" class="btn me-2 dataActionBtn btnDelete"  title="Delete"><img style="height: 18px;"src="{{$deleteImage}}" alt="" /></a></td>
					  @endif
                      </tr>
                     @endforeach
                     
			          </tbody>
						</table>
					  </div> 
					  @endif
						</div>
						</div>
@endsection