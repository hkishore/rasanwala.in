 <?php
$oldfile="";
 if(isset($model) && $model->image_name!='')
 {
	 $oldfile='uploads/pages/'.$model->image_name;
 }	   
?> 
<div class="row">
	<div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-6 {{ $errors->has('title') ? 'has-error' : '' }}">
		  <label for="title" class="form-label">Title</label>
			{!! Form::text('title', null, ['id' => 'title', 'class' => 'form-control', 'placeholder' => 'title', 'value' => old('title')]) !!}
			@if ($errors->has('title'))
			<span class="help-block">
			<strong>{{ $errors->first('title') }}</strong>
			</span>
			@endif
		</div>	
		<div class="mb-2rem col-md-6 {{ $errors->has('status') ? 'has-error' : '' }}">
		  <label for="status" class="form-label">Status</label>
            {{ Form::select('status', [	1 => 'Active',0 => 'Inactive']	, null, ['class' => 'form-select']) }}
            @if ($errors->has('status'))
                <span class="help-block">
                    <strong>{{ $errors->first('status') }}</strong>
                </span>
            @endif
		 </div>
		</div>
	 </div>
   </div>
    <div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-12{{ $errors->has('description') ? 'has-error' : '' }}">
		  <label for="description" class="form-label">Description</label>
          {!! Form::textarea('description', null, ['id' => 'description', 'class' => 'form-control', 'placeholder' => 'Description', 'value' => old('description')]) !!}
			@if ($errors->has('description'))
			<span class="help-block">
			<strong>{{ $errors->first('description') }}</strong>
			</span>
			@endif
		</div>	
		</div>
	 </div>
	 <div class="col-md-12">
	  <div class="row">
		
		<div class="mb-2rem col-md-6 {{ $errors->has('url') ? 'has-error' : '' }}">
		  <label for="url" class="form-label">Url</label>
			{!! Form::text('url', null, ['id' => 'url', 'class' => 'form-control', 'placeholder' => 'Url', 'value' => old('url')]) !!}
			 @if ($errors->has('url'))
			  <span class="help-block">
			  <strong>{{ $errors->first('url') }}</strong>
			  </span>
			 @endif
		 </div>
		 <div class="mb-2rem col-md-4 {{ $errors->has('image_name') ? 'has-error' : '' }}">
		@include('admin.image-uploader',['label'=>'Featured Image','key'=>'image_name','oldfile'=>$oldfile])		   
            @if ($errors->has('image_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('image_name') }}</strong>
                </span>
            @endif		
		</div>	
		</div>
	 </div>
	<div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-6 {{ $errors->has('meta_title') ? 'has-error' : '' }}">
		  <label for="meta_title" class="form-label">Meta Title</label>
			{!! Form::text('meta_title', null, ['id' => 'meta_title', 'class' => 'form-control', 'placeholder' => 'Meta Title', 'value' => old('meta_title')]) !!}
			@if ($errors->has('meta_title'))
			<span class="help-block">
			<strong>{{ $errors->first('meta_title') }}</strong>
			</span>
			@endif
		</div>	
		<div class="mb-2rem col-md-6 {{ $errors->has('status') ? 'has-error' : '' }}">
		  <label for="meta_tag" class="form-label">Meta KeyWord</label>
            {!! Form::text('meta_tag', null, ['id' => 'meta_tag', 'class' => 'form-control', 'placeholder' => 'Meta Keyword', 'value' => old('meta_tag')]) !!}
			@if ($errors->has('meta_tag'))
			<span class="help-block">
			<strong>{{ $errors->first('meta_tag') }}</strong>
			</span>
			@endif
		 </div>
		</div>
	 </div>
   </div>
   <div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-12{{ $errors->has('meta_description') ? 'has-error' : '' }}">
		  <label for="meta_description" class="form-label">Meta Description</label>
          {!! Form::textarea('meta_description', null, ['id' => 'meta_description', 'class' => 'form-control', 'placeholder' => 'Meta Description', 'value' => old('meta_description')]) !!}
			@if ($errors->has('meta_description'))
			<span class="help-block">
			<strong>{{ $errors->first('meta_description') }}</strong>
			</span>
			@endif
		</div>	
		</div>
	 </div>
	<div class="d-flex justify-content-start ">
	  <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
	  <a href="{{route('page.index')}}" class="btn btn-primary themeBtn themeUnFill me-3">Cancel</a>
	</div>
</div>
@push('scripts')
 <script>
 loadTinyMCe('description',"{{route('page.imageupload','page-editor')}}?_token={{ csrf_token() }}");
  var filepath="{{route('file.upload')}}";
$( document ).ready(function() {
	

	if($('#image_name').length>0)
	{
	  addDropZone('image_name',filepath);
	}
});
 </script>
 @endpush

