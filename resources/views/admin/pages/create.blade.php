@extends('layouts.admin')

@section('content')
    <div class="pageTtl">
            <h1>Add Page</h1>
        </div>       
    <div class="greyBx">
            <div class="innerWrap"> 

                {{ Form::open(['route' => 'page.store',"class"=>"themeForm"]) }}

                    {{ csrf_field() }} 
                                      
                    @include('admin.pages.form') 

                {{ Form::close() }}
           </div>
    </div>
@endsection
