@extends('layouts.admin')

@section('content')
	
	 <div class="pageTtl">
            <h1>Edit Page</h1>
        </div>       
    <div class="greyBx">
            <div class="innerWrap"> 
              {{ Form::model($model,['route' => ['page.update', $model->id],'enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }}
                    @method('PUT')
                    @include('admin.pages.form')            

                {{ Form::close() }}
              </div>
    </div>

@endsection