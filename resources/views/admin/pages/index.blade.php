@extends('layouts.admin')
@section('content')
<div class="themeTable">
          <div class="table_id DatatableHeader" >
            <div class="pgTitle">
              <h1>Pages</h1>
            </div>
            <div class="searchBx">
              <input type="text" id="myInputTextField" placeholder="Search">
            </div>
            <div class="tableLength">
              <a href="{{route('page.create')}}" class="btn btn-primary themeBtn themeFill iconLeft">
                
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M0 8.69814H6.56325V15.261H8.69814V8.69814H15.261V6.56322H8.69814V0H6.56325V6.56322H0V8.69814Z" fill="white"/>
                </svg>
                &nbsp; Add new</a>
            </div>
          </div>
          <div class="table-responsive">
            <table id="listtable1" class="display table table-striped">
			 <thead>
                <tr>
                    <th>Title</th>
                    <th>Created Date</th> 
                    <th>Status</th>                  
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
			 </tbody>
            </table>
          </div>
      </div>
@endsection
@push('scripts')
<script type="text/javascript">
$( document ).ready(function() {
   datatableRender(1,"{{route('page.dataTable')}}");

} );
function deletePage(obj)
{
	var url=$(obj).data('href');
	$('#confirm-delete .modal-title').html('Delete Page');
    $('#confirm-delete form').attr('action',url);
	$('#confirm-delete .text-center').html('Are you sure want to delete this Page?');
	$('#confirm-delete').modal('show');
}
</script>
@endpush