@extends('layouts.admin')
@section('content')
<?php
$date=date('d M Y');
?>
<div class="pageTtl">
            <h1>Add Order {{$date}} </h1>
        </div>      
<div class="greyBx">
            <div class="innerWrap"> 
               

                {{ Form::open(['route' => 'order.store','enctype'=>"multipart/form-data"]) }}

                    {{ csrf_field() }} 
                                      
                    @include('admin.orders.add-order') 

                {{ Form::close() }}
           
                </div>
    </div>
@endsection
