<?php
$status=config('constant.shippingStatus');
?>
<div class="modal fade themeModal"  id="status-pop-up" tabindex="-1" aria-labelledby="deleteConfirmLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header justify-content-center">
        <h5 class="modal-title" id="">Update Status</h5>
        <button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-footer justify-content-center pt-3">
      <form action="{{route('shipping-address-update')}}" method="POST">
	   @csrf
            <div class="col-md-12">
        <div class="mb-2rem {{ $errors->has('status') ? 'has-error' : '' }}">
		  <!--  <label for="status" class="form-label">Select Status</label>-->
		   {{ Form::select('status',$status, null, ['id'=>'status','class' => 'form-select','required'=>'required','value' => old('status')]) }}
		   @if ($errors->has('status'))
				<span class="help-block">
				<strong>{{ $errors->first(status) }}</strong>
				</span>
			@endif
		   </div>            	
		</div>
	
     <div class="d-flex justify-content-start ">
	 <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
	
		 <button type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
		 <input type="hidden" name="order_id" value="" id="update_Order_id"/>
    </div>      
    </form>

      </div>
    </div>
  </div>
</div>