<?php
use App\Models\User;
use App\Models\Address;
$billamt=number_format($totalAmount+$shippingcharges,2, '.', '');
$user=User::where('id',$order->user_id)->first();
$address=Address::where('id',$user->address_id)->first();
//dd($address);
?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sah Rasan Wala</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>

        body{
            background-color: #F6F6F6; 
            margin: 0;
            padding: 0;
        }
        h1,h2,h3,h4,h5,h6{
            margin: 0;
            padding: 0;
        }
        p{
            margin: 0;
            padding: 0;
        }
        .container{
            width: 80%;
            margin-right: auto;
            margin-left: auto;
        }
        .brand-section{
           background-color: #0d1033;
           padding: 10px 40px;
        }
        .logo{
            width: 50%;
        }

        .row{
            display: flex;
            flex-wrap: wrap;
        }
        .col-6{
            width: 50%;
            flex: 0 0 auto;
        }
        .text-white{
            color: #fff;
        }
        .company-details{
            float: right;
            text-align: right;
        }
        .body-section{
            padding: 16px;
            border: 1px solid gray;
        }
        .heading{
            font-size: 20px;
            margin-bottom: 08px;
        }
        .sub-heading{
            color: #262626;
            margin-bottom: 05px;
        }
        table{
            background-color: #fff;
            width: 100%;
            border-collapse: collapse;
        }
        table thead tr{
            border: 1px solid #111;
            background-color: #f2f2f2;
        }
        table td {
            vertical-align: middle !important;
            text-align: center;
        }
        table th, table td {
            padding-top: 08px;
            padding-bottom: 08px;
        }
        .table-bordered{
            box-shadow: 0px 0px 5px 0.5px gray;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
        }
        .text-right{
            text-align: end;
        }
        .w-20{
            width: 20%;
        }
        .float-right{
            float: right;
        }
.watermarked {
  position: relative;
}

.watermarked:after {
  content: "";
  display: block;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0px;
  left: 0px;
  right:0px;
  bottom:0px;
  background-image: url("http://mrasanwala.com/img/logo.jpg");
  background-position: center;
  background-repeat: no-repeat;
  opacity: 0.2;
}	
    </style>
</head>
<body>
    <div class="container watermarked">
        <div class="brand-section">
            <div class="row">
<div class="col-3">
<img src="https://rasanwala.in/img/logo.jpg" alt="Smiley face" width="42" height="42" style=" background-color:white;">
</div>

                <div class="col-6">
              
                    <h1 class="text-white">SAH RASHAN WALA</h1>
                </div>
                <div class="col-3" style="padding-left: 0px;">
                    <div class="company-details">
                        <p class="text-white"><b>GSTIN : 06AMUPD8013C1ZA</b></p>
                        <p class="text-white"><i class="fa fa-envelope"></i><strong> : support@rasanwala.in</strong></p>
                        <p class="text-white"><i class="fa fa-phone"></i><span> : +91-7988418408</span></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="body-section">
            <div class="row">
                <div class="col-6">
                    <h2 class="heading">Invoice No.: #{{$order->id}}</h2>
                    <!--<p class="sub-heading">Tracking No. fabcart2025 </p>-->
                    <p class="heading">Order Date: {{$order->created_at}}</p>
                    <p class="heading">Email:{{$user->email}} </p>
                </div>
                <div class="col-6">
                    <p class="heading">Full Name: {{$user->name}} </p>
					@if(!empty($address))
                    <p class="heading">Address:{{$address->address_1}}  </p>
				@endif
                    <p class="heading">Phone Number:- {{$user->phone}} </p>
                    
                </div>
            </div>
        </div>

        <div class="body-section">
            <h3 class="heading">Ordered Items</h3>
            <br>
            <table class="table-bordered">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th class="w-20">Price</th>
                        <th class="w-20">Quantity</th>
                        <th class="w-20">Grandtotal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ordProd as $ordP)
                    <tr>
                        <td><b>{{$ordP['proname']}}</b><b> ({{$ordP['prosize']}})</b></td>
                        <td><b>₹{{$ordP['price']}}</b></td>
                        <td><b>{{$ordP['qty']}}</b></td>
                        <td><b>₹{{$ordP['grandamt']}}</b></td>
                    </tr>
					@endforeach
                    <tr>
                        <td colspan="3" class="text-right"><b>Sub Total</b></td>
                        <td><b>₹{{$totalAmount}}</b></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-right"><b>Shipping Charge</b></td>
                        <td><b>₹{{$shippingcharges}}</b></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-right"><b>Total Amount</b></td>
                        <td> <b>₹{{$billamt}}</b></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <!--<h3 class="heading">Payment Status: Paid</h3>
            <h3 class="heading">Payment Mode: Cash on Delivery</h3>-->
        </div>

        <div class="body-section">
            <p>© 2022 Sah Rasan Wala. All Rights Reserved. 
                <a href="https://rasanwala.in/" class="float-right">https://rasanwala.in/</a>
            </p>
        </div>      
    </div>      

</body>

</html>
<div class="pull-right" style="margin-right:205px; margin-top:30px;" >
<a href="javascript:void(0);" class="btn btn-primary" onclick="printDiv();">Print Page</a>
</div>
<script type="text/javascript">
function printDiv() {
     // var printContents = document.getElementById(divName).innerHTML;
     // var originalContents = document.body.innerHTML;
     // document.body.innerHTML = printContents;
     window.print();
     // document.body.innerHTML = originalContents;
}
</script>

