<div class="modal fade themeModal"  id="shipping-pop-up"  tabindex="-1" aria-labelledby="deleteConfirmLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header justify-content-center">
        <h5 class="modal-title" id="deleteConfirmLabel">Shipping</h5>
        <button type="button" class="btn-close d-none" data-f-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body pt-1 pb-2">
        <p class="msg-o mb-0 text-center"></p>
		  <form action="" id="popform">
	   <?php
$userRole=config('constant.userRole');
?>
  <div class="row">
	 <div class="col-md-12">
	  <div class="row">
	  <h4>Shipping Detail</h4>
	  
	  
		<div class="mb-3 col-md-6 {{ $errors->has('height') ? 'has-error' : '' }}">
		
		   {{ Form::text('height',  null, ['class' => 'form-control','placeholder'=>'height','id'=>'height']) }}
			@if ($errors->has('height'))
				<span class="help-block">
					<strong>{{ $errors->first('height') }}</strong>
				</span>
			@endif
		</div>
	    <div class="mb-3 col-md-6 {{ $errors->has('width') ? 'has-error' : '' }}">
		 
			 {{ Form::text('width',null, ['class' => 'form-control','id'=>'width','placeholder'=>'width']) }}
			@if ($errors->has('width'))
				<span class="help-block">
					<strong>{{ $errors->first('width') }}</strong>
				</span>
			@endif
		</div>
        
		<div class="mb-3 col-md-6 {{ $errors->has('length') ? 'has-error' : '' }}">
		 
			 {{ Form::text('length',null, ['class' => 'form-control','id'=>'length','placeholder'=>'Length']) }}
			@if ($errors->has('length'))
				<span class="help-block">
					<strong>{{ $errors->first('length') }}</strong>
				</span>
			@endif
		</div>	
		<div class="mb-3 col-md-6 {{ $errors->has('breath') ? 'has-error' : '' }}">
		 
			 {{ Form::text('breath',null, ['class' => 'form-control','id'=>'breath','placeholder'=>'Breath']) }}
			@if ($errors->has('breath'))
				<span class="help-block">
					<strong>{{ $errors->first('breath') }}</strong>
				</span>
			@endif
		</div>		
		</div>
	 </div>


	 <div class="col-md-12">
	  <div class="row">
	  <h4>Vendor Address</h4>
	  
	  
		<div class="mb-3 col-md-6 {{ $errors->has('first_name') ? 'has-error' : '' }}">
		  
		   {{ Form::text('first_name',  null, ['class' => 'form-control','placeholder'=>'First Name','id'=>'first_name','required'=>'required']) }}
			@if ($errors->has('first_name'))
				<span class="help-block">
					<strong>{{ $errors->first('first_name') }}</strong>
				</span>
			@endif
		</div><div class="mb-3 col-md-6 {{ $errors->has('last_name') ? 'has-error' : '' }}">
		  
		   {{ Form::text('last_name',  null, ['class' => 'form-control','placeholder'=>'Last Name','id'=>'last_name','required'=>'required']) }}
			@if ($errors->has('last_name'))
				<span class="help-block">
					<strong>{{ $errors->first('last_name') }}</strong>
				</span>
			@endif
		</div><div class="mb-3 col-md-6 {{ $errors->has('email_id') ? 'has-error' : '' }}">
		  
		   {{ Form::text('email_id',  null, ['class' => 'form-control','placeholder'=>'Email Id','id'=>'email_id','required'=>'required']) }}
			@if ($errors->has('email_id'))
				<span class="help-block">
					<strong>{{ $errors->first('email_id') }}</strong>
				</span>
			@endif
		</div>
<div class="mb-3 col-md-6 {{ $errors->has('phone_no') ? 'has-error' : '' }}">
		 
			 {{ Form::text('phone_no',null, ['class' => 'form-control','id'=>'phone_no','placeholder'=>'phone','required'=>'required']) }}
			@if ($errors->has('phone_no'))
				<span class="help-block">
					<strong>{{ $errors->first('phone_no') }}</strong>
				</span>
			@endif
		</div>		
<div class="mb-3 col-md-6 {{ $errors->has('address_1') ? 'has-error' : '' }}">
		  
		   {{ Form::text('address_1',  null, ['class' => 'form-control','placeholder'=>'address 1','id'=>'address_1','required'=>'required']) }}
			@if ($errors->has('address_1'))
				<span class="help-block">
					<strong>{{ $errors->first('address_1') }}</strong>
				</span>
			@endif
		</div>
	    <div class="mb-3 col-md-6 {{ $errors->has('address_2') ? 'has-error' : '' }}">
		
			 {{ Form::text('address_2',null, ['class' => 'form-control','id'=>'address_2','placeholder'=>'address 2']) }}
			@if ($errors->has('address_2'))
				<span class="help-block">
					<strong>{{ $errors->first('address_2') }}</strong>
				</span>
			@endif
		</div>
        <div class="mb-3 col-md-6 {{ $errors->has('city') ? 'has-error' : '' }}">
		 
			 {{ Form::text('city',null, ['class' => 'form-control','id'=>'city','placeholder'=>'city','required'=>'required']) }}
			@if ($errors->has('city'))
				<span class="help-block">
					<strong>{{ $errors->first('city') }}</strong>
				</span>
			@endif
		</div>	
 <div class="mb-3 col-md-6 {{ $errors->has('state') ? 'has-error' : '' }}">
		 
			 {{ Form::text('state_id',null, ['class' => 'form-control','id'=>'state_id','placeholder'=>'state','required'=>'required']) }}
			@if ($errors->has('state_id'))
				<span class="help-block">
					<strong>{{ $errors->first('state') }}</strong>
				</span>
			@endif
		</div>	
 <div class="mb-3 col-md-6 {{ $errors->has('zipcode') ? 'has-error' : '' }}">
		 
			 {{ Form::text('zipcode',null, ['class' => 'form-control','id'=>'zipcode','placeholder'=>'zipcode','required'=>'required']) }}
			@if ($errors->has('zipcode'))
				<span class="help-block">
					<strong>{{ $errors->first('zipcode') }}</strong>
				</span>
			@endif
		</div>	

		</div>
	 </div>
	 </div>
	 <div class="d-flex justify-content-start ">
	 <button type="button" class="btn btn-primary themeBtn themeFill me-3" onclick="submitData(this)" id="submit_button">Submit</button>
	
		 <button type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
		 <input type="hidden" name="formType" value="3" />
		<input type="hidden" name="product_id" value="" id="product_id"/>
		<input type="hidden" name="order_id" value="" id="order_id" />
		<input type="hidden" name="vendor_id" value="" id="vendor_id" />
		<!--<input type="hidden" name="phone_no" value="" id="phone_no" />-->
</div> 
   
 </form>
      </div>
      <div class="modal-footer justify-content-center pt-0">
	 
	<!--<div class="d-flex justify-content-start ">
	  <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
	  <a href="{{route('user.index')}}" class="btn btn-primary themeBtn themeUnFill me-3">Cancel</a>
	</div> -->
 
        
		
      </div>
    </div>
  </div>
</div>
@push('scripts')
<script type="text/javascript">
function submitData(obj){
		var first_name=$('#first_name').val();
		var last_name=$('#last_name').val();
		var email_id= $('#email_id').val();
		var address_1= $('#address_1').val();
		var address_2= $('#address_2').val();
		var city= $('#city').val();
		var state_id= $('#state_id').val();
		var zipcode= $('#zipcode').val();
		var phone_no= $('#phone_no').val();
		var height= $('#height').val();
		var width= $('#width').val();
		var product_length= $('#length').val();
		var breath= $('#breath').val();
		var order_id= $('#order_id').val();
		var vendor_id= $('#vendor_id').val();
		if(first_name==" " || last_name=="" || email_id=="" || address_1=="" || address_2=="" || city=="" || zipcode=="" || phone_no=="")
		{
				toastr.error('Please Provide all details of user.');
			  return false;
		}
		if(height=="" || width=="" || product_length=="" || breath=="")
		{
				toastr.error('Please Provide all details of Product.');
			  return false;
		}
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  if(!regex.test(email_id)) {
		  toastr.error('Please Enter a valid Email.');
	  return false;
	  }
	 $('#shipping-pop-up').modal('hide');
	 Swal.fire('Please wait');
       Swal.showLoading();
    var cat = $.ajax({
	url:  "{{route('create.order.tracking')}}",
	type: 'POST',
	dataType: 'json',
	headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
	data: $(obj).parents('form:first').serialize(),
	success: function(json){
		
			if(json  == "" || json == null)
			{
		
			var icon = 'success';
			var cencel =  false;
			var text = 'Order create shipping Seccesfully';
			
			
			}
			else{
			var icon = 'error';
			var cencel =  true;
			var text = json;
			}
			Swal.fire({
			icon: icon,
			text: text,
			showCloseButton: cencel,
			allowOutsideClick: false, 
			showConfirmButton: true,
			}).then((result) => {
				console.log(result);
			if (result.isConfirmed) {
			window.location.reload();
			}
			});

		}
		});
}
	
</script>
@endpush