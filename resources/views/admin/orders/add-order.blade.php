	<?php 
use App\Models\Product;
use App\Models\User;
use App\Models\Variant;
use App\Models\Unit;
use App\Models\OrderLine;
$date=date('d M Y');
$users=User::select(DB::raw("CONCAT(phone) AS name"),'id','email')->pluck('name','id','email');
$showProduct=Product::where('status',1)->get();
$uid=null;
$pCnt=1;
$productList="<select id='selUser' onChange='setvariant(this);' class ='form-control product_id' name='product_id[]' required><option value=''>Select Categeory</option>";
foreach($showProduct as $key=>$pr)
{
	$productList.="<option value='". $pr->id ."' >".$pr->name."</option>";
	
}
$productList.='</select>';
$name=null;

if(!empty($model))
{

	$name=$model->user->name;
}
?>			
	<div class="col-md-12">
	  <div class="row">
		<div class="mb-1rem col-md-6 {{ $errors->has('parent_id') ? 'has-error' : '' }}">
      {{ Form::select('user_id', $users, $uid, ['id'=>'user_id','class' => 'form-select','placeholder'=>'Phone No','onchange'=>"userName(this)",]) }}
	 
			@if ($errors->has('user_id'))
				<span class="help-block">
					<strong>{{ $errors->first('user_id') }}</strong>
				</span>						
			@endif
		 <div class="hindiTitle" id="phoneNumber"></div>	
		</div>
		<div class="mb-2rem col-md-6  {{ $errors->has('name') ? 'has-error' : '' }}">
		{!! Form::text('name',$name, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name', 'value' => old('name')]) !!}
			@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
		 </div>		
		</div>

 <div class="row">
    <div class="col-md-12">
        <div class="form-group">
		
            <label for="title" class="print">Order Details: <span class="red">*</span> &nbsp;<div class="pull-right"style="margin-bottom:20px;"><button type="button" class = 'btn btn-primary themeBtn themeFill me-3' onclick="AddMore()">+ Add More</button></div></label>
			
		         <table  id="listtable1" class="table table-bordered margin-top">
                    <thead>
                        <tr>
							<th class="textC">Product:</th>
							<th>Unit:</th>     					
                            <th class="columns">Quanity:</th>                           					
                            <th class="columns">Price:</th>                          					
                            <th class="columns">Amount:</th>                           					
                            <th style="width:50px;">Action:</th>
                        </tr>
                    </thead>
                    <tbody class="multipl_add" id="price_list">
						 @if(isset($model) && count($model->getOrderLines)>0)
						 <?php
						 $totalAmt = $model->total_amount;
						 ?>
						 @foreach($model->getOrderLines as $item)
						 <?php
							$data=[];
							$data[$item->product_id] = $item->product->name;
							$size=[];
							$size[$item->variant_id] = $item->variant->size;

						 ?>
					      <tr>
                            <td>
							{!! Form::select('product_id[]',$data,$item->product_id,['id'=>'old_id'.$pCnt,'class' => 'form-select product_id']) !!}
							</td>
							
							<td>
							{!! Form::select('size[]',$size,$item->variant_id,['id'=>'variantId','class' => 'form-select variantId','onchange'=>"changedPrice(this)", 'required']) !!}
							</td> 
							
                            <td><input type="text" class="form-control" placeholder="Quantity" name="quantity[]" id="quantity" value="{{$item->qty}}" onkeyup="changedQTY(this)"></td>
							
                            <td ><input type="text" class="form-control" placeholder="Price" name="price[]" id="price" onkeyup="changedQTY(this)"value="{{$item->amount}}" ></td>
							
							<td><input type="text" class="form-control amt" placeholder="Amount" name="amount[]" id="amount" onkeyup="calculateAmt()" value="{{$item->amount*$item->qty}}"></td>
							
                            <td><a href="javascript:void(0)" title="Delete" onclick="deleteRow(this)" ><i class="fa fa-trash-o"></i></a></td>
							
                        </tr>
						<? $pCnt++ ?>
						@endforeach
				      @else
				
					   <tr>
                            <td>
							
							  {!! $productList !!}
							  <div id="result"></div>
							</td>
							<td>
								<select id="variantId"  class ="form-select variantId" name="size[]" onchange="changedPrice(this)" required>
							<option value="">Select Variant</option>
								</select>
							
							</td>  					
                            <td><input type="text" class="form-control" placeholder="Quantity" name="quantity[]" id="quantity" value="1" onkeyup="changedQTY(this)"></td>
							
                            <td ><input type="text" class="form-control" placeholder="Price" name="price[]" id="price" onkeyup="changedQTY(this)"></td>
							
							<td><input type="text" class="form-control amt" placeholder="Amount" name="amount[]" id="amount" onkeyup="calculateAmt()"></td>
							
                            <td><a href="javascript:void(0)" title="Delete" onclick="deleteRow(this)" ><i class="fa fa-trash-o"></i></a></td>
                        </tr>
						@endif
                     </tbody>
					 <tfoot><tr><td colspan="4" ><div class="pull-right" >Total</div></td><td ><input type="number" id="totalAmt" name="totalAmt" class= "form-control" value=""></td><td>&nbsp;</td></tr></tfoot>
                    </table>
				
				<div class="row">
				<div class="col-md-4">
				{{ Form::select('payment_status', [0 => 'Paid', 1 => 'Pending'],null,['id'=>'payment_status','class' => 'form-select', 'placeholder' => 'Select Payment Status', 'value' => old('payment_status')]) }}
					
				</div>
				</div>
				<div class="d-flex justify-content-start " style="margin-top:25px;">
	  <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
	   <a  href="{{ url()->previous() }}" class="btn btn-primary themeBtn themeUnFill">Cancel</a>
	   <!--<a type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</a>-->
	</div>
				
        </div>
    </div>	
 </div>

@push('scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript">
var pHtm="{!!$productList!!}";
$( document ).ready(function() {
	$( "#days" ).datepicker();
   $( "#start_date" ).datepicker();
   $( "#end_date" ).datepicker();
     creatnewSelect();
	 calculateAmt()
} );
var pidcnt={{$pCnt}};
var showProductItems = {!!$showProduct !!};
var pNum=0;
var unitNum=1;
var qtyNum=2;
var priceNum=3;
var amtNum=4;
$("#user_id").select2({
  tags: true,  
  placeholder: 'Enter Mobile Number',
  allowClear: true
}).on('select2:select', function (e) {
    var data = e.params.data;
    var name=data.text.split(' : ');
	$("#name").val(name[1]);
	$("#phoneNumber").html(name[0]);
	$(document).prop('title', name[1]);
	
});
function userName(obj)
{
	var users = $(obj).val();
	$.ajax({
			type: "get",
			url: '/find-user/'+users,
			dataType:"json",
			success:function(data)
			{
			$('#name').val(data.name);
			}
	  });
}
function setvariant(obj){
var c=$('#selUser').val();
  if(c>0)
  {
	  $.ajax({
           type: "get",
           url: "{{route('variant.data')}}",
           dataType: "json",
           data: "product="+c,
           success: function (data) {
			   console.log(data);
              data.forEach(element => {
					console.log(element);
				if(element.sale_price > element.regular_price)
				{
				var priceCount=element.regular_price;
				}else{
					var priceCount=element.sale_price;
				}
					var id=element.id;
					var size=element.size;
					$('#variantId').appaend('<option value="'+id+'" >'+size+'/'+priceCount+'</option>');
});
           }
	  });		   
  }
}
function changedPrice(obj)
{
	console.log(obj);
	var proPrice = $(obj).find(':selected').data('variant');
	console.log(proPrice);
	var trObj=$(obj).closest('td').parent();
    var priceObj=trObj.find("td:eq("+priceNum+")").find("input");
	var price=priceObj.val(proPrice);
	if(price=="")
	{
		price=0
	}
	
	changedQTY(obj)
}

function changedQTY(obj)
{
	
	var trObj=$(obj).closest('td').parent();
	
	var qty=trObj.find("td:eq("+qtyNum+")").find("input").val();
      if(qty=="")
	  {
		  qty=0;
	  }
	var priceObj=trObj.find("td:eq("+priceNum+")").find("input");
	var price=priceObj.val();
	if(price=="")
	{
		price=0
	}
	var amt=trObj.find("td:eq("+amtNum+")").find("input").val();
	  if(amt=="")
	  {
		  amt=0;
	  }
	var total=parseFloat(price)*parseFloat(qty);
	var totalObj=trObj.find("td:eq("+amtNum+")").find("input");
	     totalObj.val(total); //units	
		 
		 sumAmount();
}

function creatnewSelect()
{
	var newId='product_id'+pidcnt;
	var lastObj=$(".product_id").last().attr('id',newId);
	$('#'+newId).select2({
	placeholder: 'Select Product',
	allowClear: true
}).on('select2:select', function (e) {
    var data = e.params.data;
	var product_name = data.text;

	var trObj=$(this).closest('td').parent();
	trObj.find("td:eq(1)").find(".variantId").html('<option value="">Select Variant</option>');
	if(product_name != "")
	{
		$.ajax({
			type: "get",
			url: '/product-variant/'+product_name,
			dataType:"json",
			success:function(data){
				console.log(data);
				$.each(data,function(key,value){
					trObj.find("td:eq(1)").find('#variantId').append('<option value="'+value.id+'" data-variant="'+value.sale_price+'">'+value.size+'</option>');
				});
			}
	  });
	}
	
	var qty=trObj.find("td:eq("+qtyNum+")").find("input").val();
	var priceObj=trObj.find("td:eq("+priceNum+")").find("input");
	priceObj.val(data.price);
     var total=parseFloat(priceObj.val())*parseFloat(qty);
	var totalObj=trObj.find("td:eq("+amtNum+")").find("input");
	totalObj.val(total); //units
	calculateAmt();
	sumAmount();
});
pidcnt++;
}

function calculateAmt()
{
	var amount=0;
	$('#amount').each(function() {
        var amountVal=$(this).val();
		if(amountVal!="")
		{
			amountVal=parseFloat(amountVal);
		amount=amountVal;
		}
     });
	$('#amount').val(amount);
	sumAmount();
}
function sumAmount()
{
	 var tAmt=0;
	$(".amt").each(function() {
        var amtVal=$(this).val();
		if(amtVal!="")
		{
			amtVal=parseFloat(amtVal);
		tAmt=(tAmt+amtVal);
		}
     });
	$('#totalAmt').val(tAmt);
}

function AddMore()
{
	
var trObj=$('#price_list tr:first');
var con=trObj.clone();

con.find('td:first').html(pHtm);
con.find("td:eq(1)").find(".variantId").html('<option value="">Select Variant</option>');
con.find("td:eq("+priceNum+")").find("input").val("");
con.find("td:eq("+amtNum+")").find("input").val("NaN");
$('#totalAmt').val("NaN");
con.appendTo("#price_list");
creatnewSelect();
				
}


function deleteRow(obj)
{
	$(obj).parent().parent().remove();
	sumAmount();
}

</script>
<style>
.columns
{
	width:100px;
}
.textC
{
	width:400px;
}
.hindiTitle
{
	display:none;
}
.form-control {
   
    border: none;
}
</style>
@endpush