<div class="modal fade themeModal"  id="shipping_id" tabindex="-1" aria-labelledby="deleteConfirmLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header justify-content-center">
        <h5 class="modal-title" id="">Order Track Id</h5>
        <button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-footer justify-content-center pt-0">
      <form action="{{route('shipping.update')}}" method="POST">
	   @csrf
	        <div class="row">
            <div class="col-md-12">
                <div class="row">
                 
        
            <div class="mb-2rem {{ $errors->has('status') ? 'has-error' : '' }}">
            
            {{ Form::text('track_id',  null, ['class' => 'form-control','id'=>'track_id','placeholder'=>'Track Id']) }}
                @if ($errors->has('track_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('track_id') }}</strong>
                    </span>
                @endif
            </div>            	
		</div>
	 </div>
     </div>
     <div class="d-flex justify-content-start ">
	 <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
	
		 <button type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
		 <input type="hidden" name="vendor_id" value=" " id="vendor_id_self"/>
		 <input type="hidden" name="order_id" value=" " id="order_id_self"/>
    </div>    
    </form>

      </div>
    </div>
  </div>
</div>