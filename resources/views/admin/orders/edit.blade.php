@extends('layouts.admin')
@section('content')
<?php
$date=date('d M Y');
?>
<div class="pageTtl">
            <h1>Edit Order {{$date}} </h1>
        </div>      
<div class="greyBx">
            <div class="innerWrap"> 
				{{ Form::model($model,['route' => ['order.update', $model->id],'enctype'=>"multipart/form-data","class"=>"themeForm"]) }}
					{{ csrf_field() }}
					@method('PUT')
                    @include('admin.orders.add-order') 

                {{ Form::close() }}
           
                </div>
    </div>
@endsection