@extends('layouts.admin')
@section('content')
<?php
use App\Models\User;
$user=Auth::user();
?>
	<div class="tab" style="margin-top: -55px;">
	  <button class="tablinks"id="orderdetailBoutton" onclick="orderDetail('todayOrder')">Today Order</button>
	<button class="tablinks" id="reportBoutton"onclick="orderDetail('allOrder')">All Order</button>
	<div class="pull-right">
			<a class="btn btn-primary themeBtn themeFill iconLeft"  style="margin-right:10px;margin-top: 4px;" href="{{route('order.create')}}">Add Orders</a>
</div>
		
	</div>
	<div id="todayOrder" class="themeTable">
          <div class="table_id DatatableHeader" >
            
                    </div>
          
          <div class="table-responsive">
            <table id="listtable1" class="display table table-striped">
			 <thead>
                <tr>
                    <th>Order#</th>
                    <th>Product Name</th> 
                    <th>Customer</th> 
                    <th>Shiping Address</th> 
                    <th>Payment Status</th> 
                    <th>Payment Method</th> 
                    <th>Payment Order Id</th> 
                    <th>Order Status</th>              
                    <th>Order Amount</th>                 
                    <th>Created At</th>                 
                    <th>Updated At</th>              
                    <th>Action</th>    
                </tr>
            </thead>
            <tbody>
			 </tbody>
            </table>
          </div>
      </div>
		<div id="allOrder" class="themeTable">
          <div class="table_id DatatableHeader" >
                      <div class="col-md-2" style="margin-top: 27px;margin-left: 10px;">
                        <input id="all_start_date" class="form-control" placeholder="Select Start Date" name="start_date" type="text">
                      </div>
					  &nbsp;
                      <div class="col-md-2" style="margin-top: 27px;margin-left: 25px;">
                         <input id="all_end_date" class="form-control" placeholder="Select End Date" name="end_date" type="text">
                      </div>
					  &nbsp;
                      <div class="col-md-1" style="margin-left: 15px;margin-top: 27px;">
                      <a href="javascript:void(0);" class="btn btn-primary themeBtn btnGrey " onclick="filterData()">Filter</a>
                     </div>
					 &nbsp;
					<!-- <div class="col-md-1" style="margin-left: 50px;margin-top: 27px;">
                      <a href="javascript:void(0);" class="btn btn-primary themeBtn themeFill iconLeft" onclick="exportOrder()">Export</a>
                    </div>-->
                    </div>
          
          <div class="table-responsive">
            <table id="listtable2" class="display table table-striped">
			 <thead>
                <tr>
                    <th>Order#</th>
                    <th>Product Name</th> 
                    <th>Customer</th> 
                    <th>Shiiping Address</th> 
                    <th>Status</th>                 
                    <th>Order Amount</th>                 
                    <th>Created At</th>                 
                    <th>Updated At</th>                 
                    <th>Action</th>    
                </tr>
            </thead>
            <tbody>
			 </tbody>
            </table>
          </div>
      </div>

@endsection
@push('modals')
@include('admin.orders.status-update-pop-up')
@endpush
@push('scripts')
<script type="text/javascript">
	$( document ).ready(function() {
	dataTable=$('#listtable1').DataTable({
			responsive: true,
			processing: true,
			serverSide: true,
			searching: false,
			ajax:  {
				"url":"{{route('order.dataTable')}}",
				"data": function(data){
				}
			},
			stateSave: true,
			 bLengthChange: true,
			bInfo: false,

		});
		dataTable=$('#listtable2').DataTable({
			responsive: true,
			processing: true,
			serverSide: true,
			searching: false,
			ajax:  {
				"url":"{{route('allorder.dataTable')}}",
				"data": function(data){
					data.start_date = $('#all_start_date').val();
          data.end_date = $('#all_end_date').val();
				}
			},
			stateSave: true,
			 bLengthChange: true,
			bInfo: false,

		});
		$( "#all_start_date" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
		$( "#all_end_date" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
	orderDetail('todayOrder');
	});

  function updateStatus(oid){
 
            $('#update_Order_id').val(oid);
           $('#status-pop-up').modal('show');
  }
 function filterData()
{
	dataTable.draw();
}
function orderDetail(oderId) {
	var odrderO= oderId;
	if(odrderO=="allOrder"){
		$('#allOrder').show();
		$('#reportBoutton').addClass('active');
		$('#todayOrder').hide();
		$('#orderdetailBoutton').removeClass('active');
	}
	if(odrderO=="todayOrder"){
		$('#allOrder').hide();
		$('#reportBoutton').removeClass('active');
		$('#orderdetailBoutton').addClass('active');
		$('#todayOrder').show();
	}
}
</script>
@endpush