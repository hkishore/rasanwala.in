<div class="dragFileWrap">
<label for="" class="form-label">{{$label}}</label>
<div class="dropzone fileType" id="{{$key}}">
<input type="hidden" class="uploaded_file" name="{{$key}}">
<input type="hidden" class="remove_uploaded_file"  name="remove_{{$key}}" id="remove_{{$key}}">
  <div class="dz-message"> 
   @if($oldfile!="")
   <img src="{{asset($oldfile)}}"  style="max-height: 80px;"/>
   @else
	<img src="{{asset('img/ico-drag-n-drop.svg')}}" alt="" style="max-height: 80px;">
	jpeg,bmp,jpg,png,svg
   @endif	
  </div>
 
</div>
</div>

<div class="row exitfile">
@if($oldfile!="")
 <a href="javascript:void(0)" class="btn dataActionBtn btnDelete" onclick="deleteFile(this,'{{$key}}')"><img src="{{asset('img/datatable-delete.svg')}}" alt=""></a>
@endif
</div>
<div class="col-md-6 newfileSec">
</div>