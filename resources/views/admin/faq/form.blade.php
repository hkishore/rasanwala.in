						<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="mb-2rem col-md-12 {{ $errors->has('title') ? 'has-error' : '' }}">
											<label for="title" class="form-label">Question</label>
												{!! Form::textarea('question', null, ['id' => 'question','rows'=>'10', 'class' => 'form-control', 'placeholder' => 'Write a question  here..', 'value' => old('faq_question')]) !!}			
										</div>	
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="mb-2rem col-md-12 {{ $errors->has('title') ? 'has-error' : '' }}">
											<label for="title" class="form-label">Answer</label>
												{!! Form::textarea('answer', null, ['id' => 'answer' ,'class' => 'form-control', 'placeholder' => 'Write a answer here..', 'value' => old('faq_answer')]) !!}			
										</div>		
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="mb-2rem col-md-6 {{ $errors->has('name') ? 'has-error' : '' }}">
											<label for="name" class="form-label"> Status</label>
												{{ Form::select('status', ['1'=>'Active','0'=>'InActive'],null, ['class' => 'form-select', 'value' => old('status')]) }}			
										</div>
									
									</div>
								</div>
							</div>
							<div class="d-flex justify-content-start ">
								<button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
								<a href="{{route('faqs.index')}}" class="btn btn-primary themeBtn themeUnFill me-3">Cancel</a>
							</div>
   
	
@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
            // Select2 Multiple
            $('.select2-multiple').select2({
                placeholder: "Select",
                allowClear: true
            });

        });
</script>
@endpush


