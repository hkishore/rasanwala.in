
@extends('layouts.admin')
@section('content')
<div class="themeTable">
          <div class="table_id DatatableHeader" >
            <div class="pgTitle">
              <h1>Faq</h1>
            </div>
            <div class="searchBx">
              <input type="text" id="myInputTextField" placeholder="Search">
            </div>
     <div class="tableLength">
              <a href="{{route('faqs.create')}}" class="btn btn-primary themeBtn themeFill iconLeft">
                
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M0 8.69814H6.56325V15.261H8.69814V8.69814H15.261V6.56322H8.69814V0H6.56325V6.56322H0V8.69814Z" fill="white"/>
                </svg>
                &nbsp; Add new</a>
            </div>
          </div>
          
          <div class="table-responsive">
            <table id="listtable1" class="display table table-striped">
			 <thead>
                <tr>
					<th>Question</th>
					<th>Answer</th>              
					<th>Status</th>               
                    <th>Action</th>    
                </tr>
            </thead>
            <tbody>
			 </tbody>
            </table>
          </div>
      </div>
@endsection
@push('scripts')
<script type="text/javascript">
$( document ).ready(function() {
	datatableRender=$('#listtable1').DataTable({
			responsive: true,
			processing: true,
			serverSide: true,
			//searching: false,
			ajax:  {
				"url":"{{route('faqs.dataTable')}}",
				"data": function(data){
				}
			},
				
			stateSave: true,
			 bLengthChange: true,
			//bInfo: false,

		});
$('#myInputTextField').keyup(function(){
       datatableRender.search($(this).val()).draw() ;
         });
	});
function deleteUser(obj)
{
	var url=$(obj).data('href');
	$('#confirm-delete .modal-title').html('Delete Review');
    $('#confirm-delete form').attr('action',url);
	$('#confirm-delete .text-center').html('Are you sure want to delete this Review?');
	$('#confirm-delete').modal('show');
}
</script>
@endpush