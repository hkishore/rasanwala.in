@extends('layouts.admin')

@section('content')

    <div class="pageTtl">
            <h1>Add Faq</h1>
        </div>       
    <div class="greyBx">
            <div class="innerWrap"> 
                {{ Form::open(['route' => 'faqs.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }} 
                    @include('admin.faq.form') 
                {{ Form::close() }}
           </div>
    </div>

@endsection
