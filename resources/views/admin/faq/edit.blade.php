@extends('layouts.admin')
@section('content')
    <div class="pageTtl">
            <h1>Edit Faq</h1>
	</div>		
	 <div class="greyBx">
            <div class="innerWrap">
              {{ Form::model($model,['route' => ['faqs.update', $model->id],'enctype'=>"multipart/form-data" ,"class"=>"themeForm","autocomplete"=>"off"]) }}

                {{ csrf_field() }}
                    @method('PUT')
                    @include('admin.faq.form')  
                {{ Form::close() }}
    </div>
    </div>

@endsection