<?php
$userRole=config('constant.userRole');
?>
	<div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-6 {{ $errors->has('name') ? 'has-error' : '' }}">
		  <label for="name" class="form-label">Name</label>
		   {{ Form::text('name',  null, ['class' => 'form-control','placeholder'=>'Name','id'=>'name']) }}
			@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
		</div>
	    <div class="mb-2rem col-md-6 {{ $errors->has('email') ? 'has-error' : '' }}">
		  <label for="email" class="form-label">Email</label>
			 {{ Form::text('email',null, ['class' => 'form-control','id'=>'email','placeholder'=>'Email']) }}
			@if ($errors->has('email'))
				<span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
			@endif
		</div>	
		</div>
	 </div>
	<div class="col-md-12">
	  <div class="row">
	  	<div class="mb-2rem col-md-6 {{ $errors->has('password') ? 'has-error' : '' }}">
		  <label for="password" class="form-label">Password</label>
			<input type="password" name="password" id="password" class='form-control'>
			@if(isset($model) && !empty($model))
             <span class="">
                    <small>(Leave it blank for older)</small>
                </span>
			@endif
			@if ($errors->has('password'))
				<span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
			@endif
		</div>	
		 <div class="mb-2rem col-md-6 {{ $errors->has('phone') ? 'has-error' : '' }}">
		  <label for="phone" class="form-label">Contact No</label>
			 {{ Form::text('phone',null, ['class' => 'form-control','id'=>'phone','placeholder'=>'Contact No']) }}
			@if ($errors->has('phone'))
				<span class="help-block">
					<strong>{{ $errors->first('phone') }}</strong>
				</span>
			@endif
		</div>	
		 </div>
		</div>
	 <div class="col-md-12">
	  <div class="row">	
		<div class="mb-2rem col-md-6 status {{ $errors->has('status') ? 'has-error' : '' }}">
		  <label for="status" class="form-label">STATUS</label>
			 {{ Form::select('status', ['1'=>'Active','0'=>'InActive'],null, ['class' => 'form-select']) }}
			@if ($errors->has('status'))
				<span class="help-block">
					<strong>{{ $errors->first('status') }}</strong>
				</span>
			@endif
		 </div>
		<div class="mb-2rem col-md-6 status {{ $errors->has('status') ? 'has-error' : '' }}">
		  <label for="status" class="form-label">Role</label>
			 {{ Form::select('role', ['3'=>'Customer','2'=>'Store Keeper'],null, ['id'=>'role','class' => 'form-select','onchange'=>'chengeProduct(this)']) }}
			@if ($errors->has('role'))
				<span class="help-block">
					<strong>{{ $errors->first(role) }}</strong>
				</span>
			@endif
		 </div>
		</div>
	 </div>
	 <div class="col-md-12">
	  <div class="row codHide" >	
		<div class="mb-2rem col-md-6 status {{ $errors->has('status') ? 'has-error' : '' }}">
		  <label for="status" class="form-label">Cash On Dilevery  <input type="checkbox" class="form-check-input" id="cod" name="cod" @if(isset($model) && $model->cod == 'on') checked @endif/> </label>
		 </div>
		 <div class="mb-2rem col-md-6 status {{ $errors->has('status') ? 'has-error' : '' }}">
		  <label for="status" class="form-label">Free Shipping  <input type="checkbox" class="form-check-input" id="freeshipping" name="freeshipping" @if(isset($model) && $model->freeshipping == 'on') checked @endif/> </label>
		 </div>
	  </div>
		 </div>
	 <div class="d-flex justify-content-start">
     <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
     <a  href="{{ url()->previous() }}" class="btn btn-primary themeBtn themeUnFill">Cancel</a>
     </div>
   
	
@push('scripts')
<script type="text/javascript">
$('document').ready(function(){	
    renderTinymce('description');
	if($('#role').val() == 3) {
		$('#codHide').show();
	}else{
	
		$('.codHide').hide();
		$('#cod').val("");
	}
});
function chengeProduct(obj) {
	if(obj.value== 2){
		$('.codHide').hide();
		$('#cod').val("");
	}else{
	$('.codHide').show();
	}
}
</script>
@endpush


