@extends('layouts.admin')

@section('content')
<?php
$user="User";
if($userType==2){
$user="Vender";
}
?>
    <div class="pageTtl">
            <h1>Add {{$user}}</h1>
        </div>       
    <div class="greyBx">
            <div class="innerWrap"> 
                {{ Form::open(['route' => 'user.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }} 
                         
                    @if($userType == 2)
					@include('admin.users.add-vender-form') 
                   	@else				 
                    @include('admin.users.form') 
					@endif	
                {{ Form::close() }}
           </div>
    </div>

@endsection
