
                  <div class="row">
                    <div class="mb-2rem col-md-6">
                       <label for="" class="form-label">NAME <span class="red">*</span></label>
                      {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name', 'value' => old('name')]) !!}
					  @if ($errors->has('name'))
						<span class="help-block">
						<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
                    </div>
                   <!-- <div class="mb-2rem col-md-6">
                      <label for="" class="form-label">SLUG</label>
                      {!! Form::text('slug', null, ['id' => 'slug', 'class' => 'form-control', 'placeholder' => 'Slug', 'value' => old('slug')]) !!}

			          @if ($errors->has('Slug'))
			          <span class="help-block">
			          <strong>{{ $errors->first('slug') }}</strong>
			          </span>
			          @endif
                    </div>-->
					<div class="mb-2rem col-md-6">
					<label for="" class="form-label">PHOTO</label>
                      {!! Form::file('image', null, ['id' => 'image', 'class' => 'form-control', 'placeholder' => 'Image', 'value' => old('image')]) !!}

			          @if ($errors->has('image'))
			          <span class="help-block">
			          <strong>{{ $errors->first('image') }}</strong>
			          </span>
			          @endif
                    </div>
                    </div>
				        <div class="d-flex justify-content-start">
                        <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
                        <a  href="{{ url()->previous() }}" class="btn btn-primary themeBtn themeUnFill">Cancel</a>
                      </div>