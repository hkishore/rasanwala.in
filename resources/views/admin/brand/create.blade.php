@extends('layouts.admin')

@section('content')

    <div class="tableWrap sectionDash">
            <h1 class="title">Add Brand</h1>
			</div>
			<div class="greyBx">
            <div class="innerWrap">
              {{ Form::open(['route' => 'brand.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }} 
                                      
                    @include('admin.brand.form') 

                {{ Form::close() }}
            

                
   
	</div>
	</div>

@endsection
