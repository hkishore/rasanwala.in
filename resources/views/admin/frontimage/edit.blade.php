@extends('layouts.admin')
@section('content')
    <div class="tableWrap sectionDash">
            <h1 class="title">Edit Slider Image</h1>
			</div>
			<div class="greyBx">
            <div class="innerWrap">
              {{ Form::model($model,['route' => ['frontimage.update', $model->id],'enctype'=>"multipart/form-data"]) }}

                    {{ csrf_field() }}
                    @method('PUT')
                    @include('admin.frontimage.form')   
                {{ Form::close() }}
           
   </div>
	</div>

@endsection