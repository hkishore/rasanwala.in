 <?php
$SliderType=\Config('constant.SliderType');
$imageType=\Config('constant.ImageType');
$oldfile="";
 if(isset($model) && $model->image!='')
 {
	 $oldfile=$model->image;
 }	   
?>        
<div class="row">
<div class=" mb-2rem col-md-6">
        <div class="form-group {{ $errors->has('slider_type') ? 'has-error' : '' }}">
            <label for="slider_type">Slider Type:</label>

            {{ Form::select('slider_type', $SliderType	, null, ['placeholder' => 'Select Slider Type','id' => 'section_id','class' => 'form-select']) }}

            @if ($errors->has('slider_type'))
                <span class="help-block">
                    <strong>{{ $errors->first('slider_type') }}</strong>
                </span>
            @endif
        </div>
    </div>
<div class="mb-2rem col-md-6">
        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label for="status">Status:</label>

            {{ Form::select('status', [	1 => 'Active',0 => 'Inactive']	, null, ['class' => 'form-select']) }}

            @if ($errors->has('status'))
                <span class="help-block">
                    <strong>{{ $errors->first('status') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>
<div class="row">
	<div class="mb-2rem col-md-6">
        <div class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
           <label>Position</label>
			{!! Form::text('position', null, ['id' => 'position', 'class' => 'form-control', 'placeholder' => 'Position', 'value' => old('position')]) !!}

			@if ($errors->has('position'))
			<span class="help-block">
			<strong>{{ $errors->first('position') }}</strong>
			</span>
			@endif
        </div>
    </div>
    <div class="mb-2rem col-md-6">
        <div class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
           <label>URL </label>
			{!! Form::text('url', null, ['id' => 'url', 'class' => 'form-control', 'placeholder' => 'URL', 'value' => old('url')]) !!}

			@if ($errors->has('url'))
			<span class="help-block">
			<strong>{{ $errors->first('url') }}</strong>
			</span>
			@endif
        </div>
    </div>
</div>
<div class= "hideFile">
<div class="row">
	<div class="mb-2rem col-md-6">
        <div class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
           <label>Small Title</label>
			{!! Form::text('small_title', null, ['id' => 'small_title', 'class' => 'form-control', 'placeholder' => 'Small Title', 'value' => old('small_title')]) !!}

			@if ($errors->has('small_title'))
			<span class="help-block">
			<strong>{{ $errors->first('small_title') }}</strong>
			</span>
			@endif
        </div>
    </div>
    <div class="mb-2rem col-md-6">
        <div class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
           <label>Large Title</label>
			{!! Form::text('large_title', null, ['id' => 'large_title', 'class' => 'form-control', 'placeholder' => 'Large Title', 'value' => old('large_title')]) !!}

			@if ($errors->has('large_title'))
			<span class="help-block">
			<strong>{{ $errors->first('large_title') }}</strong>
			</span>
			@endif
        </div>
    </div>
</div>
<div class="row">
    <div class="mb-2rem col-md-6">
        <div class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
           <label>Image Type</label>
			{!! Form::select('image_type',$imageType,null, ['id' => 'image_type', 'class' => 'form-control', 'placeholder' => 'Select Image Type', 'value' => old('image_type')]) !!}
			@if ($errors->has('image_type'))
			<span class="help-block">
			<strong>{{ $errors->first('image_type') }}</strong>
			</span>
			@endif
        </div>
        </div>
        <div class="mb-2rem col-md-6">
        <div class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
           <label>Description</label>
			{!! Form::text('description', null, ['id' => 'description', 'class' => 'form-control', 'placeholder' => 'Description', 'value' => old('description')]) !!}

			@if ($errors->has('description'))
			<span class="help-block">
			<strong>{{ $errors->first('description') }}</strong>
			</span>
			@endif
        </div>
    </div>
</div>
</div>
	<div class="row">
	<div class="mb-2rem col-md-6">
		<div class="form-group {{ $errors->has('image_name') ? 'has-error' : '' }}">
			
              @include('admin.image-uploader',['label'=>'Slider Image','key'=>'image','oldfile'=>$oldfile])
			@if ($errors->has('image'))
				<span class="help-block">
					<strong>{{ $errors->first('image') }}</strong>
				</span>
			@endif
		</div>
	</div>		                         
	</div>                      
   
	<div class="d-flex justify-content-start">
	<button type="submit" id="submitButton" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
	<a  href="{{ url()->previous() }}" class="btn btn-primary themeBtn themeUnFill">Cancel</a>
	</div>
	@push('scripts')
<script type="text/javascript">
var filepath="{{route('file.upload')}}";
$( document ).ready(function() {
	

	if($('#image').length>0)
	{
		//$('#submitButton').hide();
	  addDropZone('image',filepath);
	}
		
$(".hideFile").hide();
$('#section_id').trigger('change');
} );

$('#section_id').on('change', function() {
   var sectionId = $('#section_id').val();
   if(sectionId=="section_first")
   {
	$('#strongid').html('Note:');
   }
   if(sectionId=="section_fourth")
   { 
    $(".hideFile").hide();
   }
   if(sectionId=="section_thread")
   { $(".hideFile").hide();
   }
   if(sectionId=="section_second")
   {
   $(".hideFile").show();
   }
});
</script>
@endpush

					  
					  
					 

