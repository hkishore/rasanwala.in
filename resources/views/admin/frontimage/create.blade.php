@extends('layouts.admin')
@section('content')
    <div class="tableWrap sectionDash">
            <h1 class="title">Add Slider Image</h1>
			</div>
			<div class="greyBx">
            <div class="innerWrap">
              {{ Form::open(['route' => 'frontimage.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }} 
                                      
                    @include('admin.frontimage.form') 

                {{ Form::close() }}
	</div>
	</div>

@endsection
