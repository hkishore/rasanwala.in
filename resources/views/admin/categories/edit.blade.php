@extends('layouts.admin')

@section('content')
<div class="pageTtl">
            <h1>Edit Categeory</h1>
			</div>
<div class="greyBx">
            <div class="innerWrap">	
              {{ Form::model($model,['route' => ['category.update', $model->id],'enctype'=>"multipart/form-data"]) }}

                    {{ csrf_field() }}
                    @method('PUT')
                    @include('admin.categories.form')            

                {{ Form::close() }}
           
                </div>
    </div>

@endsection