@extends('layouts.admin')
@section('content')
<?php
use App\Models\Category;
$category=Category::where('parent_id',null)->get();
?>
<div class="themeTable">
          <div class="table_id DatatableHeader" >
            <div class="pgTitle">
              <h1>Category</h1>
            </div>
            <div class="searchBx">
              <input type="text" id="myInputTextField" placeholder="Search">
            </div>
			&nbsp;&nbsp;
			<div class="col-md-3 pull-right searchBx" >
					<select id='cat_id'class="form-control searchBx">
					    <option value="0" >Select Main Catgeory</option>
					 @foreach($category as $key=>$cat)
							<option value="{{$cat->id}}">{{$cat->name}}</option>
					@endforeach
					</select>
            </div>
            <div class="tableLength">
		    <a href="{{route('category.create')}}" class="btn btn-primary themeBtn themeFill iconLeft">
                
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M0 8.69814H6.56325V15.261H8.69814V8.69814H15.261V6.56322H8.69814V0H6.56325V6.56322H0V8.69814Z" fill="white"/>
                </svg>
                &nbsp; Add new</a>
            </div>
          </div>
          
          <div class="table-responsive">
            <table id="listtable1" class="display table table-striped">
			 <thead>
                <tr>
                    <th>Image</th>
				    <th>Main Category</th>
                    <th>Child Category</th>
                    <th>Slug</th> 
                    <th>Status</th> 
                    <th>Action</th>					
                    
                </tr>
            </thead>
            <tbody>
			
			 </tbody>
            </table>
          </div>
      </div>

@endsection
@push('scripts')
<script type="text/javascript">
var catTable;
$( document ).ready(function() {
	catTable=$('#listtable1').DataTable({
			responsive: true,
			processing: true,
			serverSide: true,
			searching: true,
			ajax:  {
				"url":"{{route('cat.dataTable')}}",
				"data": function(data){
					data.cat_id = $('#cat_id').val();
				}
			},
			stateSave: true,
			bLengthChange: true,
			bInfo: true,

		});
		$('#myInputTextField').keyup(function(){
		catTable.search($(this).val()).draw() ;
		});
		$('#cat_id').change(function(){
			catTable.draw();
		});

} );
function deleteCategory(obj)
{
  var url = $(obj).data('href');
	$('#confirm-delete .modal-title').html('Delete Category');
    $('#confirm-delete form').attr('action',url);
	$('#confirm-delete .text-center').html('Are you sure want to delete this Category?');
	$('#confirm-delete').modal('show');
}
</script>
@endpush
