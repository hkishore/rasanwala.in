@extends('layouts.admin')

@section('content')
<div class="pageTtl">
            <h1>Add Categeory</h1>
        </div>      
<div class="greyBx">
            <div class="innerWrap"> 
               

                {{ Form::open(['route' => 'category.store','enctype'=>"multipart/form-data"]) }}

                    {{ csrf_field() }} 
                                      
                    @include('admin.categories.form') 

                {{ Form::close() }}
           
                </div>
    </div>
@endsection
