<?php
use App\Models\Category;
$menuList=Category::where('parent_id',null)->pluck('id');
$oldfile="";
 if(isset($model) && $model->image!='')
 {
	 $oldfile=$model->image;
 }	
?>

	<div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-6 {{ $errors->has('parent_id') ? 'has-error' : '' }}">
		  <label for="parent_id" class="form-label"> CATEGEORY</label>
      {{ Form::select('parent_id', $categories, null, ['id'=>'parent_id','onchange'=>'catValue(this)','class' => 'form-control','placeholder'=>'Main Cateogory']) }}
			@if ($errors->has('parent_id'))
				<span class="help-block">
					<strong>{{ $errors->first('parent_id') }}</strong>
				</span>
			@endif
		</div>
		<div class="mb-2rem col-md-6  {{ $errors->has('name') ? 'has-error' : '' }}">
		  <label for="name" class="form-label">NAME <span class="red"> *</span></label>
      {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name', 'value' => old('name')]) !!}
			@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
		 </div>		
		</div>
	 </div>
	<div class="col-md-12">
	  <div class="row">
	    <div class="mb-2rem col-md-6  {{ $errors->has('name') ? 'has-error' : '' }}">
		  <label for="position" class="form-label">POSITION</label>
      {!! Form::text('position', null, ['id' => 'position', 'class' => 'form-control', 'placeholder' => 'position', 'value' => old('position')]) !!}
			@if ($errors->has('position'))
				<span class="help-block">
					<strong>{{ $errors->first('position') }}</strong>
				</span>
			@endif
		 </div>	
		
		<div class="mb-2rem col-md-6 {{ $errors->has('status') ? 'has-error' : '' }}">
		  <label for="status" class="form-label">STATUS</label>
      {{ Form::select('status', ['1'=>'Active','0'=>'InActive'],null, ['class' => 'form-select']) }}
			@if ($errors->has('status'))
				<span class="help-block">
					<strong>{{ $errors->first('status') }}</strong>
				</span>
			@endif
		</div>	
		 </div>
		</div>
		<div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-6 {{ $errors->has('image') ? 'has-error' : '' }}">
      @include('admin.image-uploader',['label'=>'Image','key'=>'image','oldfile'=>$oldfile])
			@if ($errors->has('image'))
				<span class="help-block">
					<strong>{{ $errors->first('image') }}</strong>
				</span>
			@endif
		</div>	
		
		 <div class="mb-2rem col-md-6 {{ $errors->has('show_list') ? 'has-error' : '' }}" id="menu_data">
		  <label for="show_list" class="form-label">Show List <input type="checkbox" name="show_list" @if('checked') value="1" @endif @if(isset($model) && $model->show_list == 1 ) checked @endif /></label>
      
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="mb-2rem col-md-6 {{ $errors->has('meta_title') ? 'has-error' : '' }}">
					<label for="meta_title" class="form-label">Meta Title</label>
					{{ Form::text('meta_title',  null, ['class' => 'form-control','placeholder'=>'Meta Title','id'=>'meta_title','value' => old('meta_title')]) }}
					@if ($errors->has('meta_title'))
					<span class="help-block">
						<strong>{{ $errors->first('meta_title') }}</strong>
					</span>
					@endif
				</div>
				<div class="mb-2rem col-md-6 {{ $errors->has('meta_tag') ? 'has-error' : '' }}">
					<label for="meta_tag" class="form-label">Meta Keyword</label>
					{{ Form::text('meta_tag',null, ['class' => 'form-control','placeholder'=>'Meta Keywords','value' => old('meta_tag'),'id'=>'meta_tag']) }}
					@if ($errors->has('meta_tag'))
					<span class="help-block">
						<strong>{{ $errors->first('meta_tag') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="mb-2rem col-md-12 {{ $errors->has('meta_description') ? 'has-error' : '' }}">
					<label for="meta_description" class="form-label">Meta Description</label>
					{{ Form::textarea('meta_description',null, ['class' => 'form-control','placeholder'=>'Write somthing here............','value' => old('meta_description'),'id'=>'meta_description']) }}
					@if ($errors->has('meta_description'))
					<span class="help-block">
						<strong>{{ $errors->first('meta_description') }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		</div>
		</div>
			<div class="d-flex justify-content-start ">
	  <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
	   <a  href="{{ url()->previous() }}" class="btn btn-primary themeBtn themeUnFill">Cancel</a>
	   <!--<a type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</a>-->
	</div>
@push('scripts')
<script type="text/javascript">
//var menuList = { !! $menuList !!};
var filepath="{{route('file.upload')}}";
$('document').ready(function(){
	if($('#image').length>0)
	{
		//$('#submitButton').hide();
	  addDropZone('image',filepath);
	}
});
	
 
</script>
@endpush










