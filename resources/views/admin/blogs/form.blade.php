
  <div class="row">
	<div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-6 {{ $errors->has('title') ? 'has-error' : '' }}">
		  <label for="title" class="form-label">Title</label>
		    {!! Form::text('title', null, ['id' => 'title', 'class' => 'form-control', 'placeholder' => 'Title', 'value' => old('title')]) !!}
			@if ($errors->has('title'))
			<span class="help-block">
			<strong>{{ $errors->first('title') }}</strong>
			</span>
			@endif
		</div>	
		</div>
	 </div>
	<div class="col-md-12">
		<div class="row">
		  <div class="mb-2rem col-md-12 {{ $errors->has('description') ? 'has-error' : '' }}">
		  <label for="description" class="form-label">Description</label>
			 {!! Form::textarea('description', null, ['id' => 'description', 'class' => 'form-control', 'placeholder' => 'Write somthing here................', 'value' => old('description')]) !!}
			@if ($errors->has('description'))
				<span class="help-block">
					<strong>{{ $errors->first('description') }}</strong>
				</span>
			@endif
		 </div>
		</div>
	</div>

	 <div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-6 {{ $errors->has('publish_at') ? 'has-error' : '' }}">
		  <label for="publish_at" class="form-label">Publish Date</label>
            {!! Form::text('publish_at', null, ['id' => 'publish_at', 'class' => 'form-control', 'placeholder' => 'mm/dd/yyyy', 'value' => old('publish_at')]) !!}
            @if ($errors->has('publish_at'))
                <span class="help-block">
                    <strong>{{ $errors->first('publish_at') }}</strong>
                </span>
            @endif
		</div>	
		<div class="mb-2rem col-md-6 status {{ $errors->has('status') ? 'has-error' : '' }}">
		  <label for="status" class="form-label">STATUS</label>
			 {{ Form::select('status', ['0'=>'Active','1'=>'InActive'],null, ['class' => 'form-select']) }}
			@if ($errors->has('status'))
				<span class="help-block">
					<strong>{{ $errors->first('status') }}</strong>
				</span>
			@endif
		 </div>
		</div>
	 </div>
	 <div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-6 {{ $errors->has('image_name') ? 'has-error' : '' }}">
		  <label for="image_name" class="form-label">Featured Image</label>
		   {!! Form::file('image_name', null, [ 'class' => 'form-control']) !!}				
            @if ($errors->has('image_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('image_name') }}</strong>
                </span>
            @endif
			<div class=" mb-2rem col-md-6">
		        @if(isset($model) && $model->image_name!='')
				<img src="{{asset('uploads/blogs/'.$model->image_name)}}" width="150px"/>
				@endif
	       </div>		
		</div>	
		<div class="mb-2rem col-md-6 {{ $errors->has('url') ? 'has-error' : '' }}">
		  <label for="url" class="form-label">Url</label>
			{!! Form::text('url', null, ['id' => 'url', 'class' => 'form-control', 'placeholder' => 'Url', 'value' => old('url')]) !!}
			 @if ($errors->has('url'))
			  <span class="help-block">
			  <strong>{{ $errors->first('url') }}</strong>
			  </span>
			 @endif
		 </div>
		</div>
	 </div>
	 <hr>
	<div class="col-md-12">
	  <div class="row">
		<div class="mb-2rem col-md-6 {{ $errors->has('meta_title') ? 'has-error' : '' }}">
		  <label for="meta_title" class="form-label">Meat Title</label>
		   {{ Form::text('meta_title',  null, ['class' => 'form-control','placeholder'=>'Meat Title','id'=>'meta_title']) }}
			@if ($errors->has('meta_title'))
				<span class="help-block">
					<strong>{{ $errors->first('meta_title') }}</strong>
				</span>
			@endif
		</div>	
		<div class="mb-2rem col-md-6 {{ $errors->has('meta_keyword') ? 'has-error' : '' }}">
		  <label for="meta_keyword" class="form-label">Meta Keyword</label>
			 {{ Form::text('meta_keyword',null, ['class' => 'form-control','id'=>'meta_keyword']) }}
			@if ($errors->has('meta_keyword'))
				<span class="help-block">
					<strong>{{ $errors->first('meta_keyword') }}</strong>
				</span>
			@endif
		 </div>
		</div>
	 </div>
	 <div class="col-md-12">
		<div class="row">
		  <div class="mb-2rem col-md-12 {{ $errors->has('meta_description') ? 'has-error' : '' }}">
		  <label for="meta_description" class="form-label">Meta Description</label>
			 {{ Form::textarea('meta_description',null, ['class' => 'form-control','placeholder'=>'Write somthing here............','id'=>'meta_description']) }}
			@if ($errors->has('meta_description'))
				<span class="help-block">
					<strong>{{ $errors->first('meta_description') }}</strong>
				</span>
			@endif
		 </div>
		</div>
	 </div>
   </div>
	<div class="d-flex justify-content-start ">
	  <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
	  <a href="{{route('blog.index')}}" class="btn btn-primary themeBtn themeUnFill me-3">Cancel</a>
	</div>
@push('scripts')
<script type="text/javascript">
$('document').ready(function(){	
    renderTinymce('description');
});
</script>
@endpush


