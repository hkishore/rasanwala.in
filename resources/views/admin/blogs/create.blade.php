@extends('layouts.admin')

@section('content')
    <div class="pageTtl">
            <h1>Add Blog</h1>
        </div>       
    <div class="greyBx">
            <div class="innerWrap"> 
                {{ Form::open(['route' => 'blog.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }} 
                                      
                    @include('admin.blogs.form') 

                {{ Form::close() }}
             </div>
    </div>

@endsection
