@extends('layouts.admin')

@section('content')
    <div class="pageTtl">
            <h1>Edit Blog</h1>
        </div>       
    <div class="greyBx">
            <div class="innerWrap"> 
              {{ Form::model($model,['route' => ['blog.update', $model->id],'enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }}
                    @method('PUT')
                    @include('admin.blogs.form')            

                {{ Form::close() }}
              </div>
    </div>

@endsection