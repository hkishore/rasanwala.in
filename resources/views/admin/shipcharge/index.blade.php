@extends('layouts.admin')
@section('content')
<div class="themeTable">
          <div class="table_id DatatableHeader" >
            <div class="pgTitle">
              <h1>Shipping Charges</h1>
            </div>
            <div class="searchBx">
              <input type="text" id="myInputTextField" placeholder="Search">
            </div>
            <div class="tableLength">
              <a href="{{route('charge.create')}}" class="btn btn-primary themeBtn themeFill iconLeft">
                <svg xmlns="http://www.w3.org/2000/svg" width="06" height="06" viewBox="0 0 06 06" fill="none">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M0 8.69814H6.56325V15.261H8.69814V8.69814H15.261V6.56322H8.69814V0H6.56325V6.56322H0V8.69814Z" fill="white"/>
                </svg>
                &nbsp; Add new</a>
            </div>
          </div>
          
          <div class="table-responsive">
            <table id="listtable1" class="display table table-striped">
			 <thead>
                <tr>
                    <th>Shipping Area</th>
					<th>Start Time</th>
					<th>End Time</th>
                    <th>Shipping Charge</th>
                    <th>Action</th>					
					
                </tr>
            </thead>
            <tbody>
			@if(count($model)>0)
               @foreach($model as $sC)
               <tr>
			   @if($sC->area_id != "")
                 <td>{{$sC->areaName->area}}</td>
			 @else
				 <td>All Area</td>
			 @endif
				 <td>{{$sC->start_time}}</td>
				 <td>{{$sC->end_time}}</td>
				 <td>{{$sC->charge}}</td>
                <td>
				<div class="d-flex justify-content-start">
                <a  href="{{ route('charge.edit',$sC->id) }}" class="btn me-2 dataActionBtn btnEdit" ><img src="{{asset('img/datatable-edit-plus.svg')}}"/></a> 		 
                <a  onclick="deleteArea('{{route('charge.destroy',$sC->id) }}')" href="javascript:void(0);" class="btn me-2 dataActionBtn btnDelete"><img src="{{asset('img/datatable-delete.svg')}}"/></a>
                </div> 				
				 </td>
               </tr>
               @endforeach
             @endif
			 </tbody>
            </table>
          </div>
      </div>
@endsection
@push('scripts')
<script type="text/javascript">
$( document ).ready(function() {
   datatableRenderWithoutAjax(1)

} );
function deleteArea(obj)
{
	$('#confirm-delete .modal-title').html('Delete Area Shipping Charge');
    $('#confirm-delete form').attr('action',obj);
	$('#confirm-delete .text-center').html('Are you sure want to delete this Area Charge?');
	$('#confirm-delete').modal('show');
}
</script>
@endpush