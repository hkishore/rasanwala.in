@extends('layouts.admin')

@section('content')

    <div class="tableWrap sectionDash">
            <h1 class="title">Edit Ship Charge</h1>
			</div>
			<div class="greyBx">
            <div class="innerWrap">
              {{ Form::model($model,['route' => ['charge.update', $model->id],'enctype'=>"multipart/form-data"]) }}
                    {{ csrf_field() }}
                    @method('PUT')
                    @include('admin.shipcharge.form')            

                {{ Form::close() }}
           
   </div>
	</div>

@endsection