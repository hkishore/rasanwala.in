@extends('layouts.admin')

@section('content')

    <div class="tableWrap sectionDash">
            <h1 class="title">Add Ship Charge</h1>
			</div>
			<div class="greyBx">
            <div class="innerWrap">
              {{ Form::open(['route' => 'charge.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}

                    {{ csrf_field() }} 
                                      
                    @include('admin.shipcharge.form') 

                {{ Form::close() }}
            

                
   
	</div>
	</div>

@endsection
