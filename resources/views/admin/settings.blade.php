@extends('layouts/admin')
@section('content')
<?php

use App\Models\Setting;

$opene = Setting::getFieldVal('sp_enble_today');
$app_maintenance = Setting::getFieldVal('app_maintenance');
$web_maintenance = Setting::getFieldVal('web_maintenance');
//dd($home_test);
?>
<div class="pageTtl">
	<h1>Settings</h1>
</div>
<div class="greyBx mb-3">
	<div class="innerWrap">
		{{ Form::open(['route' => 'setting.update','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="mb-2rem col-md-6 {{ $errors->has('title') ? 'has-error' : '' }}">
						<label for="title" class="form-label">Email</label>
						{!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Email', 'value' => old('email')]) !!}
					</div>

					<div class="mb-2rem col-md-6">
						<label for="title" class="form-label">Minimum Order Amount for Home Delivery</label>
						{!! Form::text('mini_order_amt', null, ['id' => 'mini_order_amt', 'class' => 'form-control', 'placeholder' => 'Minimum Order Amount for Home Delivery', 'value' => old('mini_order_amt')]) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="mb-2rem col-md-6 {{ $errors->has('title') ? 'has-error' : '' }}">
						<label for="title" class="form-label">Shop Open And Close Time</label>
						{!! Form::text('open_close_time', null, ['id' => 'open_close_time', 'class' => 'form-control', 'placeholder' => 'Exp. 01-24', 'value' => old('open_close_time')]) !!}
					</div>
					<div class="mb-2rem col-md-6">
						<label for="title" class="form-label">Shipping Time</label>
						{!! Form::text('shipping_time', null, ['id' => 'shipping_time', 'class' => 'form-control', 'placeholder' => 'shipping time', 'value' => old('shipping_time')]) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="mb-2rem col-md-6">
				<label for="title" class="form-label">Shipping Charge Default</label>
				{!! Form::text('sp_charge', null, ['id' => 'sp_charge', 'class' => 'form-control', 'placeholder' => 'Shipping Charge', 'value' => old('sp_charge')]) !!}

			</div>

			<div class="mb-2rem col-md-6">
				<label for="title" class="form-label">Number Of Free Shipping/Month</label>
				{!! Form::text('shipping_month', null, ['id' => 'shipping_month', 'class' => 'form-control', 'placeholder' => 'Number Of Free Shipping/Month', 'value' => old('shipping_month')]) !!}
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="mb-2rem col-md-6">
					<label for="title" class="form-label">Minimum Order Amount for Free Shiping</label>
					{!! Form::text('mini_order_amt_free_shiping', null, ['id' => 'mini_order_amt_free_shiping', 'class' => 'form-control', 'placeholder' => 'Minimum Order Amount for Free Shiping', 'value' => old('mini_order_amt_free_shiping')]) !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="mb-2rem col-md-3 pt-4{{ $errors->has('api_password') ? 'has-error' : '' }}">
				<label for="title" class="form-label">Shipping Enable Today</label>
				<input type="checkbox" id="checkId" class="form-check-input" value="" @if($opene==1) checked @endif>
				<input type="hidden" id="sp_enble_today" name="sp_enble_today" value="" />
			</div>
			<div class="mb-2rem col-md-3 pt-4 {{ $errors->has('app_maintenance') ? 'has-error' : '' }}">
				<label for="title" class="form-label">App is in Maintenance</label>
				<input type="checkbox" id="app_maintenance_checkBox" class="form-check-input" value="" @if($app_maintenance==1) checked @endif>
				<input type="hidden" id="app_maintenance" name="app_maintenance" value="" />
			</div>
			<div class="mb-2rem col-md-3 pt-4 {{ $errors->has('app_maintenance') ? 'has-error' : '' }}">
				<label for="title" class="form-label">Website is in Maintenance</label>
				<input type="checkbox" id="web_maintenance_checkBox" class="form-check-input" value="" @if($web_maintenance==1) checked @endif>
				<input type="hidden" id="web_maintenance" name="web_maintenance" value="" />
			</div>
		</div>

		<div class="d-flex justify-content-start ">
			<button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
		</div>
		{{ Form::close() }}
	</div>

</div>
<div class="pageTtl">
	<h1>Razorpay</h1>
</div>
<div class="greyBx">
	<div class="innerWrap">
		{{ Form::open(['route' => 'setting.update','enctype'=>"multipart/form-data" ,"class"=>"themeForm"]) }}
		{{ csrf_field() }}
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="mb-2rem col-md-6 {{ $errors->has('title') ? 'has-error' : '' }}">
						<label for="title" class="form-label">Razorpay Test Key Id</label>
						{!! Form::text('razorpay_key_id_test', null, ['id' => 'razorpay_key_id_test', 'class' => 'form-control', 'placeholder' => 'Razorpay Test Key Id', 'value' => old('razorpay_key_id_test')]) !!}
					</div>
					<div class="mb-2rem col-md-6 {{ $errors->has('shipping_api') ? 'has-error' : '' }}">
						<label for="title" class="form-label">Razorpay Test Key Secret</label>
						{!! Form::text('razorpay_key_secret_test',null, ['id' => 'razorpay_key_secret_test', 'class' => 'form-control', 'placeholder' => 'Razorpay Test Key Secret', 'value' => old('razorpay_key_secret_test')]) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="mb-2rem col-md-6 {{ $errors->has('title') ? 'has-error' : '' }}">
						<label for="title" class="form-label">Razorpay Live Key Id</label>
						{!! Form::text('razorpay_key_id_live', null, ['id' => 'razorpay_key_id_live', 'class' => 'form-control', 'placeholder' => 'Razorpay Live Key Id', 'value' => old('razorpay_key_id_live')]) !!}
					</div>

					<div class="mb-2rem col-md-6 {{ $errors->has('shipping_api') ? 'has-error' : '' }}">
						<label for="title" class="form-label">Razorpay Live Key Secret</label>
						{!! Form::text('razorpay_key_secret_live',null, ['id' => 'razorpay_key_secret_live', 'class' => 'form-control', 'placeholder' => 'Razorpay Live Key Secret', 'value' => old('razorpay_key_secret_live')]) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-md-12">
				<div class="form-group radioList">
					<label for="title" class="form-label">Razorpay Mode</label>
					<span for="title" class="form-label">{{ Form::radio('razorpay_mode', 'test' , false,["id"=>"razorpay_mode_test"]) }} Test </span>
					<span for="title" class="form-label">{{ Form::radio('razorpay_mode', 'live' , false,["id"=>"razorpay_mode_live"]) }} Live</span>
					<span for="title" class="form-label"> {{ Form::radio('razorpay_mode', 'disable' ,false,["id"=>"razorpay_mode_disable"]) }} Disable </span>
				</div>

			</div>
		</div>

		<div class="d-flex justify-content-start ">
			<button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
		</div>
		{{ Form::close() }}

	</div>
</div>
<div class="pageTtl" style="margin-top:20px;">
	<h1>Cashfree</h1>
</div>
<div class="greyBx">
	<div class="innerWrap">
		{{ Form::open(['route' => 'setting.update','enctype'=>"multipart/form-data" ,"class"=>"themeForm"]) }}
		{{ csrf_field() }}
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="mb-2rem col-md-6 {{ $errors->has('title') ? 'has-error' : '' }}">
						<label for="title" class="form-label">Test Key Id</label>
						{!! Form::text('cashfree_key_id_test', null, ['id' => 'cashfree_key_id_test', 'class' => 'form-control', 'placeholder' => 'Sendbox Key Id', 'value' => old('cashfree_key_id_test')]) !!}
					</div>
					<div class="mb-2rem col-md-6 {{ $errors->has('shipping_api') ? 'has-error' : '' }}">
						<label for="title" class="form-label">Test Key Secret</label>
						{!! Form::text('cashfree_key_secret_test',null, ['id' => 'cashfree_key_secret_test', 'class' => 'form-control', 'placeholder' => 'Test Key Secret', 'value' => old('cashfree_key_secret_test')]) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="mb-2rem col-md-6 {{ $errors->has('title') ? 'has-error' : '' }}">
						<label for="title" class="form-label">Live Key Id</label>
						{!! Form::text('cashfree_key_id_live', null, ['id' => 'cashfree_key_id_live', 'class' => 'form-control', 'placeholder' => 'Live Key Id', 'value' => old('cashfree_key_id_live')]) !!}
					</div>

					<div class="mb-2rem col-md-6 {{ $errors->has('shipping_api') ? 'has-error' : '' }}">
						<label for="title" class="form-label">Live Key Secret</label>
						{!! Form::text('cashfree_key_secret_live',null, ['id' => 'cashfree_key_secret_live', 'class' => 'form-control', 'placeholder' => 'Live Key Secret', 'value' => old('cashfree_key_secret_live')]) !!}
					</div>
				</div>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-md-12">
				<div class="form-group radioList">
					<label for="title" class="form-label">Cashfree Api Mode</label>
					<span for="title" class="form-label">{{ Form::radio('cashfree_mode', 'test' , false,["id"=>"cashfree_mode_test"]) }} Test </span>
					<span for="title" class="form-label">{{ Form::radio('cashfree_mode', 'live' , false,["id"=>"cashfree_mode_live"]) }} Live</span>
					<span for="title" class="form-label"> {{ Form::radio('cashfree_mode', 'disable' ,false,["id"=>"cashfree_mode_disable"]) }} Disable </span>
				</div>

			</div>
		</div>

		<div class="d-flex justify-content-start ">
			<button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
		</div>
		{{ Form::close() }}

	</div>
</div>

<div class="pageTtl" style="margin-top:20px;">
	<h1>Flash Messages</h1>
</div>
<div class="greyBx">
	<div class="innerWrap">
		{{ Form::open(['route' => 'setting.update','enctype'=>"multipart/form-data" ,"class"=>"themeForm"]) }}
		{{ csrf_field() }}
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="mb-2rem col-md-12 {{ $errors->has('home_test') ? 'has-error' : '' }}">
						<label for="home_test" class="form-label">Flash Messages</label>
						{!! Form::textarea('home_test', null, ['id' => 'home_test', 'class' => 'form-control', 'placeholder' => 'Enter Some Text', 'value' => old('home_test')]) !!}


					</div>
				</div>
			</div>
		</div>

		<div class="d-flex justify-content-start ">
			<button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
		</div>
		{{ Form::close() }}

	</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
	var setting = <?php echo json_encode($settings); ?>;
	var fieldsArr = [];
	$(document).ready(function() {
		renderTinymce('home_test');
		var radiosKey = ['razorpay_mode'];
		var radiosCash = ['cashfree_mode'];
		for (var i = 0; i < setting.length; i++) {
			var fieldName = setting[i].data_name;
			var fieldValue = setting[i].data_value;
			if (radiosKey.indexOf(fieldName) > -1) {
				$('#' + fieldName + '_' + fieldValue).attr('checked', 'checked');
			} else {

				$('#' + fieldName).val(fieldValue);
			}
			if (radiosCash.indexOf(fieldName) > -1) {
				$('#' + fieldName + '_' + fieldValue).attr('checked', 'checked');
			} else {

				$('#' + fieldName).val(fieldValue);
			}

			fieldsArr[fieldName] = fieldValue;
		}


	});


	$('#checkId').click(function() {
		if ($(this).is(':checked')) {
			$('#sp_enble_today').val('1');
		} else {
			$('#sp_enble_today').val('2');
		}
	});
	$('#app_maintenance_checkBox').click(function() {
		if ($(this).is(':checked')) {
			$('#app_maintenance').val('1');
		} else {
			$('#app_maintenance').val('2');
		}
	});
	$('#web_maintenance_checkBox').click(function() {
		if ($(this).is(':checked')) {
			$('#web_maintenance').val('1');
		} else {
			$('#web_maintenance').val('2');
		}
	});
</script>
@endpush