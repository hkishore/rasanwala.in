<!-- Add Category modal  -->
<div class="modal fade themeModal" id="questionModel" tabindex="-1" aria-labelledby="addCategoryLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header ">
        <div class="modalForm">
          <div class="row">
            <div class="d-flex justify-content-start">
              <h5 class="modal-title" id="addCategoryLabel">Ask a Question</h5>
              <button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
          </div>
        </div>
      </div>
        <form class="themeForm"  id="qForm" >
		@csrf
          <div class="modal-body pt-4 pb-4">
            <div class="modalForm">
				<div class="row">
				<div class="mb-2rem col-md-12 {{ $errors->has('name') ? 'has-error' : '' }}">
				<label for="vendor" class="form-label">Name<span class="red">*</span></label>
				{!! Form::text('name', null, ['id'=>'name','class' => 'form-control','required'=>'required']) !!}
				<span class="help-block">
				<small class="red" id="nam"></small>
				</span>
				</div>
				</div>
				
				<div class="row">
				<div class="mb-2rem col-md-12 {{ $errors->has('email') ? 'has-error' : '' }}">
				<label for="vendor" class="form-label">Email<span class="red">*</span></label>
				{!! Form::email('email', null, ['id'=>'email','class' => 'form-control','required'=>'required']) !!}
				<span class="help-block">
				   <small class="red" id="eml"></small>
				</span>
			<span class="pull-right">Your email address will not be published.</span>
			  </div>
			</div>

			<div class="row">
				<div class="mb-2rem col-md-12 {{ $errors->has('phone') ? 'has-error' : '' }}">
				<label for="vendor" class="form-label">Contact<span class="red">*</span></label>
				{!! Form::text('phone', null, ['id'=>'phone','class' => 'form-control','required'=>'required']) !!}
				<span class="help-block">
				   <small class="red" id="ph"></small>
				</span>
			  </div>
			</div>
			
				<div class="row">
			 <div class="mb-2rem col-md-12 {{ $errors->has('question') ? 'has-error' : '' }}">
				<label for="vendor" class="form-label">Your Inquiry<span class="red">*</span></label>
				  {!! Form::textarea('question', null, ['id'=>'question_id','class' => 'form-control','required'=>'required']) !!}
				<span class="help-block">
				   <small class="red" id="inq"></small>
				</span>
			 </div>
			</div>
           </div>
          </div>
		  <input type="hidden" name="product_id" value="" id="product_id" />
		  <input type="hidden" name="vendor_id" value="" id="vendor_id" />
          <div class="modal-footer justify-content-start ">
            <div class="modalForm">
              <div class="row">
                <div class="d-flex justify-content-start">
                  <button type="button" onclick="questionVendor()" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
                  <button type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </form>
    </div>
  </div>
</div>

 @push('scripts')

<script type="text/javascript">

function questionVendor()
		{
		var email=$('#email').val();
		var name=$('#name').val();
		var phone=$('#phone').val();
		var question=$('#question_id').val();
		var product_id=$('#product_id').val();
		var vendor_id=$('#vendor_id').val();
		//var email=$('#email_id').val();
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		$('.help-block').hide();
		var isErroFound=false;
		if(name == "" || name == null)
		 {
			$("#nam").html("Name is required");
			$("#nam").parent().show();	
             isErroFound=true;			
		}
		if(email == "" || email == null)
		{  
	        $("#eml").html("Email is required");
			$("#eml").parent().show();
			isErroFound=true;
		}
		else if (reg.test(email) == false) 
	    {
	        $("#eml").html("Please enter a valid email");
			$("#eml").parent().show();
			isErroFound=true;	
		}
		 if(phone == "" || phone == null)
		 {
			$("#ph").html("Contact is required");
			$("#ph").parent().show();
			isErroFound=true;	
		}
		if(question == "" || question == null)
		 {
			$("#inq").html("Inquiry is required");
			$("#inq").parent().show();
			isErroFound=true;	
		}
		if(isErroFound	)
		{
			return;
		}
		Swal.fire('Please wait')
        Swal.showLoading()
		$.ajax({
		url:"{{route('inquiry.product')}}",
		type: "POST",
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		data:{email: email ,name: name, phone: phone, question: question, product_id: product_id, vendor_id: vendor_id},
		success: function (data) {
			//$('#questionModel').hide();
			Swal.fire({
			icon: 'success',
			title: 'Thank You',
			showConfirmButton: false,
			timer: 4000,
			});	
			reload();
		}
		});
		 
		}
function reload()
{
    window.location.reload();
}		
</script>
@endpush