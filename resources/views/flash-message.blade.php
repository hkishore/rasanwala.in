                            
							
							@if ($message = Session::get('success'))
							<div  id="alert_success" class="alert alert-success text-center">{{ $message }}</div>
							@endif
							


							@if ($message = Session::get('error'))
							<div  id="alert_error" class="alert alert-danger text-center">{{ $message }}</div>
							@endif


							@if ($message = Session::get('warning'))
							<div  id="alert_warning" class="alert alert-warning text-center">{{ $message }}</div>
							@endif


							@if ($message = Session::get('info'))
							<div  id="alert_infos" class="alert alert-info text-center">{{ $message }}</div>
							@endif

							@if ($errors->any())
							<div  id="alert_infos" class="alert alert-danger text-center">Please check the form below for errors</div>
							@endif
							
						<?php
                            session()->forget('warning');
                            session()->forget('thankyou');
                            session()->forget('info');
                            session()->forget('success');
                            session()->forget('error');

						?>