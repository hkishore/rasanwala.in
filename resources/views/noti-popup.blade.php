<style>
	#notificationModel .modal-dialog {
		position: fixed;
		margin: auto;
		width: 450px;
		height: 400px;
		right: 15px;
		top: 55px;
	}
</style>
<?php
use App\Models\Notification;
$user = Auth::user();
$msg = Notification::where('status', 1);
// if ($user->role == 3) {
//     $msg->where('user_type', 'customer');
// }
// if ($user->msg_id > 0) {
//     $msg->where('id', '>', $user->msg_id);
// }
$notification = $msg->orderBy('id', 'DESC')->limit(4)->get();
// dd($notification);
?>
<div class="container demo">
	<div class="modal fade" id="notificationModel" tabindex="-1" role="dialog" aria-labelledby="notificationModel" data-backdrop="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5>Notifications</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
				@if(count($notification)>0)
					<a href="{{route('allNoti')}}" class="button d-flex justify-content-end">See all</a>
				@else
						<p>No Notification</p>
					@endif
					@foreach($notification as $notify)
					
					<div class="card">
						<div class="card-body">
							{{$notify->notification}}
						</div>
					</div>
					&nbsp;
					@endforeach
					
				</div>
			</div>
		</div>
	</div>
</div>