@component('mail::message')
<div align="center" style="white-space: pre-wrap;">Hi! Your Product quantity is low.</div><br>

<table style="width:100%;text-align:left;padding-left:30px;">
<tr>
<th style="text-align:left">Product * Qty</th>
<th style="text-align:left">{{$orderLine->product->name}} * {{$orderLine->qty}}</th>
</tr>
</table>
@endcomponent