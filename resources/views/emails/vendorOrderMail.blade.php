@component('mail::message')
<div align="center" style="white-space: pre-wrap;">Congratulations! Your Product has Been sold out.</div><br>

<table style="width:100%;text-align:left;padding-left:30px;">
<tr>
<th style="text-align:left">Product * Qty</th>
<th style="text-align:left">{{$orderLine->product->name}} * {{$orderLine->qty}}</th>
</tr>
<tr>
<th style="text-align:left">Description of Services</th>
<th style="text-align:left">Amount</th>
</tr>

<tr>
<th style="width:50%;text-align:left">Amount</th>
<th style="width:50%;padding-right:20px;text-align:left">₹{{number_format($orderLine->amount,2, '.', '')}}</th>
</tr>

<tr>
<td style="width:50%">CGST:</td>
<td style="width:50%;padding-right:40px">₹{{number_format($orderLine->cess,2, '.', '')}}</td>
</tr>

<tr>
<td style="width:50%">SGST</td>
<td style="width:50%;padding-right:20px">₹{{number_format($orderLine->shipping_charge,2, '.', '')}}</td>
</tr>

<tr>
<td style="width:50%">IGST:</td>
<td style="width:50%;padding-right:20px">₹{{number_format($orderLine->gst,2, '.', '')}}</td>
</tr>

<tr>
<th style="width:50%;padding-bottom:25px;text-align:left">Total Amount:</th>
<th style="width:50%;padding-right:20px;padding-bottom:25px;">₹{{number_format($orderLine->total_amount,2, '.', '')}}</th>
</tr>
</table>
@endcomponent