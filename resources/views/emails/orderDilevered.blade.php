<?php
use App\Models\Order;
$user=Auth::user();
$orderlines=$order->getOrderLines;
?>
@component('mail::message')
<div align="center" style="white-space: pre-wrap;margin-bottom:20px;">Thank You! Your Order has been Placed.</div>
<table style="width: 100%;">
<tr>
<th style="text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">Product:</th>
<th style="text-align:center;padding:10px; border-bottom: 1px solid #EFEFEF;">Quantity:</th>
<th style="text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">Amount:</th>
</tr>
@if(count($orderlines)>0)
@foreach($orderlines as $ol)
<tr>
<td style="width: 50px;%;text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">{{$ol->product->name}}</td>
<th style="width:50%;text-align:center;padding:10px; border-bottom: 1px solid #EFEFEF;">{{$ol->qty}}</th>
<th style="text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">₹{{number_format($ol->amount,2, '.', '')}}</th>
</tr>
@endforeach
@endif
<tr>
<th style="padding-bottom:25px;text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;" COLSPAN="2">Total Amount:<small>(includes <span class="red"></span> CGST, <span class="red"></span> SGST)</small></th>
<th style="text-align:right;padding-bottom:25px;padding:10px; border-bottom: 1px solid #EFEFEF;">₹{{number_format($order->total_amount,2, '.', '')}}</th>
</tr>
</table>
@endcomponent