<?php
use App\Models\Order;
$total=0;
$orderlines=$order->getOrderLines;
$totalamount=number_format($order->order_amount,2, '.', '');
$shippingCharge=0;
if($order->shipping_charge!="" && $order->shipping_charge!=null)
{
$shippingCharge=number_format($order->shipping_charge,2, '.', '');
}
$totalAmount=number_format($totalamount+$shippingCharge,2, '.', '');
//dd($orderlines);
?>
@component('mail::message')
<div align="center" style="white-space: pre-wrap;margin-bottom:20px;">{{$msg}}</div>
<table style="width: 100%;">
<tr>
<th style="text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">Product:</th>
<th style="text-align:center;padding:10px; border-bottom: 1px solid #EFEFEF;">Quantity:</th>
<th style="text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">Amount:</th>
</tr>
@if(count($orderlines)>0)
@foreach($orderlines as $ol)
<?php 
$totalUtem =number_format($ol->amount,2, '.', '');
?>
<tr>
<td style="width: 50px;%;text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">{{$ol->product->name}}</td>
<th style="width:50%;text-align:center;padding:10px; border-bottom: 1px solid #EFEFEF;">{{$ol->qty}}</th>
<th style="text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">₹{{$totalUtem}}</th>
</tr>

@endforeach
@endif
<tr>
<th style="padding-bottom:25px;margin-left: 150px;padding:10px; border-bottom: 1px solid #EFEFEF;" COLSPAN="2">Sub Total:</th>
<th style="text-align:left;padding-bottom:25px;padding:10px; border-bottom: 1px solid #EFEFEF;">₹ {{$totalamount}}</th>
</tr>
<tr>
<th style="padding-bottom:25px;margin-left: 150px;padding:10px; border-bottom: 1px solid #EFEFEF;" COLSPAN="2">Shipping Charge:</th>
<th style="text-align:left;padding-bottom:25px;padding:10px; border-bottom: 1px solid #EFEFEF;">₹ {{$shippingCharge}}</th>
</tr>
<tr>
<th style="padding-bottom:25px;margin-left: 150px;padding:10px; border-bottom: 1px solid #EFEFEF;" COLSPAN="2">Total Amount:</th>
<th style="text-align:left;padding-bottom:25px;padding:10px; border-bottom: 1px solid #EFEFEF;">₹ {{$totalAmount}}</th>
</tr>
</table>
@endcomponent