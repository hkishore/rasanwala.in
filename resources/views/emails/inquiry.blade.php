@component('mail::message')
<?php
use App\Models\Product;
$product=Product::find($inquiry->product_id);
$product_name="";
if(!empty($product)){
	$product_name=$product->name;
}
?>
<div align="center" style="white-space: pre-wrap;">Hi i am {{$inquiry->name}} </div><br>
<div align="left" style="white-space: pre-wrap;">{{$inquiry->question}} </div><br>
<div align="left" style="white-space: pre-wrap;">Regarding Product: {{$product->name}} </div><br>
<div align="center" style="white-space: pre-wrap;">You Can Contact me through. </div><br>
<table style="width:100%;text-align:left;padding-left:30px;">
<tr>
<td>Email:</td>
<td>{{$inquiry->email}}</td>
</tr>

<tr>
<td>Phone No.:</td>
<td>{{$inquiry->phone}}</td>
</tr>
</table>
@endcomponent