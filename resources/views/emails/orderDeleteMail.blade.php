<?php
use App\Models\Order;
$user=Auth::user();
$total=0;
?>
@component('mail::message')
<div align="center" style="white-space: pre-wrap;margin-bottom:20px;">{{$msg}}</div>
<table style="width: 100%;">
<tr>
<th style="text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">Product:</th>
<th style="text-align:center;padding:10px; border-bottom: 1px solid #EFEFEF;">Quantity:</th>
<th style="text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">Amount:</th>
</tr>
<?php 
$totalUtem =number_format($orderlines->total_amount,2, '.', ''); 
$total=$total+$totalUtem;
?>
<tr>
<td style="width: 50px;%;text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">{{$orderlines->product->name}}</td>
<th style="width:50%;text-align:center;padding:10px; border-bottom: 1px solid #EFEFEF;">{{$orderlines->qty}}</th>
<th style="text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;">₹{{$totalUtem}}</th>
</tr>
<tr>
<th style="padding-bottom:25px;text-align:left;padding:10px; border-bottom: 1px solid #EFEFEF;" COLSPAN="2">Total Amount:<small>(includes <span class="red"></span> CGST, <span class="red"></span> SGST)</small></th>
<th style="text-align:right;padding-bottom:25px;padding:10px; border-bottom: 1px solid #EFEFEF;">₹{{$total}}</th>
</tr>
</table>
@endcomponent