<?php
use App\Models\Order;
$orderlines=$order->getOrderLines;
$msg="Thank You! Your Order has been Placed.";
if($order->tracking_id!="" && $order->tracking_id!=null){
	$msg="Congratulations! Tracking has been created of your order.";
}
?>
@component('mail::message')
<div align="center" style="white-space: pre-wrap;margin-bottom:20px;">{!!$msg!!}</div><br>

<table style="width:100%;text-align:left;padding-left:30px;">
<tr>
<th style="text-align:left">Product * Qty:</th>
<th style="text-align:left">Amount:</th>
<th style="text-align:left">Vendor Name:</th>
<th style="text-align:left">Vendor Details:</th>
</tr>
@if(count($orderlines)>0)
	@foreach($orderlines as $ol) 
		<tr>
		<td style="width:50%;text-align:left">{{$ol->product->name}} * {{$ol->qty}}</td>
		<td style="width:50%;padding-right:20px;text-align:left">₹{{number_format($ol->amount,2, '.', '')}}</td>
		<td style="width:50%;padding-right:20px;text-align:left">{{$ol->seller->vendor_name}}</td>
		<td style="width:50%;padding-right:20px;text-align:left">{{$ol->seller->vendor_name}}</td>
		</tr>
	@endforeach
@endif
</table>

<table style="width:100%;text-align:left;padding-left:30px;">
@if($order->tracking_id!="" && $order->tracking_id!=null)
<tr>
<th style="width:50%;text-align:left">Tracking ID</th>
<th style="width:50%;padding-right:20px;text-align:left">{{$order->tracking_id}}</th>
</tr>
@endif
<tr>
<th style="text-align:left">Description of Services</th>
<th style="text-align:left">Amount</th>
</tr>

<tr>
<th style="width:50%;text-align:left">Amount</th>
<th style="width:50%;padding-right:20px;text-align:left">₹{{number_format($order->amount,2, '.', '')}}</th>
</tr>

<tr>
<td style="width:50%">CGST:</td>
<td style="width:50%;padding-right:40px">₹{{number_format($order->cess,2, '.', '')}}</td>
</tr>

<tr>
<td style="width:50%">SGST</td>
<td style="width:50%;padding-right:20px">₹{{number_format($order->shipping_charge,2, '.', '')}}</td>
</tr>

<tr>
<td style="width:50%">IGST:</td>
<td style="width:50%;padding-right:20px">₹{{number_format($order->gst,2, '.', '')}}</td>
</tr>

<tr>
<th style="width:50%;padding-bottom:25px;text-align:left">Total Amount:</th>
<th style="width:50%;padding-right:20px;padding-bottom:25px;">₹{{number_format($order->total_amount,2, '.', '')}}</th>
</tr>
</table>
@endcomponent