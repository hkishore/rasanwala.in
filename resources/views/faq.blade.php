<?php
use App\Models\Common;
$meta=Common::getMetaInfo('homepage');
?>
@extends('layouts.front',$meta)

@section('content')

		<div class="container mb-5">
			<div class="pb-5"style="text-align:center;">
            <h2>FAQ</h2>
            <p>Frequently Asked Questions</p>
          </div>
		  
		@if(count($model)>0)
			<div class="card mt-3">
			<div class="card-body">
	    @foreach($model as $key=>$faq)
			 
			
			
<h4>
<a data-bs-toggle="collapse" href="#collapse{{$key}}"  aria-expanded="false" aria-controls="collapseExample"style="color:black">{!!$faq->question!!}</a>
</h4>
<div class="collapse" id="collapse{{$key}}">
<div class="innerWrap"> 
<p class="card-text" style="white-space: pre-wrap;">
{!!$faq->answer!!}
</p>
  </div>
  </div>
		<hr>	
				
		@endforeach
		</div>
			</div>
		@endif
		</div>
  @endsection
  