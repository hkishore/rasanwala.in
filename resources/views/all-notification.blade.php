<?php

use App\Models\Notification;

$notification = Notification::where('user_type', 'all_user')->where('status', 1)->get();

use App\Models\Common;

$meta = Common::getMetaInfo('homepage');
?>
@extends('layouts.front',['name'=>'All Notifications',$meta])
@section('content')
<div class="container">
	&nbsp;
	<h2>All Notifications</h2>
	<div class="greyBx">
		<div class="innerWrap">

			@if(count($notification)>0)
			<div class="row">
				@foreach($notification as $key=>$noti)
				<div class="col-md-12 col-lg-7">
					<div class="card ">
						<div class="card-body">
							<p class="card-text">{!!$noti->notification!!}</p>
						</div>
					</div>
					&nbsp;
				</div>
				@endforeach
			</div>
			@endif

		</div>
</div>
&nbsp;
</div>

@endsection