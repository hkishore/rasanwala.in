<?php
use App\Models\Common;
$meta=Common::getMetaInfo('homepage');
?>
@extends('layouts.front',$meta)	
@section('content')	
<style>
.btn-outline-success {
    color: #f3640c;
    border-color: #f3640c;
}
.btn-outline-success:hover {
    color: #fff;
    border-color: #f3640c;
	background-color:#f3640c;
}
</style>
		
		<div class="content-part" style="align-content:center">
			
		<div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
              <div class="loginMain" style="max-width:500px;margin-left: auto; margin-right: auto;">
                        <img src="{{asset('img/logo.jpg')}}" alt="logo"/ style="height:100px;  display: block; margin-left: auto; margin-right: auto;">
                <div class="loginContainer">
                  
<x-jet-validation-errors class="mb-4" />
<div class="mb-4">
			<div class="font-large"style="color:red;" id="errorId"></div>
            </div>
                  <div class="cntfm">
                      <form>
                          <div class="row">
                              
                              <div class="clearfix">
                                  <div class="form-group col-md-12 ">
									<label for="" style="padding:5px">{{ __('E-Mail Address') }}</label>
									<input id="phone" type="email" class="form-control @error('email') is-invalid @enderror" name="phone" :value="old('phone')" placeholder="Phone or Email" required autofocus/>

									<span class="help-block"><small style="color:red;" id="ph"></small></span>
                                  </div>
                              </div>
                              <div class="clearfix">    
                                  <div class="form-group col-md-12">
                                      <label for="" style="padding:5px">{{ __('Password') }}</label>
                                     <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" type="password" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                  </div>
                              </div>
							  
                                                         

                          </div>     
						<div class="row"style="padding:5px">	
						<div class="col-md-6 ">
								<div class="checkbox col-md-12"  style="padding:5px">
                                <label> <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>Remember me</label>
                              </div> 
							      </div> 
						<div class="col-md-6 " style="text-align:right">							  
                          <button type="button" onclick="submitForm()" class="btn btn-outline-success">Login</button>
						     </div>
						</div>
                        </form>

                        <div class="clearfix">  
                            <a href="{{ route('register') }}">Register</a>@if (Route::has('password.request'))
                                     | <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                        </div>
                  </div>
                </div>              
              </div>
              </div>
          </div>
@endsection	
		@push('scripts')
	<script type="text/javascript">
	function submitForm()
	{
		var phone=$('#phone').val();
		var password=$('#password').val();
		var email="";
		$('.help-block').hide();
		var isErroFound=false;
		   
				if(phone == "" || phone == null)
				{
				$("#ph").html("Phone is required");
				$("#ph").parent().show();
				isErroFound=true;	
				}else if(/@/.test(phone))
				{
					email=phone;
					phone="";
				}else{
					if(phone.length>10 ||phone.length<10)
					{
					$("#ph").html("Enter 10 digit of Phone");
					$("#ph").parent().show();
					isErroFound=true;	
					}else{
					var validate=validatePhoneNumber(phone);
					if(validate!=true)
					{
					$("#ph").html("Please enter valid Phone number");
					$("#ph").parent().show();
					}
					}
					
				}
				if(password == "" || password == null)
				{
				$("#pas").html("Password is required");
				$("#pas").parent().show();	
				 isErroFound=true;			
				}
				if(isErroFound	){
				return;
				}
			$.ajax({
			url:"{{route('custom.login')}}",
			type: "POST",
			dataType: "json",
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data:{
			phone: phone,
			email: email,
			password: password,
			},

			success: function (data) {
				console.log(data.error);
			if(data.error)
			{
				$('#errorId').html(data.error);
			}else{
			var url="{{route('dashboard')}}";
				window.location.href=url;
			}
			}
			});
	}
	function validatePhoneNumber(input_str) {
	var re= /^([6-9][0-9]{9})$/im;

    return re.test(input_str);
}
	</script>
	@endpush
