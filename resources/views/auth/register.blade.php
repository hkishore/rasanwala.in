@extends('layouts.front',['pageTitle'=>'Register','title'=>'Register'])

@section('content')

<style>
.btn-outline-success {
    color: #f3640c;
    border-color: #f3640c;
}
.btn-outline-success:hover {
    color: #fff;
    border-color: #f3640c;
	background-color:#f3640c;
}
</style>
		
		<div class="content-part">
			
		<div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
	
              <div class="loginMain" style="max-width:500px;margin-left: auto; margin-right: auto;">
                        <img src="{{asset('img/logo.jpg')}}" alt="logo"/ style="height:100px;  display: block; margin-left: auto; margin-right: auto;">
                <div class="loginContainer">
                  <x-jet-validation-errors  class="mb-4" />

                  <div class="cntfm">
				  <div><small style="color:red;" id="errorId" ></small></div>
                      <form>
                          <div class="row">
						   <div class="clearfix">
                                  <div class="form-group col-md-12 ">
									<label for="" style="padding:5px">{{ __('Name') }}</label>
									<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="phone" :value="old('name')" placeholder="Name" required autofocus/>
									<span class="help-block"><small style="color:red;" id="nameF"></small></span>
                                  </div>
                              </div>
                              <div class="clearfix">
                                  <div class="form-group col-md-12 ">
									<label for="" style="padding:5px">{{ __('Phone') }}</label>
									<input id="phone" type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" :value="old('phone')" placeholder="Phone" required autofocus/>
									<span class="help-block"><small style="color:red;" id="ph"></small></span>
                                  </div>
                              </div>
                              <div class="clearfix">
                                  <div class="form-group col-md-12 ">
									<label for="" style="padding:5px">{{ __('E-Mail Address') }} (optional)</label>
									<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" :value="old('email')" placeholder="Email"  autofocus/>
								<span class="help-block"><small style="color:red;" id="em"></small></span>
                                  </div>
                              </div>
                              <div class="clearfix">    
                                  <div class="form-group col-md-12">
                                      <label for="" style="padding:5px">{{ __('Password') }}</label>
                                     <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" type="password" name="password" required autocomplete="current-password">
									<span class="help-block"><small style="color:red;" id="pas"></small></span>
                                  </div>
                              </div>
							  
							  <div class="clearfix">    
                                  <div class="form-group col-md-12">
                                      <label for="" style="padding:5px">{{ __('Confirm-Password') }}</label>
                                     <input id="password_confirmation" type="password" placeholder="Confirm-Password" class="form-control @error('password') is-invalid @enderror" type="password" name="password_confirmation" required autocomplete="new-password" >
										<span class="help-block"><small style="color:red;" id="repas"></small></span>
                                  </div>
                              </div>
							  
							  
                                                         

                          </div>     
						<div class="row"style="padding:5px">	
						<div class="col-md-6 ">
								<div class="checkbox col-md-12"  style="padding:5px">
                                <a class="btn btn-link" href="{{ route('custom.login') }}">
                                       {{ __('Already registered?') }}
                                    </a>
                              </div> 
							      </div> 
						<div class="col-md-6 " style="text-align:right">							  
                          <button type="button" onclick="submitForm()" class="btn btn-outline-success">Register</button>
						     </div>
						</div>
                        </form>

                        <div class="clearfix">  
                            
                        </div>
                  </div>
                </div>              
              </div>
          </div>
          </div>







  
@endsection

	@push('scripts')
	<script type="text/javascript">
	function submitForm()
	{
		var name=$('#name').val();
		var phone=$('#phone').val();
		var email=$('#email').val();
		var password=$('#password').val();
		var repas=$('#password_confirmation').val();
		$('.help-block').hide();
		var isErroFound=false;
		      if(name == "" || name == null)
				{
				$("#nameF").html("Name is required");
				$("#nameF").parent().show();	
				 isErroFound=true;			
				}
				if(phone == "" || phone == null)
				{
				$("#ph").html("Phone is required");
				$("#ph").parent().show();
				isErroFound=true;	
				}
				if(password == "" || password == null)
				{
				$("#pas").html("Password is required");
				$("#pas").parent().show();	
				 isErroFound=true;			
				}
				if(repas == "" || repas == null)
				{
				$("#repas").html("Confirm Password is required");
				$("#repas").parent().show();
				isErroFound=true;	
				}
				if(repas!=password)
				{
				$("#repas").html("Confirm Password dose not match");
				$("#repas").parent().show();
				isErroFound=true;	
				}
				if(isErroFound	){
				return;
				}
			$.ajax({
			url:"{{route('user.register')}}",
			type: "POST",
			dataType: "json",
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data:{
			email: email,
			name: name,
			phone: phone,
			password: password,
			},

			success: function (data) {
			if(data.error)
			{
				$('#errorId').html(data.error);
			}else{
			var url="{{route('dashboard')}}";
				window.location.href=url;
			}
			}
			});
	}
	</script>
	@endpush
