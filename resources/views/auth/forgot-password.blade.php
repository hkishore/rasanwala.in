@extends('layouts.front',['pageTitle'=>'Register','title'=>'Register'])

@section('content')
<style>
.btn-outline-success {
    color: #f3640c;
    border-color: #f3640c;
}
.btn-outline-success:hover {
    color: #fff;
    border-color: #f3640c;
	background-color:#f3640c;
}
</style>
		
		<div class="content-part">
			
		<div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg" >
              <div class="loginMain" style="max-width:500px;margin-left: auto; margin-right: auto;">
                        <img src="{{asset('img/logo.jpg')}}" alt="logo"/ style="height:100px;  display: block; margin-left: auto; margin-right: auto;">
                <div class="loginContainer">
                  
		<div class="mb-4 text-sm text-gray-600">
            {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
        </div>
        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif
		<x-jet-validation-errors class="mb-4"  style="color:red"/>
                  <div class="cntfm">
                      <form method="POST" action="{{ route('password.email') }}">
            @csrf
                          <div class="row">
                              
                              <div class="clearfix">
                                  <div class="form-group col-md-12 ">
									<label for="" style="padding:5px">{{ __('E-Mail Address') }}</label>
									<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" :value="old('phone')" placeholder="Phone or Email" required autofocus/>

									<span class="help-block"><small style="color:red;" id="ph"></small></span>
                                  </div>
                              </div>                                                        

                          </div>     
						<div class="row"style="padding:5px">	
						<div class="col-md-12 " style="text-align:center;padding:5px">							  
                          <button type="submit" class="btn btn-outline-success"> Email Password Reset Link</button>
						     </div>
						</div>
                        </form>
                  </div>
                </div>              
              </div>
              </div>
          </div>
@endsection