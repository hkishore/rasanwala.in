<?php
use App\Models\Common;
$meta=Common::getMetaInfo('homepage');
?>
@extends('layouts.front',$meta)
@section('content') 	
<?php
//dd($products);
use App\Models\Product;
use App\Models\Category;
use App\Models\Variant;
use App\Models\Brand;
use App\Models\Order;
use App\Models\FrontImage;
use App\Models\OrderLine;
use App\Models\MultipleCategory;
use Carbon\Carbon;

    $date = Carbon::now()->subDays(7);
	$brands=Brand::whereNotNull('image')->inRandomOrder()->limit(5)->get();
   $products=Product::where('status',1)->orderBy('updated_at', 'desc')->limit(5)->get();
   $BestSellProduct=Product::where('status',1)->whereNotNull('image')->inRandomOrder()->limit(5)->get();
  // dd($BestSellProduct);
   $scategoris=Category::whereNotNull('parent_id')->whereNotNull('image')->orderBy('updated_at', 'desc')->take(10)->get();
  
   $slider1=FrontImage::where('slider_type','section_first')->where('status',1)->orderBy('updated_at','desc')->limit(5)->get();
   $slider3=FrontImage::where('slider_type','section_thread')->where('status',1)->orderBy('updated_at','desc')->take(2)->get();
   //$slider2=FrontImage::where('slider_type','section_second')->where('status',1)->orderBy('updated_at','desc')->get();
   $slider4=FrontImage::where('slider_type','section_fourth')->where('status',1)->orderBy('updated_at','desc')->first();
   
  //dd($slider2);
 ?>
 <!-- section first -->
        <div class="homeBannerSec ">
          <div class="homBannerWrap">
		  @foreach($slider1 as $slider)
            <div class="item">
              <img src="{{$slider->image}}" style="width:100%;height:500px"alt="" />
            </div>
           @endforeach
          </div>
</div>
<!--end section first-->

		@if(count($BestSellProduct)>0)
		@include('front.product-page',['products'=>$BestSellProduct,'title'=>'BEST PRODUCTS','class'=>'homeBestSeller'])

		@endif
		@if(!empty($slider4))
			@if($slider4->image !="" || $slider4->image != null)
        <div class="partner section mb-0">
          <img src="{{$slider4->image}}" style="height:100px" alt="" />
          <div class="partnerContent">
            <div class="container">
             <!-- <div class="innerPtnr">
                <h4>Become a </h4>
                <h2>Bartender Pro</h2>
                <a class="btn brandBtn" href="">
                  <i aria-hidden="true" class="fa fa-star"></i>
                  Join Now
                </a>
              </div>-->
            </div>
          </div>
        </div>
		 @endif
		@endif
		@if(count($scategoris)>0)
			
        <div class="categorySec section mb-0" style="margin-top:30px;">
          <div class="container">
		   <div class="sectionTtl">
              <h2>CATEGORIES</h2>
            </div>
            <div class="catSecInr ">
         @foreach($scategoris as $key=>$subcategory)
		 <?php
		 $image =$subcategory->image;
		 ?>
		 @if($subcategory->image !="" && $subcategory->image != null)
              <div class="item">
				<a href="{{route('catCat',$subcategory->slug)}}">
                <div class="iteminr">                  
                    <img src="{{$image}}" alt=""  / style="width: 100%;height: 130px">                  
				  </div>
				  <h5 style="padding-top:10px;text-align:center;color:black">{!!$subcategory->name!!}</h5>
				  </a>
              </div>
		@endif	  
		@endforeach
            </div>
          </div>
        </div>
		
		@endif
		@if(count($slider3)>0)
        <div class="offerSec section">
          <div class="container">
            <div class="row">
			@foreach($slider3 as $sli)
			@if($sli->image != "" || $sli->image != null)
				<?php
			$url="#";
			if($sli->url !="")
			{
			 $url=$sli->url;
			}
			?>
              <div class="col-md-6">
                <a href="{{$url}}">
                  <img src="{{$sli->image}}" alt="" />
                </a>
              </div>
			 @endif
			@endforeach
            </div>
          </div>
        </div>
        @endif
<div style="padding-top:40px">
      @include('front.product-page',['products'=>$products,'title'=>'NEWLY ADDED PRODUCTS','class'=>'nwlyAdded'])
	  </div>

       <!-- <div class="clentSec section ">
		<div class="container">
		   <div class="sectionTtl">
              <h2>Brands</h2>
            </div>
          <div class="clientInr">
		  @foreach($brands as $brand)
		  @if($brand->image != "" || $brand->image != null)
            <div class="item">
		       <div class="iteminr">                  
              <img src="{{$brand->image}}" style="height: 100px;width: 100px;"alt="" />
            </div>
			<h5 style="padding-top:10px;text-align:center;color:black">{!!$brand->name!!}</h5>			
            </div>
			@endif
			@endforeach
          </div>
        </div>
		</div>-->

        <div class="categorySec section mb-0" style="margin-top:30px;">
          <div class="container">
		   <div class="sectionTtl">
              <h2>Brands</h2>
            </div>
            <div class="catSecInr ">
         @foreach($brands as $key=>$brand)
		 <?php
		 $image =$subcategory->image;
		 ?>
		 @if($brand->image !="" && $brand->image != null)
              <div class="item">
				<a href="{{route('brand.product',$brand->slug)}}">
                <div class="iteminr">                  
                    <img src="{{$brand->image}}" alt=""  / style="width: 100%;height: 130px">                  
				  </div>
				  <h5 style="padding-top:10px;text-align:center;color:black">{!!$brand->name!!}</h5>
				  </a>
              </div>
		@endif	  
		@endforeach
            </div>
          </div>
        </div>
		
        <div class="achieveSec section">
          <div class="container-fluid">
            <div class="achivBox">

              <div class="item">
                <div class="icon">
                  <i class="fa fa-trophy" aria-hidden="true"></i>
                </div>
                <div class="cont">
                  <h5>Quality Products</h5>
                  <p>Highest quality guarantee</p>
                </div>
              </div>
              <div class="item">
                <div class="icon">
                  <i class="fa fa-ship" aria-hidden="true"></i>
                </div>
                <div class="cont">
                  <h5>1000+ Products </h5>
                  <p>From 100+ brands</p>
                </div>
              </div>
              <div class="item">
                <div class="icon">
                  <i aria-hidden="true" class="fa fa-credit-card"></i>
                </div>
                <div class="cont">
                  <h5>Secure Payment</h5>
                  <p>100% Secure payment</p>
                </div>
              </div>
              <div class="item">
                <div class="icon">
                  <i aria-hidden="true" class="fa fa-phone"></i>
                </div>
                <div class="cont">
                  <h5>24/7 Support</h5>
                  <p>Dedicated support</p>
                </div>
              </div>
              <div class="item">
                <div class="icon">
                  <i aria-hidden="true" class="fa fa-gift"></i>
                </div>
                <div class="cont">
                  <h5>Gift Packs</h5>
                  <p>Customisable gift box with messeges & tags</p>
                </div>
              </div>

            </div>
          </div>
        </div>
@endsection	
@push('scripts')
<script type="text/javascript">

 </script>
@endpush
