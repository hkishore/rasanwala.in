<?php
$url=$_SERVER['REQUEST_URI'];
$uri_parts = explode('/', $url);
$slug = end($uri_parts);
	use App\Models\Common;
	$meta=Common::getMetaInfo($slug);

use App\Models\Category;
// use App\Models\Brand;
// use App\Models\Product;
 use App\Models\Variant;

// $category=Category::where('parent_id',null)->get();
// $productObj = Product::where('status',1);
// $catProduct=Category::where('parent_id','!=',null)->pluck('id')->toArray();
// $productObj->whereIn('categeory_id',$catProduct);
// $pb=$productObj->groupBy('brand_id')->pluck('brand_id')->toArray();
// $brands=Brand::whereIn('id',$pb)->get();
$counter=0;
$min_price=0;
$max_price=0;
$variantPrice=Variant::all();
	if(count($products) > 0){
		$min_price=$variantPrice->min('sale_price');
		$max_price=$variantPrice->max('sale_price');
		
	}
?>
@extends('layouts.front',$meta)	
@section('content') 
 <div class="container">
<div class="row">
    <div class="col-12 col-md-3 filterBar">
        <div class="filterInner" >
		
		   <div class="sideSec categorySideNav">
            <h4>PRODUCT CATEGORIES</h4>
            <div class="verticalNv scrolls">
              <ul class="nav flex-column navbaru ">
               @foreach($category as $key =>$cat) 
			    <?php
			      $sbcat=Category::where('parent_id',$cat->id)->where('status',1)->get();
				 ?>
			   @if(count($sbcat)==0)
				 <!-- <li class="nav-item">
                  <a class="nav-link" href="{{route('catCat',$cat->slug)}}">{{$cat->name}}</a> 
                </li>-->
				@else


<li class="nav-item dropdown">
                  <a class="nav-link" href="{{route('catCat',$cat->slug)}}" id="" data-bs-toggle="dropdown" aria-expanded="false">{{$cat->name}} <i class="icon-chevron-down"></i></a>
                          
                  <ul class="dropdown-menu" aria-labelledby="">
				  @foreach($sbcat as $key=>$subcategory)
                    <li><a class="dropdown-item" href="{{route('catCat',$subcategory->slug)}}" style="white-space: normal;"><span class="right-arrow"></span>&nbsp;&nbsp;{{$subcategory->name}}</a></li>
					@endforeach
                    
                  </ul>
                </li>
                <!--<li class="nav-item dropdown" >
                  <a class="nav-link" href="{{route('catCat',$cat->slug)}}" id="" >{{$cat->name}}<i data-bs-toggle="dropdown" class="icon-chevron-down" aria-expanded="false"></i></a> 
                  <ul class="dropdown-menu" aria-labelledby="">
                   @foreach($sbcat as $key=>$subcategory)
				   <li><a class="dropdown-item" href="{{route('catCat',$subcategory->slug)}}">{{$subcategory->name}}</a></li>
				  
					@endforeach
                  </ul>
                </li>-->
			@endif
               @endforeach
              </ul>
            </div>
          </div>
          </div>		  
		  &nbsp
		  @if(count($brands)>0)
			  <div class="filterInner">	
			<form class="themeForm" action="{{route('shop')}}" method="get" id="categeoryProductFilteration">
			<div class="sideSec brandSec mb-c-30">
				<h4>BRANDS</h4>
				<!--<div class="brandSearch">
				  <input type="text" class="mf-input-search-nav" onkeyup="filterSeachText()" name="searchText"id="searchText"/>
				</div>-->
				<div class="brandSearch">
              <input type="text" id="myInputTextField" placeholder="Search">
            </div>
				
				<table id="listtable1" style=" border-collapse: separate;" class="table">
			 <thead>
                <tr>
				<th></th>
                </tr>
            </thead>
            <tbody>
               <tr>
			  <td>
				<div class="multiCheck" style ="max-height:350px;overflow-y:auto;white-space:nowrap;">
				
				  <ul>
				  
				  @foreach($brands as $key=>$brand)
				   <?php
					   $checked=[];
					   if(isset($_GET['brandcheck']))
					   {
						   $checked=$_GET['brandcheck'];
					   }
					   ?>
					<li>
					  <div class="form-check">
						<input class="form-check-input checkboxIn" type="checkbox" name="brandcheck[]"value="{{$brand->id}}" id="" @if(in_array($brand->id,$checked)) checked @endif />
					
						<label class="form-check-label" for="">
						  {{$brand->name}}
						</label>	 
					  </div>
					</li>
					
					@endforeach
					
				  </ul>
				</div>
				</td>
               </tr>
			 </tbody>
            </table>
				 <button type="submit" class="btn btn-outline-success">Apply Filter</button>
			  </div>
			  </form>
			</div>
@endif
		  </div>
		  <div class="col-12 col-md-9 contentBar">
	  @include('front.shop')
 </div>
 </div>
 </div>
@endsection
@push('scripts')
<script type="text/javascript">
var min_price="{{$min_price}}";
var max_price="{{$max_price}}";
$(document).ready(function(){	
var Table=$('#listtable1').DataTable({
		responsive: false,
		processing: false,
		searching: true,
		serverSide: false,
		stateSave: false,
		bLengthChange: false,
		bInfo: false,
		ordering: false,
		bPaginate: false,
		bFilter: false,
	});
		$('#myInputTextField').keyup(function(){
		Table.search($(this).val()).draw() ;
});

 var checkedVals = $('.checkboxIn:checkbox').map(function() {
	 console.log(21342);
    return this.value;
	}).get();
	
	$(".priceFilter").slider({
        min: 0,
        max: max_price,
        step: 1,
        values: [min_price, max_price],
        slide: function(event, ui) {
            for (var i = 0; i < ui.values.length; ++i) {
                $("input.sliderValue[data-index=" + i + "]").val(ui.values[i]);
            }
        }
    });
    
    $(".sliderValue").change(function() {
        var $this = $(this);
        $(".priceFilter").slider("values", $this.data("index"), $this.val());
    });
});

 </script>
@endpush