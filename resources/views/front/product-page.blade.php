<?php

use App\Models\Variant;
use App\Models\Brand;
use App\Models\Common;
use App\Models\Order;
use App\Models\FavoriteItem;
use App\Models\OrderLine;

$order = Order::getUserOrders(true);
$orderlines = OrderLine::where('order_id', $order->id)->get()->keyBy('variant_id');
$maxQuantity = config('custom.maxQuantity');
// dd($maxQuantity);
?>
<div class="bestSeller section">
	<div class="container">
		<div class="sectionTtl">
			<h2>{{$title}}</h2>
		</div>

		<div class="productItemWrap">
			<div class="{{$class}}">
				@foreach($products as $product)
				<?php
				// dd($product->variant);
				$image = $product->image;

				// $variantObj= Common::variantGet();
				$variant = $product->variant;

				//$variantId=$variantObj->where('product_id',$product->id)->first();
				$url = "#";
				$brandName = "";
				if ($product->brand_id != "" && $product->brand_id != null) {
					$brands = Brand::where('id', $product->brand_id)->first();
					if (!empty($brands)) {
						$url = route('brand.product', $brands->slug);
						$brandName = $brands->name;
					}
				}
				$vId = 0;
				if (count($variant) > 0) {

					$vId = $variant[0]->id;
				}

				?>
				@if(!empty($variant) && count($variant)>0)
				<div class="item col-12 col-md-4 col-lg-3">
					<div class="itemInnter">
						<div class="mf-product-thumbnail">
							<a href="{{route('detail.product',$product->slug)}}" class="thumbNail">
								@if($product->image!="" && $product->image!=null)
								<img src="{{$image}}" alt="" height="200">
								@else
								<img src="/img/no-pictures.png" alt="" height="200">
								@endif
								<span class="ribbons_{{$product->id}}" id="ribbons_{{$product->id}}"></span>
							</a>
							<div class="footer-button">
								<!--<div class="viewIcoWrap">
                                
                                <a class="viewIco" href="{{route('detail.product',$product->slug)}}" data-lightbox="2-1-2-300x338.jpg" data-title="">
                                  <i class="p-icon icon-eye" title="Quick View"></i>
                                </a>
                              </div>-->
								<div class="wishWrap">
									@if(Auth::check())
									<?php
									$auth = Auth::user();
									$favzproduct = FavoriteItem::where('user_id', $auth->id)->where('product_id', $product->id)->first();
									$class = "";
									$title = "Add to wishlist";
									if (!empty($favzproduct)) {
										$class = "background-color: #28afb1;";
										$title = "Remove to wishlist";
									}
									?>
									<a style="{{$class}}" href="javascript:void(0);" id="fav_{{$product->id}}" title="{{$title}}" onclick="addWishList('{{$product->id}}')">
										<i class="yith-wcwl-icon fa fa-heart-o"></i>
									</a>

									@else

									<a href="{{route('custom.login')}}" title="Add to wishlist">
										<i class="yith-wcwl-icon fa fa-heart-o"></i>
									</a>

									@endif
								</div>
							</div>
						</div>
						<div class="mf-product-details">
							<div class="product-content">
								<div class="sold-by-meta">

								</div>
								<h2><a href="{{route('detail.product',$product->slug)}}">{{$product->name}}</a></h2>
							</div>

							<div class="mf-product-price-box">
								<!--select box -->
								<select name="varian_name" class="form-control varients variant_{{$product->id}}" id="variant_{{$product->id}}" />
								@foreach($variant as $key=>$vr)
								<?php
								$qty = 1;
								$olqty = 0;
								if (isset($orderlines[$vr->id])) {
									$qty = $orderlines[$vr->id]->qty;
									$olqty = $orderlines[$vr->id]->qty;
								}
								$order_limit = $vr->order_limit;

								$per = $product->getPercentage($vr);
								$vari = json_encode($vr->only(['sale_price', 'regular_price', 'size']));
								?>
								<option value="{{$vr->id}}" data-qty="{{$qty}}" data-productid="{{$product->id}}" data-variant="{{$vari}}" data-olqty="{{$olqty}}" data-charge="{{$vr->charge}}" data-orderlimit="{{$order_limit}}" data-per="{{$per}}">{{$vr->size}} @if($vr->regular_price>$vr->sale_price)₹ {{$vr->sale_price}}@else ₹ {{$vr->regular_price}}@endif</option>
								@endforeach
								</select>
								<div class="product-content">
									<span class="price">
										<del>
											<span class="woocommerce-Price-amount amount">
												<bdi>
													<span class="woocommerce-Price-currencySymbol offPrice{{$product->id}}" id=""></span>
												</bdi>
											</span>
										</del>
										<ins>
											<span class="woocommerce-Price-amount amount">
												<bdi>
													<span class="woocommerce-Price-currencySymbol salePrice{{$product->id}}" id=""></span>
												</bdi>
											</span>
										</ins>
									</span>
								</div>
								<div class="product-content itemfooter">
									<div class="qty-box">
										<input type="button" onclick="decrementValue({{$product->id}})" value="-" />
										<input type="text" name="quantity" value="" class="qtyValue_{{$product->id}}" maxlength="2" max="10" size="1" id="qtyValue_{{$product->id}}" />
										<input type="button" onclick="incrementValue({{$product->id}})" value="+" />
									</div>
									<!--<span><input type="number" id="qtyValue_{{$product->id}}" maxlength="2" min="1" max="10" class="form-control qtyfield" name="qty" value="" /><span>-->
									<span><button type="button" class="add-to-cart-text" onclick="addcard({{$product->id}})">Add to Cart</button></span>
								</div>

							</div>

						</div>
					</div>
				</div>
				@endif
				@endforeach
			</div>
		</div>
	</div>
</div>
@push('scripts')
<script type="text/javascript">
	var variantId;
	var qty;
	var setValue;
	var maxQuantity = "{{$maxQuantity}}";
	$(document).ready(function() {
		$('#filterProduct').on('change', function() {
			var vl = (this.value);
			var featured = $('#featured').val(vl);
			$('#formSubmit').submit();
		});
		$('.varients').trigger("change");
	});
	$('.varients').on('change', function() {

		var qty1 = $(this).parent().find(':selected').data('qty');
		var pId = $(this).parent().find(':selected').data('productid');
		var variant = $(this).parent().find(':selected').data('variant');
		var per = $(this).parent().find(':selected').data('per');
		if (parseFloat(variant.sale_price).toFixed(2) < parseFloat(variant.regular_price).toFixed(2)) {
			$(this).parent().find('.offPrice' + pId).html('₹' + parseFloat(variant.regular_price).toFixed(2));
		}
		$(this).parent().find('.salePrice' + pId).html('₹' + parseFloat(variant.sale_price).toFixed(2));
		$(this).parent().find('.qtyValue_' + pId).val(qty1);
		if (per > 0 && per != 0) {
			$(this).parent().parent().parent().find('.ribbons_' + pId).show();
			$(this).parent().parent().parent().find('.ribbons_' + pId).html(per + '% off');
			// $('.ribbons_'+pId).html(per+'% off');
		} else {
			$(this).parent().parent().parent().find('.ribbons_' + pId).hide();
			// $('.ribbons_'+pId).hide();
		}
	});

	function incrementValue(productId) {
		//$('.qtyValue_'+productId).val(10);
		var count = $('.qtyValue_' + productId).val();
		var value = parseInt(count);
		value = isNaN(value) ? 0 : value;
		var order_limit = $('.variant_' + productId).find(':selected').data('orderlimit');
		if (order_limit < 1) {
			order_limit = maxQuantity;
		}
		if (value < order_limit) {
			value++;
			$('.qtyValue_' + productId).val(value);;
		} else {
			toastr["info"]("You cannot Order More Than " + order_limit + " quantity");
		}
	}

	function decrementValue(productId) {
		var count = $('.qtyValue_' + productId).val();
		var value = parseInt(count);
		value = isNaN(value) ? 0 : value;
		if (value > 1) {
			value--;
			$('.qtyValue_' + productId).val(value);
		}
	}

	function addcard(id) {
		var variantId = $('.variant_' + id).find(':selected').val();
		var qty1 = $('.variant_' + id).find(':selected').data('qty');
		var qty = parseInt($('.qtyValue_' + id).val());
		var order_limit = $('variant_' + id).find(':selected').data('orderlimit');
		if (order_limit < 1) {
			order_limit = maxQuantity;
		}
		order_limit=parseInt(order_limit);
		
		if (qty > order_limit) {
			toastr["info"]("You cannot Order More Than " + order_limit + " quantity");
			//return;
		}
		if (qty <= order_limit) {
			$.ajax({
				type: 'POST',
				url: "{{route('add.cart')}}",
				contentType: "application/json",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: JSON.stringify({
					p_id: id,
					qty: qty,
					variant_id: variantId,
					type: "update"
				}),
				success: function(data) {
					if(data.status==true)
					{

						$('.variant_' + id).find(':selected').data('qty', qty);
					//$('#qtyValue_'+id).val(qty);

					$('#count_cart_value').html(data.count_cart);
					$('#amount').html(data.amount);
					toastr["success"]("Add to cart " + qty + " quantity");
					}
					else{
						toastr["error"](data.msg);
					}
					
				}
			});
		} else {
			toastr["error"]("max quantity"+order_limit);
			return false;
		}
	}
</script>
@endpush