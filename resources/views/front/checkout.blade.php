<?php

use App\Models\State;
use App\Models\Vender;
use App\Models\Setting;
use App\Models\User;
use App\Models\Common;

use App\Models\ShippingArea;
use App\Models\ShippingCharge;

$meta = Common::getMetaInfo('homepage');
$state = State::pluck('name', 'id');
$user = Auth::user();
$auth=Auth::check();
$cod = "off";
$id = "";
if (Auth::check()) {
	$users = $user->role;
	$id = $user->id;
	$cod = $user->cod;
} else {
	$users = "";
}
$payDetails = Common::getRazorpayApi();
$razorpay_mode = $payDetails['payMode'];
$location = ShippingArea::where('status', 1)->get()->keyBy('id');


$Maintenance = Setting::getFieldVal('app_maintenance');
$WebMaintenance = Setting::getFieldVal('web_maintenance');
$GetenableToday = Setting::getFieldVal('sp_enble_today');
//self pickup
date_default_timezone_set('Asia/Kolkata');
$cHour = date('H');
$getShopTime = Setting::getFieldVal('open_close_time');
$shippingTime = Setting::getFieldVal('shipping_time');
$splitShTime = explode("-", $shippingTime);
$openShTime = $splitShTime[0];
$closeShTime = $splitShTime[1];
$splitTime = explode("-", $getShopTime);
$openeTime = $splitTime[0];
$closeTime = $splitTime[1];
$setArray = [];
$time = $splitTime;
$pickupTime = [];
$shopStatus = "close";
$ShippingStatus = "close";
$mytime = Carbon\Carbon::now();
$time = $mytime->toTimeString();
$sTime = explode(":", $time);
$currentTime = $sTime[0];
if ($currentTime >= $openeTime && $currentTime < $closeTime) {
	$shopStatus = 'open';
}

if ($currentTime >= $openShTime && $currentTime < $closeShTime) {
	$ShippingStatus = 'open';
}
$cashfree_mode = 'disable'; //Common::getCashFreeyApi();
$homedeliveryEnableToday = Setting::getFieldVal('sp_enble_today');
$selfCodPickup = Setting::getFieldVal('self_pickup_cod');
$minOrderAmount = Setting::getFieldVal('mini_order_amt');
$minOrderAmountFreeDelivery = Setting::getFieldVal('mini_order_amt_free_shiping');
// code get all active locations by key
?>
@extends('layouts.front',$meta)
@section('content')
<div class="container">
	<div class="row backTo mt-5 mb-5">
	<div class="text-start col-md-2">
		<a href="{{route('cart')}}" class="btn btnBackTo">
			<i class="icon-arrow-left"></i>
			Back To Cart
		</a>
		</div>
	<div class="pageTitle text-center mb-3 col-md-8">
		<h1 class="entry-title">Checkout</h1>
	</div>
	</div>

	<div class="checkout">
		<form action="{{route('createOrder')}}" method="post" id="orderSubmit" class="themeForm">
			{{ csrf_field() }}
			<div class="row">
				<div class="col-12 col-lg-7">
					<div class="greyHeading">
						<div class="row">
							<div class="col-md-4">
								<input type="radio" id="self_pickup" class="form-check-input deliveryType" name="delivery" value="selfpickup" />
								<label class="form-check-label " for="coDelivery">Self Pickup</label>
							</div>
							@if(($homedeliveryEnableToday=="1" || $homedeliveryEnableToday==1) && ($order->total_amount>=$minOrderAmount))
							<div class="col-md-8">
								<input type="radio" class="form-check-input deliveryType" id="home_delivery" name="delivery" value="homedelivery" />
								<label class="form-check-label" for="coDelivery">Home Delivery <small class="red">( Only for Hisar city )</small></label>
							</div>
							@elseif(($homedeliveryEnableToday=="1" || $homedeliveryEnableToday==1) && ($order->total_amount< $minOrderAmount)) <div class="col-md-8">

								<label> Home Delivery <small class="red">( Order amount should be greater than ₹{{$minOrderAmount}} )</small></label>
						</div>

						@elseif($homedeliveryEnableToday!="1" || $homedeliveryEnableToday!=1)

						<div class="col-md-8">

							<label> Home Delivery <small class="red">( Service is not available today )</small></label>
						</div>


						@endif
					</div>
				</div>
				@if(!Auth::check())
				<div class="greyHeading">
					<i class="fa fa-comment-o" aria-hidden="true"></i> Returning customer? <a href="{{route('custom.login')}}" class="showlogin">Click here to login</a>
				</div>
				@endif
				<div class="secu">
					<div class="row">
						<div class="col-md-6">
							<h3 id="hadding"></h3>
						</div>
						<div class="col-md-6" id="address_button">
							<a onclick="addAddress()" class="btn btnBackTo btn-outline-success pull-right">Add +</a>
						</div>
					</div>
					@if(!Auth::check() || count($user->userAddress)==0)
					<div id="hide_contect">
						<div class="mb-3">
							<div class="row">
								<div class="col-md-6">
									<label for="" class="form-label">Name<span class="red">*</span></label>
									{{ Form::text('first_name',null, ['class' => 'form-control','id'=>'first_name','placeholder'=>'Name','required'=>'required']) }}
									@if ($errors->has('first_name'))
									<span class="help-block red">
										<strong>{{ $errors->first('first_name') }}</strong>
									</span>
									@endif
									<span class="help-block">
										<small class="red" id="fn"></small>
									</span>
								</div>
								<div class="col-md-6">
									<label for="" class="form-label">Phone<span class="red">*</span></label>
									{{ Form::text('phone_no',null, ['class' => 'form-control','id'=>'phone_no','placeholder'=>'Phone No','required'=>'required']) }}
									@if ($errors->has('phone_no'))
									<span class="help-block red">
										<strong>{{ $errors->first('phone_no') }}</strong>
									</span>
									@endif
									<span class="help-block">
										<small class="red" id="ph"></small>
									</span>
								</div>
							</div>
						</div>
						
					</div>
					<div id="hide_add">
						<div class="mb-3">
							<label for="" class="form-label">Street address<span class="red">*</span></label>
							{{ Form::text('address_1',null, ['class' => 'form-control mb-2','id'=>'address_1','placeholder'=>'House number and street name','required'=>'required']) }}
							@if ($errors->has('address_1'))
							<span class="help-block red">
								<strong>{{ $errors->first('address_1') }}</strong>
							</span>
							@endif
							<span class="help-block">
								<small class="red" id="ad"></small>
							</span>
						</div>
						<div class="mb-3 form-label" id="locationShow">
							<label for="" class="form-label">Select Location<span class="red">*</span></label>
							<select id="location" class="form-control addId" name="add_id" onchange="calShipping(this.value)">
								@foreach($location as $key=>$lo)
								<option value="{{$key}}" data-locationid="{{$lo->id}}">{{$lo->area}}</option>
								@endforeach
								<option value="other">Others</option>
							</select>
						</div>
						<div class="mb-3" id="ad_loc">
							<label for="" class="form-label">Add Location</label>
							{{ Form::text('add_location',null, ['class' => 'form-control','id'=>'add_location','placeholder'=>'Add Location']) }}
							@if ($errors->has('add_location'))
							<span class="help-block red">
								<strong>{{ $errors->first('add_location') }}</strong>
							</span>
							@endif
							<span class="help-block">
								<small class="red" id="adl"></small>
							</span>
						</div>
					</div>
					@else
					<div id="add_hide">
						@if(count($user->userAddress)>0)
						@foreach($user->userAddress as $add)
						<div class="row mb-3" style="border: 1px solid #ccc;padding:10px;">
							<div class="col-md-10">
								<div class="row">Name: {{$add->first_name}} </div>
								<div class="row">Address: {{$add->address_1}} </div>
								@if($add->area_id != 0)
								<div class="row">Location: {{$add->location->area}}</div>
								@else
								<div class="row">Location: {{$add->other}} </div>
								@endif
								<div class="row">Phone: {{$add->phone_no}}</div>
							</div>
							<div class="col-md-2" style="padding: 5px;">
								<div class="d-flex gap-1">
									<input type="radio" name="address_id" id="address_check" value="{{$add->area_id}}" data-loction="{{$add->area_id}}" onclick="calShipping('{{$add->area_id}}')" checked />
									<button type="button" class="btn btn-primary" onclick="addressDelete('{{$add->id}}')" href="javascript:void(0);" class="removeFrmCart">Delete</button>
								</div>
							</div>
						</div>
						@endforeach

						@endif
					</div>
					@endif
					<div class="mb-3">
						<label for="" class="form-label">Order notes (optional)</label>
						<textarea class="form-control" name="notes" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-5">
				<div class="secu">
					<div class="cartTotal mb-4">
						<div class="ttlHeader">
							<h4><strong>PRODUCT</strong></h4>
							<h4><strong>SUB-TOTAL</strong></h4>
						</div>
						@if(count($orderlines)>0)
						@foreach($orderlines as $ol)
						<div class="ttlFn mb-3">
							<div class="prc text-start">
								<small>
									<p class="mb-2">{{$ol->product->name}} × {{$ol->qty}}</p>
								</small>
							</div>
							<div class="prc">
								<p> ₹{{number_format($ol->amount,2, '.', '')}}</p>
							</div>
						</div>
						@endforeach
						@endif
						<hr>

						<div class="ttlFn">
							<div class="prc text-start">
								<p><strong>Shipping Charge</strong></p>
								<p id="sh" style="font-size: 11px;"></p>
							</div>
							<div class="prc">
								<p id="sprice"><strong>₹{{number_format($order->shipping_charge,2, '.', '')}}</strong> </p>
								
							</div>
						</div>

						<hr>
						<div class="ttlFn">
							<h4>Total</h4>
							<div class="prc">
								<h5 class="black">
									<div id="total_amount">₹{{number_format($order->total_amount,2, '.', '')}}</div>
								</h5>

							</div>
						</div>
						<hr>
						@if($WebMaintenance==2)
						@if($razorpay_mode!='disable')
						<div class="mb-3 form-check" id="raz">
							<input type="radio" id="rozorpay" class="form-check-input" name="payment" value="rozorpay" checked />
							<label class="form-check-label" for="coDelivery">Razorpay
								<br><small>Pay with razorpay online.</small></label>
						</div>
						@endif
						@endif
						@if($cashfree_mode!='disable')
						<div class="mb-3 form-check">
							<input type="radio" id="cashfree" class="form-check-input" name="payment" value="cashfree" />
							<label class="form-check-label" for="coDelivery">Cashfree</label>
							<br><small>Pay with cashfree online.</small>
						</div>
						@endif

						<div class="mb-3 form-check" id="cod_filed">
							@if($WebMaintenance==2)
							<input type="radio" class="form-check-input" id="coDelivery" name="payment" value="cod" checked />
							<label class="form-check-label" for="coDelivery" id="cash">COD ( <small>Pay with cash </small> )</label>
							@endif
						</div>
						<div id="paySelect" style="display:none;"><small class="red">Please choose a payment method</small>
						</div>
					</div>

					<div class="red" id="warningText" style="text-size:5px;text-align:center;display:none;">
						<h6>{{config('constant.warningText')}}
							<h6>
					</div>
					<div class="red" id="PickupText" style="text-size:5px;text-align:center;display:none;">
						<h6>Pickup Timing {{$openeTime}}:00 Hours to {{$closeTime}}:00 Hours <h6>
					</div>
					<div class="red" id="PickupText" style="text-size:5px;text-align:center;display:none;">
						<h6>Pickup Timing {{$openeTime}}:00 Hours to {{$closeTime}}:00 Hours <h6>
					</div>

					@if($GetenableToday==1)
					<div class="red" id="ShippingText" style="text-size:5px;text-align:center;">
						<h6>Shipping Timing {{$openShTime}}:00 Hours to {{$closeShTime}}:00 Hours <h6>
					</div>
					@endif
					
					@if($WebMaintenance==2)
					<button type="button" onclick="submitButton()" style="margin-bottom: 50px;" class="btn btnBackTo w100p">Proceed to checkout</button>
					@endif
				</div>

			</div>
	</div>

	<input type="hidden" name="order_Id" id="orderId" value="{{$order->id}}" />
	<input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" value="" />
	<input type="hidden" name="razorpay_order_id" id="razorpay_order_id" value="" />
	<input type="hidden" name="razorpay_signature" id="razorpay_signature" value="" />
	</form>
</div>
</div>

<!-------------------------------------------------------->

<div class="modal" tabindex="-1" id="delteModel" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Delete Address</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<input type="hidden" id="hiddenFieldControlId" class='form-control' name="vids[]"  />
				<button type="button" onclick="addDelete()" class="btn btn-primary">Yes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade themeModal" id="timingW" tabindex="-1" aria-labelledby="deleteConfirmLabe2" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center">
				<h5 class="modal-title" id="deleteConfirmLabe2"></h5>
				<button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body pt-4 pb-4">
				<p class="msg-o mb-0 text-center">
				<h6> Delivery timings {{$openeTime}} :00 hours to {{$closeTime}} :00 hours
				</h6>
				</p>
			</div>
			<div class="modal-footer justify-content-center pt-0">


				<button onclick="CodSubmit()" class="btn btn-primary themeBtn themeFill me-3">OK</button>
				<button type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</button>

			</div>
		</div>
	</div>
</div>


<div class="modal fade themeModal" id="Shop_timing" tabindex="-1" aria-labelledby="deleteConfirmLabe2" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center">
				<h5 class="modal-title" id="deleteConfirmLabe2"></h5>
				<button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body pt-4 pb-4" id="timing_alert">

			</div>
			<div class="modal-footer justify-content-center pt-0">


				<button onclick="HomeSubmit()" class="btn btn-primary themeBtn themeFill me-3">OK</button>
				<button type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</button>

			</div>
		</div>
	</div>
</div>

<!-------------------------------------------------------->



<div class="modal fade themeModal" id="address-pop" tabindex="-1" aria-labelledby="deleteConfirmLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header justify-content-center">
				<h5 class="modal-title" id="">Add New Address </h5>
				<button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-footer justify-content-center pt-0">

				<div id="add_address_form" style="display: block;">
					{{ Form::open(['route' => 'new.address.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}
					{{ csrf_field() }}
					<div class="mb-3">
						<div class="row">
							<div class="col-md-6">
								<label for="" class="form-label">Name<span class="red">*</span></label>
								{{ Form::text('first_name1',null, ['class' => 'form-control','id'=>'first_name1','placeholder'=>'Name','required'=>'required']) }}
							</div>
							<div class="col-md-6">
								<label for="" class="form-label">Phone<span class="red">*</span></label>
								{{ Form::text('phone_no1',null, ['class' => 'form-control','id'=>'phone_no1','placeholder'=>'Phone No','required'=>'required']) }}

							</div>
						</div>
					</div>
					<div class="mb-3">
						<label for="" class="form-label">Street address<span class="red">*</span></label>
						{{ Form::text('address_11',null, ['class' => 'form-control mb-2','id'=>'address_11','placeholder'=>'House number and street name','required'=>'required']) }}

					</div>
					<div class="mb-3 form-label" id="locationShow">
						<label for="" class="form-label">Select Location<span class="red">*</span></label>
						<select id="location1" class="form-control addId" name="add_id1" onchange="chengesthis(this)">
							@foreach($location as $key=>$lo)
							<option value="{{$key}}" data-locationid="{{$lo->id}}">{{$lo->area}}</option>
							@endforeach
							<option value="other">Others</option>
						</select>
					</div>
					<div class="mb-3" id="ad_loc1">
						<label for="" class="form-label">Add Location</label>
						{{ Form::text('add_location1',null, ['class' => 'form-control','id'=>'add_location1','placeholder'=>'Add Location']) }}

					</div>
					<div class="d-flex justify-content-start ">
						<button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>

						<button type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
						<input type="hidden" name="user_id" value="{{$id}}" id="user_id" />
					</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$image = asset('img/logo.jpg');
$login = "";
$name = "";
$email = "";
$contect = "";
$ad = array();
if (Auth::check()) {
	$login = "users";
	$name = $user->name;
	$id = $user->id;
	$email = $user->email;
	$contect = $user->phone;
	$ad = $user->userAddress;
}
?>

@endsection
@push('modals')

@endpush
@push('scripts')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript">
	var curentTime;
	var shopOpentTime;
	var shopCloseTime;
	var shippingCharge;
	var delivry;
	var sAreaId;
	var userLogin = "{{$login}}";
	var cod = "{{$cod}}";
	var auth="{{$auth}}";
	var role = "{{$users}}";
	var addressCount = {!!count($ad)!!};
	var shopOpen = <?php echo json_encode($openeTime); ?>;
	var ShopClose = <?php echo json_encode($closeTime); ?>;
	var shippingOpen = <?php echo json_encode($openShTime); ?>;
	var ShippingClose = <?php echo json_encode($closeShTime); ?>;

	$(document).ready(function() {

		$('#self_pickup').prop('checked', true);
		$('#self_pickup').click();
		$('#address_button').hide();
		$('#delbutton').hide();
		$('#warningText').hide();
		$('#ShippingText').hide();
		$('#PickupText').show();
	});
	$('input[name=delivery]').on('click', function() {


		var del = $(this).val();
		$('#sh').html('');
		if (del == "selfpickup") {
			$('#hadding').hide();
			$('#address_button').hide();
			$('#PickupText').show();
			$('#warningText').hide();
			$('#ShippingText').hide();
			$('#coDelivery').prop('checked', true);
			$('#raz').hide();
			$('#coDelivery').show();
			$('#cash').show();
			$('#add_hide').hide();
			$('#hide_add').hide();
			
			calShipping(0);


		} else {
			$('#hadding').html('Billing &amp; Shipping');
			$('#address_button').show();
			$('#delbutton').show();
			$('#warningText').show();
			$('#ShippingText').show();
			$('#PickupText').hide();
			$('#hadding').show();
			$('#raz').show();
			$('#sh').append('<small class="red">(For Free Delivery Order amount should be greater than ₹{{$minOrderAmountFreeDelivery}})</small>');
			if(cod=="on" && auth=="true"){
				$('#coDelivery').show();
				$('#cash').show();
			}
			else{
				$('#coDelivery').hide();
				$('#cash').hide();
			}

			$('#rozorpay').prop('checked', true);
			$('#add_hide').show();
			$('#hide_add').show();
			$('#hide_contect').show();

		}
		if (addressCount == 0 && del == "homedelivery") {
			$('#location').trigger("change");
		} else if (addressCount > 0 && del == "homedelivery") {
			//$('#location').trigger("change");
			if ($('input[name=address_id]:checked').length > 0) {
				var adid = $('input[name=address_id]:checked').val();
				calShipping(adid);
			} else {
				$('input[name=address_id]:first').click();
			}
		}
	});

	function chengesthis(Obj) {
		var valu = $(Obj).val();
		calShipping(valu)
		$('#ad_loc1').hide();
		if (valu == "other") {
			$('#ad_loc1').show();
		}
	}

	function addAddress() {
		if (addressCount < 5) {
			$('#address-pop').modal('show');
		} else {
			alert('please remove any one address then add new address');
		}
	}
    function addressDelete(id){
		$("#hiddenFieldControlId").val(id);
		$('#delteModel').modal('show');

	}
	function addDelete() {
		var id = $("#hiddenFieldControlId").val();

		$.ajax({
			type: 'POST',
			url: "{{route('add.delete')}}",
			contentType: "application/json",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: JSON.stringify({
				id: id
			}),
			success: function(data) {
				let decodedData = JSON.parse(data);
				if (decodedData.status) {
					window.location.reload();
				}
			}
		});
	}

	function calShipping(id) {
		$.ajax({
			url: "{{route('shippingCharge')}}",
			type: 'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			dataType: "json",
			data: {
				areaid: id
			},
			success: function(result) {
				if (result) {
					$('#sprice').html('₹' + parseFloat(result.shiping).toFixed(2));
					$('#total_amount').html('₹' + (result.totalAmount).toFixed(2));
				}
			}
		});
	}

	function submitButton() {

		var ShippingTiming = <?php echo json_encode($ShippingStatus); ?>;
		var mode = $('input[name=delivery]:checked').val();
		var ShopTiming = <?php echo json_encode($shopStatus); ?>;



		var pay_Cod = ($('input[name=payment]:checked').val());
		var del = $('input[name=delivery]:checked').val();
		var valu = $('input[name="address_id"]:checked').attr("data-loction");
		var selectedlocation = $('#location').find(':selected').val();
		if ((userLogin == "users" && addressCount == 0) || (userLogin == "")) {
			sAreaId = selectedlocation;
		}
		if (userLogin == "users" && addressCount > 0) {
			sAreaId = valu;
		}
		var address_id = ($('input[name=address_id]:checked').val());
		var orderId = $('#orderId').val();
		var email = $('#email_id').val();
		var first_name = $('#first_name').val();
		var phone_no = $('#phone_no').val();
		var address_1 = $('#address_1').val();
		var email = $('#email_id').val();
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var login = "{{$login}}";
		if (login == "users") {

			var prefillData = {
				"name": "{{$name}}",
				"email": "{{$email}}",
				"contact": "{{$contect}}",

			}
		} else {
			var prefillData = {
				"name": first_name,
				"email": email,
				"contact": phone_no,
			}
		}
		var isErroFound = false;
		if (del == "homedelivery") {
			if ((address_id == "" || address_id == null)) {

				if (first_name == "" || first_name == null) {
					$("#fn").html("Name is required");
					$("#fn").parent().show();
					isErroFound = true;
				}
				if (phone_no == "" || phone_no == null) {
					$("#ph").html("Contact is required");
					$("#ph").parent().show();
					isErroFound = true;
				}
				if (address_1 == "" || address_1 == null) {
					$("#ad").html("Address is required");
					$("#ad").parent().show();
					isErroFound = true;
				}

			}
		} else {
			if (userLogin != "users") {
				if (first_name == "" || first_name == null) {
					$("#fn").html("Name is required");
					$("#fn").parent().show();
					isErroFound = true;
				}
				if (phone_no == "" || phone_no == null) {
					$("#ph").html("Contact is required");
					$("#ph").parent().show();
					isErroFound = true;
				}
			}

		}
		if (isErroFound) {
			return;
		}


		if ((mode == 'homedelivery' && ShippingTiming == 'open') || (ShopTiming == 'open' && mode == 'selfpickup')) {
			if (pay_Cod == "cod") {
				CodSubmit();
			} else if (pay_Cod == "rozorpay") {
				HomeSubmit();

			} else {
				$('#paySelect').show();
			}
		}

		if (ShopTiming == 'close' && mode == 'selfpickup') {
			$('#timingW').modal('show');
		}

		if (mode == 'homedelivery' && ShippingTiming == 'close') {

			$('#Shop_timing .modal-body').html('<p class="msg-o mb-0 text-center"><h6>Shipping timings ' + shippingOpen + ':00 hours to ' + ShippingClose + ':00 hours</h6></p>');
			$('#Shop_timing').modal('show');
		}

	}

	function CodSubmit() {
		Swal.fire({
			title: 'Please Wait !',
			html: 'Processing...', // add html attribute if you want or remove
			allowOutsideClick: false,
			didOpen: () => {
				Swal.showLoading()
			},
		});
		$('#orderSubmit').submit();
	}

	function HomeSubmit() {
		var orderId = $('#orderId').val();
		var del = $('input[name=delivery]:checked').val();
		var login = "{{$login}}";
		if (login == "users") {
			var prefillData = {
				"name": "{{$name}}",
				"email": "{{$email}}",
				"contact": "{{$contect}}",

			}
		}
		$('#Shop_timing').modal('hide');
		Swal.fire({
			title: 'Please Wait !',
			html: 'Processing...', // add html attribute if you want or remove
			allowOutsideClick: false,
			didOpen: () => {
				Swal.showLoading()
			},
		});
		$.getJSON("{{route('razorpay.payment.get')}}?order_id=" + orderId, 'areaId=' + sAreaId + '_' + del, function(data) {
			if (data == 2 || data == 1 || data == 3) {
				if (data == 1) {
					var icon = 'error';
					var tital = 'Something went wrong. Please try again in a few minutes';
				}
				if (data == 2) {
					var icon = '';
					var tital = 'Your Cart is empty or you have already started the payment process on another page. Please complete payment on same page or wait approx 10 minutes to reset the previous process.';
				}
				if (data == 3) {
					var icon = 'error';
					var tital = 'This email already exists.';
				}
				Swal.fire({
					icon: icon,
					text: tital,
					showConfirmButton: true,
					allowOutsideClick: false,
					timer: 3000
				}).then((result) => {
					if (result.isConfirmed && data == 2) {
						window.location.reload();
					}
				});
				return;
			}

			data = JSON.parse(data);
			var options = {
				"key": data.key,
				"amount": data.amount,
				"currency": "INR",
				"name": "Rasanwala",
				"description": "Transaction",
				"image": "/img/logo.jpg",
				"order_id": data.order_id,
				"prefill": prefillData,
				"handler": function(response) {
					Swal.fire({
						title: 'Please Wait !',
						html: 'Processing...', // add html attribute if you want or remove
						allowOutsideClick: false,
						didOpen: () => {
							Swal.showLoading()
						},
					});
					$('#razorpay_payment_id').val(response.razorpay_payment_id);
					$('#razorpay_order_id').val(response.razorpay_order_id);
					$('#razorpay_signature').val(response.razorpay_signature);
					$('#orderSubmit').submit();
				}
			};
			var rzp1 = new Razorpay(options);
			Swal.close();
			rzp1.open();

			/*rzp1.on('payment.failed', function (response){
				Swal.fire({
					icon: 'error',
					text: "Your payment has been failed. So please try again.",
					showConfirmButton: true,
					allowOutsideClick: false,
					timer: 3000
					}).then((result) => {
						if (result.isConfirmed) {
							window.location.reload();
						}
					});
			});	*/

		});

	}

	var total_amount = "{{number_format($order->total_amount,2, '.', '')}}";
	var id = "{{($order->id)}}";
	var coupon_amount = "{{($order->coupon_amount)}}";

	function applyCoupon() {
		var c = $('#coupon_code').val();
		if (c != "") {
			$.ajax({
				type: "get",
				url: "{{route('cart.checkcoupon')}}",
				dataType: "json",
				data: {
					coupon: c,
					total_amount: total_amount,
					order_id: id
				},
				success: function(data) {
					if (data.type == "success") {
						var amount = (total_amount - data.amount).toFixed(2);
						$('#coupon_code').val('');
						$('#discount_amount').html('₹' + data.amount);
						$('#total_amount').html('₹' + amount);
						$('#coupon_data').show();
						toastr[data.type](data.msg);
						return true;
					} else {
						$('#coupon_data').hide();
						toastr[data.type](data.msg);
						return false;
					}

				}

			});
		} else {
			toastr["error"]("Please Enter a Coupon Code. ");
			return false;
		}

	}

	function percentage(num, per) {
		return ((num / 100) * per).toFixed(2);
	}

	function deleteCoupon() {
		$.ajax({
			type: "get",
			url: "{{route('delete.order.coupon')}}",
			dataType: "json",
			data: "order_id=" + id,
			success: function(data) {
				if (data.type == "success") {
					$('#coupon_code').val('')
					$('#coupon_name').html('');
					$('#total_amount').html('₹' + total_amount);
					$('#coupon_data').hide();
					toastr[data.type](data.msg);
					return true;
				} else {
					$('#coupon_data').show();
					toastr[data.type](data.msg);
					return false;
				}
			}
		});
	}
</script>

@endpush