<?php
use App\Models\Common;

$meta=Common::getMetaInfo('category');
// $sbcat=Category::where('slug',$url)->first();
// $subcatgs=Category::where('parent_id',$sbcat->id)->get();
    // if($sbcat->parent_id != "" || $sbcat->parent_id !=null)
		// {
			// $catProduct=Category::where('id',$sbcat->id)->first();
			// $productObj->where('categeory_id',$catProduct->id);
			
		// }else{
			// $sCp=Category::where('parent_id',$sbcat->id)->pluck('id')->toArray();
			
			// $productObj->whereIn('categeory_id',$sCp);
			
		// }
// $pb=$productObj->groupBy('brand_id')->pluck('brand_id')->toArray();
// $brands=Brand::whereIn('id',$pb)->get();
$min_price=0;
$max_price=0;
	if(count($products) > 0){
		$min_price=$products->min('sale_price');
		$max_price=$products->max('sale_price');
	}
?>
@extends('layouts.front', $meta)
@section('content') 
 <div class="container">
<div class="row">
    <div class="col-12 col-md-3 filterBar">
        <div class="filterInner">
		<div class="sideSec mb-c-30">
		
            <h4>Categories</h4>
            <div class="catego">
               <ul>
                <li><a href="{{route('shop')}}"><i class="angleArrowLeft" aria-hidden="true"></i> All Categories</a></li>

				@if(count($categeory)>0)
					@foreach($categeory as $mancat)
					@if(count($mancat->getProduct($mancat->id))>0)
					<?php
					$style="";
					if($mancat->slug==$url){
					$style="color:#fcb800";
					}

					?>
					<li><a href="{{route('catCat',$mancat->slug)}}" style="{{$style}}">{{$mancat->name}}</a></li>
					@endif
					@endforeach
				@endif
				</ul>
            </div>
          </div>
          </div>
		   @if(count($brands)>0)
		  <div class="filterInner">	
			<form class="themeForm" method="get" >
			 <!-- <div class="sideSec borderBot">
            <h4>FILTER BY PRICE</h4>
            <div class="filtr mb-1">
              <div class="priceFilter"></div>
              <p>Price: ₹<input type="text" class="sliderValue" data-index="0" value="0"  style="width:45px" name="min_price"//> — ₹<input type="text" class="sliderValue" data-index="1" value="10000"  style="width:45px" name="max_price"/></p>
            </div>
          </div>-->
			<div class="sideSec brandSec mb-c-30">
				<h4 class="mb-1">BRANDS</h4>
			<div class="brandSearch">
              <input type="text" id="myInputTextField" placeholder="Search">
            </div>
			
				<table id="listtable1" style=" border-collapse: separate;" class="table">
			 <thead>
                <tr>
				<th></th>
                </tr>
            </thead>
            <tbody>
               <tr>
			  <td>
				<div class="multiCheck mb-3" style ="max-height:350px;overflow-y:auto">
				
				  <ul>
				 
				  @foreach($brands as $key=>$brand)
				   <?php
					   $checked=[];
					   if(isset($_GET['checkBrand']))
					   {
						   $checked=$_GET['checkBrand'];
					   }
					   ?>
					<li>
					  <div class="form-check">
						<input class="form-check-input checkboxIn" type="checkbox" name="checkBrand[]"value="{{$brand->id}}" id="" @if(in_array($brand->id,$checked)) checked @endif />
					
						<label class="form-check-label" for="">
						  {{$brand->name}}
						</label>	 
					  </div>
					</li>
					
					@endforeach
					
				  </ul>
				  
				</div>
				</td>
               </tr>
			 </tbody>
            </table>
				<button type="submit" class="btn btn-primary" style="background-color: #df861a;border-color: #df861a;">Apply Filter</button>
			  </div>
			  </form>
			</div>
			@endif
		  </div>
<div class="col-12 col-md-9 contentBar">
	  @include('front.shop')
 </div>
 </div>
 </div>
@endsection
@push('scripts')
<script type="text/javascript">
var min_price="{{$min_price}}";
var max_price="{{$max_price}}";
$(document).ready(function(){
var Table=$('#listtable1').DataTable({
		responsive: false,
		processing: false,
		searching: true,
		serverSide: false,
		stateSave: false,
		bLengthChange: false,
		bInfo: false,
		ordering: false,
		bPaginate: false,
		bFilter: false,
	});
		$('#myInputTextField').keyup(function(){
		Table.search($(this).val()).draw() ;
});
 var checkedVals = $('.checkboxIn:checkbox').map(function() {
	 console.log(21342);
    return this.value;
	}).get();
	
	$(".priceFilter").slider({
        min: 10,
        max: 10000,
        step: 1,
        values: [10, 10000],
        slide: function(event, ui) {
            for (var i = 0; i < ui.values.length; ++i) {
                $("input.sliderValue[data-index=" + i + "]").val(ui.values[i]);
            }
        }
    });
    
    $(".sliderValue").change(function() {
        var $this = $(this);
        $(".priceFilter").slider("values", $this.data("index"), $this.val());
    });
});
	// $(document).on('click','.checkboxIn',function(){
	// var ids=[];	
	// var counter=0;
	// $('.checkboxIn').each(function(){
	// catfilter(ids)
	// });
// });
// function catfilter(ids){
// }
 
 </script>
@endpush