<?php
use App\Models\Variant;
use App\Models\Brand;
$oderLines=[];
if(!empty($order))
{
	$oderLines=$order->getOrderLines;
}
use App\Models\Common;
$meta=Common::getMetaInfo('homepage');
?>
@extends('layouts.front',$meta)	
@section('content')	
<div class="container">
	<div class="pageTitle text-center mb-3">
		<h1 class="entry-title">Order Items List</h1>
	</div>
	<form class="themeForm">
		<div class="cartWrap">
			<div class="d-flex justify-content-center row">
				<div class="table-responsive col-md-8">
					<table class="table align-middle">
						<thead class="table-light">
						@if(!empty($order) && count($oderLines)>0)
							<tr>
								<th>PRODUCT</th>
								<th>QUANTITY</th>
								<th>ORDER AMOUNT</th>
							</tr>
						@endif
						</thead>
						<tbody>
						@if(!empty($order) && count($oderLines)>0)
							@foreach($oderLines as $ol)
							<?php
								$variant=Variant::where('id',$ol->variant_id)->first();
								$size='';
								$priceCount=0;
								if(!empty($variant))
								{
									$size=	$variant->size;
									
									if($variant->sale_price>$variant->regular_price)
									{
									$priceCount=$variant->regular_price;
									}else{
										$priceCount=$variant->sale_price;
									}
								}
							?>
							<tr>
							<td>
								<div class="prod pt1">
									<div class="img">
										<img src="{{$ol->product->image}}" alt="img" height="40px" />
									</div>
									<div class="txt">
										<a href="{{route('detail.product',$ol->product->slug)}}">{{$ol->product->name}} ({{$size}})</a>
									</div>
								</div>
							</td>	
							<td>{{$ol->qty}}</td>
							<td id="total_{{$ol->id}}">₹ {{$priceCount*$ol->qty}}</td></tr>
							@endforeach
						@endif
						  <tr>	
							<td colspan="2">Shipping Charge</td>
							<td>₹ {{$order->shipping_charge}}</td>
							</tr>
							 <tr>	
							<td colspan="2">Total Amount</td>
							<td>₹ {{$order->total_amount}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>  
		</div>
	</form>
</div>
@endsection	