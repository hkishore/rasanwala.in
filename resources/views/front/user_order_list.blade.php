<?php
use App\Models\Variant;
use App\Models\User;
use App\Models\OrderLine;
use App\Models\Product;
use App\Models\Order;
use App\Models\Brand;
use Carbon\Carbon;
$user= Auth::user();
$order=Order::where('user_id',$user->id)->where('status','>',0)->whereMonth('created_at', Carbon::now()->month)->paginate(10);
use App\Models\Common;
$meta=Common::getMetaInfo('homepage');
?>
@extends('layouts.front',$meta)	
@section('content')	
   <div class="container">
	<div class="pageTitle text-center mb-3">
	  <h1 class="entry-title">Order List</h1>
	</div>
	  <div class="cartWrap">
	
		<div class="table-responsive">
		 
		  <table class="table align-middle">
		  
			<thead class="table-light">
			 @if(count($order)>0)
			  <tr>
				<th>Order NO.</th>
				<th>Total Amount</th>
				<th>Items</th>
				<th>Status</th>
				<th>Views</th>
			  </tr>
			  @endif
			</thead>
			 
			<tbody>
			@if(count($order)==0)
				<td>
			<div class="woocommerce">
				<p class="cart-empty woocommerce-info">Your Order List is currently empty.</p>
				</div>
				</td>
				@endif
				@foreach($order as $key=>$ol)
				<?php
						$status=" Process";
				  if($ol->status==2){
						$status="Out of Delivery";
					}
					else if($ol->status==3){
						$status="Delivered";
					}
					else if($ol->status==4){
						$status="Cancelled";
					}
				$orderLine=OrderLine::where('order_id',$ol->id)->get();
				$items=count($orderLine);
				
				?>
					 <tr>
				<td>#{{$ol->id}}</td>
				<td>₹ {{$ol->total_amount}}</td>
				
				<td>{{$items}}</td>
				
				<td >{{$status}}</td>
				<td ><a href="{{route('views.items',$ol->id)}}" class="btn btn-outline-success">
			Views items
		  </a></td>
            @endforeach				
			</tbody>
		  </table>
		</div>
		@if(!empty($order) && count($order)>0)
		<div class="pagination">
	  <ul class="page-numbers">
		<li><span class="page-numbers">{!! $order->links() !!}</span></li>
	 </ul>
	 </div>
	 @endif
		<div class="backTo mt-5 mb-5">
		  <a href="{{route('shop')}}" class="btn btnBackTo">
			<i class="icon-arrow-left"></i>
			Back To Shop
		  </a>
		</div>
		</div>
		
	  </div>
@endsection	
<script type="text/javascript">
</script>
