<?php
use App\Models\Setting;
use App\Models\Common;
$meta=Common::getMetaInfo('homepage');
$email=Setting::getFieldVal('email');
if($model == "help-center"){
	$tittle="Ask your Query";
	$message="Message";
}else{
$tittle=" Feedback/Suggestion/support";
$message="Give Your Feedback/Suggestion/support.Your Feedback/Suggestion is helpfull for us.";
}

?>
@extends('layouts.front',$meta)	
@section('content')
   <div class="container">
		{{ Form::open(['route' => 'sendmail.user','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}
		 {{ csrf_field() }} 
          <div class="checkout contactPg pt-5 pb-5">
            <div class="row">
              <div class="col-12 col-lg-6">
                
                <div class="secu">
                  <h3 class="bigger150">Rasan Wala</h3>


                  <div class="font16">
                    <p><strong>Address:-</strong>Sah RasanWala<br>
                  Balaji Market, Near by<br>
                  Jindal Stainless Ltd.</br>
                  Hisar, 125001, Haryana</br>
                  Phone: +91-7988418408<br>

                      <a href="mailto:{{$email}}">support@rasanwala.in</a> 
                    </p>

                  </div>
                  
                </div>
              </div>
              
              <div class="col-12 col-lg-6">
                
                
                <div class="secu bgBlueLight p-4">
                  <h2 class="col000 weight600 text-center mb-5">{{$tittle}}</h2>
                  <div class="row">
                    <div class="mb-4 col-lg-6">
                    	
				{!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name', 'value' => old('name')]) !!}
				@if ($errors->has('name'))
				<span class="help-block red">
				<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
							
                    </div>
					
                    <div class="mb-4 col-lg-6">
				{!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Email', 'value' => old('email')]) !!}
				@if ($errors->has('email'))
				<span class="help-block red">
				<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
                    </div>
                    <div class="mb-4 col-lg-12">
					{!! Form::text('subject', null, ['id' => 'subject', 'class' => 'form-control', 'placeholder' => 'Subject', 'value' => old('subject')]) !!}
					@if ($errors->has('subject'))
					<span class="help-block red">
					<strong>{{ $errors->first('subject') }}</strong>
					</span>
					@endif
                    </div>
                    <div class="mb-4 col-lg-12">
					  {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'form-control', 'placeholder' => $message, 'value' => old('message')]) !!}
					@if ($errors->has('message'))
					<span class="help-block red">
					<strong>{{ $errors->first('message') }}</strong>
					</span>
					@endif
                    </div>
						<div class="mb-4 col-lg-12">
						<span id="captcha_img">{!! captcha_img() !!}</span>
						<button type="button" id="cap" class="btn btn-success btn-refresh"><i class="fa fa-refresh"></i></button>

						</div>
						<div class="mb-4 col-lg-12">
						{!! Form::text('captcha', null, ['id' => 'captcha', 'class' => 'form-control', 'placeholder' => 'Enter Captcha', 'value' => old('captcha')]) !!}
						@if ($errors->has('captcha'))
						<span class="help-block red">
						<strong>{{ $errors->first('captcha') }}</strong>
						</span>
						@endif
						</div>
		
                    <div class="mb-4 col-lg-12 text-center">
                      <button type="submit" class="btn btnBackTo">Post</button>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
		  {{ Form::close() }}
</div>
@endsection	
@push('scripts')
<script type="text/javascript">
var va=$('#captcha_img').val();
console.log(va);
$(document).ready(function() {
$("#cap").on('click',function(){
  $.ajax({
     type: "get",
           url: "{{route('refresh.captcha')}}",
           success: function (data){
		 console.log(data);
        $("#captcha_img").html(data.captcha);
     }
  });
});
});
</script>
@endpush