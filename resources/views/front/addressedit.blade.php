<?php
use App\Models\ShippingArea;
$location=ShippingArea::all()->pluck('area','id')->toArray();
?>
@extends('layouts.front')

@section('content')
 <div class="container">
   <div class="pageTtl">
            
			@if(isset($model) && !empty($model))
			<h1>Edit Address</h1>
		@else
		<h1>Add Address</h1>
	@endif
        </div>       
    <div class="greyBx">
	@if(isset($model) && !empty($model))
              {{ Form::model($model,['route' => ['address.update', $model->id],'enctype'=>"multipart/form-data","class"=>"themeForm"]) }}
	@else
                {{ Form::open(['route' => 'address.store','enctype'=>"multipart/form-data","class"=>"themeForm"]) }}
			
    @endif
                    {{ csrf_field() }} 
             @if(isset($model) && !empty($model))                         
                  @method('PUT')
			  @endif
<div class="pageTitle text-center mb-3">
                 </div>
                 <div class="row">
                    <div class="mb-3 col-md-6">
                    <label for="" class="form-label">Name <span class="red">*</span></label>
                    {{ Form::text('first_name',null, ['class' => 'form-control','id'=>'first_name','placeholder'=>'First Name','required'=>'required']) }}
					@if ($errors->has('first_name'))
					<span class="help-block">
					<strong>{{ $errors->first('first_name') }}</strong>
					</span>
					@endif
                  </div>
                 <div class="mb-3 col-md-6">
                   <label for="" class="form-label">Email address</label>
 					{{ Form::email('email_id',null, ['class' => 'form-control','id'=>'email_id','placeholder'=>'Email']) }}
					@if ($errors->has('email_id'))
					<span class="help-block red">
					<strong>{{ $errors->first('email_id') }}</strong>
					</span>
					@endif
                  </div>
				</div>  
				  <div class="row">
					 <div class="mb-3 col-md-6">
                    <label for="" class="form-label">Phone<span class="red">*</span></label>
                    {{ Form::text('phone_no',null, ['class' => 'form-control','id'=>'phone_no','placeholder'=>'Phone No','required'=>'required']) }}
					@if ($errors->has('phone_no'))
					<span class="help-block">
					<strong>{{ $errors->first('phone_no') }}</strong>
					</span>
					@endif
                  </div>
				   <div class="mb-3 col-md-6">
                  <label for="" class="form-label">Street address<span class="red">*</span></label>
                    {{ Form::text('address_1',null, ['class' => 'form-control mb-2','id'=>'address_1','placeholder'=>'House number and street name','required'=>'required']) }}
					@if ($errors->has('address_1'))
					<span class="help-block">
					<strong>{{ $errors->first('address_1') }}</strong>
					</span>
					@endif
					</div>
                    </div>
				<div class="row">
				<div class="mb-3 col-md-6">
                    <label for="" class="form-label">Location<span class="red">*</span></label>
					<select id="location" class="form-control" name="add_id" onchange="chenges(this)">
					<option value="">Select Location</option>
					@foreach($location as $key=>$lo)
					<option value="{{$key}}">{{$lo}}</option>
					@endforeach
					<option value="other">Others</option>
					</select>
                  </div>
				   <div class="mb-3 col-md-6" id="add_hide">
                   <label for="" class="form-label">Add Location<span class="red">*</span></label>
                    {{ Form::text('add_location',null, ['class' => 'form-control','id'=>'add_location','placeholder'=>'Add Location']) }}
					@if ($errors->has('add_location'))
					<span class="help-block red">
					<strong>{{ $errors->first('add_location') }}</strong>
					</span>
					@endif
					</div>
                 </div>
				   <div class="col-12 col-md-0 mb-0 d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Submit</button>
                        <a  href="{{ url()->previous() }}" class="btn btn-primary themeBtn themeUnFill">Cancel</a>
                      </div>
				
 

                {{ Form::close() }}
           </div>
    </div>
    </div>

@endsection
@push('scripts')
<script type="text/javascript">
$('document').ready(function(){	
$('#add_hide').hide();
});
function chenges(Obj)
{
var valu=$(Obj).val();
if(valu=="other")
{
$('#add_hide').show();
}else{
$('#add_hide').hide();
}
}
</script>
@endpush