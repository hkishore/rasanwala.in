    <?php

    use App\Models\Page;

    $pages = Page::where('status', '1')->get();
    ?><footer>
      <div class="container ">
        <div class="footTop">
          <div class="row">
            <div class="footCol footoOne row ">
              <div class="fotLogo col-md-6 p-0 pe-5">
                <a href="{{('/')}}">
                  <img src="{{asset('img/logo.jpg')}}" alt="logo" />
                </a>
              </div>
              <div class="fotDiscrp col-md-6 col-ml-6">
                <p>Address:-Sah RasanWala<br>
                  Balaji Market, Near by<br>
                  Jindal Stainless Ltd.</br>
                  Hisar,125001, Haryana</br>
                  Phone: +91-79884-18408<br>
                  <span class="gst">Email: support@rasanwala.in</span>
                  <span class="gst">GSTIN : 06AMUPD8013C1ZA</span>
                </p>

              </div>
            </div>
            <!--<div class="footCol footoTwo">
              <h4>Brand</h4>
              <ul>
                <li><a href="#!">About us</a></li>
                <li><a href="#!">Contact</a></li>
                <li><a href="#!">Shop</a></li>
                <li><a href="#!">FAQs</a></li>
                <li><a href="#!">Privacy Policy</a></li>
                <li><a href="#!">Refund Policy</a></li>
                <li><a href="#!">Terms and Conditions</a></li>
              </ul>
            </div>-->
            <div class="footCol footoThree">
              <h4 class="mb-2">Customer Support</h4>
              <ul>
                <li><a href="{{ route('contact.user','help-center') }}">Contact Us</a></li>
                <li><a href="{{route('shop')}}">Shop</a></li>
                <li><a href="{{route('faq')}}">FAQs</a></li>
				
				<!--<li><a href="{{ route('contact.user','2') }}">Support & Feedback</a></li>-->
                @if(count($pages)>0)
                @foreach($pages as $page)
                <?php
                $view = route('static', [$page->url]);
                ?>
                <li><a href="{{$view}}" title="View" target="_blank">{{ucwords($page->title)}}</a></li>
                @endforeach
                @endif
                <li class="mt-2"><a href="https://play.google.com/store/apps/details?id=com.sah.rasanwala" target="_blank"><img src="{{asset('img/gp.png')}}" alt="rasan Wala Application"></a></li>
              </ul>
            </div>
            <div class="footCol footoFour">
              <div class="mt-3">
                <a href="https://www.facebook.com/sahrasanwala" target="blank"><i class="fab fa-facebook" style="font-size: 30px !important; margin-left: 20px;"></i></a>
              </div>
              <div class="img">
                <img src="{{asset('img/stripe-badge-transparent.png')}}" alt="" />
              </div>
			  <button class="button button5" style="background-color: black; height:50px; border-radius: 10px; margin-left: 20px;"><a href="{{ route('contact.user','feedback') }}" class="text-white" style="padding: 5px;" target="_blank"><b>Missing Product</b> </a></button>
            </div>
          </div>
        </div>
        <div class="footBot">
          <p>© 2022 Sah Rasan Wala. All Rights Reserved</p>
        </div>
      </div>
    </footer>