<?php
use App\Models\Common;
$meta=Common::getMetaInfo('homepage');

use App\Models\Variant;
use App\Models\Brand;
use App\Models\Setting;

$oderLines = [];
if (!empty($order)) {
	$oderLines = $order->getOrderLines;
}
$maxQuantity = config('custom.maxQuantity');
$WebMaintenance = Setting::getFieldVal('web_maintenance');
?>
@extends('layouts.front',$meta)
@section('content')
<div class="container">
	<div class="pageTitle text-center mb-3">
		<h1 class="entry-title">Cart</h1>
	</div>
	<form class="themeForm">
		<div class="cartWrap">
			<div class="row">
				<div class="table-responsive col-md-8">

					<table class="table align-middle">

						<thead class="table-light">
							@if(!empty($order) && count($oderLines)>0)
							<tr>
								<th class="text-center">PRODUCT</th>
								<th>PRICE</th>
								<th>QUANTITY</th>
								<th>TOTAL</th>
								<th></th>
							</tr>
							@endif
						</thead>

						<tbody>

							@if(empty($order) || count($oderLines)==0)
							<td>
								<div class="woocommerce">
									<p class="cart-empty woocommerce-info">Your cart is currently empty.</p>
								</div>
							</td>
							@endif
							@if(!empty($order) && count($oderLines)>0)
							@foreach($oderLines as $ol)
							<?php
							// $brandId=$ol->product->brand_id;
							// $brand=Brand::where('id',$brandId)->first();
							$variant = $ol->variant;
							$orderLimit = $variant->order_limit;
							$size = '';
							$priceCount = 0;
							if (!empty($variant) && (!empty($variant->sale_price) || !empty($variant->regular_price))) {
								$size =	$variant->size;
								if ($variant->sale_price > $variant->regular_price) {
									$priceCount = $variant->regular_price;
								} else {
									$priceCount = $variant->sale_price;
								}
							}



							?>
							@if($priceCount>0)
							<tr>
								<td>

									<div class="prod pt1">
										<div class="img">
											<img src="{{$ol->product->image}}" alt="" height="50px" />
										</div>
										<div class="txt">
											<a href="{{route('detail.product',$ol->product->slug)}}">{{$ol->product->name}} ({{$size}})</a>

										</div>
									</div>
								</td>

								<td>₹ {{$priceCount}}</td>

								<td>
									<div class="qty-box">
										<input type="button" onclick="decrementValue({{$ol->product_id}},{{$priceCount}},{{$ol->id}},{{$ol->variant_id}})" value="-" />
										<input type="text" name="quantity" value="{{$ol->qty}}" maxlength="2" max="10" size="1" id="number_{{$ol->id}}" />
										<input type="button" onclick="incrementValue({{$ol->product_id}},{{$priceCount}},{{$ol->id}},{{$ol->variant_id}},{{$orderLimit}})" value="+" />
									</div>
								</td>
								<td id="total_{{$ol->id}}">₹ {{$priceCount*$ol->qty}}</td>
								<td><a href="javascript:void(0);" onclick="orderDelete({{$ol->id}})" class="removeFrmCart"><i class="icon-cross2"></i></a></td>
								@endif
								@endforeach
								@endif
						</tbody>
					</table>
				</div>
				@if(!empty($order) && count($oderLines)>0)
				<div class="cartDetails col-md-4">
					<div class="item">
						<div class="red" style="margin-left:; text-align: justify;">
							<h6>{{config('constant.warningText')}}
								<h6>
						</div>
						<div class="cartTotal mb-4">
							<div class="ttlFn">
								<h4>Total</h4>
								<div class="prc">
									<h5 class="black" id="total_amount"> ₹{{number_format($order->total_amount,2, '.', '')}}</h5>
								</div>
							</div>
						</div>
						@if($WebMaintenance==2)
						<a class="btn btnBackTo d-block w100p" href="{{route('checkout')}}">Proceed to checkout</a>
						@else
						<div class="red" style="margin-left:; text-align: justify;">
							<h6>Website is in Maintainance<h6>
						</div>
						@endif
					</div>

				</div>
				@endif
			</div>
			<div class="backTo mt-5 mb-5">
				<a href="{{route('shop')}}" class="btn btnBackTo">
					<i class="icon-arrow-left"></i>
					Back To Shop
				</a>
			</div>


		</div>
	</form>
</div>
@endsection
<script type="text/javascript">
	var maxQuantity = "{{$maxQuantity}}";

	function incrementValue(productId, price, id, variant_id, order_limit) {
		var value = parseInt(document.getElementById('number_' + id).value, 10);
		value = isNaN(value) ? 0 : value;
		if (order_limit < 1) {
			order_limit = maxQuantity;
		}
		if (value < order_limit) {
			value++;
			document.getElementById('number_' + id).value = value;
			document.getElementById('total_' + id).innerHTML = '₹ ' + value * price;
			addcard(productId, 1, variant_id, order_limit);
		} else {
			toastr["info"]("You cannot Order More Than " + order_limit + " quantity");
		}
	}

	function decrementValue(productId, price, id, variant_id) {
		var value = parseInt(document.getElementById('number_' + id).value, 10);
		value = isNaN(value) ? 0 : value;
		if (value > 1) {
			value--;
			document.getElementById('number_' + id).value = value;
			document.getElementById('total_' + id).innerHTML = '₹ ' + value * price;
			addcard(productId, -1, variant_id);
		}

	}

	function addcard(id, value, variant_id, order_limit) {
		if (value < 11) {
			$.ajax({
				type: 'POST',
				url: "{{route('add.cart')}}",
				contentType: "application/json",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: JSON.stringify({
					p_id: id,
					qty: value,
					variant_id: variant_id
				}),
				success: function(data) {
					if (data.status == true) {
						$('#total_amount').html("₹ " + data.total_amount);
					} else {
						toastr["error"](data.msg);

					}
				}
			});
		} else {
			toastr["error"]("max quantity 10. ");
			return false;
		}
	}

	function orderDelete(id) {
		$.ajax({
			type: 'POST',
			url: "{{route('remove.cart')}}",
			contentType: "application/json",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: JSON.stringify({
				id: id
			}),
			success: function(data) {
				let decodedData = JSON.parse(data);
				//console.log(decodedData.status);
				if (decodedData.status) {
					window.location.reload();
				}
			}
		});
	}
</script>