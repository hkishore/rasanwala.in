	<?php

	use App\Models\Variant;
	use App\Models\Brand;
	use App\Models\Common;
	use App\Models\Order;
	use App\Models\OrderLine;
	use App\Models\FavoriteItem;

	$order = Order::getUserOrders(true);
	$orderlines = OrderLine::where('order_id', $order->id)->get()->keyBy('variant_id');
	//$variantObj= Common::variantGet();
	$variantID = Variant::all()->keyBy('id');
	$maxQuantity = config('custom.maxQuantity');
	?>
	@if(($products->count() > 0 ))
	<div class="productFilterHead">
		<div class="totalProduct">
			<strong>{{$products->count()}}</strong> Products found
		</div>
		<div class="rightAccess">
			<form action="{{route('shop')}}" id="formSubmit" method="get">
				<div class="shop-view">
					<span>View</span>
					<a href="#" class="grid-view mf-shop-view current" data-view="grid">
						<i class="icon-grid"></i>
					</a>
					<a href="#" class="list-view mf-shop-view " data-view="list">
						<i class="icon-list4"></i>
					</a>
				</div>
		</div>
	</div>
	<div class="productItemWrap">
		<div class="row">
			@foreach($products as $product)
			<?php
			$image = $product->image;
			//$variantObj= Common::variantGet();
			$variant = $product->variant;
			//$variantSize=$variantObj->where('product_id',$product->id)->first();
			//$variantID=$variantObj->where('product_id',$product->id)->get()->keyBy('id');
			$url = "#";
			$brandName = "";
			if ($product->brand_id != "" && $product->brand_id != null) {
				$brands = Brand::where('id', $product->brand_id)->first();
				if (!empty($brands)) {
					$url = route('brand.product', $brands->slug);
					$brandName = $brands->name;
				}
			}
			?>
			@if(!empty($variant) && count($variant)>0)
			<div class="item col-12 col-md-4 col-lg-3">
				<div class="itemInnter">
					<div class="mf-product-thumbnail">
						<a href="#!" class="thumbNail">
							<a href="{{route('detail.product',$product->slug)}}" class="thumbNail">
								@if($product->image!="" && $product->image!=null)
								<img src="{{$image}}" alt="" height="200">
								@else
								<img src="/img/no-pictures.png" alt="" height="200">
								@endif
								<span class="ribbons" id="ribbons_{{$product->id}}"></span>
							</a>
							<div class="footer-button">
								<!--<div class="viewIcoWrap">
                                
                                <a class="viewIco" href="{{route('detail.product',$product->slug)}}" data-lightbox="2-1-2-300x338.jpg" data-title="">
                                  <i class="p-icon icon-eye" title="Quick View"></i>
                                </a>
                              </div>-->
								<div class="wishWrap">
									@if(Auth::check())
									<?php
									$auth = Auth::user();
									$favzproduct = FavoriteItem::where('user_id', $auth->id)->where('product_id', $product->id)->first();
									$class = "";
									$title = "Add to wishlist";
									if (!empty($favzproduct)) {
										$class = "background-color: #28afb1;";
										$title = "Remove to wishlist";
									}
									?>
									<a style="{{$class}}" href="javascript:void(0);" id="fav_{{$product->id}}" title="{{$title}}" onclick="addWishList('{{$product->id}}')">
										<i class="yith-wcwl-icon fa fa-heart-o"></i>
									</a>

									@else

									<a href="{{route('custom.login')}}" title="Add to wishlist">
										<i class="yith-wcwl-icon fa fa-heart-o"></i>
									</a>

									@endif
								</div>
							</div>
					</div>
					<div class="mf-product-details">
						<div class="product-content">
							<div class="sold-by-meta">
								<a class="wcfm_dashboard_item_title" href="{{$url}}">{{$brandName}}</a>
							</div>
							<h2><a href="{{route('detail.product',$product->slug)}}">{{$product->name}}</a></h2>
						</div>
						<div class="mf-product-price-box">

							<!--select box -->
							<div class="product-content">
								<select name="varian_name" class="form-control varients" id="variant_{{$product->id}}" onchange="changeVarinat({{$product->id}})" />
								@foreach($variant as $key=>$vr)
								<?php

								$qty = 1;
								if (isset($orderlines[$vr->id])) {
									$qty = $orderlines[$vr->id]->qty;
								}
								$order_limit = $vr->order_limit;
								$charge=$vr->charge;
								$per = $product->getPercentage($vr);
								?>
								<option value="{{$vr->id}}" data-qty="{{$qty}}" data-sale="{{$vr->sale_price}}" data-regular="{{$vr->regular_price}}" data-orderlimit="{{$order_limit}}" data-charge="{{$vr->charge}}" data-per="{{$per}}">{{$vr->size}} @if($vr->regular_price>$vr->sale_price)₹ {{$vr->sale_price}}@else ₹ {{$vr->regular_price}}@endif </option>
								@endforeach
								</select>

							</div>
							<div class="product-content">
								<span class="price">
									<del>
										<span class="woocommerce-Price-amount amount">
											<bdi>
												<span class="woocommerce-Price-currencySymbol" id="offPrice{{$product->id}}"></span>
											</bdi>
										</span>
									</del>
									<ins>
										<span class="woocommerce-Price-amount amount">
											<bdi>
												<span class="woocommerce-Price-currencySymbol" id="salePrice{{$product->id}}"></span>
											</bdi>
										</span>
									</ins>
								</span>
							</div>
							<div class="product-content itemfooter">
								<div class="qty-box">
									<input type="button" onclick="decrementValue({{$product->id}})" value="-" />
									<input type="text" name="quantity" value="" maxlength="2" max="10" size="1" id="qtyValue_{{$product->id}}" />
									<input type="button" onclick="incrementValue({{$product->id}})" value="+" />
								</div>
								<!--<span><input type="number" id="qtyValue_{{$product->id}}" maxlength="2" min="1" max="10" class="form-control qtyfield" name="qty" value="" /><span>-->
								<span><button type="button" class="add-to-cart-text" onclick="addcard({{$product->id}})">Add to Cart</button></span>
							</div>
						</div>

					</div>

				</div>
			</div>
			@endif
			@endforeach
		</div>
	</div>
	<div class="pagination">
		<ul class="page-numbers">
			<li><span class="page-numbers">{!! $products->appends(request()->except('page'))->links() !!}</span></li>
		</ul>
	</div>
	@else
	<div class="totalProduct">
		<strong>Products Not found</strong>
	</div>


	@endif
	@push('scripts')

	<script type="text/javascript">
		var variantId;
		var qty;
		var setValue;
		var maxQuantity = "{{$maxQuantity}}";
		$(document).ready(function() {
			$('#filterProduct').on('change', function() {
				var vl = (this.value);
				var featured = $('#featured').val(vl);
				$('#formSubmit').submit();
			});
			$('.varients').trigger("change");
		});

		function changeVarinat(pId) {
			var qty1 = $('#variant_' + pId).find(':selected').data('qty');
			var saleP = $('#variant_' + pId).find(':selected').data('sale');
			var regularP = $('#variant_' + pId).find(':selected').data('regular');
			var per = $('#variant_' + pId).find(':selected').data('per');
			var valu = $('#variant_' + pId).find(':selected').val();
			if (saleP < regularP) {
				$('#offPrice' + pId).html('₹' + parseFloat(regularP).toFixed(2));
			}
			$('#salePrice' + pId).html('₹' + parseFloat(saleP).toFixed(2));
			$('#qtyValue_' + pId).val(qty1);
			if (per > 0 && per != 0) {
				$('#ribbons_' + pId).show();
				$('#ribbons_' + pId).html(per + '% off');
			} else {
				$('#ribbons_' + pId).hide();
			}
		}

		function incrementValue(productId) {
			var value = parseInt(document.getElementById('qtyValue_' + productId).value, 10);
			value = isNaN(value) ? 0 : value;
			var order_limit = $('#variant_' + productId).find(':selected').data('orderlimit');
			if (order_limit < 1) {
				order_limit = maxQuantity;
			}
			if (value < order_limit) {
				value++;
				document.getElementById('qtyValue_' + productId).value = value;
			} else {
				toastr["info"]("You cannot Order More Than " + order_limit + " quantity");
			}
		}

		function decrementValue(productId) {
			var value = parseInt(document.getElementById('qtyValue_' + productId).value, 10);
			value = isNaN(value) ? 0 : value;
			if (value > 1) {
				value--;
				document.getElementById('qtyValue_' + productId).value = value;
			}

		}

		function addcard(id) {
			var variantId = $('#variant_' + id).find(':selected').val();
			var qty1 = $('#variant_' + id).find(':selected').data('qty');
			var qty = parseInt($('#qtyValue_' + id).val());
			var order_limit = $('variant_' + id).find(':selected').data('orderlimit');
			if (order_limit < 1) {
				order_limit = maxQuantity;
			}
			order_limit = parseInt(order_limit);
			if (qty > order_limit) {
				toastr["info"]("You cannot Order More Than " + order_limit + " quantity");
				return;
			}
			if (qty <= order_limit) {
				$.ajax({
					type: 'POST',
					url: "{{route('add.cart')}}",
					contentType: "application/json",
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: JSON.stringify({
						p_id: id,
						qty: qty,
						variant_id: variantId,
						type: "update"
					}),
					success: function(data) {
						if (data.status == true) {
							$('#variant_' + id).find(':selected').data('qty', qty);
							$('#count_cart_value').html(data.count_cart);
							$('#amount').html(data.amount);
							toastr["success"]("Add to card " + qty + " quantity");
						} else {
							toastr["error"](data.msg);

						}
					}
				});
			} else {
				toastr["error"]("max quantity 10. ");
				return false;
			}
		}
	</script>
	@endpush