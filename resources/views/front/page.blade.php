<?php
use App\Models\Common;
$meta=Common::getMetaInfo($model);
$meta['pageTitle']=$model->title;
?>
@extends('layouts.front', $meta)
@section('content')
 <div class="container">
    <section class="bots hero">
		<div class="container">
			<div class="pb-2"style="text-align:center;color:black"">
              
                  <h2>{!!$model->title!!}</h2>
            </div>
			<div class="card mt-3">
			<div class="card-body" style="color:black">
			
			<p class="card-text"> {!!$model->description!!}</p>
			</div>
			</div>			
       
	     </div>
	</section><br><br>
</div>
	
    
@endsection