<?php
use App\Models\Common;
$meta=Common::getMetaInfo($product);
?>
@extends('layouts.front',$meta)
@section('content')
<?php
use App\Models\OrderLine;
use App\Models\Order;
use App\Models\Review;

$user = Auth::user();
$order = Order::getUserOrders(true);
$orderlines = OrderLine::where('order_id', $order->id)->get()->keyBy('variant_id');
$maxQuantity = config('custom.maxQuantity');
?>
@if(!empty($product))
<div class="container">
	<form class="themeForm">
		<div class="productDetails">
			<div class="row">
				<?php
				$image = $product->image;
				$brand = $product->brandName;
				$catName = $product->CatName;

				$variantObj = Common::variantGet();
				$variants = $variantObj->where('product_id', $product->id)->get();
				$vPrice = $variantObj->where('product_id', $product->id)->first();
				$varient_id = $vPrice->id;
				$qty = 1;
				if (isset($orderlines[$varient_id])) {
					$qty = $orderlines[$varient_id]->qty;
				}
				?>
				<div class="col-12 col-lg-6 mb-5">
					<div class="img mb-3">
						<a class="example-image-link" href="#" data-lightbox="example-set" data-title="">
							@if($image!="" && $image!=null)
							<img id="NZoomImg" data-NZoomscale="2" src="{{$image}}" alt="" />
							@else
							<img src="/img/no-pictures.png" alt="" height="200">
							@endif
						</a>
					</div>
					<div class="text-center"><small>Roll over image to zoom in</small></div>
				</div>
				<div class="col-12 col-lg-6">
					<h1>{{$product->name}}</h1>
					@if(!empty($product->brandName))
					<div class="meta-brand">
						Brand:
						<a href="{{route('brand.product',$brand->name)}}" class="meta-value">{{$product->brandName->name}}&amp; Co.</a>
					</div>
					@endif
					<hr>
					<div class="midCont">
						<div id="data-price" class="price">₹ {{ $vPrice->sale_price}}</div>

						<select name="varian_name" class="form-control varients" id="cart_{{$product->id}}" onchange="changeVarinat({{$product->id}})" />
						@foreach($variants as $key=>$variantName)
						<?php
						$order_limit = $variantName->order_limit;
						$qty = 1;
						if (isset($orderlines[$variantName->id])) {
							$qty = $orderlines[$variantName->id]->qty;
						}

						?>
						<option value="{{$variantName->id}}" data-qty="{{$qty}}" data-sale="{{$variantName->sale_price}}" data-regular="{{$variantName->regular_price}}" data-orderlimit="{{$order_limit}}">{{$variantName->size}} ( ₹ {{$variantName->sale_price}} )</option>
						@endforeach
						</select>
						<!--<a id="cart_{{$variantName->id}}" onclick="variantProdcut({{$variantName->id}},{{$variantName->sale_price}})" > {{$variantName->size}}
						
						<small>( ₹ {{$variantName->sale_price}} )</small>
						</a>-->
						<div class="hasStock">
							<!-- <p>Status: <small class="fontGreen"><strong> In Stock</strong></small></p>
                    <hr>  -->
							<div class="actWrap" style="margin-top: 20px;">
								<div class="qty-box">
									<input type="button" onclick="decrementValue()" value="-" />
									<input type="text" name="quantity" value="" maxlength="2" max="10" size="1" id="number" />
									<input type="button" onclick="incrementValue({{$product->id}})" value="+" />
									<input type="hidden" name="variant_id" value="" id="variant_id" />
								</div>
								<a onclick="addcard({{$product->id}},'')" value="" class="btn btn-outline-dark btnThemeShap">Add to cart</a>
								<a onclick="addcard({{$product->id}},'buynow')" value="" class="btn btnBackTo">Buy Now</a>

								<!-- <a onclick="javascript:void(0)" class="add-to-cart-text">Out Of Stock</a> -->

							</div>
							<a href="javascript:void(0);" onclick="question({{$product->id}})" class="btn themeBtnBlue btn-sm"><i class="fa fa-question-circle" aria-hidden="true"></i> Ask a Question</a>
							<hr>
							<div class="sku">
								<span class="sku_wrapper">
									SKU: <span class="skuv">{{$product->sku}}</span>
								</span><br>
								@if(!empty($catName))
								<span class="posted_in">
									CATEGORY :
									<a href="{{route('catCat',$catName->slug)}}">{{$catName->name}}</a>
									<!-- <a href="#!">Combo Packs</a>-->
								</span>
								@endif
							</div>
							<div style="margin-top:15px">
								@include('share')
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<div class="description mb-5">
		<h4 class="lineStyle">Description</h4>
		<p>{!! $product->description !!}</p>
	</div>
	<div class="description mb-5">
		<h4 class="lineStyle">Specification</h4>
		<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>Weight</th>
					<td>{{$product->weight_shipping}}</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php
	if (Auth::check()) {
		$pReviews = Review::where('user_id', $user->id)->where('product_id', $product->id)->orderBy('id')->orWhere('status', 1)->take(25)->get();
		$Reviews = Review::where('product_id', $product->id)->orWhere('user_id', $user->id)->orWhere('status', 1)->count();
	} else {
		$pReviews = Review::where('status', 1)->where('product_id', $product->id)->orderBy('id')->take(25)->get();
		$Reviews = Review::where('status', 1)->where('product_id', $product->id)->count();
	}
	?>
	<div class="description mb-5">
		<h4 class="lineStyle">Reviews ({{$Reviews}})</h4>
		@foreach($pReviews as $key=>$review)
		<div class="note">
			<h6>
				<i class="fa fa-user" aria-hidden="true"></i> {{$review->userN->name}} :
				@if($review->rating>0)
				<a class="ratingdis">
					@for($i=0;$i<5;$i++) <i class="fa fa-star @if($i<$review->rating) filled @else unfilled @endif"></i>
						@endfor
				</a>
				@endif
			</h6>
			<small>{{$review->review}}</small>
			<hr>
		</div>
		@endforeach
		@if(!Auth::check())
		<div class="note">
			<small>Only logged in customers who have purchased this product may leave a review.</small>
		</div>
		<div style=" margin: 15px 0" class="btnWrap">
			<a href="{{route('custom.login')}}" class="btn btnBackTo">Login</a>
		</div>
		@endif
		@if(Auth::check())
		<?php
		$orderLine = OrderLine::where('product_id', $product->id)->pluck('order_id');
		$order = Order::where('user_id', $user->id)->where('status', 5)->whereIn('id', $orderLine)->first();
		?>
		@if(!empty($order))
		<div class="writeReview">
			<h5>WRITE A REVIEW</h5>
			<form action="{{route('review-store')}}" method="get" class="themeForm">
				<div class="form-group mb-3">
					<input type="text" name="review" style="margin-bottom: 3px;" id="review" class="form-control input-icon-pencil" placeholder="your review" required />
					<input type="hidden" value="1" name="status" />
					<input type="hidden" value="{{$product->id}}" name="product_id" />
					<input type="hidden" value="{{$product->vendor->user_id}}" name="vendor_id" />

					<div id="full-stars-example-two">
						<div class="rating-group">
							<input disabled checked class="rating__input rating__input--none" name="rating" id="rating3-none" value="0" type="radio">
							<label aria-label="1 star" class="rating__label" for="rating3-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
							<input class="rating__input" name="rating" id="rating3-1" value="1" type="radio">
							<label aria-label="2 stars" class="rating__label" for="rating3-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
							<input class="rating__input" name="rating" id="rating3-2" value="2" type="radio">
							<label aria-label="3 stars" class="rating__label" for="rating3-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
							<input class="rating__input" name="rating" id="rating3-3" value="3" type="radio">
							<label aria-label="4 stars" class="rating__label" for="rating3-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
							<input class="rating__input" name="rating" id="rating3-4" value="4" type="radio">
							<label aria-label="5 stars" class="rating__label" for="rating3-5"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
							<input class="rating__input" name="rating" id="rating3-5" value="5" type="radio">
						</div>
					</div>

				</div>
				<div class="btnWrap">
					<button class="btn btnBackTo">Add Your Review</button>
				</div>
			</form>
		</div>
		@else
		<div class="btnWrap">
			<a href="#" class="nav-link active" onClick="refreshPage()">Click here for Buy Products </a>
		</div>
		@endif
		@endif
	</div>
</div>
@endif
@endsection
@push('models')
@include('question-popup')
@endpush
@push('scripts')
<script type="text/javascript">
	var price;
	var $inp = $('#rating-input');
	var maxQuantity = "{{$maxQuantity}}";
	$inp.rating({
		min: 0,
		max: 5,
		step: 1,
		size: 'lg',
		showClear: false,
		showCaption: false
	});
	$(document).ready(function() {
		if($('.varients').length > 0)
		{
            $('.varients').trigger("change");
		}
		
	});

	function incrementValue(id) {
		var value = parseInt(document.getElementById('number').value, 10);
		value = isNaN(value) ? 0 : value;
		var order_limit = $('#cart_' + id).find(':selected').data('orderlimit');
		if (order_limit < 1) {
			order_limit = maxQuantity;
		}
		if (value < order_limit) {
			value++;
			document.getElementById('number').value = value;
		} else {
			toastr["info"]("You cannot Order More Than " + order_limit + " quantity");
		}
		// if (value < 10) {
		// 	value++;
		// 	document.getElementById('number').value = value;
		// }
	}

	function decrementValue() {
		var value = parseInt(document.getElementById('number').value, 10);
		value = isNaN(value) ? 0 : value;
		if (value > 1) {
			value--;
			document.getElementById('number').value = value;
		}

	}

	function changeVarinat(pId) {
		var qty1 = $('#cart_' + pId).find(':selected').data('qty');
		var sale = $('#cart_' + pId).find(':selected').data('sale');
		var regular = $('#cart_' + pId).find(':selected').data('regular');
		var valu = $('#cart_' + pId).find(':selected').val();
		if (regular > sale) {
			price = sale;
		} else {
			price = regular;
		}
		$('#data-price').html('₹' + parseFloat(price).toFixed(2));
		$('#number').val(qty1);
	}

	function addcard(id, btn) {
		var value = parseInt(document.getElementById('number').value);
		var qty1 = $('#cart_' + id).find(':selected').data('qty');
		var vId = $('#cart_' + id).find(':selected').val();

		var order_limit = $('#cart_' + id).find(':selected').data('orderlimit');
		
		if (order_limit < 1) {
			order_limit = maxQuantity;
		}
		order_limit=parseInt(order_limit);
		if (value > order_limit) {
			toastr["info"]("You cannot Order More Than " + order_limit + " quantity");
			return;
		}

		if (value <= order_limit || btn != '') {
			$.ajax({
				type: 'POST',
				url: "{{route('add.cart')}}",
				contentType: "application/json",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: JSON.stringify({
					p_id: id,
					qty: value,
					variant_id: vId,
					type: 'update'
				}),
				success: function(data) {
					if (data.status == true) {
					$('#cart_' + id).find(':selected').data('qty', value);
					$('#amount').html(data.amount);
					if (btn == 'buynow') {
						window.location.href = "{{route('cart')}}";
					} else {
						toastr["success"]("Add to card " + value + " quantity");
					}
				}else{
					toastr["error"](data.msg);
				}

				}
			});
		} else {
			toastr["error"]("max quantity 10. ");
			return false;
		}
	}

	function variantProdcut(id, price) {
		//console.log(id);
		document.getElementById("data-price").innerHTML = "₹ " + price;
		document.getElementById("variant_id").value = id;

	}


	function question(pid) {
		$('#product_id').val(pid);
		$('#questionModel').modal('show');
	}

	function refreshPage() {
		window.location.reload();
	}

	function rating(id) {
		$("#rating_id").val(id);
	}
</script>
@endpush