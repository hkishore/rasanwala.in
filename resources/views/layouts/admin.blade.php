<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Rasanwala.in</title>

   <link rel="icon" type="image/jpg" sizes="32x32" href="{{ asset('img/logo.jpg')}}">
   
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.11/cropper.min.css" integrity="sha512-NCJ1O5tCMq4DK670CblvRiob3bb5PAxJ7MALAz2cV40T9RgNMrJSAwJKy0oz20Wu7TDn9Z2WnveirOeHmpaIlA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	
	<link href="{{ asset('css-admin/responsive.dataTables.min.css')}}?v=9.02" rel="stylesheet">
	<link href="{{ asset('css-admin/jquery.mCustomScrollbar.css')}}?v=1.0" rel="stylesheet">
    <link href="{{ asset('css-admin/admin-style.css')}}?v=1.0" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">	
	<link href="{{ asset('css-admin/custom-dropzon.css')}}?v=1.0" rel="stylesheet">
	   
  </head>
  <body>
  <?php
  $user =Auth::user();
  ?>
<div class="mainWrap">
      <nav class="navbar navbar-expand-lg navbar-light bg-light sidenav">
        <div class="container-fluid">
          <div class="logoWrap">
            <a class="navbar-brand" href="{{('/')}}">
              <img src="{{ asset('img/logo.jpg')}}" style=" width: 70%;" alt="">
            </a>
          </div>

          <div class="collapse navbar-collapse flex-wrap show" id="menudiv">
            <ul class="navbar-nav me-auto flex-grow-0 flex-shrink-0 col-12">
           <li class="nav-item">
			<a class="nav-link {!! (Request::is('dashboard') ? 'active' : '') !!}" aria-current="page" href="{{route('dashboard')}}"><img src="{{asset('img/menu-ico-1-admin-dashboard.svg')}}" alt=""> <span>Dashboard</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('user') ? 'active' : '') !!}" aria-current="page" href="{{route('user.index')}}"><img src="{{asset('img/menu-ico-3-customers.svg')}}" alt=""> <span>Users</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('brand') ? 'active' : '') !!}" aria-current="page" href="{{route('brand.index')}}"><img src="{{asset('img/menu-ico-1-admin-dashboard.svg')}}" alt=""> <span>Brand</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('category') ? 'active' : '') !!}" aria-current="page" href="{{route('category.index')}}"><img src="{{asset('img/menu-ico-1-admin-dashboard.svg')}}" alt=""> <span>Categories</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('blog') ? 'active' : '') !!}" aria-current="page" href="{{route('blog.index')}}"><img src="{{asset('img/menu-ico-1-admin-dashboard.svg')}}" alt=""> <span>Blogs</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('faqs') ? 'active' : '') !!}" aria-current="page" href="{{route('faqs.index')}}"><img src="{{asset('img/menu-ico-1-admin-dashboard.svg')}}" alt=""> <span>Faq</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('page') ? 'active' : '') !!}" aria-current="page" href="{{route('page.index')}}"><img src="{{asset('img/menu-ico-1-admin-dashboard.svg')}}" alt=""> <span>Page</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('product') ? 'active' : '') !!}" aria-current="page" href="{{route('product.index')}}"><img src="{{asset('img/menu-ico-3-customers.svg')}}" alt=""> <span>Products</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('order') ? 'active' : '') !!}" aria-current="page" href="{{route('order.index')}}"><img src="{{asset('img/menu-ico-3-customers.svg')}}" alt=""> <span>Orders</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('query') ? 'active' : '') !!}" aria-current="page" href="{{route('query.index')}}"><img src="{{asset('img/menu-ico-3-customers.svg')}}" alt=""> <span>Query</span></a>
			</li>
			
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('frontimage') ? 'active' : '') !!}" aria-current="page" href="{{route('frontimage.index')}}"><img src="{{asset('img/menu-ico-3-customers.svg')}}" alt=""> <span>Slider Image</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('charge') ? 'active' : '') !!}" aria-current="page" href="{{route('charge.index')}}"><img src="{{asset('img/menu-ico-3-customers.svg')}}" alt=""> <span>Shipping Cost</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('charge') ? 'active' : '') !!}" aria-current="page" href="{{route('notification.index')}}"><img src="{{asset('img/menu-ico-3-customers.svg')}}" alt=""> <span>Notification</span></a>
			</li>
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('area') ? 'active' : '') !!}" aria-current="page" href="{{route('area.index')}}"><img src="{{asset('img/menu-ico-3-customers.svg')}}" alt=""> <span>Shipping Area</span></a>
			</li>
			
			<li class="nav-item">
			<a class="nav-link {!! (Request::is('settings') ? 'active' : '') !!}" aria-current="page" href="{{route('settings.index')}}"><img src="{{asset('img/menu-ico-3-customers.svg')}}" alt=""> <span>Settings</span></a>
			</li>
            </ul>
            
          </div>

        </div>
      </nav>
      
    <div class="containerWrap">
      <div class="contHeader">
        <div class="container">
          <div class="headerInr" style="flex-direction:column;align-items: flex-end">
		    @if(Auth::check())  	
	        <div class="logoutBx">
              <a href="javascript:void(0)" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <img src="{{asset('img/ico-logout.svg')}}" alt="" />
                <span>logout</span>
              </a>
			 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			  @csrf
			 </form>		
            </div>
		   @endif
          </div>
        </div>
      </div>    
			@if ($message = Session::get('success'))
			<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>	
			<strong>{{ $message }}</strong>
			</div>
			@endif


			@if ($message = Session::get('error'))
			<div class="alert alert-danger alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>	
			<strong>{{ $message }}</strong>
			</div>
			@endif


			@if ($message = Session::get('warning'))
			<div class="alert alert-warning alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>	
			<strong>{{ $message }}</strong>
			</div>
			@endif


			@if ($message = Session::get('info'))
			<div class="alert alert-info alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>	
			<strong>{{ $message }}</strong>
			</div>
			@endif


			@if ($errors->any())
			<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>	
			Please check the form below for errors
			</div>
			@endif
			
      <div class="pageContent">
        <div class="container">
           @yield('content')
        </div>
      </div>
     </div>
  </div>
  
<!--Delete Modal Start -->
<div class="modal fade themeModal" id="confirm-delete" tabindex="-1" aria-labelledby="deleteConfirmLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header justify-content-center">
        <h5 class="modal-title" id="deleteConfirmLabel"></h5>
        <button type="button" class="btn-close d-none" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body pt-4 pb-4">
        <p class="msg-o mb-0 text-center"></p>
      </div>
      <div class="modal-footer justify-content-center pt-0">
	   <form action="" method="POST">
	    @csrf		
	    @method('DELETE')		
        <button type="submit" class="btn btn-primary themeBtn themeFill me-3">Yes</button>
		</form>
		 <button type="button" class="btn btn-primary themeBtn themeUnFill me-3" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
      </div>
    </div>
  </div>
</div>
@stack('modals')
<!-- Modal End -->
    
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->
   
   <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	
	<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.11/cropper.min.js" integrity="sha512-FHa4dxvEkSR0LOFH/iFH0iSqlYHf/iTwLc5Ws/1Su1W90X0qnxFxciJimoue/zyOA/+Qz/XQmmKqjbubAAzpkA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.tiny.cloud/1/840qlilo1fu79xqhnwaoqvufkmmddx3c266ubvvg6p7ov0xl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ asset('js-admin/jquery.min.js')}}" ></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript" src="{{asset('js-admin/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('js-admin/jquery.mCustomScrollbar.concat.min.js')}}" ></script>
    <script src="{{ asset('js-admin/dataTables.responsive.min.js')}}" ></script>
    <script src="{{ asset('js-admin/custom.js')}}?v=1.0" ></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js" defer></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="{{ asset('js-admin/dropzone.min.js')}}" ></script>
    <script src="{{ asset('js-admin/custom-dropzone.js')}}" ></script>	
@stack('scripts')
  </body>
</html>