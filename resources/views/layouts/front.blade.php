<!doctype html>
<?php

use App\Models\OrderLine;
use App\Models\Order;
use App\Models\Priduct;
use App\Models\FavoriteItem;
use App\Models\Setting;

$user = Auth::user();
$route = Route::current()->getName();
$home_test = Setting::getFieldVal('home_test');
$web_main=Setting::getFieldVal('web_maintenance');
// dd($web_main);
$title=$meta['title'];
$description=$meta['description'];
$keyword=$meta['keywords'];


use App\Models\Notification;
$user = Auth::user();
$msg = Notification::where('status', 1);
// if ($user->role == 3) {
//     $msg->where('user_type', 'customer');
// }
// if ($user->msg_id > 0) {
//     $msg->where('id', '>', $user->msg_id);
// }
$notification = $msg->orderBy('id', 'DESC')->limit(4)->get();
?>
<html lang="en">

<head>
	
	<title>{!! (isset($title) && $title!=""?$title.' - ':'') !!}{{ config('app.name', 'Laravel') }}</title>
	<meta name="cryptomus" content="94042cf5" />
	@if(isset($description) && $description!="")
    <meta name="description" content="{{$description}}">
     <meta property="og:description" content="{{$description}}" />
	 <meta name="twitter:description" content="{{$description}}" />
    @endif
	@if(isset($keywords) && $keywords!="")
    <meta name="keywords" content="{{$keywords}}">
    @endif
	<meta property="og:locale" content="en_GB" />
	<meta property="og:type" content="{{ (isset($page_type) && $page_type!=""?$page_type:'website')}}" />
	<meta property="og:title" content="{!! (isset($title) && $title!=""?$title.' - ':'') !!}{{ config('app.name', 'Rasanwala') }}" />
	<meta property="og:url" content="{{url()->current()}}" />
	<meta property="og:site_name" content="{{config('app.name')}}" />
	<meta name="twitter:card" content="summary_large_image" />	
	<meta name="twitter:title" content="{!! (isset($title) && $title!=""?$title.' - ':'') !!}{{ config('app.name', 'Rasanwala') }}" />
	
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="google-site-verification" content="tETOeBz9Mjy1pIc5yVRJoSSGWRbLrmuEDM-HRgxw_Fg" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<!-- Bootstrap CSS -->
      

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link href="{{asset('css/linearicons.min.css')}}?v=1.0" rel="stylesheet" />
	<link href="{{asset('css/ionicons.min.css')}}?v=1.0" rel="stylesheet" />
	<!--<link href="{{asset('css/font-awesome.css')}}?v=1.0" rel="stylesheet" />
    <link href="{{asset('css/lightbox.css')}}" rel="stylesheet" />-->
	<link href="{{asset('css/slick.css')}}?v=1.0" rel="stylesheet" />
	<link href="{{asset('css/style.css')}}?v=1.6" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css" />
	<link href="{{asset('css/star-rating.css')}}?v=1.0" rel="stylesheet" />
	<link rel="icon" type="image/jpg" sizes="32x32" href="{{ asset('img/logo.jpg')}}">
	@stack('style')
	<title>Rasanwala.in</title>
	<style>
		.dropdown-text {
			color: black;
		}
	</style>
	<meta name="google-site-verification" content="FiUeTBkt3SC4N4VVOHfe88lC6d6PP6r3P0q6lk7C8xk" />
	<!-- Google tag (gtag.js) -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-6JZ5H0J2X5"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'G-6JZ5H0J2X5');
	</script>
</head>

<body>

	<?php
	$order = Order::getUserOrders(false);
	$countFav = 0;
	if (Auth::check()) {
		$countFav = FavoriteItem::where('user_id', $user->id)->count();
	}
	$count_cart = 0;
	if (!empty($order) && count($order->getOrderLines) > 0) {
		$count_cart = $order->getOrderLines->count();
	}
	?>

	<nav class="navbar navbar-expand-lg navbar-light ccTlNav">
		<div class="mt-2 pb-2 container-fluid topStrp ">

			<div class="col-md-5 marqueePC">
				@if($web_main==1)
				<marquee width="100%" scrollamount="10" onmouseover="this.stop();" onmouseout="this.start();" scrolldelay="70">
					<div class="red">
						<h3>Website Is In Maintainance</h3>
					</div>
					<!--<div class="red" style="margin-left:25%;"><h4>{{config('constant.warningText')}}<h4></div>-->
				</marquee>
				@else
				@if($home_test!='')
				<marquee width="100%" scrollamount="10" onmouseover="this.stop();" onmouseout="this.start();" scrolldelay="70">
					<div class="red">
						<h3>{!!$home_test!!}</h3>
					</div>
					<!--<div class="red" style="margin-left:25%;"><h4>{{config('constant.warningText')}}<h4></div>-->
				</marquee>
				@endif
				@endif
			</div>
			<div class="col-md-7">
				<ul class="navbar-nav nvright">
					<div class="essnIcon">
						<ul>
							<li class="mt-1"><i class="fa fa-phone"></i><span> : +91-79884-18408</span></li>
							<li class="mt-1 emailHeader"><a class="essLink" href=""><i class="fa fa-envelope"></i><span> : support@rasanwala.in</span></a></li>
							<li class="mt-1 emailHeader"><a class="essLink" style="pointer-events: none;" href="">GSTIN : 06AMUPD8013C1ZA</a></span></li>
							@if(Auth::check())
							<li><a class="essLink" href="{{route('favorite.product')}}"><i class="icon-heart extra-icon" rel="tooltip"></i><span class="count" id="count_fav">{{$countFav}}</span> </a> </li>
							@endif

							<li><a class="essLink" href="{{route('cart')}}"><i class="icon-bag2 extra-icon"></i><span class="count">
								<div id="count_cart_value" style="color: black;">{{$count_cart}}</div>
							</span> </a> </li>
							<!--<li>
							<a class="essLink" href="javascript:void(0);" onclick="showNotPopup()" aria-expanded="false"><i class="fa fa-bell extra-icon" aria-hidden="true" ar></i> </a>
							</li>-->
							
							@if(Auth::check())
							<!-- <li>
								<div class="dropdown" >		
									<div type="" data-toggle="dropdown">
										<a class="essLink" href="##">
											<i class="fa fa-bell extra-icon"></i>
										</a>
									</div>					
									<div class="dropdown-menu" style="margin-left: -300px; margin-top: 20px;">
									<div class="notification-single-top">
									<h4>Notifications</h4>
                              </div>
										@if(count($notification)>0)
										@foreach($notification as $notify)
										<div class="notification-content">
											<p>												
												{{$notify->notification}}
											</p>
										</div>
										&nbsp;
										@endforeach	
										<div class="notification-view">
											<a href="{{route('allNoti')}}" class="justify-content-end">See all</a>
										</div>
										@else
										<h5>No Notification</h5>
										@endif
										
									</div>
								</div>
							</li> -->
							@endif
							<li>
								<div class="dropdown" style="padding-right:20px ">
									<div type="" data-toggle="dropdown">
										<a class="essLink" href="##">
											<i class="extra-icon icon-user"></i>
										</a>
									</div>
									<div class="dropdown-menu" style="margin-left: -100px; margin-top: 10px;">
										@if(Auth::check())
										<a href="{{route('user.order.list')}}" class="nav-link dropdown-text">MY ORDER LIST</a>
										@if($user->role==1)
										<a href="{{route('dashboard')}}" class="nav-link dropdown-text">DASHBOARD</a>
										@endif
										@else
										<a href="{{route('custom.login')}}" class="nav-link dropdown-text">MY ORDER LIST</a>
										@endif

										@if(Auth::check())
										<a href="{{route('myprofile.index')}}" class="nav-link dropdown-text">MY PROFILE</a>
										<a href="javascript:void(0)" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link dropdown-text">LOGOUT</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											@csrf
										</form>
										@else
										<a href="{{route('custom.login')}}" class="nav-link dropdown-text">LOG IN</a>
										<a href="{{route('register')}}" class="nav-link dropdown-text">REGISTER</a>
										@endif
									</div>
								</div>
							</li>
						</ul>
					</div>

				</ul>


			</div>
		</div>


		<div class="container-fluid middleStrp flex100">

			<a class="navbar-brand" href="{{('/')}}"><img src="{{asset('img/logo.jpg')}}" alt="" /></a>
			<form class="navSearch deskShow" method="get" action="{{route('shop')}}">
				<input class="form-control" style="350px;" type="search" name='searchText' aria-label="Search" placeholder="I'm shopping for..." required>
				<button class="btn btn-outline-success" type="submit">Search</button>
			</form>


			<div class="essnIcon">
			</div>

		</div>
		<div class="container-fluid botStrp flex100">
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<form class="navSearch mobShow">
				<input class="form-control" type="search" placeholder="Search" aria-label="Search">
				<button class="btn btn-outline-success" type="submit">Search</button>
			</form>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link active" aria-current="page" href="{{('/')}}">HOME</a>
					</li>

					<?php

					use App\Models\Category;
					use App\Models\Product;
					use App\Models\Variant;

					$menuList = Category::where('parent_id', null)->where('show_list', 1)->where('status', 1)->orderBy('position')->take(6)->get();

					?>
					@foreach($menuList as $key=>$menu)
					<?php
					$variant = Variant::groupBy('product_id')->pluck('product_id')->toArray();
					$productAll = Product::whereIn('id', $variant)->where('status', 1)->groupBy('categeory_id')->pluck('categeory_id')->toArray();
					$category_list = Category::whereIn('id', $productAll)->where('parent_id', $menu->id)->where('show_list', 1)->where('status', 1)->orderBy('position')->get();
					$icon = "ion-android-restaurant";
					?>

					<li class="nav-item dropdown">
						<div class="dropdown" style="padding:15px 0px;">
							<a href="#" style="padding: 0px 29px 0px 0px" class="nav-link active dropdown-toggle" data-bs-toggle="dropdown"></i>{{strtoupper($menu->name)}}</a>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
								@if(count($category_list)>0)
								@foreach($category_list as $cat)
								<li><a class="dropdown-item" style="background-color:white;" href="{{route('catCat',$cat->slug)}}">{{$cat->name}}</a></li>
								@endforeach
								@endif
							</ul>
						</div>
					</li>
					@endforeach

					<li class="nav-item">
						<a class="nav-link active" aria-current="page" href="{{route('shop')}}">SHOP</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" aria-current="page" href="{{route('offer')}}">OFFER</a>
					</li>

				</ul>
			</div>
		</div>
	</nav>
	@if($home_test!='')
	<marquee class="marqueeMobile" width="100%" scrollamount="10" onmouseover="this.stop();" onmouseout="this.start();" scrolldelay="70" bgcolor="#f2f0f0">
		<div class="red">
			<h3>{!!$home_test!!}</h3>
		</div>
	</marquee>
	@endif
	<?php
	$class = "pageContent";
	if ($route == "/" || $route == "") {
		$class = "pageContent frontPage";
	}
	?>
	<div class="{{$class}}">
		@include('flash-message')
		@yield('content')
		@stack('models')
	</div>

	<div class="subscribeBx">
		<div class="container">
			<div class="row" style="">
				<div class="textSd col-12 col-md-5">
					<h3>Stay up to date!</h3>
					<p>Subscribe to be the first to know about our new products and access to exclusive member only coupons</p>
				</div>
				<div class="subBoxWrap col-12 col-md-6">
					<form>
						<div class="subscribeWrap">

							<input type="email" id="email_id" name="EMAIL" placeholder="Email Address" required />
							<input type="button" onclick="emailSubscribe()" value="Subscribe" class="btn btnBackTo d-block" name="subscribe" />
						</div>
					</form>
					<div style="color:red;display:none;" id="errorMsg"></div>
				</div>
			</div>
		</div>
	</div>
@include('noti-popup')
	@include('front.footer')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<!--<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>-->
	<!--<script src="{{asset('js/lightbox-plus-jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"></script>-->
	<script src="{{asset('js/slick.js')}}?v=1.0"></script>
	<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
	<script src="{{asset('js/custom.js')}}?v=1.0"></script>
	<!--<script src="{{asset('js/nzoom.min.js')}}"></script>-->
	<!--<script src="{{asset('js/lightbox.js')}}?v=1.0"></script>-->
	<!--<script src="{{ asset('js/dataTables.responsive.min.js')}}?v=1.0" ></script>
	<script src="{{asset('js/jquery.dataTables.min.js')}}?v=1.0"></script>-->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	<!-- Optional JavaScript; choose one of the two! -->
	<!--<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>-->

	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js" integrity="sha512-XKa9Hemdy1Ui3KSGgJdgMyYlUg1gM+QhL6cnlyTe2qzMCYm4nAZ1PsVerQzTTXzonUR+dmswHqgJPuwCq1MaAg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
	<!-- Option 1: Bootstrap Bundle with Popper -->

	@stack('scripts')
</body>


<script type="text/javascript">
	function emailSubscribe() {
		var email = $('#email_id').val();
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if (email == "" || email == null) {
			$("#errorMsg").html("Please enter email address");
			$("#errorMsg").show();
			return false;
		}
		if (reg.test(email) == false) {
			$("#errorMsg").html("Please enter a valid email");
			$("#errorMsg").show();
			return false;
		}
		Swal.fire('Please wait')
		Swal.showLoading()
		$.ajax({
			url: "{{route('subscribe')}}",
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: {
				email: email
			},

			success: function(data) {
				console.log(data);
				if (data != "") {
					Swal.fire({
						icon: 'success',
						title: 'Thank You',
						showConfirmButton: false,
						timer: 3000
					}, );
					$('#email_id').val("");
				} else

				{
					Swal.fire({
						icon: 'error',
						title: 'This email already exists.',
						showConfirmButton: false,
						timer: 3000
					}, );
				}
				$("#errorMsg").hide();

			}
		});
	}

	function addWishList(pId) {
		console.log(pId);
		$.ajax({
			type: 'POST',
			url: "{{route('add.favorite')}}",
			contentType: "application/json",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: JSON.stringify({
				p_id: pId
			}),
			success: function(success) {
				if (success.data == "save") {
					$('#fav_' + pId).attr('style', 'background-color: #28afb1;');
					toastr["success"]("Add product in favorite list");
				} else {
					$('#fav_' + pId).removeAttr("style");
					toastr["error"]("Remove product in favorite list");
				}
				$('#count_fav').html(success.count);

			}
		});
	}
	function showNotPopup(){
		var modal = $('#notificationModel');
        $(modal).css('width','0px');
		$(modal).modal('show');
	 }
	window.onclick = function(event) {
	  var modal = $('#notificationModel');	
       if (event.target.id != "notificationModel") {
          $(modal).modal('hide');
       }
    }
</script>

</html>