// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyCfe1gDOR1_qD5uzfMSn2c8_KA8xaKFRiY',
    appId: '1:272972853445:web:641ed4b50dd592206600ac',
    messagingSenderId: '272972853445',
    projectId: 'sah-rasan-wala',
    authDomain: 'sah-rasan-wala.firebaseapp.com',
    storageBucket: 'sah-rasan-wala.appspot.com',
    measurementId: 'G-Y95F695472',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyAsZ7VOHwhSF6psBoPi6Vxo_RTIsZ1CHik',
    appId: '1:272972853445:android:640181258d66a9576600ac',
    messagingSenderId: '272972853445',
    projectId: 'sah-rasan-wala',
    storageBucket: 'sah-rasan-wala.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyByeKqmeOzspZt9EIQlSghYMyOc54TTVk0',
    appId: '1:272972853445:ios:366d6b750153e31e6600ac',
    messagingSenderId: '272972853445',
    projectId: 'sah-rasan-wala',
    storageBucket: 'sah-rasan-wala.appspot.com',
    iosBundleId: 'com.sah.rasanwala',
  );
}
