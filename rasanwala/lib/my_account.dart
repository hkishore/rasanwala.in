import 'package:flutter/material.dart';
import 'package:rasanwala/components/all_popus.dart';
import 'components/apis.dart';
import 'components/notify.dart';
import 'globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';
import 'components/add_new_address.dart';

class MyAccount extends StatefulWidget {
  const MyAccount({Key? key}) : super(key: key);

  @override
  _MyAccountState createState() => _MyAccountState();
}

class _MyAccountState extends State<MyAccount> {
  final _nameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _isUploading = false;
  List addresses = [];
  bool gettingaddress = false;
  int addressCnt = 0;

  InputDecoration inputBoxDesign(Icon icon, String hint) {
    return InputDecoration(
      prefixIcon: icon,
      hintText: hint,
    );
  }

  @override
  void initState() {
    super.initState();
    _nameController.text = globals.name.toString();
    _phoneController.text = globals.phoneNo.toString();
    _emailController.text = globals.email.toString();
    _passwordController.text = "";
    gettingaddress = true;
    reload();
  }

  reload() {
    getUserAddresses(globals.userId, showAddresses);
  }

  showAddresses(status, data) {
    if (status) {
      addresses = data;
      addressCnt = data.length;
    } else {
      //  notify(context, "Unable to load addresses", Colors.red);
    }
    setState(() {
      gettingaddress = false;
    });
  }

  userUpdation(bool status, data) async {
    // print(data);
    if (status) {
      globals.name = _nameController.text;
      globals.email = _emailController.text;
      globals.phoneNo = _phoneController.text;
      _passwordController.text = "";

      // write value
      final prefs = await SharedPreferences.getInstance();

      prefs.setString('name', globals.name.toString());
      prefs.setString('email', globals.email.toString());
      prefs.setString('phoneNo', globals.phoneNo.toString());
      setState(() {
        _isUploading = false;
      });
      notify(context, "Information updated successfully", Colors.green);
    } else {
      _nameController.text = globals.name.toString();
      _emailController.text = globals.email.toString();
      _phoneController.text = globals.phoneNo.toString();
      setState(() {
        _isUploading = false;
      });
      notify(context, "Something went wrong", Colors.red);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: globals.colors['headers'],
            floating: true,
            pinned: true,
            snap: true,
            centerTitle: true,
            title: const Text('My Profile'),
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios_new),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Column(
                        children: <Widget>[
                          const SizedBox(height: 15),
                          Form(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                const Padding(
                                  padding: EdgeInsets.only(left: 20, right: 20),
                                  child: Text(
                                    "Account Details",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                // Padding(
                                //   padding:
                                //       const EdgeInsets.only(left: 20, right: 20, top: 20),
                                //   child: SizedBox(
                                //     height: 120,
                                //     child: Stack(
                                //       children: <Widget>[
                                //         Center(
                                //           child: InkWell(
                                //             onTap: _isUploading
                                //                 ? null
                                //                 : () => imagePickerAlert(context, setImage),
                                //             child: CircleAvatar(
                                //               radius: 70.0,
                                //               child: ClipRRect(
                                //                 borderRadius: BorderRadius.circular(300),
                                //                 child: showImage(),
                                //               ),
                                //             ),
                                //           ),
                                //         ),
                                //       ],
                                //     ),
                                //   ),
                                // ),
                                const SizedBox(height: 30),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 15, left: 20, right: 20),
                                  child: TextFormField(
                                    //validator: (value) => nameValidator(value),
                                    controller: _nameController,
                                    decoration: inputBoxDesign(
                                      const Icon(Icons.person,
                                          color: Colors.black),
                                      "Name",
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 15, left: 20, right: 20),
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    //  validator: (value) => phoneValidator(value),
                                    controller: _phoneController,
                                    decoration: inputBoxDesign(
                                      const Icon(Icons.phone_iphone,
                                          color: Colors.black),
                                      "Phone",
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 15, left: 20, right: 20),
                                  child: TextFormField(
                                    keyboardType: TextInputType.emailAddress,
                                    controller: _emailController,
                                    decoration: inputBoxDesign(
                                      const Icon(Icons.mail,
                                          color: Colors.black),
                                      "Email",
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 2, left: 20, right: 20),
                                  child: TextFormField(
                                    // validator: (value) => passwordValidator(value),
                                    controller: _passwordController,
                                    obscureText: true,
                                    decoration: inputBoxDesign(
                                      const Icon(Icons.lock,
                                          color: Colors.black),
                                      "Password",
                                    ),
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(
                                      bottom: 15, left: 20, right: 20),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: Text("Leave it blank for older one"),
                                  ),
                                ),
                                const SizedBox(height: 15),
                                _isUploading
                                    ? const Align(
                                        alignment: Alignment.center,
                                        child: Padding(
                                          padding: EdgeInsets.only(top: 8),
                                          child: CircularProgressIndicator(
                                            semanticsLabel:
                                                'Circular progress indicator',
                                          ),
                                        ),
                                      )
                                    : Align(
                                        alignment: Alignment.center,
                                        child: SizedBox(
                                          width: 100,
                                          child: ElevatedButton(
                                            child: const Text("Update"),
                                            style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty.all<
                                                            Color>(
                                                        globals.colors[
                                                            'headers']!),
                                                shape: MaterialStateProperty.all<
                                                        RoundedRectangleBorder>(
                                                    RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          18.0),
                                                ))),
                                            onPressed: _isUploading
                                                ? null
                                                : () {
                                                    setState(() {
                                                      _isUploading = true;
                                                    });
                                                    if (_nameController.text !=
                                                        '') {
                                                      updateUser({
                                                        'name': _nameController
                                                            .text,
                                                        'email':
                                                            _emailController
                                                                .text,
                                                        'password':
                                                            _passwordController
                                                                .text,
                                                        'phone_no':
                                                            _phoneController
                                                                .text,
                                                      }, userUpdation);
                                                    } else {
                                                      notify(
                                                          context,
                                                          "Name should not blank.",
                                                          Colors.red);
                                                    }
                                                  },
                                          ),
                                        ),
                                      ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Divider(color: Colors.grey),
                    const SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        const Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: Text(
                              "Address",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15, right: 15),
                            child: OutlinedButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            AddNewAddress(reload),
                                      ));
                                },
                                style: OutlinedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  side: BorderSide(
                                    width: 1.0,
                                    color: globals.colors['headers']!,
                                  ),
                                ),
                                child: addressCnt <= 5
                                    ? Text(
                                        "Add New",
                                        style: TextStyle(
                                          color: globals.colors['headers'],
                                          fontSize: 15,
                                        ),
                                      )
                                    : Text(
                                        "You can'not add more thn 5 Address",
                                        style: TextStyle(
                                          color: globals.colors['headers'],
                                          fontSize: 18,
                                        ),
                                      )),
                          ),
                        ),
                      ],
                    ),
                    gettingaddress
                        ? const Column(
                            children: [
                              SizedBox(height: 60),
                              CircularProgressIndicator(
                                semanticsLabel: 'Circular progress indicator',
                              ),
                            ],
                          )
                        : (addresses.isNotEmpty
                            ? ListView.builder(
                                padding: EdgeInsets.zero,
                                itemCount: addresses.length,
                                shrinkWrap: true,
                                physics: const ScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const SizedBox(height: 10),
                                      Container(
                                        margin: const EdgeInsets.only(
                                            top: 5, left: 10, right: 10),
                                        width: double.infinity,
                                        constraints: const BoxConstraints(
                                          maxHeight: double.infinity,
                                          minHeight: 150,
                                        ),
                                        decoration: ShapeDecoration(
                                          color: Colors.white,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            side: const BorderSide(
                                                width: 1, color: Colors.black),
                                          ),
                                        ),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5,
                                                            left: 10,
                                                            right: 10),
                                                    child: Text(
                                                      addresses[index]
                                                          ['first_name'],
                                                      style: const TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5,
                                                            left: 10,
                                                            right: 10),
                                                    child: Text(
                                                      addresses[index]
                                                          ['phone_no'],
                                                      style: const TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  // Padding(
                                                  //   padding:
                                                  //       const EdgeInsets.only(
                                                  //           top: 5,
                                                  //           left: 10,
                                                  //           right: 10),
                                                  //   child: Text(
                                                  //     addresses[index]
                                                  //         ['email_id'],
                                                  //     style: const TextStyle(
                                                  //         fontSize: 16,
                                                  //         fontWeight:
                                                  //             FontWeight.bold),
                                                  //   ),
                                                  // ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5,
                                                            left: 10,
                                                            right: 10),
                                                    child: Text(
                                                      addresses[index]
                                                          ['address_1'],
                                                      style: const TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 15),
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: OutlinedButton(
                                                    onPressed: () {},
                                                    style: OutlinedButton
                                                        .styleFrom(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(30.0),
                                                      ),
                                                      side: const BorderSide(
                                                          color: Colors.red),
                                                    ),
                                                    child: IconButton(
                                                      icon: const Icon(
                                                        Icons.close,
                                                        color: Colors.red,
                                                      ),
                                                      onPressed: () {
                                                        deleteUserAddresses(
                                                          {
                                                            "user_id":
                                                                globals.userId,
                                                            "address_id":
                                                                addresses[index]
                                                                        ['id']
                                                                    .toString(),
                                                          },
                                                          showAddresses,
                                                        );
                                                        setState(() {
                                                          gettingaddress = true;
                                                        });
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  );
                                })
                            : notFoundIcon("You don't add any address yet!!")),
                    const SizedBox(height: 40),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  // Widget showImage() {
  //   if (_imagePath != "" && !_isRemoved) {
  //     return Image.file(File(_imagePath),
  //         width: 130.0, height: 130.0, fit: BoxFit.cover);
  //     // } else if (_imagePath == "" &&
  //     //     !_isRemoved &&
  //     //     (globals.profile_photo != null && globals.profile_photo != "")) {
  //     //   return Image.network(
  //     //     globals.profile_photo,
  //     //     width: 130.0,
  //     //     height: 130.0,
  //     //     fit: BoxFit.cover,
  //     //   );
  //   } else {
  //     return const Icon(Icons.person, size: 90);
  //   }
  // }
}
