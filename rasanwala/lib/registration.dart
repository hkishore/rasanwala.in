import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'globals.dart' as globals;
import 'components/notify.dart';
import 'components/apis.dart';
import 'login.dart';

class Registration extends StatefulWidget {
  final Function updateAuth;
  const Registration(this.updateAuth, {Key? key}) : super(key: key);

  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  // final TextEditingController _emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool isLoading = false;
  String deviceId = "";
  String deviceType = "";

  InputDecoration inputBoxDesign(Icon icon, String fName, String hint) {
    return InputDecoration(
      prefixIcon: icon,
      label: Text(fName),
      hintText: hint,
    );
  }

  void update(bool status, data) {
    if (status) {
      widget.updateAuth(data);
      Navigator.of(context).pop();
    } else {
      notify(context, data, Colors.red);
    }
    setState(() {
      isLoading = false;
    });
  }

  void setIdAndType() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      deviceId = iosDeviceInfo.identifierForVendor!; // unique ID on iOS
      deviceType = "ios";
    } else if (Platform.isAndroid) {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      deviceId = androidDeviceInfo.id; // unique ID on Android
      deviceType = "android";
    }
  }

  @override
  void initState() {
    super.initState();
    setIdAndType();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: true,
          title: const Text('Sign Up'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            Column(
              children: <Widget>[
                Container(
                  height: 150.0,
                  margin: const EdgeInsets.all(10),
                  child: Stack(
                    children: <Widget>[
                      Positioned.fill(
                          child: Image.asset(
                        "images/pngLogo.png",
                        fit: BoxFit.contain,
                      )),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                      left: 20, top: 20, right: 20, bottom: 20),
                  // height: 400.0,
                  width: double.infinity,
                  constraints: const BoxConstraints(
                    maxHeight: double.infinity,
                    minHeight: 430,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        const Padding(
                          padding:
                              EdgeInsets.only(left: 20, right: 20, top: 20),
                          child: Text(
                            "Create your account !!",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        const SizedBox(height: 30),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 15, left: 20, right: 20),
                          child: TextFormField(
                            validator: (value) => nameValidator(value),
                            controller: _nameController,
                            decoration: inputBoxDesign(
                              const Icon(Icons.person, color: Colors.black),
                              "Full Name",
                              "Enter your name",
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 15, left: 20, right: 20),
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            validator: (value) => phoneValidator(value),
                            controller: _phoneController,
                            decoration: inputBoxDesign(
                              const Icon(Icons.phone_iphone,
                                  color: Colors.black),
                              "Phone No",
                              "Enter your phone no",
                            ),
                          ),
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.only(
                        //       bottom: 15, left: 20, right: 20),
                        //   child: TextFormField(
                        //     keyboardType: TextInputType.emailAddress,
                        //     controller: _emailController,
                        //     decoration: inputBoxDesign(
                        //       const Icon(Icons.mail, color: Colors.black),
                        //       "Email",
                        //       "Enter your email",
                        //     ),
                        //   ),
                        // ),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 15, left: 20, right: 20),
                          child: TextFormField(
                            validator: (value) => passwordValidator(value),
                            controller: _passwordController,
                            obscureText: true,
                            decoration: inputBoxDesign(
                              const Icon(Icons.lock, color: Colors.black),
                              "Password",
                              "Enter your password",
                            ),
                          ),
                        ),
                        const SizedBox(height: 15),
                        isLoading
                            ? const Padding(
                                padding: EdgeInsets.only(top: 8),
                                child: CircularProgressIndicator(
                                  semanticsLabel: 'Circular progress indicator',
                                ),
                              )
                            : SizedBox(
                                width: 100,
                                child: ElevatedButton(
                                  child: const Text("SignUp"),
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              globals.colors['headers']!),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                      ))),
                                  onPressed: () {
                                    var status =
                                        _formKey.currentState?.validate();
                                    if (status != null && status) {
                                      setState(() {
                                        isLoading = true;
                                      });
                                      register({
                                        'name': _nameController.text,
                                        'phone_no': _phoneController.text,
                                        'device_id': deviceId,
                                        'device_type': deviceType,
                                        'password': _passwordController.text,
                                      }, update);
                                    }
                                  },
                                ),
                              ),
                        const SizedBox(height: 15),
                        RichText(
                          text: TextSpan(children: [
                            const TextSpan(
                                text: "Already have an account ? ",
                                style: TextStyle(color: Colors.black)),
                            TextSpan(
                              text: "Login",
                              style:
                                  TextStyle(color: globals.colors['headers']),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            LoginPage(widget.updateAuth)),
                                  );
                                },
                            ),
                          ]),
                        ),
                        const SizedBox(height: 30)
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ]),
        ),
      ]),
    );
  }

  // validators ----------------
  nameValidator(value) {
    if (value == null || value.isEmpty) {
      return "Name can't be empty";
    }
    return null;
  }

  phoneValidator(value) {
    String pattern = r'^\+?[6-9][0-9]{9}$';
    RegExp regExp = RegExp(pattern);
    if (value == null || value.isEmpty) {
      return "Phone can't be empty";
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid mobile number';
    }
    return null;
  }

  passwordValidator(value) {
    if (value == null || value.isEmpty) {
      return "Password can't be empty";
    } else if (value.length < 6) {
      return "Password must be atleast 6 character";
    }
    return null;
  }
}
