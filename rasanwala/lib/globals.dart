// dynamic colors ---------------
import 'package:flutter/material.dart';

//url
String url = "https://rasanwala.in/api";
//String url = "192.168.1.8:8000/api";

final Map<String, Color> colors = {};
updateTheme() {
  colors['headers'] = const Color(0xFFFFC680);
  colors['body'] = const Color(0xffffffff);
  colors['icons'] = const Color(0xffffffff);
  colors['text'] = const Color(0xff000000);
}

// login infos -----------------
String? apiToken;
String? userId;
String? name;
String? phoneNo;
String? email;
String? role;

//errors message
const String unKnownError = "Something went wrong. Please try after some time.";

// setstates
Function? homeSetState;
Function? logout;

//badge count related
int cartItemCount = 0;
int favItemCount = 0;

//order qty
int maxOrder = 10;
