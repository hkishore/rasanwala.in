import 'dart:convert';
// import 'dart:ffi';
// import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'components/apis.dart';
import 'components/notify.dart';
import 'globals.dart' as globals;
import 'components/addresses.dart';
import 'components/all_popus.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:intl/intl.dart';

class CheckOut extends StatefulWidget {
  final List cartitems;
  final Function refreshCart;
  const CheckOut(this.cartitems, this.refreshCart, {Key? key})
      : super(key: key);

  @override
  State<CheckOut> createState() => _CheckOutState();
}

class _CheckOutState extends State<CheckOut> {
  String _selectedShipMode = "self";
  String _selectedPayMode = "cod";
  double totalPrice = 0;
  double shippingCharge = 0;
  bool gettingaddress = false;
  String shipName = "";
  String shipPhone = "";
  String shipEmail = "";
  String shipAddress = "";
  String shipId = "";
  String shipArea = "";
  int shippingData = 0;
  List areas = [];
  String payMode = "";
  String homeDelivery = "";
  String minOrdAmt = "";
  String razorpayKey = "";
  String razorpaySecret = "";
  String selfPickupTime = "";
  String currtime = "";
  String payOnDelivery = "off";
  String freeship = "off";
  String hometext = "";
  String shippingdata = "";
  final _nameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();
  final _othersController = TextEditingController();
  final _addressController = TextEditingController();
  final _noteController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  int? _locationController;
  bool ordering = false;
  late Razorpay _razorpay;

  InputDecoration inputBoxDesign(Icon icon, String hint) {
    return InputDecoration(
      prefixIcon: icon,
      hintText: hint,
    );
  }

  @override
  void initState() {
    super.initState();
    getUserAddresses(globals.userId, showAddresses);
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
    gettingaddress = true;
    getMinAmount(_updateIndex);
  }

  void _updateIndex(data) {
    setState(() {
      hometext = data;
    });
  }

  void setArea(status, data) {
    if (status) {
      areas = data;
      _locationController = areas[0]['id'];
      getShipCharge(areas[0]['id'], setShippingcharge);
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      gettingaddress = false;
    });
  }

  timeStr(msg) {
    DateTime date1 = DateTime.now();
    currtime = DateFormat("HH").format(date1);
    if (int.parse(currtime) > int.parse(selfPickupTime)) {
      if (msg == 'home') {
        warningPopup(context,
            "We deliver your order Tomorrow. Do you want to Continue ?");
      } else {
        warningPopup(context,
            "Today Self pickup timing is over you can collect your  order Tomorrow. Continue?");
      }
      return false;
    } else {
      return true;
    }
  }

  warningPopup(context, msg) {
    return showDialog(
        // barrierDismissible: false,
        context: context,
        builder: (BuildContext ctx) {
          return AlertDialog(
            title: Text(msg),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text('Close'),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  if (totalPrice == 0) {
                    notify(context, globals.unKnownError, Colors.red);
                    return;
                  }
                  if (totalPrice > int.parse(minOrdAmt)) {
                    var status = _formKey.currentState?.validate();
                    if ((status != null && status) ||
                        shipId != "" ||
                        _selectedShipMode != "home") {
                      lodingPopup(context);
                      _selectedPayMode == "cod"
                          ? orderOnCod()
                          : openPaymentGateway();
                    } else {
                      notify(
                          context, "Plesse fil the address form", Colors.red);
                    }
                  } else {
                    notify(context, "Minimum order amount is ₹ " + minOrdAmt,
                        Colors.red);
                  }
                },
                child: const Text('Continue'),
              )
            ],
          );
        });
  }

  showAddresses(status, data) async {
    if (status) {
      int i;
      for (i = 0; i < data.length; i++) {
        if (data[i]['last_selected'] == 1) {
          setState(() {
            shipId = data[i]['id'].toString();
            shipArea = data[i]['area_id'].toString();
            shipName = data[i]['first_name'];
            shipPhone = data[i]['phone_no'].toString();
            //shipEmail = data[i]['email_id'];
            shipAddress = data[i]['address_1'];
          });
          await getShipCharge(data[i]['area_id'], setShippingcharge);
          break;
        }
      }
    } else {
      getShippingArea(setArea);
    }
    setState(() {
      gettingaddress = false;
    });
  }

  setShippingcharge(status, data) {
    if (status) {
      shippingData = int.parse(data['shippingCharge']);
      payMode = data['payMode'];
      homeDelivery = data['homeDelivery'];
      minOrdAmt = data['minOrdAmt'];
      razorpayKey = data['razorpayKey'];
      razorpaySecret = data['razorpaySecret'];
      selfPickupTime = data['selfPickupTime'];
      selfPickupTime = selfPickupTime.split("-")[1];
      if (data['freeShipEnable'] != null && data['freeShipEnable'] != "") {
        freeship = data['freeShipEnable'];
      }
      if (data['payOnDelivery'] != null && data['payOnDelivery'] != "") {
        payOnDelivery = data['payOnDelivery'];
      }
      // timeStr();
      allItemsPrice();
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
  }

  void allItemsPrice() {
    setState(() {
      totalPrice = 0;
    });
    for (int i = 0; i < widget.cartitems.length; i++) {
      totalPrice = totalPrice + double.parse(widget.cartitems[i]['amount']);
    }
    shippingdata = shippingData.toString();
    _selectedShipMode == "self" || freeship == "on"
        ? shippingCharge = 0.0
        : shippingCharge = double.parse(shippingdata);
    totalPrice = totalPrice + shippingCharge;
  }

  void refreshpage() {
    setState(() {
      gettingaddress = true;
      getUserAddresses(globals.userId, showAddresses);
    });
  }

  afterCheckout(status, msg) {
    widget.refreshCart();
    Navigator.of(context).pop();
    Navigator.of(context).pop();
    if (status) {
      notify(context, msg, Colors.green);
      //sendOrderNotification();
      setcartItemCount();
    } else {
      notify(context, msg, Colors.red);
    }
    setState(() {
      ordering = false;
    });
  }

  // Future sendOrderNotification() async {
  //   String orderInfo = "";
  //   int i = 1;
  //   for (var index in widget.cartitems) {
  //     // print(index);
  //     orderInfo += "\n" +
  //         i.toString() +
  //         ") " +
  //         index['proName'] +
  //         " (" +
  //         index['variantSize'] +
  //         ")";
  //     i++;
  //   }
  //   orderInfo = "\nUser id=" +
  //       globals.userId.toString() +
  //       "\nUser Name: " +
  //       globals.name.toString() +
  //       "\nAction: Order Recived" +
  //       "\n" +
  //       "\nOrder Details :\n" +
  //       orderInfo +
  //       "\n"
  //           "\n Total Order Amount:" +
  //       totalPrice.toString() +
  //       "\n Delivery Type: " +
  //       _selectedShipMode +
  //       "\n Payment Mode: " +
  //       _selectedPayMode;
  //   try {
  //     await http.post(Uri.parse(
  //         'https://api.telegram.org/bot5437461604:AAFPF7H5FcRz3JbNC2m1hu3ewLP2oV1pGFY/sendmessage?chat_id=-1001816846995&text=' +
  //             orderInfo));
  //   } catch (err) {
  //     // print(err);
  //   }
  // }

  void setcartItemCount() async {
    final prefs = await SharedPreferences.getInstance();
    globals.cartItemCount = 0;
    prefs.setInt('cartItemCount', globals.cartItemCount);
    setState(() {});
  }

  void openPaymentGateway() async {
    var response =
        await getRazorPayOrderId(totalPrice * 100, razorpayKey, razorpaySecret);
    // String orderId = response.body.split(',')[0].split(":")[1];
    String orderId = jsonDecode(response.body)['id'];
    // orderId = orderId.substring(1, orderId.length - 1);
    var options = {
      'key': razorpayKey,
      'amount': totalPrice * 100,
      "currency": "INR",
      "order_id": orderId,
      'name': shipName,
      'description': 'Payment',
      'prefill': {
        'contact': shipPhone
      }, //'email': shipEmail removed by me because email is not used in addrees
      // 'external': {
      //   'wallets': ['paytm']
      // }
    };
    try {
      _razorpay.open(options);
      //  sendErrors(options.toString(), "OPTION");
    } catch (e) {
      // print(e);
      //sendErrors(e.toString(), "catchERoro");
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    // sendErrors({
    //   "razorpay_payment_id": response.paymentId,
    //   "razorpay_order_id": response.orderId,
    //   "razorpay_signature": response.signature,
    // }, "paymentsuccess171");
    shipId != "" && globals.userId != null
        ? checkout({
            "user_id": globals.userId,
            "area_id": shipArea,
            "address_id": shipId,
            "payment_type": "razorpay",
            "razorpay_payment_id": response.paymentId,
            "razorpay_order_id": response.orderId,
            "razorpay_signature": response.signature,
            "delivery_type": _selectedShipMode,
            "totalamount": totalPrice,
            "shipemail": shipEmail,
            "shipCharge": shippingCharge.toString(),
            "note": _noteController.text,
            "name": globals.name,
          }, afterCheckout)
        : checkout({
            "user_id": globals.userId,
            "name": _nameController.text,
            "email_id": _emailController.text,
            "phone": _phoneController.text,
            "area_id": _locationController,
            "address": _addressController.text,
            "others": _othersController.text,
            "last_selected": "1",
            "payment_type": "razorpay",
            "razorpay_payment_id": response.paymentId,
            "razorpay_order_id": response.orderId,
            "razorpay_signature": response.signature,
            "totalamount": totalPrice,
            "shipemail": shipEmail,
            "delivery_type": _selectedShipMode,
            "shipCharge": shippingCharge.toString(),
            "note": _noteController.text,
          }, afterCheckout);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Navigator.of(context).pop();
    notify(
        context,
        "You may have cancelled the payment or there was a delay in response from the selected payment method.",
        Colors.red);
    sendErrors({
      "code": response.code,
      "msg": response.message,
    }, "paymentErr207");
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Navigator.of(context).pop();
    notify(context, "Please try again with different method.", Colors.red);
    sendErrors({
      "code": response.walletName,
    }, "externalWalletErr211");
  }

  void orderOnCod() {
    ordering
        ? null
        : (shipId != "" && globals.userId != null
            ? checkout({
                "user_id": globals.userId,
                "area_id": shipArea,
                "address_id": shipId,
                "payment_type": "cod",
                "delivery_type": _selectedShipMode,
                "shipCharge": shippingCharge.toString(),
                "note": _noteController.text,
                "totalamount": totalPrice,
                "name": globals.name,
              }, afterCheckout)
            : checkout({
                "user_id": globals.userId,
                "name": _nameController.text,
                "email_id": _emailController.text,
                "phone": _phoneController.text,
                "address": _addressController.text,
                "others": _othersController.text,
                "area_id": _locationController,
                "payment_type": "cod",
                "last_selected": "1",
                "delivery_type": _selectedShipMode,
                "shipCharge": shippingCharge.toString(),
                "note": _noteController.text,
                "totalamount": totalPrice,
              }, afterCheckout));
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: globals.colors['headers'],
            floating: true,
            pinned: true,
            snap: true,
            centerTitle: true,
            title: const Text('Checkout'),
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios_new),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              gettingaddress
                  ? const Column(
                      children: [
                        SizedBox(height: 250),
                        CircularProgressIndicator(
                          semanticsLabel: 'Circular progress indicator',
                        ),
                      ],
                    )
                  : Stack(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding:
                                  EdgeInsets.only(top: 20, left: 10, right: 10),
                              child: Text(
                                "Shipping Mode",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: ListTile(
                                    leading: Radio<String>(
                                      value: 'self',
                                      groupValue: _selectedShipMode,
                                      onChanged: (value) {
                                        setState(() {
                                          _selectedShipMode = value!;
                                          shippingCharge = 0.0;
                                          _selectedPayMode = "cod";
                                          allItemsPrice();
                                        });
                                      },
                                    ),
                                    title: const Text('Self Pickup'),
                                  ),
                                ),
                                homeDelivery == "1"
                                    ? Expanded(
                                        child: ListTile(
                                          leading: Radio<String>(
                                            value: 'home',
                                            groupValue: _selectedShipMode,
                                            onChanged: (value) {
                                              setState(() {
                                                _selectedShipMode = value!;
                                                shippingCharge = shippingCharge;
                                                _selectedPayMode = "razorpay";
                                                allItemsPrice();
                                              });
                                            },
                                          ),
                                          title: const Text('Home Delivery'),
                                        ),
                                      )
                                    : const SizedBox(width: 0),
                              ],
                            ),
                            _selectedShipMode == "home"
                                ? const Padding(
                                    padding: EdgeInsets.only(
                                        top: 20, left: 10, right: 10),
                                    child: Text(
                                      "Shipping Address",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  )
                                : const SizedBox(width: 0),
                            _selectedShipMode == "home"
                                ? Container(
                                    margin: const EdgeInsets.only(
                                        top: 5, left: 10, right: 10),
                                    width: double.infinity,
                                    constraints: const BoxConstraints(
                                      maxHeight: double.infinity,
                                      minHeight: 150,
                                    ),
                                    decoration: ShapeDecoration(
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        side: const BorderSide(
                                            width: 1, color: Colors.black),
                                      ),
                                    ),
                                    child: shipId != "" &&
                                            globals.userId != null
                                        ? Row(
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5,
                                                            left: 10,
                                                            right: 10),
                                                    child: Text(
                                                      shipName,
                                                      style: const TextStyle(
                                                        fontSize: 16,
                                                      ),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5,
                                                            left: 10,
                                                            right: 10),
                                                    child: Text(
                                                      shipPhone,
                                                      style: const TextStyle(
                                                        fontSize: 16,
                                                      ),
                                                    ),
                                                  ),
                                                  // Padding(
                                                  //   padding:
                                                  //       const EdgeInsets.only(
                                                  //           top: 5,
                                                  //           left: 10,
                                                  //           right: 10),
                                                  //   child: Text(
                                                  //     shipEmail,
                                                  //     style: const TextStyle(
                                                  //       fontSize: 16,
                                                  //     ),
                                                  //   ),
                                                  // ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5,
                                                            left: 10,
                                                            right: 10),
                                                    child: Text(
                                                      shipAddress,
                                                      style: const TextStyle(
                                                        fontSize: 16,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 15),
                                                  child: Align(
                                                    alignment:
                                                        Alignment.centerRight,
                                                    child: OutlinedButton(
                                                      onPressed: () {
                                                        Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder: (context) =>
                                                                  Addresses(
                                                                      refreshpage),
                                                            ));
                                                      },
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape:
                                                            RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      30.0),
                                                        ),
                                                        side: const BorderSide(
                                                            color: Colors.blue),
                                                      ),
                                                      child: Text(
                                                        "Change",
                                                        style: TextStyle(
                                                            color:
                                                                globals.colors[
                                                                    'headers']),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )
                                        : unauthorizedForm(),
                                  )
                                : const SizedBox(height: 0),

                            Padding(
                              padding: const EdgeInsets.only(
                                  bottom: 15, top: 20, left: 10, right: 10),
                              child: TextFormField(
                                maxLines: 3,
                                validator: (value) => nameValidator(value),
                                controller: _noteController,
                                decoration: InputDecoration(
                                  label: const Text('Notes (optional)'),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                ),
                              ),
                            ),
                            const Divider(
                              color: Colors.grey,
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(top: 20, left: 10, right: 10),
                              child: Text(
                                "Item Details",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ),
                            const Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.all(10),
                                      child: Text(
                                        "Product",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 7, left: 10, right: 10),
                                      child: Text(
                                        "Price",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            ListView.builder(
                                padding: EdgeInsets.zero,
                                itemCount: widget.cartitems.length,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return Column(
                                    children: [
                                      const Divider(color: Colors.grey),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 7,
                                                          left: 15,
                                                          right: 10),
                                                  child: Text(
                                                    widget.cartitems[index]
                                                            ['proName'] +
                                                        "(" +
                                                        widget.cartitems[index]
                                                            ['variantSize'] +
                                                        ")" +
                                                        " × " +
                                                        widget.cartitems[index]
                                                                ['qty']
                                                            .toString(),
                                                    style: const TextStyle(
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 7,
                                                          left: 20,
                                                          right: 10),
                                                  child: Text(
                                                    widget.cartitems[index]
                                                            ['amount']
                                                        .toString(),
                                                    style: const TextStyle(
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  );
                                }),
                            const SizedBox(height: 7),
                            const Divider(
                              color: Colors.black,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: 7, left: 10, right: 10),
                                        child: Text(
                                          "Shipping Charge",
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 7, left: 10, right: 10),
                                        child: Text(
                                          shippingCharge.toString(),
                                          style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.blue),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            // ignore: prefer_const_constructors
                            SizedBox(height: 10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 7, left: 10, right: 10),
                                        child: Text(
                                          // ignore: unnecessary_brace_in_string_interps
                                          "(For Free Delivery Order amount should be greater than ₹ ${hometext} )",
                                          style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.red),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const Expanded(
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: 7, left: 10, right: 10),
                                        child: Text(
                                          "",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.blue),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const Divider(
                              color: Colors.black,
                            ),
                            const SizedBox(height: 10),
                            Row(
                              children: [
                                const Expanded(
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: 7, left: 10, right: 10),
                                        child: Text(
                                          "Total",
                                          style: TextStyle(
                                            fontSize: 18,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 7, left: 10, right: 10),
                                        child: Text(
                                          "₹ " + totalPrice.toString(),
                                          style: const TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.red),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 10),
                            const Divider(
                              color: Colors.black,
                            ),

                            payMode != "disable" && _selectedShipMode == 'home'
                                ? ListTile(
                                    visualDensity: const VisualDensity(
                                        horizontal: 0, vertical: -4),
                                    leading: Radio<String>(
                                      value: 'razorpay',
                                      groupValue: _selectedPayMode,
                                      onChanged: (value) {
                                        setState(() {
                                          _selectedPayMode = value!;
                                        });
                                      },
                                    ),
                                    title: const Text('Pay Online'),
                                  )
                                : const SizedBox(height: 0),
                            _selectedShipMode == 'self' || payOnDelivery == "on"
                                ? ListTile(
                                    visualDensity: const VisualDensity(
                                        horizontal: 0, vertical: -4),
                                    leading: Radio<String>(
                                      value: 'cod',
                                      groupValue: _selectedPayMode,
                                      onChanged: (value) {
                                        setState(() {
                                          _selectedPayMode = value!;
                                        });
                                      },
                                    ),
                                    title: const Text('Cash On Delivery'),
                                  )
                                : const SizedBox(width: 0),
                            // ListTile(
                            //   visualDensity:
                            //       const VisualDensity(horizontal: 0, vertical: -4),
                            //   leading: Radio<String>(
                            //     value: 'cashfree',
                            //     groupValue: _selectedPayMode,
                            //     onChanged: (value) {
                            //       setState(() {
                            //         _selectedPayMode = value!;
                            //       });
                            //     },
                            //   ),
                            //   title: const Text('Pay online with Cashfree'),
                            // ),
                          ],
                        ),
                      ],
                    ),
              gettingaddress
                  ? const SizedBox(height: 0)
                  : Padding(
                      padding:
                          const EdgeInsets.only(top: 30, left: 10, right: 10),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: globals.colors['headers'],
                          // minimumSize: const.fromHeight(50), // NEW
                        ),
                        onPressed: () {
                          if (!timeStr(_selectedShipMode)) {
                            return;
                          }
                          if (totalPrice == 0) {
                            notify(context, globals.unKnownError, Colors.red);
                            return;
                          }
                          if (totalPrice > int.parse(minOrdAmt)) {
                            var status = _formKey.currentState?.validate();
                            if ((status != null && status) ||
                                shipId != "" ||
                                _selectedShipMode != "home") {
                              lodingPopup(context);
                              _selectedPayMode == "cod"
                                  ? orderOnCod()
                                  : openPaymentGateway();
                            } else {
                              notify(context, "Plesse fil the address form",
                                  Colors.red);
                            }
                          } else {
                            notify(
                                context,
                                "Minimum order amount is ₹ " + minOrdAmt,
                                Colors.red);
                          }
                        },
                        child: const Text(
                          "Proceed to Checkout",
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                    ),
              const SizedBox(height: 30),
            ]),
          ),
        ],
      ),
    );
  }

  Widget unauthorizedForm() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 15),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 15, left: 20, right: 20),
                      child: TextFormField(
                        validator: (value) => nameValidator(value),
                        controller: _nameController,
                        decoration: inputBoxDesign(
                          const Icon(Icons.person, color: Colors.black),
                          "Name",
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 15, left: 20, right: 20),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        validator: (value) => phoneValidator(value),
                        controller: _phoneController,
                        decoration: inputBoxDesign(
                          const Icon(Icons.phone_iphone, color: Colors.black),
                          "Phone",
                        ),
                      ),
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.only(
                    //       bottom: 15, left: 20, right: 20),
                    //   child: TextFormField(
                    //     validator: (value) => emailValidator(value),
                    //     keyboardType: TextInputType.emailAddress,
                    //     controller: _emailController,
                    //     decoration: inputBoxDesign(
                    //       const Icon(Icons.mail, color: Colors.black),
                    //       "Email",
                    //     ),
                    //   ),
                    // ),
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 15, left: 20, right: 20),
                      child: TextFormField(
                        validator: (value) => streetValidator(value),
                        controller: _addressController,
                        decoration: inputBoxDesign(
                          const Icon(Icons.home, color: Colors.black),
                          "House No. and Street Name",
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 15, left: 20, right: 20),
                      child: DropdownButtonFormField<dynamic>(
                        isExpanded: true,
                        items: areaDropdown(),
                        value: _locationController,
                        onChanged: (value) {
                          _locationController = value;
                          getShipCharge(_locationController, setShippingcharge);
                        },
                        decoration: inputBoxDesign(
                          const Icon(Icons.location_city, color: Colors.black),
                          "Select a Area",
                        ),
                      ),
                    ),
                    _locationController == 0
                        ? Padding(
                            padding: const EdgeInsets.only(
                                bottom: 15, left: 20, right: 20),
                            child: TextFormField(
                              controller: _othersController,
                              decoration: inputBoxDesign(
                                const Icon(Icons.alt_route_sharp,
                                    color: Colors.black),
                                "Location",
                              ),
                            ),
                          )
                        : const SizedBox(height: 0),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  // validators ----------------
  nameValidator(value) {
    if (value == null || value.isEmpty) {
      return "Name can't be empty";
    }
    return null;
  }

  phoneValidator(value) {
    if (value == null || value.isEmpty) {
      return "Phone can't be empty";
    }
    return null;
  }

  emailValidator(value) {
    if (value == null || value.isEmpty) {
      return "Email can't be empty";
    }
    return null;
  }

  streetValidator(value) {
    if (value == null || value.isEmpty) {
      return "Street and House No. can't be empty";
    }
    return null;
  }

  locationValidator(value) {
    if (value == null || value.isEmpty) {
      return "Location can't be empty";
    }
    return null;
  }

  List<DropdownMenuItem> areaDropdown() {
    return areas.isNotEmpty
        ? [
            ...areas.map((e) {
              return DropdownMenuItem(
                child: Text(e['area']),
                value: e['id'],
              );
            }),
            const DropdownMenuItem(
              child: Text("Others"),
              value: 0,
            ),
          ]
        : [
            const DropdownMenuItem(
              child: Text("Area unavilable"),
              value: 0,
            )
          ];
  }
}
