import 'dart:convert';
import 'package:flutter/material.dart';
import 'notify.dart';
import '../globals.dart' as globals;
import 'apis.dart';
import 'all_popus.dart';

class OrdersPage extends StatefulWidget {
  const OrdersPage({Key? key}) : super(key: key);

  @override
  State<OrdersPage> createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  bool _loading = false;
  List orders = [];
  List orderDetails = [];
  double totalPrice = 0;
  double shipCharge = 0;
  @override
  void initState() {
    getOrders({
      "user_id": globals.userId,
    }, ordersLsit);
    super.initState();
    setState(() {
      _loading = true;
    });
  }

  void ordersLsit(status, data) {
    if (status) {
      setState(() {
        orders = data;
      });
    } else {
      // notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      _loading = false;
    });
  }

  void orderitems(orderId) async {
    lodingPopup(context);
    var response = await getOrderItems(orderId);
    setState(() {
      orderDetails = jsonDecode(response.body)['data'];
      totalPrice = 0;
      shipCharge = 0;
    });
    if (orderDetails.isNotEmpty) {
      for (int i = 0; i < orderDetails.length; i++) {
        if (orderDetails[i]['shipCharge'] != null &&
            orderDetails[i]['shipCharge'] != "") {
          shipCharge = double.parse(orderDetails[i]['shipCharge']);
        }
        totalPrice = totalPrice + double.parse(orderDetails[i]['amount']);
      }
      totalPrice = totalPrice + shipCharge;
      Navigator.of(context).pop();
      orderDetailsPopup(context, orderDetails, totalPrice, shipCharge);
    } else {
      Navigator.of(context).pop();
      notify(context, globals.unKnownError, Colors.red);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: true,
          title: const Text("Orders"),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            _loading
                ? const Column(
                    children: [
                      SizedBox(height: 250),
                      CircularProgressIndicator(
                        semanticsLabel: 'Circular progress indicator',
                      ),
                    ],
                  )
                : (orders.isNotEmpty
                    ? ListView.builder(
                        itemCount: orders.length,
                        shrinkWrap: true,
                        padding: EdgeInsets.zero,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: orders[index]['status'] == 1
                                        ? Colors.blue
                                        : (orders[index]['status'] == 2
                                            ? Colors.yellow
                                            : (orders[index]['status'] == 3
                                                ? Colors.green
                                                : Colors.red)),
                                    width: 2),
                                borderRadius: BorderRadius.circular(4),
                              ),
                              elevation: 3.0,
                              color: const Color.fromARGB(255, 186, 177, 188),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: ListTile(
                                  leading: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const SizedBox(height: 7),
                                      Text(
                                        "Order Id : " +
                                            orders[index]['id'].toString(),
                                        style: const TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14),
                                      ),
                                      const SizedBox(height: 10),
                                      Text(
                                        "Total Items : " +
                                            orders[index]['itemCount']
                                                .toString(),
                                        style: const TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14),
                                      ),
                                    ],
                                  ),
                                  title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Order Date : " +
                                            orders[index]['order_date']
                                                .toString(),
                                        style: const TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14),
                                      ),
                                      const SizedBox(height: 10),
                                      Row(
                                        children: [
                                          const Text(
                                            "Status : ",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 14),
                                          ),
                                          Expanded(
                                            child: Text(
                                              orders[index]['ordStatus'],
                                              style: TextStyle(
                                                  color: orders[index]
                                                              ['status'] ==
                                                          1
                                                      ? Colors.blue
                                                      : (orders[index]
                                                                  ['status'] ==
                                                              2
                                                          ? Colors.yellow
                                                          : (orders[index][
                                                                      'status'] ==
                                                                  3
                                                              ? Colors.green
                                                              : Colors.red)),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  trailing: ElevatedButton(
                                    child: const Text("Details"),
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              globals.colors['headers']!),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                      )),
                                    ),
                                    onPressed: () {
                                      orderitems(orders[index]['id']);
                                    },
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    : notFoundIcon("Order history is empty!!")),
          ]),
        ),
      ]),
    );
  }
}
