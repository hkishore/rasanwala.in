import 'package:flutter/material.dart';
import 'package:rasanwala/components/all_popus.dart';
import '../globals.dart' as globals;
import 'notify.dart';
import 'apis.dart';

class FAQPage extends StatefulWidget {
  const FAQPage({Key? key}) : super(key: key);

  @override
  FAQPageState createState() => FAQPageState();
}

class FAQPageState extends State<FAQPage> {
  bool _loading = false;
  List faqsList = [];
  int selected = 0;

  @override
  void initState() {
    super.initState();
    _loading = true;
    getFaqs(faqs);
  }

  void faqs(status, data) {
    if (status) {
      faqsList = data;
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: globals.colors['headers'],
            floating: true,
            pinned: true,
            snap: true,
            centerTitle: true,
            title: const Text('FAQs'),
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios_new),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              _loading
                  ? const Column(
                      children: [
                        SizedBox(height: 250),
                        CircularProgressIndicator(
                          semanticsLabel: 'Circular progress indicator',
                        ),
                      ],
                    )
                  : (faqsList.isNotEmpty
                      ? ListView.builder(
                          padding: EdgeInsets.zero,
                          key: Key('builder ${selected.toString()}'),
                          itemCount: faqsList.length,
                          shrinkWrap: true,
                          physics: const ScrollPhysics(),
                          itemBuilder: (context, index) {
                            return Column(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.grey.shade200,
                                          offset: const Offset(1.0, 1.0),
                                          spreadRadius: 0.2)
                                    ],
                                  ),
                                  child: Card(
                                    elevation: 0,
                                    shadowColor: Colors.grey,
                                    margin: const EdgeInsets.only(
                                      bottom: 3,
                                    ),
                                    child: ExpansionTile(
                                      key: Key(index.toString()),
                                      backgroundColor: const Color(0xfff6f7f9),
                                      initiallyExpanded: index == selected,
                                      iconColor: Colors.grey,
                                      title: Text(
                                        faqsList[index]['question'],
                                        style: const TextStyle(
                                            color: Color(0xFF444444),
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 10.0,
                                              bottom: 10,
                                              left: 17,
                                              right: 17),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: Text(
                                                  faqsList[index]['answer'],
                                                  style: const TextStyle(
                                                    color: Color(0xFF444444),
                                                    fontSize: 13,
                                                  ),
                                                  textAlign: TextAlign.start,
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                      onExpansionChanged: ((newState) {
                                        if (newState) {
                                          setState(() {
                                            selected = index;
                                          });
                                        } else {
                                          setState(() {
                                            selected = -1;
                                          });
                                        }
                                      }),
                                    ),
                                  ),
                                ),
                              ],
                            );
                          })
                      : notFoundIcon("FAQs Not Avilable"))
            ]),
          )
        ],
      ),
    );
  }
}
