import 'package:flutter/material.dart';
import 'add_new_address.dart';
import 'apis.dart';
import 'notify.dart';
import '../globals.dart' as globals;

class Addresses extends StatefulWidget {
  final Function refreshCheckout;
  const Addresses(this.refreshCheckout, {Key? key}) : super(key: key);

  @override
  State<Addresses> createState() => _AddressesState();
}

class _AddressesState extends State<Addresses> {
  List addresses = [];
  bool gettingaddress = false;
  String _selectedaddress = "0";
  @override
  void initState() {
    super.initState();
    gettingaddress = true;
    reload();
  }

  reload() {
    getUserAddresses(globals.userId, showAddresses);
  }

  setRadioId() {
    int i;
    for (i = 0; i < addresses.length; i++) {
      if (addresses[i]['last_selected'] == 1) {
        setState(() {
          _selectedaddress = addresses[i]['id'].toString();
        });
      }
    }
  }

  showAddresses(status, data) {
    if (status) {
      addresses = data;
    } else {
      notify(context, "Unable to load addresses", Colors.red);
    }
    setState(() {
      gettingaddress = false;
    });
    setRadioId();
  }

  updateShipAdd(status) {
    if (status) {
      widget.refreshCheckout();
      Navigator.of(context).pop();
    } else {
      notify(context, "Unable to change addresses", Colors.red);
    }
    setState(() {
      gettingaddress = false;
    });
    setRadioId();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: true,
          title: const Text('Checkout'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            ListView.builder(
                padding: EdgeInsets.zero,
                itemCount: addresses.length,
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                itemBuilder: (context, index) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 10),
                      Container(
                        margin:
                            const EdgeInsets.only(top: 5, left: 15, right: 10),
                        width: double.infinity,
                        constraints: const BoxConstraints(
                          maxHeight: double.infinity,
                          minHeight: 150,
                        ),
                        decoration: ShapeDecoration(
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side:
                                const BorderSide(width: 1, color: Colors.black),
                          ),
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5, left: 15, right: 10),
                                    child: Text(
                                      addresses[index]['first_name'],
                                      style: const TextStyle(
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5, left: 15, right: 10),
                                    child: Text(
                                      addresses[index]['phone_no'],
                                      style: const TextStyle(
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                  // Padding(
                                  //   padding: const EdgeInsets.only(
                                  //       top: 5, left: 15, right: 10),
                                  //   child: Text(
                                  //     addresses[index]['email_id'],
                                  //     style: const TextStyle(
                                  //       fontSize: 16,
                                  //     ),
                                  //   ),
                                  // ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5, left: 15, right: 10),
                                    child: Text(
                                      addresses[index]['address_1'],
                                      style: const TextStyle(
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 15),
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Radio<String>(
                                    value: addresses[index]['id'].toString(),
                                    groupValue: _selectedaddress,
                                    onChanged: (value) {
                                      setState(() {
                                        _selectedaddress = value!;
                                        setLastAddress(_selectedaddress,
                                            globals.userId, updateShipAdd);
                                      });
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                }),
            const SizedBox(height: 30),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: OutlinedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AddNewAddress(reload),
                        ));
                  },
                  style: OutlinedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    side: BorderSide(
                      width: 1.0,
                      color: globals.colors['headers']!,
                    ),
                  ),
                  child: Text(
                    "Add New",
                    style: TextStyle(
                      color: globals.colors['headers'],
                      fontSize: 15,
                    ),
                  )),
            ),
          ]),
        ),
      ]),
    );
  }
}
