import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../globals.dart' as globals;

String url = globals.url;

Future<http.Response> register(Map data, Function updateLogin) async {
  final response = await http.post(
    Uri.parse(url + "/register"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(data),
  );
  if (response.statusCode == 201) {
    updateLogin(true, jsonDecode(response.body)['data']);
  } else {
    updateLogin(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> login(Map data, Function updateLogin) async {
  final response = await http.post(
    Uri.parse(url + "/login"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(data),
  );
  if (response.statusCode == 200) {
    updateLogin(true, jsonDecode(response.body)['data']);
  } else {
    updateLogin(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> logoutApi(Map data) async {
  final response = await http.post(
    Uri.parse(url + "/logout"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
    body: jsonEncode(data),
  );
  if (response.statusCode != 200) {
    // sendErrors(response.body, 'logoutApi');
  } else {
    //
  }
  return response;
}

Future<http.Response> checkLogin() async {
  final response = await http.post(
    Uri.parse(url + "/check-login"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
      'Accept': 'application/json',
    },
  );
  if (response.statusCode != 200) {
//sendErrors(response.body, 'checkLogin');
  }
  return response;
}

Future<http.Response> updateUser(Map data, Function update) async {
  final response = await http.put(
    Uri.parse(url + "/update-user"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
    body: jsonEncode(data),
  );
  if (response.statusCode == 200) {
    update(true, jsonDecode(response.body)['data']);
  } else {
    update(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getCategories(Function updateCategories) async {
  final response = await http.post(
    Uri.parse(url + "/categories"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateCategories(true, jsonDecode(response.body)['data']['categories'],
        jsonDecode(response.body)['data']['app_maintenance']);
  } else {
    updateCategories(false, jsonDecode(response.body)['message'], []);
  }
  return response;
}

Future<http.Response> getCommonConfig(Function updateIndex) async {
  final response = await http.post(
    Uri.parse(url + "/setting-value"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateIndex(jsonDecode(response.body)['data']);
  } else {
    updateIndex(jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getMinAmount(Function updateIndex) async {
  final response = await http.post(
    Uri.parse(url + "/setting-value-amount"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateIndex(jsonDecode(response.body)['data']);
  } else {
    updateIndex(jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getAllNotification(Function updateIndex) async {
  final response = await http.post(
    Uri.parse(url + "/get-all-notification"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );

  if (response.statusCode == 200) {
    updateIndex(jsonDecode(response.body)['data']);
  } else {
    updateIndex(jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getProducts(Map data, Function updateproducts) async {
  final response = await http.post(
    Uri.parse(url + "/products"),
    body: jsonEncode(data),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateproducts(true, jsonDecode(response.body)['data']);
  } else {
    updateproducts(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> singleProduct(int proId, Function updateproducts) async {
  final response = await http.post(
    Uri.parse(url + "/single-product"),
    body: jsonEncode({"product_id": proId.toString()}),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateproducts(true, jsonDecode(response.body)['data']);
  } else {
    updateproducts(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> addToCart(Map data, Function update) async {
  final response = await http.post(
    Uri.parse(url + "/add-to-cart"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
    body: jsonEncode(data),
  );
  if (response.statusCode == 201) {
    update(true, jsonDecode(response.body)['data']);
  } else {
    update(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getCartProducts(Map data, Function updateproducts) async {
  final response = await http.post(
    Uri.parse(url + "/cart-products"),
    body: jsonEncode(data),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateproducts(true, jsonDecode(response.body)['data']['cart'],
        jsonDecode(response.body)['data']['app_maintenance']);
  } else {
    updateproducts(false, jsonDecode(response.body)['message'], []);
  }
  return response;
}

Future<http.Response> removeCartProducts(
    int orderLid, userId, Function updateproducts) async {
  final response = await http.delete(
    Uri.parse(url + "/remove-cart-products"),
    body: jsonEncode({
      "orderLid": orderLid.toString(),
      "user_id": userId,
    }),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateproducts(true, jsonDecode(response.body)['data']['cart'],
        jsonDecode(response.body)['data']['app_maintenance']);
  } else {
    updateproducts(false, jsonDecode(response.body)['message'], []);
  }
  return response;
}

Future<http.Response> updateCartQuantity(Map data, Function update) async {
  var rType = data['type'];
  final response = await http.post(
    Uri.parse(url + "/cart-product-quantity"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
    body: jsonEncode(data),
  );
  if (response.statusCode == 200) {
    if (rType == "cart") {
      update(true, jsonDecode(response.body)['data']['cart'],
          jsonDecode(response.body)['data']['app_maintenance']);
    } else {
      update(true, jsonDecode(response.body)['data']);
    }
  } else {
    if (rType == "cart") {
      update(false, jsonDecode(response.body)['message'], []);
    } else {
      update(false, jsonDecode(response.body)['message']);
    }
  }
  return response;
}

Future<http.Response> getBrands(Function updateBrands) async {
  final response = await http.post(
    Uri.parse(url + "/brands"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateBrands(true, jsonDecode(response.body)['data']);
  } else {
    updateBrands(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getspecialProducts(
    int type, Function getSpcialPro, int specId) async {
  final response = await http.post(
    Uri.parse(url + "/get-special-product"),
    body: jsonEncode({"type": type.toString(), "brand_id": specId.toString()}),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    getSpcialPro(true, jsonDecode(response.body)['data']);
  } else {
    getSpcialPro(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getUserAddresses(userId, Function setAddress) async {
  final response = await http.post(
    Uri.parse(url + "/user-address"),
    body: jsonEncode({"user_id": userId.toString()}),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    setAddress(true, jsonDecode(response.body)['data']);
  } else {
    setAddress(false, []);
  }
  return response;
}

Future<http.Response> deleteUserAddresses(Map data, Function setAddress) async {
  final response = await http.delete(
    Uri.parse(url + "/delete-user-address"),
    body: jsonEncode(data),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    setAddress(true, jsonDecode(response.body)['data']);
  } else {
    setAddress(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> addAddress(Map data, Function update) async {
  final response = await http.post(
    Uri.parse(url + "/add-address"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
    body: jsonEncode(data),
  );
  if (response.statusCode == 200) {
    update(true, jsonDecode(response.body)['data']);
  } else {
    update(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getShippingArea(Function setArea) async {
  final response = await http.post(
    Uri.parse(url + "/get-area"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    setArea(true, jsonDecode(response.body)['data']);
  } else {
    setArea(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getOrders(Map data, Function updateproducts) async {
  final response = await http.post(
    Uri.parse(url + "/orders-list"),
    body: jsonEncode(data),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateproducts(true, jsonDecode(response.body)['data']);
  } else {
    updateproducts(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getoffers(userRole, Function updateproducts) async {
  final response = await http.post(
    Uri.parse(url + "/offers-list"),
    body: jsonEncode({"user_role": userRole}),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateproducts(true, jsonDecode(response.body)['data']);
  } else {
    updateproducts(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> setLastAddress(
    addId, userId, Function updateShipAdd) async {
  final response = await http.post(
    Uri.parse(url + "/setLastAdd"),
    body: jsonEncode({"addressId": addId, "user_id": userId}),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateShipAdd(true);
  } else {
    updateShipAdd(false);
  }
  return response;
}

Future<http.Response> getShipCharge(areaId, Function shipCharge) async {
  final response = await http.post(
    Uri.parse(url + "/shipping-charge"),
    body: jsonEncode({"area_id": areaId.toString()}),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    shipCharge(true, jsonDecode(response.body)['data']);
  } else {
    shipCharge(false, []);
  }
  return response;
}

Future<http.Response> checkout(Map data, Function update) async {
  final response = await http.post(
    Uri.parse(url + "/checkout"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
    body: jsonEncode(data),
  );
  if (response.statusCode == 201) {
    update(true, jsonDecode(response.body)['message']);
    // sendErrors(data, "Order Recived");
  } else {
    update(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getOrderItems(orderId) async {
  final response = await http.post(
    Uri.parse(url + "/order-items"),
    body: jsonEncode({"order_id": orderId.toString()}),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    // setItems(true, jsonDecode(response.body)['data']);
  } else {
    // setItems(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> searchItems(searhTxt, Function update) async {
  final response = await http.post(
    Uri.parse(url + "/search"),
    body: jsonEncode({"searchTxt": searhTxt}),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    update(true, jsonDecode(response.body)['data']['categories'],
        jsonDecode(response.body)['data']['app_maintenance']);
  } else {
    update(false, jsonDecode(response.body)['message'], []);
  }
  return response;
}

Future<http.Response> getFaqs(Function update) async {
  final response = await http.post(
    Uri.parse(url + "/faqs"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    update(true, jsonDecode(response.body)['data']);
  } else {
    update(false, []);
  }
  return response;
}

Future<http.Response> submitQury(Map data, Function update) async {
  final response = await http.post(
    Uri.parse(url + "/submit-query"),
    body: jsonEncode(data),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    update(true);
  } else {
    update(false);
  }
  return response;
}

Future<http.Response> addFavorite(Map data, Function updateproducts) async {
  final response = await http.post(
    Uri.parse(url + "/add-favorite"),
    body: jsonEncode(data),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateproducts(true, jsonDecode(response.body)['data']);
  } else {
    updateproducts(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> setToken(Map data, Function updateproducts) async {
  final response = await http.post(
    Uri.parse(url + "/set-token"),
    body: jsonEncode(data),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updateproducts(true, jsonDecode(response.body)['data']);
  } else {
    updateproducts(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> onlyFavoriot(Function updatefav) async {
  final response = await http.get(
    Uri.parse(url + "/only-favorite"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer ' + globals.apiToken.toString(),
    },
  );
  if (response.statusCode == 200) {
    updatefav(true, jsonDecode(response.body)['data']);
  } else {
    updatefav(false, jsonDecode(response.body)['message']);
  }
  return response;
}

Future<http.Response> getRazorPayOrderId(amount, rKey, rSecret) async {
  final response = await http.post(
    Uri.parse("https://api.razorpay.com/v1/orders"),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization':
          "Basic ${base64Encode(utf8.encode(rKey + ':' + rSecret))}"
    },
    body: json.encode({
      "amount": amount,
      "currency": "INR",
    }),
  );
  return response;
}

Future sendErrors(data, msg) async {
  try {
    data = jsonEncode(data);
  } catch (err) {
    //   print(err);
  }
  data = "\nUser id=" +
      globals.userId.toString() +
      "\nUser Name: " +
      globals.name.toString() +
      "\nAction: " +
      msg +
      "\n" +
      "\nData :" +
      data;
  try {
    await http.post(Uri.parse(
        'https://api.telegram.org/bot5370102009:AAFOMIpCAqxRtehxYLQRehjKh6t_h_AakSo/sendmessage?chat_id=-1001720817055&text=' +
            data));
  } catch (err) {
    // print(err);
  }
  return data;
}
