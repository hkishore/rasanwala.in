import 'package:flutter/material.dart';
import 'notify.dart';
import '../globals.dart' as globals;
import 'apis.dart';

class ContactUs extends StatefulWidget {
  const ContactUs({Key? key}) : super(key: key);

  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final _nameController = TextEditingController();
  final _subjectController = TextEditingController();
  final _emailController = TextEditingController();
  final _noteController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isUploading = false;

  InputDecoration inputBoxDesign(Icon icon, String hint) {
    return InputDecoration(
      prefixIcon: icon,
      hintText: hint,
      border: const OutlineInputBorder(),
    );
  }

  void formclean(status) {
    if (status) {
      _nameController.clear();
      _subjectController.clear();
      _emailController.clear();
      _noteController.clear();
      notify(context, "Thanks for contacting us", Colors.green);
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      _isUploading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: true,
          title: const Text('Contact Us'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    children: <Widget>[
                      const SizedBox(height: 15),
                      Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            const Align(
                              alignment: Alignment.center,
                              child: Padding(
                                padding: EdgeInsets.only(bottom: 20),
                                child: Text(
                                  "Ask your Query",
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 10),
                              child: TextFormField(
                                validator: (value) => nameValidator(value),
                                controller: _nameController,
                                decoration: inputBoxDesign(
                                  const Icon(Icons.person, color: Colors.black),
                                  "Name",
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 10),
                              child: TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) => emailValidator(value),
                                controller: _emailController,
                                decoration: inputBoxDesign(
                                  const Icon(Icons.mail, color: Colors.black),
                                  "Email",
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 10),
                              child: TextFormField(
                                validator: (value) => subjectValidator(value),
                                controller: _subjectController,
                                decoration: inputBoxDesign(
                                  const Icon(Icons.phone_iphone,
                                      color: Colors.black),
                                  "Subject",
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 10),
                              child: TextFormField(
                                maxLines: 5,
                                validator: (value) => noteValidator(value),
                                controller: _noteController,
                                decoration: inputBoxDesign(
                                  const Icon(Icons.description,
                                      color:
                                          Color.fromARGB(255, 255, 251, 251)),
                                  "Write your query here.......",
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: !_isUploading
                                  ? Align(
                                      alignment: Alignment.center,
                                      child: SizedBox(
                                        width: 100,
                                        child: ElevatedButton(
                                          child: const Text("Send"),
                                          style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStateProperty
                                                      .all<Color>(globals
                                                          .colors['headers']!),
                                              shape: MaterialStateProperty.all<
                                                      RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(18.0),
                                              ))),
                                          onPressed: _isUploading
                                              ? null
                                              : () {
                                                  FocusManager
                                                      .instance.primaryFocus
                                                      ?.unfocus();
                                                  var status = _formKey
                                                      .currentState
                                                      ?.validate();
                                                  if (status != null &&
                                                      status) {
                                                    setState(() {
                                                      _isUploading = true;
                                                    });
                                                    submitQury({
                                                      'name':
                                                          _nameController.text,
                                                      'email':
                                                          _emailController.text,
                                                      'subject':
                                                          _subjectController
                                                              .text,
                                                      'message':
                                                          _noteController.text
                                                    }, formclean);
                                                  }
                                                },
                                        ),
                                      ),
                                    )
                                  : const Align(
                                      alignment: Alignment.center,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: 8),
                                        child: CircularProgressIndicator(
                                          semanticsLabel:
                                              'Circular progress indicator',
                                        ),
                                      ),
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ]),
        ),
      ]),
    );
  }

  nameValidator(value) {
    if (value == null || value.isEmpty) {
      return "Name can't be empty";
    }
    return null;
  }

  subjectValidator(value) {
    if (value == null || value.isEmpty) {
      return "Subject can't be empty";
    }
    return null;
  }

  emailValidator(value) {
    String pattern =
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    RegExp regExp = RegExp(pattern);
    if (value == null || value.isEmpty) {
      return "Email can't be empty";
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid Email';
    }
    return null;
  }

  noteValidator(value) {
    if (value == null || value.isEmpty) {
      return "Query can't be empty";
    }
    return null;
  }
}
