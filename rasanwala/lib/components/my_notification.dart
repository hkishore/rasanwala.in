import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import 'notification_listing.dart';

class MyNotification {
  static Future<void> initialize() async {
    bool isallowed = await AwesomeNotifications().isNotificationAllowed();
    if (!isallowed) {
      AwesomeNotifications().requestPermissionToSendNotifications();
    }
    AwesomeNotifications().initialize('resource://drawable/app_icon', [
      NotificationChannel(
        channelGroupKey: 'basic_notification',
        channelKey: 'basic',
        channelName: 'Basic notifications',
        channelDescription: 'Notification channel for simple text',
        channelShowBadge: true,
        importance: NotificationImportance.High,
        enableVibration: true,
      ),
    ]);
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      showNotification(message);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      showNotification(message);
    });
  }

  static Future<void> showNotification(RemoteMessage message) async {
    String? _title;
    String? _body;
    String? _image;
    String? _type;
    _title = message.data['title'];
    _body = message.data['body'];
    _type = message.data['type'];
    _image =
        (message.data['notiImage'] != null && message.data['notiImage'] != "")
            ? ""
            : null;

    if (_image != null && _image != "") {
      await showBigPictureNotificationHiddenLargeIcon(
          _title!, _body!, _image, _type!);
    } else {
      await showTextNotification(_title!, _body!, _type!);
    }
  }

  static Future<void> showTextNotification(
      String title, String body, String type) async {
    AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: 1,
        channelKey: 'basic',
        title: title,
        body: body,
        payload: {"type": type},
      ),
    );
  }

  static Future<void> showBigPictureNotificationHiddenLargeIcon(
      String title, String body, String image, String type) async {
    AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: 2,
        channelKey: 'image',
        title: title,
        body: body,
        bigPicture: image,
        notificationLayout: NotificationLayout.BigPicture,
        largeIcon: image,
        hideLargeIconOnExpand: true,
        payload: {"type": type},
      ),
    );
  }
}

@pragma("vm:entry-point")
Future<void> onActionReceivedMethod(
    BuildContext context, ReceivedAction receivedAction) async {
  if (receivedAction.payload!['type'] == "notification") {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const Notificationlist(),
      ),
    );
  }
}

@pragma('vm:entry-point')
Future<dynamic> myBackgroundMessageHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  if (message.data['notiImage'] != null && message.data['notiImage'] != "") {
    await AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: 3,
        channelKey: 'image',
        title: message.data['title'],
        body: message.data['body'],
        bigPicture: message.data['notiImage'],
        notificationLayout: NotificationLayout.BigPicture,
        largeIcon: message.data['notiImage'],
        hideLargeIconOnExpand: true,
        payload: {"type": message.data['type']},
      ),
    );
  } else {
    await AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: 4,
        channelKey: 'basic',
        title: message.data['title'],
        body: message.data['body'],
        payload: {"type": message.data['type']},
      ),
    );
  }
}
