import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'all_popus.dart';
import 'notify.dart';
import '../globals.dart' as globals;
import 'apis.dart';
import 'product_component.dart';
import 'single_product.dart';

class CommonProducts extends StatefulWidget {
  final int type;
  final String title;
  final int id;

  const CommonProducts(this.type, this.title, this.id, {Key? key})
      : super(key: key);

  @override
  State<CommonProducts> createState() => _CommonProductsState();
}

class _CommonProductsState extends State<CommonProducts> {
  List items = [];
  Map orderLine = {};
  bool _loading = false;
  bool addFav = false;

  @override
  void initState() {
    super.initState();
    refreshProducts();
  }

  void refreshProducts() {
    getspecialProducts(widget.type, specialProducts, widget.id);
    _loading = true;
  }

  setOfferPer(per) {
    setState(() {
      //     percentage = per.toDouble();
    });
  }

  void specialProducts(status, data) async {
    final prefs = await SharedPreferences.getInstance();
    if (status) {
      setState(() {
        items = data['products'];
        orderLine = {};
        if (data['orderLine'].isNotEmpty) {
          orderLine = data['orderLine'];
        }
        globals.favItemCount = data['favCnt'];
        prefs.setInt('favItemCount', globals.favItemCount);
      });
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      addFav = false;
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: globals.colors['headers'],
            floating: true,
            pinned: true,
            snap: true,
            centerTitle: true,
            title: Text(widget.title),
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios_new),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            actions: [
              cartIcon(context, refreshProducts),
            ],
          ),
          SliverList(
              delegate: SliverChildListDelegate([
            _loading
                ? const Column(
                    children: [
                      SizedBox(height: 250),
                      CircularProgressIndicator(
                        semanticsLabel: 'Circular progress indicator',
                      ),
                    ],
                  )
                : (items.isNotEmpty
                    ? Wrap(
                        alignment: WrapAlignment.start,
                        direction: Axis.horizontal,
                        children: <Widget>[
                          ...items.map((e) {
                            return SizedBox(
                              width: screenWidth,
                              child: Container(
                                margin: const EdgeInsets.all(5.0),
                                constraints:
                                    const BoxConstraints(minHeight: 56.0),
                                child: Card(
                                  elevation: 3.0,
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              FocusManager.instance.primaryFocus
                                                  ?.unfocus();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        SingleProduct(
                                                            e["id"],
                                                            orderLine,
                                                            refreshProducts),
                                                  ));
                                            },
                                            child: SizedBox(
                                              height: 120.0,
                                              width: 120.0,
                                              child: Stack(
                                                children: <Widget>[
                                                  Positioned.fill(
                                                      child: e["image"] != "" &&
                                                              e["image"] != null
                                                          ? Image.network(
                                                              e["image"],
                                                              fit: BoxFit
                                                                  .contain,
                                                            )
                                                          : Center(
                                                              child: Image.asset(
                                                                  "images/default.PNG"),
                                                            )),
                                                  Container(
                                                    color: Colors.black38,
                                                  ),
                                                  Align(
                                                    alignment:
                                                        Alignment.topRight,
                                                    child: IconButton(
                                                      icon: e['is_favorite'] ==
                                                              null
                                                          ? const Icon(
                                                              Icons
                                                                  .favorite_border,
                                                              color:
                                                                  Colors.white,
                                                            )
                                                          : const Icon(
                                                              Icons.favorite,
                                                              color: Colors
                                                                  .redAccent,
                                                            ),
                                                      onPressed: addFav == true
                                                          ? null
                                                          : () {
                                                              setState(() {
                                                                addFav = true;
                                                              });
                                                              addFavorite({
                                                                'proId':
                                                                    e['id'],
                                                                "type": widget
                                                                    .type
                                                                    .toString(),
                                                                'brand_id': widget
                                                                    .id
                                                                    .toString(),
                                                                "user_id":
                                                                    globals
                                                                        .userId,
                                                              }, specialProducts);
                                                            },
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          // Container(
                                          //   padding: const EdgeInsets.only(
                                          //       left: 3.0, bottom: 3.0, top: 5.0),
                                          //   alignment: Alignment.bottomLeft,
                                          //   child: GestureDetector(
                                          //     onTap: () {
                                          //       // Navigator.push(
                                          //       //     context,
                                          //       //     MaterialPageRoute(
                                          //       //       builder: (context) =>
                                          //       //           const AllProducts(),
                                          //       //     ));
                                          //     },
                                          //     child: Text(
                                          //       e['name'],
                                          //       style: const TextStyle(
                                          //           fontSize: 16.0,
                                          //           color: Colors.black,
                                          //           fontWeight: FontWeight.bold),
                                          //     ),
                                          //   ),
                                          // ),
                                          Expanded(
                                              child: ProductComponent(
                                            e['name'],
                                            e['id'],
                                            e['variant'],
                                            orderLine,
                                            refreshProducts,
                                            //   setOfferPer,
                                          )),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                        ],
                      )
                    : notFoundIcon("Products Not Found!!")),
          ])),
        ],
      ),
    );
  }
}
