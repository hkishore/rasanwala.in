import 'package:flutter/material.dart';
import '../globals.dart' as globals;

outLineBtn(btntext) {
  return SizedBox(
    height: 40.0,
    child: OutlinedButton(
      onPressed: () {},
      style: OutlinedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        side: BorderSide(
          width: 1.0,
          color: globals.colors['headers']!,
        ),
      ),
      child: Text(
        btntext,
        style: TextStyle(
          color: globals.colors['headers'],
          fontSize: 18,
        ),
      ),
    ),
  );
}
