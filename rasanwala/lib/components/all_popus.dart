import 'package:flutter/material.dart';
import 'package:badges/badges.dart' as notibadge;
import '../globals.dart' as globals;
import 'cart.dart';
import 'favorite.dart';

double productPrice = 0.0;
int quantity = 1;
double regularPrice = 0.0;

logoutDialog(context, logout) {
  return showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return AlertDialog(
          title: const Text('Logout Confirmation'),
          content: const Text('Are you sure,you want to logout ?'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                logout();
                Navigator.of(context).pop();
              },
              child: const Text('Logout'),
            )
          ],
        );
      });
}

lodingPopup(context) {
  return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext ctx) {
        return const AlertDialog(
            title: Text('Please wait.....'),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  semanticsLabel: 'Circular progress indicator',
                ),
              ],
            ));
      });
}

orderDetailsPopup(context, orderItems, totalPrice, shipCharge) {
  return showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return AlertDialog(
          // title: const Text('Please wait.....'),
          content: SingleChildScrollView(
            child: SizedBox(
              width: double.maxFinite,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(top: 20, left: 10, right: 10),
                    child: Text(
                      "Item Details",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              "Product",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Padding(
                            padding:
                                EdgeInsets.only(top: 7, left: 10, right: 10),
                            child: Text(
                              "Price",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Stack(
                    children: [
                      ListView.builder(
                          padding: EdgeInsets.zero,
                          itemCount: orderItems.length,
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 7, left: 10, right: 10),
                                        child: Text(
                                          orderItems[index]['proName'] +
                                              "(" +
                                              orderItems[index]['variantSize'] +
                                              ")" +
                                              " × " +
                                              orderItems[index]['qty']
                                                  .toString(),
                                          style: const TextStyle(
                                            fontSize: 18,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    // crossAxisAlignment:
                                    //  CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 7, left: 10, right: 10),
                                        child: Text(
                                          orderItems[index]['amount']
                                              .toString(),
                                          style: const TextStyle(
                                            fontSize: 18,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            );
                          }),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      const Expanded(
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  EdgeInsets.only(top: 7, left: 10, right: 10),
                              child: Text(
                                "Shipping Charge",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 7, left: 10, right: 10),
                              child: Text(
                                shipCharge.toString(),
                                style: const TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const Divider(
                    color: Colors.grey,
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      const Expanded(
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  EdgeInsets.only(top: 7, left: 10, right: 10),
                              child: Text(
                                "Total",
                                style: TextStyle(
                                  fontSize: 22,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 7, left: 10, right: 10),
                              child: Text(
                                "₹ " + totalPrice.toString(),
                                style: const TextStyle(
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.red),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Close'),
            )
          ],
        );
      });
}

notFoundIcon(text) {
  return Column(children: [
    const SizedBox(height: 250),
    const Icon(
      Icons.sentiment_dissatisfied,
      color: Colors.black,
      size: 70,
    ),
    Text(text,
        style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold))
  ]);
}

cartIcon(context, Function refreshPrivious) {
  return notibadge.Badge(
      badgeContent: Text(globals.cartItemCount.toString(),
          style: const TextStyle(color: Colors.white)),
      position: notibadge.BadgePosition.topEnd(top: 6, end: 6),
      child: Padding(
        padding: const EdgeInsets.only(right: 5),
        child: IconButton(
          icon: const Icon(Icons.shopping_cart),
          onPressed: () {
            FocusManager.instance.primaryFocus?.unfocus();
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CartPage(refreshPrivious)),
            );
          },
        ),
      ));
}

favoriteIcon(context, Function refreshPrivious) {
  return notibadge.Badge(
    badgeContent: Text(globals.favItemCount.toString(),
        style: const TextStyle(color: Colors.white)),
    position: notibadge.BadgePosition.topEnd(top: 6, end: 6),
    child: Padding(
      padding: const EdgeInsets.only(right: 5),
      child: IconButton(
        icon: const Icon(Icons.favorite_border),
        onPressed: () {
          FocusManager.instance.primaryFocus?.unfocus();
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const Favorite()),
          );
        },
      ),
    ),
  );
}

addOfferLable(salePrice, regularPrice) {
  double percentage = 0.0;
  if (salePrice != "" && regularPrice != "") {
    double per = (int.parse(regularPrice) - int.parse(salePrice)) /
        int.parse(regularPrice) *
        100;
    percentage = per.toDouble();
  }
  return Column(
    children: [
      percentage != 0.0
          ? Align(
              //top: 6,
              alignment: Alignment.topRight,
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 6),
                decoration: BoxDecoration(
                  color: globals.colors['headers'],
                  // borderRadius:
                  //     BorderRadius.only(
                  //   topLeft:
                  //       Radius.circular(8),
                  //   bottomRight:
                  //       Radius.circular(8),
                  // ) // green shaped
                ),
                child: Text(
                  "${percentage.toStringAsFixed(2)}% off",
                  style: TextStyle(color: globals.colors['body']),
                ),
              ),
            )
          : const SizedBox(width: 0),
    ],
  );
}
