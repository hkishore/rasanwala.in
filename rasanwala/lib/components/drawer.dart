import 'package:flutter/material.dart';
import 'all_popus.dart';
import 'cart.dart';
import '../globals.dart' as globals;
import '../my_account.dart';
import 'brands.dart';
import 'common_products.dart';
import 'contact_us.dart';
import 'offers_page.dart';
import 'orders.dart';
import 'faq.dart';
import 'privacy_terms.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  _blankFunc() {}

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Scaffold(
        body: ListView(
          children: <Widget>[
            SizedBox(
              height: 120,
              //  color: globals.colors['headers'],
              child: DrawerHeader(
                child: FittedBox(
                  child: Image.asset("images/pngLogo.png"),
                  fit: BoxFit.contain,
                ),
              ),
            ),
            ListTile(
              leading: const Icon(Icons.person),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const MyAccount(),
                  ),
                );
              },
              title: const Text('My Account'),
            ),
            ListTile(
              leading: const Icon(Icons.group_work),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Brands(),
                  ),
                );
              },
              title: const Text('Brands'),
            ),
            ListTile(
              leading: const Icon(Icons.inventory_2),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CommonProducts(3, "Shop", 0),
                  ),
                );
              },
              title: const Text('Shop'),
            ),
            ListTile(
              leading: const Icon(Icons.shopping_cart_checkout),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const OrdersPage(),
                  ),
                );
              },
              title: const Text('Orders'),
            ),
            ListTile(
              leading: const Icon(Icons.sell),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const OfferPage(),
                  ),
                );
              },
              title: const Text('Offers'),
            ),
            ListTile(
              leading: const Icon(Icons.shopping_cart),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CartPage(_blankFunc),
                  ),
                );
              },
              title: const Text('My Cart'),
            ),
            ListTile(
              leading: const Icon(Icons.quiz),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const FAQPage(),
                  ),
                );
              },
              title: const Text('FAQs'),
            ),
            ListTile(
              leading: const Icon(Icons.contact_support),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ContactUs(),
                  ),
                );
              },
              title: const Text('Contact Us'),
            ),
            ListTile(
              leading: const Icon(Icons.verified_user),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PrivacyTerms(
                        2.7, "privacy-policy", "Privacy Policy"),
                  ),
                );
              },
              title: const Text('Privacy Policy'),
            ),
            ListTile(
              leading: const Icon(Icons.gavel),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const PrivacyTerms(
                        3.5, "terms-condition", "Terms & Conditions"),
                  ),
                );
              },
              title: const Text('Terms & Conditions'),
            ),
            ListTile(
              leading: const Icon(Icons.logout),
              onTap: () {
                logoutDialog(context, globals.logout);
              },
              title: const Text('Logout'),
            ),
          ],
        ),
        bottomNavigationBar: const Padding(
          padding: EdgeInsets.all(25),
          child: Text('Version 1.0.1+1', style: TextStyle(fontSize: 14)),
        ),
      ),
    );
  }
}
