import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'all_popus.dart';
import 'product_component.dart';
import 'single_product.dart';
import 'apis.dart';
import '../globals.dart' as globals;
import 'notify.dart';

class OfferPage extends StatefulWidget {
  const OfferPage({Key? key}) : super(key: key);

  @override
  State<OfferPage> createState() => _OfferPageState();
}

class _OfferPageState extends State<OfferPage> {
  bool _loading = false;
  List offers = [];
  Map orderLine = {};
  bool addFav = true;

  @override
  void initState() {
    super.initState();
    refreshoffers();
  }

  refreshoffers() {
    _loading = true;
    getoffers(globals.role, offerList);
  }

  void offerList(status, data) async {
    final prefs = await SharedPreferences.getInstance();
    if (status) {
      setState(() {
        orderLine = {};
        offers = data['products'];
        if (data['orderLine'].isNotEmpty) {
          orderLine = data['orderLine'];
        }
        globals.favItemCount = data['favCnt'];
        prefs.setInt('favItemCount', globals.favItemCount);
      });
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      addFav = false;
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: true,
          title: const Text("Offers"),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          actions: [
            cartIcon(context, refreshoffers),
          ],
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            _loading
                ? const Column(
                    children: [
                      SizedBox(height: 250),
                      CircularProgressIndicator(
                        semanticsLabel: 'Circular progress indicator',
                      ),
                    ],
                  )
                : (offers.isNotEmpty
                    ? Wrap(
                        alignment: WrapAlignment.start,
                        direction: Axis.horizontal,
                        children: <Widget>[
                          ...offers.map((e) {
                            return SizedBox(
                              width: screenWidth,
                              child: Container(
                                margin: const EdgeInsets.all(5.0),
                                constraints:
                                    const BoxConstraints(minHeight: 56.0),
                                child: Card(
                                  elevation: 3.0,
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              FocusManager.instance.primaryFocus
                                                  ?.unfocus();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        SingleProduct(
                                                            e["id"],
                                                            orderLine,
                                                            refreshoffers),
                                                  ));
                                            },
                                            child: SizedBox(
                                              height: 120.0,
                                              width: 120.0,
                                              child: Stack(
                                                children: <Widget>[
                                                  Positioned.fill(
                                                      child: e["image"] != "" &&
                                                              e["image"] != null
                                                          ? Image.network(
                                                              e["image"],
                                                              fit: BoxFit.cover,
                                                            )
                                                          : Center(
                                                              child: Image.asset(
                                                                  "images/default.PNG"),
                                                            )),
                                                  Container(
                                                    color: Colors.black38,
                                                  ),
                                                  Align(
                                                    alignment:
                                                        Alignment.topRight,
                                                    child: IconButton(
                                                      icon: e['is_favorite'] ==
                                                              null
                                                          ? const Icon(
                                                              Icons
                                                                  .favorite_border,
                                                              color:
                                                                  Colors.white,
                                                            )
                                                          : const Icon(
                                                              Icons.favorite,
                                                              color: Colors
                                                                  .redAccent,
                                                            ),
                                                      onPressed: addFav == true
                                                          ? null
                                                          : () {
                                                              setState(() {
                                                                addFav = true;
                                                              });
                                                              addFavorite({
                                                                'proId':
                                                                    e['id'],
                                                                'type': "5",
                                                                "user_id":
                                                                    globals
                                                                        .userId,
                                                              }, offerList);
                                                            },
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          // Container(
                                          //   padding: const EdgeInsets.only(
                                          //       left: 3.0,
                                          //       bottom: 3.0,
                                          //       top: 5.0),
                                          //   alignment: Alignment.bottomLeft,
                                          //   child: GestureDetector(
                                          //     onTap: () {
                                          //       // Navigator.push(
                                          //       //     context,
                                          //       //     MaterialPageRoute(
                                          //       //       builder: (context) =>
                                          //       //           const AllProducts(),
                                          //       //     ));
                                          //     },
                                          //     child: Text(
                                          //       e['name'],
                                          //       style: const TextStyle(
                                          //           fontSize: 16.0,
                                          //           color: Colors.black,
                                          //           fontWeight:
                                          //               FontWeight.bold),
                                          //     ),
                                          //   ),
                                          // ),
                                          Expanded(
                                            child: ProductComponent(
                                              e['name'],
                                              e['id'],
                                              e['variant'],
                                              orderLine,
                                              refreshoffers,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                        ],
                      )
                    : notFoundIcon("Offers Not Avilable!!")),
          ]),
        ),
      ]),
    );
  }
}
