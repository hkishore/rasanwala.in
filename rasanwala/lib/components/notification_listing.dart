import 'package:flutter/material.dart';
import 'package:rasanwala/components/apis.dart';
import '../globals.dart' as globals;
import 'all_popus.dart';

class Notificationlist extends StatefulWidget {
  const Notificationlist({Key? key}) : super(key: key);

  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<Notificationlist> {
  bool _loading = false;
  List favproducts = [];
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    _loading = true;
    getAlldata();
  }

  void getAlldata() {
    getAllNotification(_updateIndex);
  }

  void _updateIndex(data) {
    //print(data);
    setState(() {
      _loading = false;
      favproducts = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: true,
          title: const Text("Notifications"),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
              globals.homeSetState!();
            },
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            _loading
                ? const Column(
                    children: [
                      SizedBox(height: 250),
                      CircularProgressIndicator(
                        semanticsLabel: 'Circular progress indicator',
                      ),
                    ],
                  )
                : (favproducts.isNotEmpty
                    ? Wrap(
                        alignment: WrapAlignment.start,
                        direction: Axis.horizontal,
                        children: <Widget>[
                          ...favproducts.map((e) {
                            return SizedBox(
                              width: screenWidth,
                              child: Container(
                                margin: const EdgeInsets.all(5.0),
                                padding: const EdgeInsets.all(5.0),
                                constraints:
                                    const BoxConstraints(minHeight: 56.0),
                                child: Card(
                                  elevation: 3.0,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      top: 12,
                                      bottom: 12,
                                      right: 20,
                                      left: 20,
                                    ),
                                    child: Column(
                                      children: [
                                        // Row(
                                        //   mainAxisAlignment:
                                        //       MainAxisAlignment.spaceBetween,
                                        //   children: [
                                        //     Expanded(
                                        //       child: Text(
                                        //         e['subject'] ?? 'NA',
                                        //         style:
                                        //             const TextStyle(fontSize: 30),
                                        //       ),
                                        //     ),
                                        //   ],
                                        // ),
                                        const SizedBox(height: 15),

                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Expanded(
                                              child: Text(
                                                e['notification'] ?? 'NA',
                                              ),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(height: 15),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              e['format'] ?? 'NA',
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }),
                        ],
                      )
                    : notFoundIcon("Notifications unavilable!!")),
          ]),
        )
      ]),
    );
  }
}
