import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../globals.dart' as globals;

imagePickerAlert(context, Function action) {
  final _picker = ImagePicker();
  getImage(int source) async {
    Navigator.of(context).pop();
    if (source == 1) {
      final pickedFile = await _picker.pickImage(source: ImageSource.camera);
      if (pickedFile != null) action(pickedFile.path);
    } else if (source == 2) {
      final pickedFile = await _picker.pickImage(source: ImageSource.gallery);
      if (pickedFile != null) action(pickedFile.path);
    } else {
      action("");
    }
  }

  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text("Choose Profile Image"),
        content: Row(
          children: [
            IconButton(
              color: globals.colors['text'],
              onPressed: () => getImage(1),
              icon: const Icon(Icons.photo_camera),
            ),
            IconButton(
              color: globals.colors['text'],
              onPressed: () => getImage(2),
              icon: const Icon(Icons.image),
            ),
            IconButton(
              color: globals.colors['text'],
              onPressed: () => getImage(0),
              icon: const Icon(Icons.delete),
            ),
          ],
        ),
      );
    },
  );
}
