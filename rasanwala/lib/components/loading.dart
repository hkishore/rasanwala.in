import 'package:flutter/material.dart';

class LoadingPage extends StatelessWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromARGB(255, 243, 238, 242),
      height: double.infinity,
      width: double.infinity,
      child: Center(
        child: SizedBox(
          height: 400,
          width: double.infinity,
          child: Column(
            children: [
              Padding(
                child: Image.asset('images/logo.jpg'),
                padding: const EdgeInsets.all(20),
              ),
              // const Expanded(
              //   child: Text(
              //     'Sah Rasan Wala',
              //     style: TextStyle(
              //       fontSize: 50,
              //       color: Colors.black,
              //       decoration: TextDecoration.none,
              //     ),
              //   ),
              // ),
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: CircularProgressIndicator(
                  semanticsLabel: 'Circular progress indicator',
                ),
              ),
              const Text(
                'Loading....',
                style: TextStyle(
                  fontSize: 20,
                  color: Color.fromARGB(255, 44, 37, 37),
                  decoration: TextDecoration.none,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
