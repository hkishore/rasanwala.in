import 'package:flutter/material.dart';
import 'package:rasanwala/components/apis.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../globals.dart' as globals;
import 'all_popus.dart';
import 'notify.dart';
import 'product_component.dart';
import 'single_product.dart';

class Favorite extends StatefulWidget {
  const Favorite({Key? key}) : super(key: key);

  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {
  bool addFav = false;
  bool _loading = false;
  List favproducts = [];
  late int variantId;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  Map orderLine = {};

  @override
  void initState() {
    super.initState();
    _loading = true;
    refreshProducts();
  }

  setOfferPer(per) {
    setState(() {
      //     percentage = per.toDouble();
    });
  }

  refreshProducts() {
    onlyFavoriot(_favProducts);
  }

  void _favProducts(status, data) async {
    final prefs = await SharedPreferences.getInstance();
    if (status) {
      setState(() {
        orderLine = {};
        favproducts = data['products'];
        if (data['orderLine'].isNotEmpty) {
          orderLine = data['orderLine'];
        }
        globals.favItemCount = data['products'].length;
        prefs.setInt('favItemCount', globals.favItemCount);
      });
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      addFav = false;
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: true,
          title: const Text("Favorite Products"),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
              globals.homeSetState!();
            },
          ),
          actions: [
            cartIcon(context, refreshProducts),
          ],
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            _loading
                ? const Column(
                    children: [
                      SizedBox(height: 250),
                      CircularProgressIndicator(
                        semanticsLabel: 'Circular progress indicator',
                      ),
                    ],
                  )
                : (favproducts.isNotEmpty
                    ? Wrap(
                        alignment: WrapAlignment.start,
                        direction: Axis.horizontal,
                        children: <Widget>[
                          ...favproducts.map((e) {
                            return SizedBox(
                              width: screenWidth,
                              child: Container(
                                margin: const EdgeInsets.all(5.0),
                                constraints:
                                    const BoxConstraints(minHeight: 56.0),
                                child: Card(
                                  elevation: 3.0,
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              FocusManager.instance.primaryFocus
                                                  ?.unfocus();
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        SingleProduct(
                                                            e["id"],
                                                            orderLine,
                                                            refreshProducts),
                                                  ));
                                            },
                                            child: SizedBox(
                                              height: 120.0,
                                              width: 120.0,
                                              child: Stack(
                                                children: <Widget>[
                                                  Positioned.fill(
                                                      child: e["image"] != "" &&
                                                              e["image"] != null
                                                          ? Image.network(
                                                              e["image"],
                                                              fit: BoxFit
                                                                  .contain,
                                                            )
                                                          : Center(
                                                              child: Image.asset(
                                                                  "images/default.PNG"),
                                                            )),
                                                  Container(
                                                    color: Colors.black38,
                                                  ),
                                                  Align(
                                                    alignment:
                                                        Alignment.topRight,
                                                    child: IconButton(
                                                      icon: e['is_favorite'] ==
                                                              null
                                                          ? const Icon(
                                                              Icons
                                                                  .favorite_border,
                                                              color:
                                                                  Colors.white,
                                                            )
                                                          : const Icon(
                                                              Icons.favorite,
                                                              color: Colors
                                                                  .redAccent,
                                                            ),
                                                      onPressed: addFav == true
                                                          ? null
                                                          : () {
                                                              setState(() {
                                                                addFav = true;
                                                              });
                                                              addFavorite({
                                                                'proId':
                                                                    e['id'],
                                                                'type': "6",
                                                                "user_id":
                                                                    globals
                                                                        .userId,
                                                              }, _favProducts);
                                                            },
                                                    ),
                                                  ),
                                                  // addOfferLable(
                                                  //   e['variant'][0]
                                                  //           ['sale_price']
                                                  //       .toString(),
                                                  //   e['variant'][0]
                                                  //           ['regular_price']
                                                  //       .toString(),
                                                  // ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          // Expanded(
                                          //   child: Container(
                                          //     padding: const EdgeInsets.only(
                                          //         left: 6.0,
                                          //         bottom: 3.0,
                                          //         top: 5.0),
                                          //     // alignment: Alignment.topRight,
                                          //     child: GestureDetector(
                                          //       onTap: () {
                                          //         // Navigator.push(
                                          //         //     context,
                                          //         //     MaterialPageRoute(
                                          //         //       builder: (context) =>
                                          //         //           const AllProducts(),
                                          //         //     ));
                                          //       },
                                          //       child: Column(
                                          //         crossAxisAlignment:
                                          //             CrossAxisAlignment
                                          //                 .start,
                                          //         children: [
                                          //           Text(
                                          //             e['name'],
                                          //             style: const TextStyle(
                                          //                 fontSize: 16.0,
                                          //                 color: Colors.black,
                                          //                 fontWeight:
                                          //                     FontWeight
                                          //                         .bold),
                                          //           ),
                                          //         ],
                                          //       ),
                                          //     ),
                                          //   ),
                                          // ),
                                          Expanded(
                                            child: ProductComponent(
                                              e['name'],
                                              e['id'],
                                              e['variant'],
                                              orderLine,
                                              refreshProducts,
                                              // setOfferPer,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                        ],
                      )
                    : notFoundIcon("Produts unavilable!!")),
          ]),
        )
      ]),
    );
  }
}
