import 'package:flutter/material.dart';
import 'all_popus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../globals.dart' as globals;
import 'apis.dart';
import 'notify.dart';
import '../checkout.dart';

class CartPage extends StatefulWidget {
  final Function refreshPrivios;
  const CartPage(this.refreshPrivios, {Key? key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  bool _loading = false;
  List cartitems = [];
  double totalPrice = 0;
  int inmaintenance = 2;
  bool priceUpdating = false;

  @override
  void initState() {
    super.initState();
    refreshCart();
  }

  refreshCart() {
    cartitems = [];
    widget.refreshPrivios();
    getCartProducts(
      {
        "user_id": globals.userId,
      },
      cartProducts,
    );
    setState(() {
      _loading = true;
    });
  }

  void cartProducts(status, data, appmaintenance) {
    if (status) {
      widget.refreshPrivios();
      setState(() {
        cartitems = data;
        inmaintenance = int.parse(appmaintenance);
        allItemsPrice();
        priceUpdating = false;
      });
    } else {
      // notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      _loading = false;
    });
  }

  void updateQuantity(status, data) {
    if (status) {
      setState(() {
        cartitems = data;
        allItemsPrice();
        setcartItemCount();
      });
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      _loading = false;
    });
  }

  void allItemsPrice() {
    setState(() {
      totalPrice = 0;
    });
    for (int i = 0; i < cartitems.length; i++) {
      totalPrice = totalPrice + double.parse(cartitems[i]['amount']);
    }
  }

  void setProductPrice(int proId) {
    for (int i = 0; i < cartitems.length; i++) {
      if (cartitems[i]['id'] == proId) {
        num rs = cartitems[i]['variantPrice'] * cartitems[i]['qty'];
        cartitems[i]['amount'] = rs.toString();
      }
      allItemsPrice();
      setState(() {});
    }
  }

  void setcartItemCount() async {
    final prefs = await SharedPreferences.getInstance();
    int iCount = globals.cartItemCount--;
    setState(() {
      prefs.setInt('cartItemCount', iCount);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: true,
          title: const Text('Cart'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            inmaintenance == 1
                ? Container(
                    margin: const EdgeInsets.all(15),
                    child: const Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(
                            "App is under Maintenance !!!",
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ]),
                  )
                : const SizedBox(width: 0),
            _loading
                ? const Column(
                    children: [
                      SizedBox(height: 250),
                      CircularProgressIndicator(
                        semanticsLabel: 'Circular progress indicator',
                      ),
                    ],
                  )
                : (cartitems.isNotEmpty
                    ? ListView.builder(
                        itemCount: cartitems.length,
                        shrinkWrap: true,
                        padding: EdgeInsets.zero,
                        physics: const ScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Card(
                              elevation: 3.0,
                              color: const Color.fromARGB(255, 186, 177, 188),
                              child: Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: ListTile(
                                  // dense: true,
                                  contentPadding: const EdgeInsets.only(
                                      right: 0, top: 0, bottom: 0, left: 10),
                                  minVerticalPadding: -8.0,
                                  leading: cartitems[index]["ProImage"] != "" &&
                                          cartitems[index]["ProImage"] != null
                                      ? Image.network(
                                          cartitems[index]["ProImage"],
                                          height: 50,
                                          width: 50,
                                        )
                                      : Image.asset(
                                          "images/default.PNG",
                                          height: 50,
                                          width: 50,
                                        ),
                                  title: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 10.0),
                                        child: Text(
                                          cartitems[index]['proName'] +
                                              " (" +
                                              cartitems[index]['variantSize'] +
                                              ")",
                                          style: const TextStyle(
                                              color: Colors.black,
                                              fontSize: 15),
                                        ),
                                      ),
                                      Transform.translate(
                                        offset: const Offset(9, -2),
                                        child: IconButton(
                                          icon: const Icon(Icons.cancel),
                                          color: Colors.red,
                                          onPressed: () {
                                            removeCartProducts(
                                              cartitems[index]['id'],
                                              globals.userId,
                                              cartProducts,
                                            );
                                            setcartItemCount();
                                          },
                                          iconSize: 22,
                                        ),
                                      ),
                                    ],
                                  ),
                                  subtitle: Transform.translate(
                                    offset: const Offset(0, -8),
                                    child: Center(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          priceUpdating == true
                                              ? const SizedBox(
                                                  height: 15,
                                                  width: 15,
                                                  child:
                                                      CircularProgressIndicator(),
                                                )
                                              : Text(
                                                  "₹ " +
                                                      cartitems[index]['amount']
                                                          .toString(),
                                                  style: const TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 14),
                                                ),
                                          Row(
                                            children: [
                                              IconButton(
                                                icon: const Icon(
                                                    Icons.do_not_disturb_on),
                                                onPressed: priceUpdating
                                                    ? null
                                                    : () {
                                                        setState(() {
                                                          if (cartitems[index]
                                                                  ['qty'] >
                                                              1) {
                                                            priceUpdating =
                                                                true;
                                                            setState(() {
                                                              cartitems[index]
                                                                  ['qty']--;
                                                            });
                                                            setProductPrice(
                                                                cartitems[index]
                                                                    ['id']);
                                                            updateCartQuantity({
                                                              "orderLineId":
                                                                  cartitems[
                                                                          index]
                                                                      ['id'],
                                                              "quantity":
                                                                  cartitems[
                                                                          index]
                                                                      ['qty'],
                                                              "amount":
                                                                  cartitems[
                                                                          index]
                                                                      [
                                                                      'amount'],
                                                              "user_id": globals
                                                                  .userId,
                                                              "type": "cart",
                                                            }, cartProducts);
                                                          }
                                                        });
                                                      },
                                                color: Colors.black,
                                              ),
                                              const SizedBox(width: 3),
                                              Text(cartitems[index]['qty']
                                                  .toString()),
                                              const SizedBox(width: 3),
                                              IconButton(
                                                icon: const Icon(
                                                    Icons.add_circle),
                                                color: Colors.black,
                                                onPressed: priceUpdating
                                                    ? null
                                                    : () {
                                                        if (cartitems[index][
                                                                    'orderLimit'] >=
                                                                1 &&
                                                            cartitems[index]
                                                                    ['qty'] <
                                                                (cartitems[index]
                                                                            [
                                                                            'orderLimit'] >
                                                                        0
                                                                    ? cartitems[
                                                                            index]
                                                                        [
                                                                        'orderLimit']
                                                                    : globals
                                                                        .maxOrder)) {
                                                          setState(() {
                                                            priceUpdating =
                                                                true;
                                                            cartitems[index]
                                                                ['qty']++;
                                                          });
                                                          setProductPrice(
                                                              cartitems[index]
                                                                  ['id']);
                                                          updateCartQuantity({
                                                            "orderLineId":
                                                                cartitems[index]
                                                                    ['id'],
                                                            "quantity":
                                                                cartitems[index]
                                                                    ['qty'],
                                                            "amount":
                                                                cartitems[index]
                                                                    ['amount'],
                                                            "user_id":
                                                                globals.userId,
                                                            "type": "cart",
                                                          }, cartProducts);
                                                        }
                                                      },
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  // trailing: IconButton(
                                  //   icon: const Icon(Icons.cancel),
                                  //   color: Colors.red,
                                  //   onPressed: () {
                                  //     removeCartProducts(
                                  //       cartitems[index]['id'],
                                  //       globals.userId,
                                  //       cartProducts,
                                  //     );
                                  //     setcartItemCount();
                                  //   },
                                  //   iconSize: 30,
                                  // ),
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    : notFoundIcon("Your cart is empty!!")),
          ]),
        ),
      ]),
      bottomNavigationBar:
          !_loading && cartitems.isNotEmpty && inmaintenance != 1
              ? Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          "₹ " + totalPrice.toString(),
                          style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                      Expanded(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: globals.colors['headers'],
                            minimumSize: const Size.fromHeight(50), // NEW
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      CheckOut(cartitems, refreshCart),
                                ));
                          },
                          child: const Text(
                            "Place Order",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : const SizedBox(width: 3),
    );
  }
}
