import 'package:flutter/material.dart';
import 'apis.dart';
import '../globals.dart' as globals;
import 'notify.dart';

class AddNewAddress extends StatefulWidget {
  final Function refresh;
  const AddNewAddress(this.refresh, {Key? key}) : super(key: key);

  @override
  State<AddNewAddress> createState() => _AddNewAddressState();
}

class _AddNewAddressState extends State<AddNewAddress> {
  final _nameController = TextEditingController();
  final _phoneController = TextEditingController();
  // final _emailController = TextEditingController();
  final _othersController = TextEditingController();
  final _addressController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  int? _locationController;
  bool _isAdding = false;
  List areas = [];

  InputDecoration inputBoxDesign(Icon icon, String hint) {
    return InputDecoration(
      prefixIcon: icon,
      hintText: hint,
    );
  }

  void updateAddress(status, data) {
    if (status) {
      setState(() {
        notify(context, " Added Successfully", Colors.green);
        widget.refresh();
        Navigator.of(context).pop();
      });
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      _isAdding = false;
    });
  }

  void setArea(status, data) {
    if (status) {
      areas = data;
      _locationController = areas[0]['id'];
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getShippingArea(setArea);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: true,
          title: const Text('Add New Address'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            Container(
              margin: const EdgeInsets.only(top: 20, left: 10, right: 10),
              width: double.infinity,
              constraints: const BoxConstraints(
                maxHeight: double.infinity,
                minHeight: 150,
              ),
              decoration: ShapeDecoration(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: const BorderSide(width: 1, color: Colors.black),
                ),
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      children: <Widget>[
                        const SizedBox(height: 15),
                        Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 15, left: 20, right: 20),
                                child: TextFormField(
                                  validator: (value) => nameValidator(value),
                                  controller: _nameController,
                                  decoration: inputBoxDesign(
                                    const Icon(Icons.person,
                                        color: Colors.black),
                                    "Name",
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 15, left: 20, right: 20),
                                child: TextFormField(
                                  keyboardType: TextInputType.number,
                                  validator: (value) => phoneValidator(value),
                                  controller: _phoneController,
                                  decoration: inputBoxDesign(
                                    const Icon(Icons.phone_iphone,
                                        color: Colors.black),
                                    "Phone",
                                  ),
                                ),
                              ),
                              // Padding(
                              //   padding: const EdgeInsets.only(
                              //       bottom: 15, left: 20, right: 20),
                              //   child: TextFormField(
                              //     validator: (value) => emailValidator(value),
                              //     keyboardType: TextInputType.emailAddress,
                              //     controller: _emailController,
                              //     decoration: inputBoxDesign(
                              //       const Icon(Icons.mail, color: Colors.black),
                              //       "Email",
                              //     ),
                              //   ),
                              // ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 15, left: 20, right: 20),
                                child: TextFormField(
                                  validator: (value) => streetValidator(value),
                                  controller: _addressController,
                                  decoration: inputBoxDesign(
                                    const Icon(Icons.home, color: Colors.black),
                                    "House No. and Street Name",
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 15, left: 20, right: 20),
                                child: DropdownButtonFormField<dynamic>(
                                  isExpanded: true,
                                  items: areaDropdown(),
                                  value: _locationController,
                                  onChanged: (value) {
                                    setState(() {
                                      _locationController = value;
                                    });
                                  },
                                  decoration: inputBoxDesign(
                                    const Icon(Icons.location_city,
                                        color: Colors.black),
                                    "Select a Area",
                                  ),
                                ),
                              ),
                              _locationController == 0
                                  ? Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 15, left: 20, right: 20),
                                      child: TextFormField(
                                        controller: _othersController,
                                        decoration: inputBoxDesign(
                                          const Icon(Icons.alt_route_sharp,
                                              color: Colors.black),
                                          "Location",
                                        ),
                                      ),
                                    )
                                  : const SizedBox(height: 0),
                              const SizedBox(height: 15),
                              Align(
                                alignment: Alignment.center,
                                child: SizedBox(
                                  width: 100,
                                  child: _isAdding
                                      ? const Column(
                                          children: [
                                            CircularProgressIndicator(
                                              semanticsLabel:
                                                  'Circular progress indicator',
                                            )
                                          ],
                                        )
                                      : ElevatedButton(
                                          child: const Text("Save"),
                                          style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStateProperty
                                                      .all<Color>(globals
                                                          .colors['headers']!),
                                              shape: MaterialStateProperty.all<
                                                      RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(18.0),
                                              ))),
                                          onPressed: () {
                                            var status = _formKey.currentState
                                                ?.validate();
                                            if (status != null && status) {
                                              setState(() {
                                                _isAdding = true;
                                              });
                                              addAddress({
                                                'name': _nameController.text,
                                                'phone': _phoneController.text,
                                                // 'email': _emailController.text,
                                                'address':
                                                    _addressController.text,
                                                'areaId': _locationController,
                                                'others':
                                                    _othersController.text,
                                                'user_id': globals.userId,
                                              }, updateAddress);
                                            }
                                          },
                                        ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ]),
        ),
      ]),
    );
  }

  // validators ----------------
  nameValidator(value) {
    if (value == null || value.isEmpty) {
      return "Name can't be empty";
    }
    return null;
  }

  phoneValidator(value) {
    if (value == null || value.isEmpty) {
      return "Phone can't be empty";
    }
    return null;
  }

  emailValidator(value) {
    if (value == null || value.isEmpty) {
      return "Email can't be empty";
    }
    return null;
  }

  streetValidator(value) {
    if (value == null || value.isEmpty) {
      return "Street and House No. can't be empty";
    }
    return null;
  }

  locationValidator(value) {
    if (value == null || value.isEmpty) {
      return "Location can't be empty";
    }
    return null;
  }

  List<DropdownMenuItem> areaDropdown() {
    return areas.isNotEmpty
        ? [
            ...areas.map((e) {
              return DropdownMenuItem(
                child: Text(e['area']),
                value: e['id'],
              );
            }),
            const DropdownMenuItem(
              child: Text("Others"),
              value: 0,
            ),
          ]
        : [
            const DropdownMenuItem(
              child: Text("Area unavilable"),
              value: 0,
            )
          ];
  }
}
