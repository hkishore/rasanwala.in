import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../globals.dart' as globals;
import 'apis.dart';
import 'notify.dart';
import 'all_popus.dart';
import 'single_product.dart';

class ProductComponent extends StatefulWidget {
  final String pName;
  final int proId;
  final List variant;
  final Map orderdId;
  final Function updateProduct;
  const ProductComponent(
      this.pName, this.proId, this.variant, this.orderdId, this.updateProduct,
      {Key? key})
      : super(key: key);

  @override
  _ProductComponentState createState() => _ProductComponentState();
}

class _ProductComponentState extends State<ProductComponent> {
  late String productPrice;
  late String regularPrice;
  late int quantity;
  late int orderLineId;
  bool _loading = false;
  bool _mloading = false;
  bool _aloading = false;
  bool matched = false;
  late int variantId;
  double percentage = 0.0;
  int orderLimit = globals.maxOrder;

  @override
  void initState() {
    super.initState();
    regularPrice = widget.variant[0]['regular_price'].toString();
    productPrice = widget.variant[0]['sale_price'].toString();
    variantId = widget.variant[0]['id'];
    quantity = 1;
    orderLineId = 0;
    if (widget.variant[0]['order_limit'] > 0) {
      orderLimit = widget.variant[0]['order_limit'];
    }
    if (quantity > orderLimit) {
      quantity = orderLimit;
    }
    setVariant();
  }

  @override
  void didUpdateWidget(covariant ProductComponent oldWidget) {
    regularPrice = widget.variant[0]['regular_price'].toString();
    productPrice = widget.variant[0]['sale_price'].toString();
    variantId = widget.variant[0]['id'];
    quantity = 1;
    orderLineId = 0;
    if (widget.variant[0]['order_limit'] > 0) {
      orderLimit = widget.variant[0]['order_limit'];
    }
    if (quantity > orderLimit) {
      quantity = orderLimit;
    }
    setVariant();
    super.didUpdateWidget(oldWidget);
  }

  void setVariant() {
    if (widget.variant.isNotEmpty) {
      final count = widget.variant.length;
      int i = 0;
      for (i = 0; i < count; i++) {
        if (widget.orderdId.isNotEmpty) {
          widget.orderdId.forEach((key, value) {
            if (value['variant_id'] == widget.variant[i]['id']) {
              quantity =
                  widget.orderdId[widget.variant[i]['id'].toString()]['qty'];
              variantId = widget.variant[i]['id'];
              orderLineId =
                  widget.orderdId[widget.variant[i]['id'].toString()]['id'];
              regularPrice = widget.variant[i]['regular_price'].toString();
              productPrice = widget.variant[i]['sale_price'].toString();
              addOfferLable(productPrice, regularPrice);
              matched = true;
              if (widget.variant[i]['order_limit'] > 0) {
                orderLimit = widget.variant[i]['order_limit'];
              }
              if (quantity > orderLimit) {
                quantity = orderLimit;
              }
            }
          });
        }
      }
    }
  }

  void update(status, data) {
    if (status) {
      setState(() {
        matched = true;
        orderLineId = data['order_line_id'];
        setcartItemCount();
        widget.updateProduct();
      });
      notify(context, " Added Successfully", Colors.green);
    } else {
      notify(context, globals.unKnownError, Colors.red);
      widget.updateProduct();
      setState(() {});
    }
    setState(() {
      _loading = false;
      _mloading = false;
      _aloading = false;
    });
  }

  void updateQuantity(status, data) {
    if (status) {
      setState(() {
        matched = true;
        orderLineId = data['order_line_id'];
        widget.updateProduct();
      });
      notify(context, " Quantity updated Successfully", Colors.green);
    } else {
      notify(context, globals.unKnownError, Colors.red);
      widget.updateProduct();
      setState(() {});
    }
    setState(() {
      _mloading = false;
      _aloading = false;
    });
  }

  void setcartItemCount() async {
    final prefs = await SharedPreferences.getInstance();
    int iCount = globals.cartItemCount++;
    prefs.setInt('cartItemCount', iCount);
    setState(() {});
  }

  void setProductPrice(int vId) {
    if (widget.variant.isNotEmpty) {
      final count = widget.variant.length;
      int i = 0;
      for (i = 0; i < count; i++) {
        if (widget.variant[i]['id'] == vId) {
          productPrice = widget.variant[i]['sale_price'].toString();
          regularPrice = widget.variant[i]['regular_price'].toString();
          variantId = widget.variant[i]['id'];
          if (widget.variant[i]['order_limit'] > 0) {
            orderLimit = widget.variant[i]['order_limit'];
          }
          if (quantity > orderLimit) {
            quantity = orderLimit;
          }
          // addOfferLable(productPrice, regularPrice);
          // widget.updateProduct();
          break;
        }
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SingleProduct(
                      widget.proId, widget.orderdId, widget.updateProduct),
                ));
          },
          child: Container(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Text(
              widget.pName,
              style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
          child: DropdownButtonFormField<dynamic>(
            isExpanded: true,
            items: varientDropdown(),
            value: variantId,
            onChanged: (value) {
              setProductPrice(value);
              setState(() {});
            },
            decoration: const InputDecoration(
              hintText: "Select product",
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Text(
                "₹ " + productPrice,
                style: TextStyle(
                    height: 1.0,
                    fontSize: 16,
                    color: int.parse(productPrice) < int.parse(regularPrice)
                        ? const Color.fromARGB(255, 17, 155, 88)
                        : const Color.fromARGB(255, 0, 0, 0),
                    fontWeight: FontWeight.bold),
              ),
              padding: const EdgeInsets.only(left: 16, top: 5, bottom: 5),
            ),
            int.parse(productPrice) < int.parse(regularPrice)
                ? Container(
                    padding: const EdgeInsets.only(top: 5, bottom: 5, right: 5),
                    child: Stack(
                      children: [
                        Text(
                          "₹ " + regularPrice,
                          style: const TextStyle(
                            height: 1.0,
                            fontSize: 16,
                            color: Colors.grey,
                          ),
                        ),
                        Positioned(
                          top: 7,
                          child: Container(
                              width: 100, height: 2, color: Colors.red),
                        ),
                      ],
                    ),
                  )
                : const SizedBox(width: 0),
            widget.orderdId.isNotEmpty && matched == true
                ? Container(
                    alignment: Alignment.bottomLeft,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        IconButton(
                          icon: _mloading
                              ? SizedBox(
                                  child: CircularProgressIndicator(
                                    color: globals.colors['headers'],
                                  ),
                                  height: 15.0,
                                  width: 15.0,
                                )
                              : const Icon(Icons.do_not_disturb_on),
                          onPressed: _mloading
                              ? null
                              : () {
                                  if (quantity > 1) {
                                    setState(() {
                                      quantity--;
                                      _mloading = true;
                                    });
                                    updateCartQuantity({
                                      'orderLineId': orderLineId,
                                      "quantity": quantity,
                                      "amount":
                                          int.parse(productPrice) * quantity,
                                      "user_id": globals.userId,
                                      "type": "product",
                                    }, updateQuantity);
                                  }
                                },
                          color: Colors.black,
                        ),
                        const SizedBox(width: 3),
                        Text(quantity.toString()),
                        const SizedBox(width: 3),
                        IconButton(
                          icon: _aloading
                              ? SizedBox(
                                  child: CircularProgressIndicator(
                                    color: globals.colors['headers'],
                                  ),
                                  height: 15.0,
                                  width: 15.0,
                                )
                              : const Icon(Icons.add_circle),
                          color: Colors.black,
                          onPressed: _aloading
                              ? null
                              : () {
                                  if (quantity < orderLimit) {
                                    setState(() {
                                      quantity++;
                                      _aloading = true;
                                    });
                                    updateCartQuantity({
                                      'orderLineId': orderLineId,
                                      "quantity": quantity,
                                      "amount":
                                          int.parse(productPrice) * quantity,
                                      "user_id": globals.userId,
                                      "type": "product",
                                    }, updateQuantity);
                                  }
                                },
                        ),
                      ],
                    ),
                  )
                : Container(
                    padding:
                        const EdgeInsets.only(left: 12, right: 12, bottom: 5),
                    height: 30,
                    width: 100,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: globals.colors['headers'],
                        minimumSize: const Size.fromHeight(24), // NEW
                      ),
                      onPressed: () {
                        setState(() {
                          _loading = true;
                        });
                        addToCart(
                          {
                            'variant_id': variantId,
                            'amount': productPrice,
                            'quantity': quantity,
                            'product_id': widget.proId,
                            'order_line_id': orderLineId,
                            'user_id': globals.userId,
                          },
                          update,
                        );
                      },
                      child: _loading
                          ? SizedBox(
                              child: CircularProgressIndicator(
                                color: globals.colors['body'],
                              ),
                              height: 15.0,
                              width: 15.0,
                            )
                          : const Text(
                              "Add",
                              style: TextStyle(fontSize: 12),
                            ),
                    ),
                  ),
          ],
        ),
      ],
    );
  }

  List<DropdownMenuItem> varientDropdown() {
    return widget.variant.isNotEmpty
        ? [
            // const DropdownMenuItem(
            //   child: Text("Select a variant"),
            //   value: 0,
            // ),
            ...widget.variant.map((e) {
              return DropdownMenuItem(
                child: Text(e['size'] + "/ ₹ " + e['sale_price'].toString()),
                value: e['id'],
              );
            }),
          ]
        : [
            const DropdownMenuItem(
              child: Text("Varients unavilable"),
              value: 0,
            )
          ];
  }
}
