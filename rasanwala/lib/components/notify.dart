import 'package:flutter/material.dart';

notify(context, msg, color) {
  final snackBar = SnackBar(
    backgroundColor: color,
    content: Text(msg),
    action: SnackBarAction(
      label: 'Close',
      onPressed: () {
        // Some code to undo the change.
      },
    ),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
