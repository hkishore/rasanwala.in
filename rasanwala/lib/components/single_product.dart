import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'apis.dart';
import '../globals.dart' as globals;
import 'notify.dart';
import 'all_popus.dart';
import 'package:html/parser.dart';

class SingleProduct extends StatefulWidget {
  final int productId;
  final Map orderdId;
  final Function refres;
  const SingleProduct(this.productId, this.orderdId, this.refres, {Key? key})
      : super(key: key);

  @override
  _SingleProductState createState() => _SingleProductState();
}

class _SingleProductState extends State<SingleProduct> {
  late int variantId;
  late int orderLineId;
  late String productPrice;
  late String regularPrice;
  late Map product;
  String offerPercentage = "0";
  bool _loading = false;
  List _varients = [];
  int quantity = 1;
  bool _isAdding = false;

  @override
  void initState() {
    super.initState();
    refreshSingle();
  }

  refreshSingle() {
    singleProduct(widget.productId, _products);
    _loading = true;
  }

  void _products(status, data) {
    if (status) {
      setState(() {
        product = data;
        _varients = data["varients"];
        variantId = _varients[0]['id'];
        regularPrice = _varients[0]['regular_price'].toString();
        productPrice = _varients[0]['sale_price'].toString();
        offerPercentage = _varients[0]['offerPercentage'];
        orderLineId = 0;
        setVariant();
      });
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      _loading = false;
    });
  }

  void setVariant() {
    if (_varients.isNotEmpty) {
      final count = _varients.length;
      int i = 0;
      for (i = 0; i < count; i++) {
        if (widget.orderdId.isNotEmpty) {
          widget.orderdId.forEach((key, value) {
            if (value['variant_id'] == _varients[i]['id']) {
              quantity = widget.orderdId[_varients[i]['id'].toString()]['qty'];
              variantId = _varients[i]['id'];
              orderLineId =
                  widget.orderdId[_varients[i]['id'].toString()]['id'];
              regularPrice = _varients[i]['regular_price'].toString();
              productPrice = _varients[i]['sale_price'].toString();
              offerPercentage = _varients[i]['offerPercentage'];
            }
          });
          break;
        }
      }
    }
    setState(() {});
  }

  void setProductPrice(int vId) {
    if (_varients.isNotEmpty) {
      final count = _varients.length;
      int i = 0;
      for (i = 0; i < count; i++) {
        if (_varients[i]['id'] == vId) {
          int rs = _varients[i]['sale_price'] * quantity;
          productPrice = rs.toString();
          regularPrice = _varients[i]['regular_price'].toString();
          offerPercentage = _varients[i]['offerPercentage'];
          break;
        }
      }
    }
  }

  void setQuantityProductPrice() {
    setState(() {
      int rs = (int.parse(productPrice) * quantity);
      productPrice = rs.toString();
    });
  }

  void update(status, data) {
    if (status) {
      setState(() {
        widget.refres();
        orderLineId = data['order_line_id'];
        setcartItemCount();
        notify(context, " Added Successfully", Colors.green);
      });
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      _isAdding = false;
    });
  }

  void setcartItemCount() async {
    final prefs = await SharedPreferences.getInstance();
    int iCount = globals.cartItemCount++;
    prefs.setInt('cartItemCount', iCount);
    setState(() {});
  }

  String _parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString =
        parse(document.body!.text).documentElement!.text;
    return parsedString;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: true,
          title: const Text('Product Details'),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          actions: [
            cartIcon(context, refreshSingle),
          ],
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            !_loading && product.isNotEmpty
                ? Column(
                    children: <Widget>[
                      product["image"] != "" && product["image"] != null
                          ? SizedBox(
                              height: 300,
                              child: Center(
                                child: Image.network(
                                  product["image"],
                                  fit: BoxFit.contain,
                                ),
                              ),
                            )
                          : SizedBox(
                              height: 300,
                              child: Center(
                                child: Image.asset("images/default.PNG"),
                              ),
                            ),
                      const SizedBox(height: 80),
                      Row(
                        children: [
                          Container(
                            child: Text(
                              product["name"],
                              style: const TextStyle(
                                  height: 1.0,
                                  fontSize: 20,
                                  color: Color.fromARGB(255, 0, 0, 0),
                                  fontWeight: FontWeight.bold),
                            ),
                            padding: const EdgeInsets.only(left: 16, bottom: 8),
                          ),
                        ],
                      ),
                      product["brandName"] != "" && product["brandName"] != null
                          ? Row(
                              children: [
                                Container(
                                  child: Text(
                                    "Brand: " + product["brandName"],
                                    style: const TextStyle(
                                        height: 1.0,
                                        fontSize: 16,
                                        color: Color(0xFF6F8398),
                                        fontWeight: FontWeight.bold),
                                  ),
                                  padding: const EdgeInsets.only(
                                      left: 16, bottom: 8),
                                ),
                              ],
                            )
                          : const SizedBox(height: 0),
                      product["description"] != "" &&
                              product["description"] != null
                          ? Container(
                              child: Text(
                                _parseHtmlString(product["description"]),
                                textAlign: TextAlign.justify,
                                style: const TextStyle(
                                  height: 1.5,
                                  color: Color(0xFF6F8398),
                                ),
                              ),
                              padding: const EdgeInsets.all(16),
                            )
                          : const SizedBox(height: 0),
                    ],
                  )
                : const Column(
                    children: [
                      SizedBox(height: 250),
                      CircularProgressIndicator(
                        semanticsLabel: 'Circular progress indicator',
                      ),
                    ],
                  ),
          ]),
        ),
      ]),
      bottomNavigationBar: !_loading && product.isNotEmpty
          ? Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding:
                      const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                  child: DropdownButtonFormField<dynamic>(
                    isExpanded: true,
                    items: varientDropdown(),
                    value: variantId,
                    onChanged: (value) {
                      setState(() {
                        variantId = value;
                      });
                      setProductPrice(variantId);
                    },
                    decoration: const InputDecoration(
                      hintText: "Select product",
                    ),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      child: Text(
                        "₹ " + productPrice,
                        style: const TextStyle(
                            height: 1.0,
                            fontSize: 20,
                            color: Color.fromARGB(255, 0, 0, 0),
                            fontWeight: FontWeight.bold),
                      ),
                      padding:
                          const EdgeInsets.only(left: 16, top: 10, bottom: 16),
                    ),
                    int.parse(offerPercentage) != 0
                        ? Container(
                            padding: const EdgeInsets.only(
                                left: 16, top: 10, bottom: 16),
                            child: Stack(
                              children: [
                                Text(
                                  "₹ " + regularPrice,
                                  style: const TextStyle(
                                    height: 1.0,
                                    fontSize: 20,
                                    color: Colors.grey,
                                  ),
                                ),
                                Positioned(
                                  top: 7,
                                  child: Container(
                                      width: 100, height: 2, color: Colors.red),
                                ),
                              ],
                            ),
                          )
                        : const SizedBox(width: 0),
                    int.parse(offerPercentage) != 0
                        ? Container(
                            padding: const EdgeInsets.only(
                                left: 16, top: 10, bottom: 16),
                            child: Text(
                              "( " + offerPercentage + "% off )",
                              style: const TextStyle(
                                height: 1.0,
                                fontSize: 14,
                                color: Colors.green,
                              ),
                            ),
                          )
                        : const SizedBox(width: 0),
                  ],
                ),
                Container(
                  padding: const EdgeInsets.only(
                      left: 16, right: 16, bottom: 10, top: 10),
                  child: Row(
                    children: [
                      orderLineId > 0
                          ? Expanded(
                              child: Container(
                                padding: const EdgeInsets.all(3),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.grey,
                                ),
                                child: Row(
                                  // mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Expanded(
                                      child: IconButton(
                                        icon:
                                            const Icon(Icons.do_not_disturb_on),
                                        onPressed: () {
                                          setState(() {
                                            if (quantity > 1) {
                                              quantity--;
                                              setProductPrice(variantId);
                                              updateCartQuantity({
                                                "orderLineId": orderLineId,
                                                "quantity": quantity,
                                                "amount": productPrice,
                                                "user_id": globals.userId,
                                                "type": "product",
                                              }, update);
                                            }
                                          });
                                        },
                                        color: Colors.black,
                                      ),
                                    ),
                                    const SizedBox(width: 10),
                                    Container(
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 3),
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 3, vertical: 2),
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(3),
                                          color: Colors.white),
                                      child: Text(
                                        quantity.toString(),
                                        style: const TextStyle(
                                            color: Colors.black, fontSize: 16),
                                      ),
                                    ),
                                    const SizedBox(width: 10),
                                    Expanded(
                                      child: IconButton(
                                        icon: const Icon(Icons.add_circle),
                                        color: Colors.black,
                                        onPressed: () {
                                          setState(() {
                                            if (quantity <= 9) {
                                              quantity++;
                                              setProductPrice(variantId);
                                              updateCartQuantity({
                                                "orderLineId": orderLineId,
                                                "quantity": quantity,
                                                "amount": productPrice,
                                                "user_id": globals.userId,
                                                "type": "product",
                                              }, update);
                                            }
                                          });
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: globals.colors['headers'],
                                  minimumSize: const Size.fromHeight(55),
                                ),
                                onPressed: () {
                                  setState(() {
                                    _isAdding = true;
                                  });
                                  addToCart(
                                    {
                                      'variant_id': variantId,
                                      'amount': productPrice,
                                      'quantity': quantity,
                                      'product_id': product['id'],
                                      'order_line_id': orderLineId,
                                      'user_id': globals.userId,
                                    },
                                    update,
                                  );
                                },
                                child: !_isAdding
                                    ? const Text(
                                        "Add to Cart",
                                        style: TextStyle(fontSize: 18),
                                      )
                                    : SizedBox(
                                        child: CircularProgressIndicator(
                                          color: globals.colors['body'],
                                        ),
                                        height: 15.0,
                                        width: 15.0,
                                      ),
                              ),
                            ),
                    ],
                  ),
                ),
              ],
            )
          : const SizedBox(height: 0),
    );
  }

  List<DropdownMenuItem> varientDropdown() {
    return _varients.isNotEmpty
        ? [
            // const DropdownMenuItem(
            //   child: Text("Select a variant"),
            //   value: 0,
            // ),
            ..._varients.map((e) {
              return DropdownMenuItem(
                child: Text(e['size'] + "/ ₹ " + e['sale_price'].toString()),
                value: e['id'],
              );
            }),
          ]
        : [
            const DropdownMenuItem(
              child: Text("Varients unavilable"),
              value: 0,
            )
          ];
  }
}
