import 'package:flutter/material.dart';
import 'apis.dart';
import 'notify.dart';
import '../globals.dart' as globals;
import 'common_products.dart';
import 'all_popus.dart';

class Brands extends StatefulWidget {
  const Brands({Key? key}) : super(key: key);

  @override
  State<Brands> createState() => _BrandsState();
}

class _BrandsState extends State<Brands> {
  List brands = [];
  bool _loading = false;

  @override
  void initState() {
    super.initState();
    _loading = true;
    refreshBarand();
  }

  refreshBarand() {
    getBrands(_allbrands);
  }

  void _allbrands(status, data) {
    if (status) {
      setState(() {
        brands = data;
      });
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width / 3;
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: globals.colors['headers'],
            floating: true,
            pinned: true,
            snap: true,
            centerTitle: true,
            title: const Text('Brands'),
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios_new),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            actions: [
              cartIcon(context, refreshBarand),
            ],
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              _loading
                  ? const Column(
                      children: [
                        SizedBox(height: 250),
                        CircularProgressIndicator(
                          semanticsLabel: 'Circular progress indicator',
                        ),
                      ],
                    )
                  : (brands.isNotEmpty
                      ? Wrap(
                          alignment: WrapAlignment.start,
                          direction: Axis.horizontal,
                          children: <Widget>[
                            ...brands.map((e) {
                              return SizedBox(
                                width: screenWidth,
                                child: GestureDetector(
                                  onTap: () {
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus();
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => CommonProducts(
                                            4, e['name'], e['id']),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    margin: const EdgeInsets.all(5.0),
                                    constraints:
                                        const BoxConstraints(minHeight: 56.0),
                                    child: Card(
                                      elevation: 3.0,
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 150.0,
                                            child: Stack(
                                              children: <Widget>[
                                                Positioned.fill(
                                                    child: e["image"] != "" &&
                                                            e["image"] != null
                                                        ? Image.network(
                                                            e["image"],
                                                            fit: BoxFit.contain,
                                                          )
                                                        : Center(
                                                            child: Image.asset(
                                                                "images/default.PNG"),
                                                          )),
                                                Container(
                                                  color: Colors.black38,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            padding: const EdgeInsets.only(
                                                left: 3.0,
                                                bottom: 3.0,
                                                top: 5.0),
                                            alignment: Alignment.bottomLeft,
                                            child: GestureDetector(
                                              onTap: () {
                                                // Navigator.push(
                                                //     context,
                                                //     MaterialPageRoute(
                                                //       builder: (context) =>
                                                //           const AllProducts(),
                                                //     ));
                                              },
                                              child: Text(
                                                e['name'],
                                                style: const TextStyle(
                                                    fontSize: 16.0,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            }),
                          ],
                        )
                      : notFoundIcon("Brands Not Avilable!!")),
            ]),
          ),
        ],
      ),
    );
  }
}
