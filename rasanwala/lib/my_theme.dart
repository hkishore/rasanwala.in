import 'package:flutter/material.dart';
import 'globals.dart' as globals;

ThemeData myTheme() {
  return ThemeData(
    canvasColor: globals.colors['body'],
    appBarTheme: AppBarTheme(backgroundColor: globals.colors['headers']),
    primaryColor: globals.colors['headers'],
    primaryColorDark: globals.colors['headers'],
    primaryColorLight: globals.colors['headers'],
    // primarySwatch: Colors.green,
    iconTheme: IconThemeData(color: globals.colors['icons']),
    textTheme: TextTheme(
      bodyLarge: TextStyle(color: globals.colors['text']),
      bodyMedium: TextStyle(color: globals.colors['text']),
      titleMedium: TextStyle(color: globals.colors['text']),
      labelLarge: TextStyle(color: globals.colors['black']),
    ),
    // textSelectionTheme: TextSelectionThemeData(
    //     cursorColor: globals.colors['headers'],
    //     selectionColor: globals.colors['headers']),
    checkboxTheme: CheckboxThemeData(
      checkColor: MaterialStateProperty.all(globals.colors['headers']),
      fillColor: MaterialStateProperty.all(globals.colors['text']),
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: globals.colors['text'],
      foregroundColor: globals.colors['headers'],
    ),
    // bottomNavigationBarTheme: BottomNavigationBarThemeData(
    //   selectedItemColor: globals.colors['headers'],
    //   unselectedItemColor: globals.colors['text'],
    // ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(globals.colors['headers']),
        foregroundColor: MaterialStateProperty.all(globals.colors['icons']),
      ),
    ),
    // dialogTheme: DialogTheme(
    //   backgroundColor: globals.colors['body'],
    //   titleTextStyle: TextStyle(
    //     color: globals.colors['headers'],
    //     fontSize: 25,
    //   ),
    // ),
    inputDecorationTheme: const InputDecorationTheme(
        // errorStyle: const TextStyle(color: Colors.red),
        // labelStyle: const TextStyle(color: Colors.black),
        // hintStyle: const TextStyle(color: Colors.black),
        // border: OutlineInputBorder(
        //   borderRadius: BorderRadius.circular(5),
        // ),
        // focusedBorder: OutlineInputBorder(
        //   borderSide: BorderSide(color: Color.fromARGB(255, 240, 88, 17)),
        // ),
        // errorBorder: OutlineInputBorder(
        //   borderSide: BorderSide(color: Colors.white),
        // ),
        ),
  );
}
