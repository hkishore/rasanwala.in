import 'dart:io';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'components/my_notification.dart';
import 'login.dart';
import 'home_page.dart';
import 'globals.dart' as globals;
import 'my_theme.dart';
import 'components/loading.dart';
import 'components/apis.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

final FirebaseMessaging messaging = FirebaseMessaging.instance;

Future<void> registerNotification() async {
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  if (Platform.isIOS) {
    await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
  }
  await MyNotification.initialize();
  FirebaseMessaging.onBackgroundMessage(myBackgroundMessageHandler);
  // if (settings.authorizationStatus == AuthorizationStatus.authorized) {
  // } else {
  //   print('User declined or has not accepted permission');
  // }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await registerNotification();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: globals.colors['headers'], // status bar color
  ));
  runApp(
    MaterialApp(
      title: 'Sah Rasan Wala',
      theme: myTheme(),
      home: const RasanApp(),
    ),
  );
}

class RasanApp extends StatefulWidget {
  const RasanApp({Key? key}) : super(key: key);

  @override
  State<RasanApp> createState() => _RasanAppState();
}

class _RasanAppState extends State<RasanApp> {
  bool _isLoaded = false;
  bool _isLogin = false;

  updateAuth(data) async {
    try {
      if (data['api_token'] != "") {
        // Saving information in globals variable
        globals.apiToken = data['api_token'];
        globals.userId = data['user_id'].toString();
        globals.name = data['name'];
        globals.phoneNo = data['phone_no'].toString();
        if (data['email'] != null && data['email'] != "") {
          globals.email = data['email'];
        }
        globals.role = data['role'].toString();
        globals.cartItemCount = data['badgeCnt'];
        globals.favItemCount = data['favCnt'];

        // write value
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('apiToken', data['api_token']);
        prefs.setString('userId', data['user_id'].toString());
        prefs.setString('name', data['name']);
        prefs.setString('phoneNo', data['phone_no'].toString());
        if (data['email'] != null && data['email'] != "") {
          prefs.setString('email', data['email']);
        }
        prefs.setString('role', data['role'].toString());
        prefs.setInt('cartItemCount', data['badgeCnt']);
        prefs.setInt('favItemCount', data['favCnt']);
        authState(true);
        setState(() {});
      }
    } catch (err) {
      // print(err);
    }
  }

  loadInfos() async {
    try {
      // read storage
      final prefs = await SharedPreferences.getInstance();
      globals.apiToken = prefs.getString('apiToken');
      globals.userId = prefs.getString('userId');
      globals.name = prefs.getString('name');
      globals.phoneNo = prefs.getString('phoneNo');
      globals.email = prefs.getString('email');
      globals.role = prefs.getString('role');
      globals.favItemCount = prefs.getInt('favItemCount') ?? 0;

      globals.cartItemCount = prefs.getInt('cartItemCount') ?? 0;
      _isLoaded = true;
      if (globals.apiToken != null && globals.userId != null) {
        var loginRespose = await checkLogin();
        if (loginRespose.statusCode == 200) {
          // print(loginRespose.statusCode);
        } else {
          logoutCode();
        }
        authState(true);
      } else {
        authState(false);
      }
      // print(prefs.getString('apiToken'));
      // print(prefs.getString('userId'));
      // print(_isLogin);
    } catch (err) {
      // print(err);
    }
  }

  logout() async {
    final response = await logoutApi({'user_id': globals.userId});
    if (response.statusCode == 200) {
      logoutCode();
    }
  }

  logoutCode() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
    authState(false);
  }

  authState(state) async {
    if ((globals.apiToken != null || globals.apiToken != "") && state == true) {
      final token = await messaging.getToken();
      // print("@@@@@@@@@@@@@@@@@@@");
      // print(token);
      setToken({
        "token": token,
      }, _allProducts);
    }
    setState(() {
      _isLogin = state;
    });
  }

  void _allProducts(status, data) async {
    if (status) {
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    globals.updateTheme();
    globals.logout = () => logout();
    loadInfos();
    AwesomeNotifications().setListeners(
      onActionReceivedMethod: (ReceivedAction receivedAction) async {
        await onActionReceivedMethod(context, receivedAction);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return _isLoaded
        ? (_isLogin ? HomePage(logout) : LoginPage(updateAuth))
        : const LoadingPage();
  }
}
