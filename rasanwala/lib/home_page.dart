// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';
import 'package:rasanwala/components/notification_listing.dart';
import 'components/notify.dart';
import 'components/products.dart';
import 'components/all_popus.dart';
import 'components/apis.dart';
import 'globals.dart' as globals;
import 'components/drawer.dart';
import 'components/common_products.dart';
import 'components/offers_page.dart';

class HomePage extends StatefulWidget {
  final Function logout;
  const HomePage(this.logout, {Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List categoryData = [];
  bool _loading = false;
  int inmaintenance = 2;
  String hometext = "";
  final _searchController = TextEditingController();

  void _allCategory(status, data, appmaintenance) {
    if (status) {
      setState(() {
        categoryData = data;
        inmaintenance = int.parse(appmaintenance);
      });
    } else {
      notify(context, globals.unKnownError, Colors.red);
    }
    setState(() {
      _loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    refreshCat();
    getCommonConfig(_updateIndex);
  }

  void _updateIndex(data) {
    setState(() {
      hometext = data;
    });
  }

  refreshCat() {
    getCategories(_allCategory);
    globals.homeSetState = () => setState(() {});
    _loading = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyDrawer(),
      body: CustomScrollView(slivers: [
        SliverAppBar(
          backgroundColor: globals.colors['headers'],
          automaticallyImplyLeading: false,
          floating: true,
          pinned: true,
          snap: true,
          centerTitle: false,
          title: const Text(
            'Sah Rasan Wala',
            style: TextStyle(
              fontSize: 22.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.notifications),
              onPressed: () {
                FocusManager.instance.primaryFocus?.unfocus();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Notificationlist(),
                  ),
                );
              },
            ),
            favoriteIcon(context, refreshCat),
            const SizedBox(width: 5),
            IconButton(
              icon: const Icon(Icons.sell),
              onPressed: () {
                FocusManager.instance.primaryFocus?.unfocus();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const OfferPage(),
                  ),
                );
              },
            ),
            const SizedBox(width: 5),
            IconButton(
              icon: const Icon(
                Icons.logout,
              ),
              onPressed: () {
                FocusManager.instance.primaryFocus?.unfocus();
                logoutDialog(context, widget.logout);
              },
            ),
          ],
          bottom: AppBar(
            backgroundColor: globals.colors['headers'],
            title: Container(
              width: double.infinity,
              height: 45,
              color: Colors.white,
              child: Center(
                child: TextField(
                  controller: _searchController,
                  decoration: const InputDecoration(
                    hintText: 'Search for something',
                    prefixIcon: Icon(Icons.search),
                    //suffixIcon: Icon(Icons.camera_alt)),
                  ),
                  onChanged: (value) => searchItems(value, _allCategory),
                ),
              ),
            ),
            actions: <Widget>[
              cartIcon(context, refreshCat),
            ],
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            hometext != ""
                ? Column(children: <Widget>[
                    SizedBox(
                      width: double.infinity,
                      height: 40.0,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: 20,
                          right: 20,
                        ),
                        child: Marquee(
                          text: hometext,
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 22,
                              fontWeight: FontWeight.bold),
                          scrollAxis: Axis.horizontal,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          blankSpace: 20,
                          showFadingOnlyWhenScrolling: true,
                          fadingEdgeStartFraction: 0.1,
                          fadingEdgeEndFraction: 0.1,
                          startPadding: 20,
                          textDirection: TextDirection.ltr,
                        ),
                      ),
                    ),
                  ])
                : Column(children: <Widget>[
                    SizedBox(
                      width: double.infinity,
                      height: 40.0,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: 20,
                          right: 20,
                        ),
                        child: Text(
                          ".........welcome",
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ]),
            inmaintenance == 1
                ? Container(
                    margin: const EdgeInsets.all(15),
                    child: const Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(
                            "App is under Maintenance !!!",
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ]),
                  )
                : const SizedBox(width: 0),
            Container(
              margin: const EdgeInsets.only(top: 7.0),
              child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => Item_Screen(
                        //               toolbarname: 'Fruits & Vegetables',
                        //             )));
                      },
                      child: const Text(
                        'Categories',
                        style: TextStyle(
                            fontSize: 17.0,
                            color: Colors.black87,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        FocusManager.instance.primaryFocus?.unfocus();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  const CommonProducts(2, "Popular", 0),
                            ));
                      },
                      child: const Text(
                        'Popular',
                        style: TextStyle(
                            fontSize: 17.0,
                            color: Colors.black26,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            FocusManager.instance.primaryFocus?.unfocus();
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    const CommonProducts(1, "Whats New", 0),
                              ),
                            );
                          },
                          child: const Text(
                            'Whats New',
                            style: TextStyle(
                                fontSize: 17.0,
                                color: Colors.black26,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    )
                  ]),
            ),
            !_loading && categoryData.isNotEmpty
                ? ConstrainedBox(
                    constraints: const BoxConstraints(minHeight: 56.0),
                    child: GridView.builder(
                      shrinkWrap: true,
                      itemCount: categoryData.length,
                      primary: false,
                      physics: const NeverScrollableScrollPhysics(),
                      padding: const EdgeInsets.all(10.0),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            FocusManager.instance.primaryFocus?.unfocus();
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AllProducts(
                                    categoryData[index]['id'],
                                    categoryData[index]['name']),
                              ),
                            );
                          },
                          child: Container(
                            margin: const EdgeInsets.all(5.0),
                            child: Card(
                              elevation: 3.0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(
                                    height: 167.0,
                                    child: Stack(
                                      children: <Widget>[
                                        Positioned.fill(
                                            child: categoryData[index]
                                                            ["image"] !=
                                                        "" &&
                                                    categoryData[index]
                                                            ["image"] !=
                                                        null
                                                ? Image.network(
                                                    categoryData[index]
                                                        ["image"],
                                                    fit: BoxFit.contain,
                                                  )
                                                : Center(
                                                    child: Image.asset(
                                                        "images/default.PNG"),
                                                  )),
                                        Container(
                                          color: Colors.black38,
                                        ),
                                        Container(
                                          alignment: Alignment.bottomCenter,
                                          child: GestureDetector(
                                            onTap: () {
                                              // Navigator.push(
                                              //     context,
                                              //     MaterialPageRoute(
                                              //       builder: (context) =>
                                              //           const AllProducts(),
                                              //     ));
                                            },
                                            child: Container(
                                                width: 200,
                                                color: Colors.white70,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(5),
                                                  child: Text(
                                                    categoryData[index]['name'],
                                                    style: const TextStyle(
                                                        fontSize: 16.0,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                )),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  )
                : const Column(
                    children: [
                      SizedBox(height: 250),
                      CircularProgressIndicator(
                        semanticsLabel: 'Circular progress indicator',
                      ),
                    ],
                  )
          ]),
        ),
      ]),
    );
  }
}
