<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\Common;
use App\Models\Variant;
use DB;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;

class ProductImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        $indexke = 1;
        //dd($collection, 111);
        foreach ($collection as $coll) {
            if ($indexke > 1) {
                $visible=0;
                if ($coll[6] == 'Customer') {
                    $visible = 2;
                } else if ($coll[6] == "Store Keeper") {
                    $visible = 3;
                } else if ($coll[6] == "Both") {
                    $visible = 4;
                }
                // dd($coll);
                $data['product_id'] = (int)$coll[0];
                $data['name'] = $coll[1];
                $data['variant_id'] = (int) $coll[2];
                $data['size'] = (int)$coll[3];
                $data['regular_price'] = (int) $coll[4];
                $data['sale_price'] = (int) $coll[5];
                $data['visible'] = $visible;
                $data['brand_name'] = $coll[7];
                $varObject = Variant::where('id', $data['variant_id'])->first();
                $product = Product::where('name', $data['name'])->first();
                $url = Common::slugify($data['name']);

                // if($data['name'] != $product->name){
                if (empty($varObject)) {
                    $model = new Product;
                    $model->name = $data['name'];
                    $model->slug = $url;
                    if ($model->save()) {
                        $modelVariant = new Variant;
                        $modelVariant->product_id = $model->id;
                        $modelVariant->size = $data['size'];
                        $modelVariant->regular_price = $data['regular_price'];
                        $modelVariant->sale_price = $data['sale_price'];
                        $modelVariant->visible = $data['visible'];
                        $modelVariant->save();
                    }
                } else {
                        $varObject->size = $data['size'];
                        $varObject->regular_price = $data['regular_price'];
                        $varObject->sale_price = $data['sale_price'];
                        $varObject->visible = $data['visible'];
                        if ($varObject->save()) {
                            $product->name = $data['name'];
                            $product->save();
                        }
                }
            }
            $indexke++;
        }
    }
    // public function model(array $row)
    // {
    //     dd(json_decode($row),222);
    //     $row[5]=0;
    // 	if ($row[5]== 'Customer') {
    // 		$row[5] = 2;
    // 	} else if ($row[5] == "Store Keeper") {
    // 		$row[5] = 3;
    // 	} else if ($row[5] == "Both") {
    // 		$row[5] = 4;
    // 	}
    //     // Define how to create a model from the Excel row data
    //     return new Variant([
    //         'visible' => $row[5],
    //         'regular_price' => $row[3],
    //         'sale_price' => $row[4],
    //         // Add more columns as needed
    //     ]);
    // }
}
