<?php

namespace App\Exports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class ProductExport implements FromCollection,WithHeadings,WithMapping,WithColumnWidths
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //return Product::all();
		return Product::select(	'products.name', 'variants.id', 'variants.size', 'variants.regular_price', 'variants.sale_price', 'variants.visible')->leftJoin('variants', 'products.id', '=', 'variants.product_id')->get();
    }
	
	public function map($product): array{
		//dd($product);
		$visible="None";
		if($product->visible==2){
			$visible="Customer";
		}
		else if($product->visible==3){
			$visible="Store Keeper";
		}
		else if($product->visible==4){
			$visible="Both";
		}
		return[
		    $product->name,
            $product->id,
		    $product->size,
		    $product->regular_price,
		    $product->sale_price,
		    $visible,
		    ""
		];
	}
	
	public function columnWidths(): array
	{
		return [
			'A' => 40,
			'B' => 30,            
			'C' => 30,            
			'D' => 20,            
			'E' => 20,            
			'F' => 20,            
			'G' => 20,
		];
	}

	public function headings(): array
    {
        return [
                "Product Name",
				"Variant ID",
                "Size",
                "Regular Price",
				"Sale Price",
				"Visible",
				"Brand Name"
        ];
    }
	
}
