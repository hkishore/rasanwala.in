<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\PaymentGateway;
use Carbon\Carbon;
use Hash,DB,Excel,Log;

class ResetPaymentOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resetpaymentorder:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		// $date=date('Y-m-d');
		// $results= DB::Select('');		
		
		//Foo::whereBetween('created_at', [now()->subMinutes(3), now()])->get()
			DB::table('orders')->where('status',0)->where('updated_at','<=',now()->subMinutes(1))->update(array('payment_order_id'=>null));
    }
	
}
