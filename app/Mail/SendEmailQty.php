<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailQty extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**	
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
	 //dd($data);
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return  $this->subject('Update Product')->view('send-product-qty')->with('data', $this->data);
    }
}

?>
