<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Inquiry extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inquiry)
    {
        $this->inquiry=$inquiry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
	  return $this->subject('Inquiry')->markdown('emails.Inquiry')->with(['inquiry'=>$this->inquiry]);
    }
}
