<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order,$msg,$ordernumber)
    {
        $this->order=$order;
		$this->msg=$msg;
		$this->ordernumber=$ordernumber;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->msg)->markdown('emails.orderCreatedMail')->with(['order'=>$this->order,'msg'=>$this->ordernumber]);
    }
}
