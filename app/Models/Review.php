<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Vendor;
use App\Models\Product;

class Review extends Model
{
    use HasFactory;
	protected $fillable = ['review'];
	public function userName()
	{
		return $this->hasone(User::class, 'id','user_id')->where('role',3);
	} 
	public function userN()
	{
		return $this->hasone(User::class, 'id','user_id');
	} 
	public function vendorName()
	{
		return $this->hasone(Vendor::class, 'user_id','vendor_id');
	} 
	public function productName()
	{
		return $this->hasone(Product::class, 'id','product_id');
	} 
	
}
