<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;
use DB;
class Faq extends Model
{
    use HasFactory;
	public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'question' => 'sometimes|required|string|max:1000',
            'answer' => 'required|string|max:2000',
        ]);
    }
	
}
