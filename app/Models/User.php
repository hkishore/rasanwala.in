<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Validator;
use Illuminate\Http\Request;
use DB;
use App\Models\Address;
class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'status',
        'cod',
		'freeshipping',
		'device_token'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];
	public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            //'email' => 'required|string|email|max:255|unique:users,email,' . ($id ? $id : ''),
            'password' => 'sometimes|nullable|string|min:8', 
        ]);
    }
	public function validatorOrderUser(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'phone_no' => 'required|string|max:255',
            'address_1' => 'required|string|max:255',
           // 'email_id' => 'required|string|email|max:255',
           // 'password' => 'sometimes|nullable|string|min:8', 
        ]);
    }

	public function userValidator($username, $id = null)
    {
        return Validator::make($username, [
           // 'name' => 'required|string|max:255',
           // 'email' => 'required|string|email|max:255|unique:users,email,',
            'password' => 'sometimes|nullable|string|min:8', 
        ]);
    }
	public function billing()
    {
        return $this->hasOne(Address::class,'user_id', 'id')->where('is_default',1);
    }
	public function shippingAddress()
	{
		return $this->hasOne(Address::class, 'user_id','id');
	}
	public function userAddress()
	{
		return $this->hasMany(Address::class, 'user_id','id');
	}
   public function userAdd()
    {
        return $this->hasOne(Address::class,'user_id', 'id');
    }
}
