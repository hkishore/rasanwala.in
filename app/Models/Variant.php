<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Order;
use App\Models\OrderLine;
use Auth;

class Variant extends Model
{
    use HasFactory;
	protected $fillable = [
        'visible',
        'regular_price',
        'sale_price',
    ];
	public function orderLine()
    {
		$user = Auth::user();
	    $orderId=0;
	    $order=Order::where('user_id',$user->id)->where('status',0)->first();
	    if($order){
	     $orderId = $order->id;
	    }
        return $this->hasOne(OrderLine::class,'variant_id', 'id')->where('order_id',$orderId);
    }
}

		