<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;

class Question extends Model
{
    protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'question'     => 'required|unique:questions,question,' . ($id ? $id : ''),
            'reply'     => 'required'
        ]);
    }
}
