<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TrackOrder;
use App\Models\Variant;
use App\Models\Order;
class OrderLine extends Model
{
    use HasFactory;
	public function calculateGst($vid)
	{
		$qty=$this->qty;
		$variant=Variant::where('id',$vid)->first();
		//dd($variant,$vid);
		$price=$variant->regular_price;
		if($variant->regular_price>$variant->sale_price)
		{
			$price=$variant->sale_price;
		}
		$amount=number_format($price*$qty,2, '.', '');
		$this->amount=$amount;
	}
	
	public function orderData($oid)
	{		
		$order=Order::find($oid);
		return $order;
	}
	public function order()
	{		
		return $this->hasOne(Order::class,'id', 'order_id');
	}
	public function product()
	{
		return $this->hasOne(Product::class, 'id','product_id');
	}
	public function ManyProduct($id)
	{ 
			$product= OrderLine::where('order_id',$id)->get();
			
				return $product;
			
	}
	public function variant()
	{
		return $this->hasOne(Variant::class, 'id','variant_id');
	}
	public function trackOrder()
	{
		return $this->hasOne(TrackOrder::class, 'coustmer_order_id','order_id');
	}
	public function trackOrderLink($id, $vid)
	{
		$trackOrder= TrackOrder::where('order_id',$id)->where('vendor_user_id',$vid)->first();
		return $trackOrder;
	}
	public function getStatus($id){
		$order_list=OrderLine::find($id);
		$status="";
		if($order_list->status==1){
			$status=" Pending";
		}
		else if($order_list->status==2){
			$status=" Out of Delivere";
		}
		else if($order_list->status==3){
			$status="Delivered";
		}
		else if($order_list->status==4){
			$status="Cancelled";
		}
		return $status;
	}
	
}
