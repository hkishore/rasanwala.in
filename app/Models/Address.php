<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;
use App\Models\States;

class Address extends Model
{
   protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
		'first_name' => 'required',
		'address_1' => 'required',
		//'email_id' => 'required',// commented for user address
		'phone_no' => 'required',
		
		
		]);
			
	}

	public function vendorValidator($data, $id = null)
    {
        return Validator::make($data, [
		'first_name' => 'required',
		'address_1' => 'required',
		'email_id' => 'required',
		'phone_no' => 'required',
		
		
		]);
			
	}
	
	public function region()
    {
        return $this->hasOne(State::class,'id', 'state_id');
    }
	public static function createNewAddress(Request $request,$user_id)
    {
		//dd($request->all());
		$add=new self;
		$add->first_name=$request->first_name;
		$add->address_1=$request->address_1;
		$add->phone_no=$request->phone_no;
		//$add->email_id=$request->email_id;
		if($request->add_id != 'other')
		{
		$add->area_id=$request->add_id;
		}else{
		$add->area_id=0;
		}
		$add->other=$request->add_location;
		$add->user_id=$user_id;
		$add->save();
		return $add;
	}
	public function location()
    {
        return $this->hasOne(ShippingArea::class,'id', 'area_id');
    }
	
	public static function createNewMobileAddress(Request $request)
    {
		$address=new self;
		$address->first_name=$request->name;
		$address->address_1=$request->address;
		$address->phone_no=$request->phone;
		$address->email_id=$request->email_id;
		$address->user_id=$request->user_id;
		if($request->area_id != 0)
		{
		 $address->area_id=$request->area_id;
		}else{
		 $address->area_id=0;
		 $address->other=$request->others;
		}
		if(isset($request->last_selected))
		{
			$address->last_selected=$request->last_selected;
		}
		$address->save();
		return $address;
	}
}
