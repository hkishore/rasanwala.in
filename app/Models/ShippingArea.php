<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;
use App\Models\ShippingCharge;
class ShippingArea extends Model
{
    use HasFactory;
	protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'area'     => 'required|unique:shipping_areas,area,' . ($id ? $id : ''),
            'charge'=> 'required',
        ]);
	}
	
}
