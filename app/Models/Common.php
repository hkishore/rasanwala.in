<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth,Log;
use App\Models\Variant;
use App\Models\Setting;
use App\Models\OrderLine;
class Common extends Model
{
    public static function slugify($text)
	{
			// replace non letter or digits by -
			$text = preg_replace('~[^\pL\d]+~u', '-', $text);

			// transliterate
			$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

			// remove unwanted characters
			$text = preg_replace('~[^-\w]+~', '', $text);

			// trim
			$text = trim($text, '-');

			// remove duplicate -
			$text = preg_replace('~-+~', '-', $text);

			// lowercase
			$text = strtolower($text);

			if (empty($text)) {
			return 'n-a';
			}

			return $text;
    }	

public static function getSubCategoryList($status)
	{
		$categories=[];
		$parents=Category::whereNull('parent_id')->orWhere('parent_id','=',0)->orderBy('name', 'asc')->get();
		foreach($parents as $pa)
		{
			$categories[$pa->name]=Category::where('parent_id','=',$pa->id)->pluck('name', 'id');
			if($status!='')
			{
				$categories[$pa->name]=Category::where('parent_id','=',$pa->id)->where('status',1)->pluck('name', 'id');
			}
		}
		return $categories;
	}
	public static function getSubCategory($id)
	{
		return Category::where('parent_id','=',$id)->orderBy('', 'asc')->get();
	}
	public static function getParentIdList()
	{

		return Category::whereNull('parent_id')->orWhere('parent_id','=',0)->orderBy('name', 'asc')->pluck('name', 'id');
		
	}
	public static function calculategstamonut($variantId,$product)
	{
		
		$variant=Variant::find($variantId);
		$amount=$variant->sale_price;
		$percent=$product->gst;
	    $gstamounts=$amount-($amount*(100 / (100 + $percent )));
        $gstamount=number_format($gstamounts, 2, ',', '.');
	    return $gstamount;
	}
	public static function shortDesc($description,$length=250)	
	{
		$description=strip_tags($description);	
		if(strlen($description)>$length)		{
         		
		$pos=strpos($description, ' ', $length);		
        $content= substr($description,0,$pos ); 
		return $content;
	    }
		return $description;
	}
	
	public static function getMetaInfo($obj)
	{
		// dd(gettype($obj),$obj);
		$meta=[];
		if(gettype($obj)=="string")
		{
			if($obj=='category'){
				$url=$_SERVER['REQUEST_URI'];
				$uri_parts = explode('/', $url);
				$slug = end($uri_parts);
				$obj=Category::where('slug','=',$slug)->first();
			}
			else{
				
				$obj=Page::where('url','=',$obj)->first();
			}
		}
		if(!empty($obj))
		{
		$meta['title']=(isset($obj->meta_title) && $obj->meta_title!=""?$obj->meta_title:$obj->name);
		$meta['description']=(isset($obj->meta_description) && $obj->meta_description!=""?$obj->meta_description:self::shortDesc($obj->description,100));
		$meta['keywords']=(isset($obj->meta_tag) && $obj->meta_tag!=""?$obj->meta_tag:'');
		}
		else{
		$meta['title']="";
		$meta['description']="";
		$meta['keywords']='';
		}		
		return $meta;
	}

  public static function variantGet()
   {
		$variant=self::varintVisibleGet();
		$data=Variant::whereIn('visible',$variant);
		return $data;
   }
	public static function dataVisibleGet()
   {
	   $variant=self::varintVisibleGet();
		$data=Variant::whereIn('visible',$variant)->groupBy('product_id')->pluck('product_id')->toArray();
		//dd($data,$variant);
		return $data;
   }
   public static function varintVisibleGet()
   {
	   $users=Auth::user();
	   $variant=[];
	   $variant[]=4;
		if(Auth::check())
		{  
			if($users->role == 2)
			{
			  $variant[]= 3;
			}else if($users->role == 3)
			{
				$variant[]=2;
			}else{
				$variant[]=2;
				$variant[]=3;
			}
		}else{
			$variant[]=2;
		}
		return $variant;
   }
   
   	public static function teligramBot($order,$user)
	{
		$domian=$_SERVER['HTTP_HOST'];
		if($domian!="" && !str_contains($domian, 'rasanwala.in'))
		{
		    return;
		}
		try
		{
	    $orderInfo = "";
		$num = 1;
		$orderL = OrderLine::where('order_id',$order->id)->get();
		//dd($orderL);
		// for ($i=0;$i >= count($orderL);$i++) {
		  // $orderInfo .= "\n" . $num . ") " . 'PRoduct name' ." (" . $orderL[$i]->size . ")";
		  // $num++;
		// }
		foreach ($orderL as $or){
			$orderInfo .= "\n".$num.' '.$or->product->name." ".$or->variant->size ." (" .$or->qty. ")";
			$num++;
		}
		$orderAdd="";
		if(!empty($order->shippingAddress))
		{
			
		$orderAdd .= "\n ". $order->shippingAddress->first_name;
		$orderAdd .= "\n ". $order->shippingAddress->phone_no;
		$orderAdd .= "\n ". $order->shippingAddress->address_1;
		if(!empty($order->addressArea)){
		$orderAdd .= "\n ". $order->addressArea->area ;
		}
		}
		
		$pstatus = $order->payment_status==1 ? "Paid":"Pending";
		
		$orderInfor = "\nOrder id=" . $order->id .  "\nUser Email=" . $user->email . "\nUser Name: " . $user->name .	"\nAction: Order Recived" . "\n" ."\nOrder Details :\n" . $orderInfo . "\n\n Total Amount:". $order->total_amount ."\n Delivery Type: " . $order->delivery_type . "\n Address: \n" . $orderAdd ."\n  Payment Mode: " .$order->payment_type . "\n Payment Status: " .$pstatus;
		
		$url = 'https://api.telegram.org/bot5370102009:AAFOMIpCAqxRtehxYLQRehjKh6t_h_AakSo/sendmessage?chat_id=-1001720817055&text='.urlencode($orderInfor);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($ch);
	    $erro=curl_error($ch);
		//dd($erro,$result);
	   } catch (\Exception $e) {
				Log::info(['Teligram ERROR'=>$e]);
				//dd($e);
			}	
	}
	public static function teligramErrorBot($msg)
	{
		$domian=$_SERVER['HTTP_HOST'];
		if($domian!="" && !str_contains($domian, 'rasanwala.in'))
		{
		    return;
		}
		try
		{
	   		
		$url = 'https://api.telegram.org/bot5370102009:AAFOMIpCAqxRtehxYLQRehjKh6t_h_AakSo/sendmessage?chat_id=-1001720817055&text='.urlencode($msg);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($ch);
	    $erro=curl_error($ch);
		//dd($erro,$result);
	   } catch (\Exception $e) {
				Log::info(['Teligram ERROR'=>$e]);
				//dd($e);
			}	
	}
	
	public static function getRazorpayApi()
	{
		$payMode=Setting::getFieldVal('razorpay_mode');
		$razorpayKey=config('constant.RazorpayTestKeyId');
		$razorpaySecret=config('constant.RazorpayTestKeySecret');
		if($payMode =='live')
		{
			$razorpayKey=config('constant.RazorpayLiveKeyId');
			$razorpaySecret=config('constant.RazorpayLiveKeySecret');
			//return [$RazorpayLiveKeyId, $RazorpayLiveKeySecret];
		}
		// else if($razorpayApiMode =='test')
		// {
			
			// return [$RazorpayTestKeyId, $RazorpayTestKeySecret];
		// }
		// else if($razorpayApiMode =='disable'){
			// return "";
		// }
		return ['payMode'=> $payMode,
		       'razorpayKey'=> $razorpayKey,
		       'razorpaySecret'=> $razorpaySecret ];
	}
	
	public static function getCashFreeyApi()
	{
		$CashfreeApiMode=config('constant.CashfreeApiMode');
		if($CashfreeApiMode =='live')
		{
			$CashFreeLiveKeyId=config('constant.RazorpayLiveKeyId');
			$CashFreeLiveKeySecret=config('constant.CashFreeLiveKeySecret');
			return [$CashFreeLiveKeyId, $CashFreeLiveKeySecret];
		}
		else if($CashfreeApiMode =='test')
		{
			$CashFreeTestKeyId=config('constant.RazorpayTestKeyId');
			$CashFreeTestKeySecret=config('constant.RazorpayTestKeySecret');
			return [$CashFreeTestKeyId, $CashFreeTestKeySecret];
		}
		else if($CashfreeApiMode =='disable'){
			return "";
		}
		return $CashfreeApiMode;
	}
	
	public static function sendemail($order,$user)
	{
	  if($user->email != "" && $user->email != null){
		try{
		  \Mail::to($user->email)->send(new \App\Mail\OrderCreated($order,'Your item is ordered seccessfully',' ( Order Number #'.$order->id.' )'));
		} catch (\Exception $e) {
		  Log::info(['Unable to send email'=>$e,'email'=>$user->email]);
		}
	  }
	  self::teligramBot($order,$user);
	}
	
	public static function sendFcmAndroidNotification($tokens, $title, $message,$notiImage,$notitype="")
	{
		$notificationtype = 'notification';
		if($notitype != ""){
			$notificationtype = $notitype;
		}
		$accesstoken=config('custom.brodcast_token');
		$url = 'https://fcm.googleapis.com/fcm/send';
		$post = array(
			'registration_ids' =>$tokens,                                                   
			'data' => array('body' => $message, 'title' => $title,'notiImage'=>$notiImage,'type'=>$notificationtype, 'click_action' => 'FLUTTER_NOTIFICATION_CLICK'),
		);
		$headers = array (
				'Authorization: key=' . $accesstoken,
				'Content-Type: application/json'
		);
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false ); 
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode($post ));
		$result = curl_exec ( $ch );
		curl_close ( $ch );
		Log::info(['notification_response'=>$result,'post_data'=>$post]);
	}
}
