<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;

class Blog extends Model
{
    use HasFactory;
	protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'title'     => 'required|unique:blogs,title,' . ($id ? $id : ''),
			'publish_at'=>'required',
            'description'     => 'required',
            'url'=> 'required|unique:blogs,url,' . ($id ? $id : ''),
            'image_name'=> 'sometimes|nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
    }
}
