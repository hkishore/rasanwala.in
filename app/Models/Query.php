<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;

class Query extends Model
{
    use HasFactory;
	protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return  Validator::make($request->all(),[
				'name' => 'required',
				'email' => 'required|email',
				'captcha' => 'required|captcha'
			],['captcha.captcha' => 'Invalid captcha code.']);
    }
}
