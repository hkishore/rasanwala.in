<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;
use App\Models\ShippingArea;
class ShippingCharge extends Model
{
    use HasFactory;
	protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'start_time'=> 'required',
            'end_time'=> 'required',
        ]);
	}
	public function areaName()
	{
		return $this->hasOne(ShippingArea::class, 'id','area_id');
	}
}
