<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;

class Brand extends Model
{
     protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'name'     => 'required|unique:brands,name,' . ($id ? $id : ''),
            'image'=> 'sometimes|nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
	}
}
