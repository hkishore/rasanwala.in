<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;

class Notification extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
			'notification'=>'required',
           
        ]);
    }
}
