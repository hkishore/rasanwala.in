<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Order;
use App\Models\Setting;
use App\Models\Common;
use App\Models\Payment;
use App\Models\TempPayments;
use App\Models\ShippingArea;
use Razorpay\Api\Api;
use Auth,DB;
use Illuminate\Support\Facades\Log;
class PaymentGateway extends Model
{
    use HasFactory;
	public function createRazorpayRequest($orderUser)
	{
		
		$payDetails=Common::getRazorpayApi();
		$key_id=$payDetails['razorpayKey'];
		$key_secret=$payDetails['razorpaySecret'];

		$amt=$orderUser->total_amount*100;
			if($orderUser->coupon_amount != "" || $orderUser->coupon_amount != null)
			{
				$amt=$orderUser->total_amount*100-$orderUser->coupon_amount*100;
			}

			$client=new Api($key_id, $key_secret);
			$order  = $client->order->create([
			'receipt'         => (string)$orderUser->id,
			'amount'          => $amt, 
			'currency'        => "INR"
			]);
		$orderUser->payment_order_id=$order->id;
		$orderUser->save();
		$tempPayment = array('rozarpay_order_id' =>$order->id , 'order_id' => $orderUser->id);
		DB::table('temp_payments')->insert($tempPayment);
		$order_id=$order->id;
		$options=[];
		$options['key']=$key_id;
		$options['order_id']=$order_id;
		$options['amount']=$amt;
		Log::info('User details.',$options);
		return json_encode($options);
	}
	
	public function verifyPayment($requestdat)
	{	
		$payDetails=Common::getRazorpayApi();
		$key_id=$payDetails['razorpayKey'];
		$key_secret=$payDetails['razorpaySecret'];	
		//dd($requestdat->razorpay_payment_id, $requestdat->razorpay_order_id, $requestdat->razorpay_signature);
		// $paymentTable=new Payment;
		// $paymentTable->razorpay_payment_id=$requestdat->razorpay_payment_id;
		// $paymentTable->razorpay_order_id=$requestdat->razorpay_order_id;
		// $paymentTable->razorpay_signature=$requestdat->razorpay_signature;
		// if($paymentTable->save())
		// {
			// return response()->json(['success' => 'order save'], 200);   
		// }
		// $razorpay_s_key=Setting::getPaymentStatus('razorpay','key_id');
		// $key_id=$razorpay_s_key['key_id'];
		// $razorpay_p_key=Setting::getPaymentStatus('razorpay','key_secret');
		// $key_secret=$razorpay_p_key['key_secret'];
			
		$txnid=$requestdat->razorpay_payment_id;
		$razorpay_order_id=$requestdat->razorpay_order_id;			
		$razorpay_signature=$requestdat->razorpay_signature;		
		$api = new Api($key_id, $key_secret);
		$attributes  = array('razorpay_signature'  => $razorpay_signature,  'razorpay_payment_id'  => $txnid , 'razorpay_order_id' => $razorpay_order_id);
		$order  = $api->utility->verifyPaymentSignature($attributes);
		return $order;
	}
}
