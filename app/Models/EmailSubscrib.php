<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;
use DB;

class EmailSubscrib extends Model
{
    use HasFactory;
	protected $fillable = ['email'];
	public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
	public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
//'email' => 'required|string|email|max:255|unique:users,email,' . ($id ? $id : ''),
        ]);
    }
}
