<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;

class Page extends Model
{
    protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'title'     => 'required|unique:pages,title,' . ($id ? $id : ''),
            'url'     => 'required|unique:pages,url,' . ($id ? $id : ''),
            'description'     => 'required',
        ]);
    }
}
