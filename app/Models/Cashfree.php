<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Models\Order;
use App\Models\Setting;
use Auth;

class Cashfree extends Model
{
    use HasFactory;
	public $clint_key='';
	public $secret_key='';
	public $mode;
	//public $sendboxUrl;
	public $apiUrl;
	public $cashfreeObj;
	public $headerR=[];
	public function __construct(array $attributes = array())
	{
		    parent::__construct($attributes);
			$cashfree_s_key=Setting::getPaymentStatus('cashfree','key_id');
			$key_id=$cashfree_s_key['key_id'];
			$cashfree_p_key=Setting::getPaymentStatus('cashfree','key_secret');
			$key_secret=$cashfree_p_key['key_secret'];
			$mode=$cashfree_s_key['status'];
			if($mode=="test")
			{
				$this->apiUrl="https://sandbox.cashfree.com/pg";
			}else{
				$this->apiUrl="https://api.cashfree.com/pg";
			}
			$this->clint_key =$key_id;
			$this->secret_key =$key_secret;
			$this->headerR=[
			'Accept' => 'application/json',
			'Content-Type' => 'application/json',
			'x-api-version' => '2022-01-01',
			'x-client-id' => $this->clint_key,
			'x-client-secret' => $this->secret_key,
			];
	}
	    //$this->directory = $this->setDirectory();
	public function cteatePaymentLink($user,$order,$shippingCharge)
	{
			//$user=Auth::user();
			//$order=Order::where('status','=',1)->where('user_id',$user->id)->first();
			
			$address=$order->shippingAddress;
			// $shippingCharge=0;
			// if($order->shipping_charge!="" && $order->shipping_charge!=null)
			// {
				// $shippingCharge=$order->shipping_charge;
			// }
			$totalAmount=$order->total_amount+$shippingCharge;
			if(empty($order) || count($order->getOrderLines)==0)
			{
			   return '';
			}
			$phone = str_replace(" ", "", $address->phone_no);
			$numlength = strlen((string)$phone);
			$diffen=$numlength-10;
			$conPhone="";
			if($numlength>9)
			{
			$conPhone = substr($phone, $diffen);
			}
			if($address->phone_no==null)
			{
				return 1;
			}
			$retrun = route('return.cashefree');
			$notify = route('notify.cashefree');
			$time=time();
			$client = new \GuzzleHttp\Client();
			$response = $client->request('POST', $this->apiUrl.'/orders', [
		   'verify'=>false,//need to comment this
			'body' => json_encode([
			"customer_details"=>[
				"customer_id"=>"$user->id",
				"customer_email"=>"$user->email",
				"customer_phone"=>"$conPhone"
				],
			"order_meta" =>[
			 "return_url"=>"$retrun?order_id={order_id}&order_token={order_token}",
			 "notify_url"=>"$notify"
			    ],
			"order_id"=>$order->id."_".$time,
			"order_amount"=>"$totalAmount",
			"order_currency"=>"INR",
			]),
			'headers' =>$this->headerR,
			]);
			$data = json_decode($response->getBody()->getContents());
			if(isset($data->payment_link))
			{
				return $data->payment_link;
			}
			return '';
	}
	public function verifyPayment($order_id)
	{
		$client = new \GuzzleHttp\Client();
			$response = $client->request('GET', $this->apiUrl.'/orders/'.$order_id, [
			'verify'=>false,
			'headers' =>$this->headerR ,
			]);
			$data = json_decode($response->getBody()->getContents());
			if(isset($data->order_status))
			{
				return $data;
			}
	     return '';
		
	}	
}
