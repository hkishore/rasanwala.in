<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Product;
use App\Models\State;
use Illuminate\Http\Request;
use Auth;
use App\Models\Address;
use App\Models\ShippingCharge;
use App\Models\ShippingArea;
use Validator;

class Order extends Model
{
    use HasFactory;
	protected $fillable = ['status','delivery_date'];
	public function validatorAddOrder(Request $request)
	{
		return Validator::make($request->all(),[
			'user_id'=> 'required'
		]);
	}
	 public function userOrderUpdate(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
			'user_id'     => 'required',
		]);
	}
	public function validator(Request $request)
    {
		$isAddressSection=true;
		if(Auth::check())
		{
			$user=Auth::user();
			if(count($user->userAddress)>0)
			{
				$isAddressSection=false;
			}
			
		}
		if($isAddressSection)
		{
			 $models = new Address;
			 $validator = $models->validator($request);
			 return $validator;
		}
		else{
			return Validator::make($request->all(), [
            'address_id'     => 'required'            
          ]);
		}
       
		
	}
    public function order()
	{
		return $this->hasOne(OrderLine::class, 'order_id','id');
	} 	
	public function customer()
	{
		return $this->hasOne(User::class, 'id','customer_id');
	} 
	public function shippingAddress()
	{
		return $this->hasOne(Address::class, 'id','address_id');
	} 
	public function state()
	{
		return $this->hasOne(State::class, 'id','state_id');
	} 
	public function user()
	{
		return $this->hasOne(User::class, 'id','user_id');
	} 
	 
	public function orderData($oid)
	{
		$order=Order::find($oid);
		return $order;
	}
	
	public function product()
	{
		return $this->hasOne(Product::class, 'id','product_id');
	}
	public function getOrderLines()
	{ 
		return $this->hasMany(OrderLine::class, 'order_id','id');
	}
	public function reCalculateFee(){
		$orderLines=$this->getOrderLines;
		if(count($orderLines)>0)
		{
			$orderAmt=$orderLines->sum('amount');
			$this->gst_amount=$orderLines->sum('gst_amount');
			$this->total_amount=$orderAmt;
			$this->order_amount=$orderAmt;
			$this->save();
		}
	}
	public function area()
	{
		return $this->hasMany(ShippingArea::class, 'id','area_id');
	}
	public function addressArea()
	{
		return $this->hasOne(ShippingArea::class, 'id','area_id');
	}
	public function shippingCharge()
	{
		return $this->hasMany(ShippingCharge::class, 'id','shipping_charge_id');
	}
	public static function getUserOrders($createNewOrder)
	{
		$currentDecivceId = request()->cookie('device_id');
		$user_id=0;
		if(Auth::check())
		{
			$user_id=Auth::user()->id;	
		}
			
		if($currentDecivceId=="" || $currentDecivceId==null || $currentDecivceId=="null")
		{
			$currentDecivceId=md5(time());					
		}
		$order=new self;
		$order=self::Where('status','=',0)->where('device_id',$currentDecivceId)->first();
		if(empty($order) && $user_id>0 )
		{
			$order=self::Where('status','=',0)->where('user_id',$user_id)->first();
		}
		if(!empty($order) && $user_id>0)
		{			
			$order->user_id=$user_id;
			$order->save();			
		}
		if(empty($order) && $createNewOrder)
		{
			$order=new self;
			$order->user_id=$user_id;
			$order->status=0;
			$order->device_id=$currentDecivceId;
			$order->save();			
		}
		return $order;
	}
	public function ManyProduct($id)
	{ 
			$product= OrderLine::where('order_id',$id)->get();
			
				return $product;
			
	}
	public function getStatus($id){
		$order_list=Order::find($id);
		$status="";
		if($order_list->status==1){
			$status=" Pending";
		}
		else if($order_list->status==2){
			$status=" Out of Delivere";
		}
		else if($order_list->status==3){
			$status="Delivered";
		}
		else if($order_list->status==4){
			$status="Cancelled";
		}
		return $status;
	}
	public function Address()
	{
		return $this->hasOne(Address::class, 'user_id','user_id');
	} 
}
