<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Models\Variant;
use App\Models\OrderLine;
use App\Models\FavoriteItem;
use Auth;


class Product extends Model
{
    use HasFactory;
	
	protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'name'     => 'required:product,name,' . ($id ? $id : ''),
		]);
	}
    public function getPercentage($variant)
	{
		$per=0;
		if(!empty($variant) && ($variant->regular_price>$variant->sale_price))
		{
			$per =($variant->regular_price-$variant->sale_price)/$variant->regular_price*100;
		}
        return number_format((int)$per);
    }
	public function brandName()
    {
        return $this->hasOne(Brand::class,'id', 'brand_id');
    }
	public function variantPrice()
    {
        return $this->hasOne(Variant::class,'product_id', 'id')->orderBy('sale_price');
    }
	
	public function CatName()
    {
        return $this->hasOne(Category::class,'id', 'categeory_id');
    }
	public function allCatName()
    {
        $ids=$this->categories->pluck('category_id')->toArray();
		$names=Category::whereIn('id',$ids)->pluck('name')->toArray();
		return implode(', ',$names);
		
    }
	public function variant()
    {
        return $this->hasMany(Variant::class,'product_id', 'id')->where('visible', '=' , 2)->orderByRaw('(regular_price - sale_price)*100/regular_price desc');
    }
	public function orderLine()
    {
        return $this->hasOne(OrderLine::class,'product_id', 'id');
    }
	public function variantName()
    {
        return $this->hasMany(Variant::class,'product_id', 'id');
    }
	public function Review()
    {
		return	$this->hasMany(Review::class, 'product_id', 'id')->where('status',1); 
	}
	public function procutReviewCount()
    {
		  return $this->Review()->count();
      
	}
	public function sizeAndPrice()
	{
		$users=Auth::user();
		if(Auth::check())
		{  
			if($users->role == 3)
			{
			   $variand=Variant::where('visible','=',2);
			}else if($users->role == 2)
			{
				$variand=Variant::where('visible','=',3);
			}else{
				$variand=Variant::where('visible','!=',1);
			}
		}else{
			
			$variand=Variant::where('visible','=',2);
		}
		return $variand;
	}

	public static function getProducts()
    {
		return self::join('categories','categories.id','=','products.categeory_id')->join('variants','variants.product_id','=','products.id')->where(
		['categories.status'=>1,'products.status'=>1])->whereIn('variants.visible',[2,4])->groupBy('products.id')->pluck('products.id');
	}
	public static function getBrand($id){
			return Product::where('categeory_id',$id)->where('status',1)->groupBy('brand_id')->pluck('brand_id')->toArray();
	}
	
	public function isFavorite(){
		$user = Auth::user();
		return $this->hasOne(FavoriteItem::class,'product_id', 'id')->where('user_id',$user->id);
	}
	
}
