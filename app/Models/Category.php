<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use Validator;
use Illuminate\Http\Request;


class Category extends Model
{
     protected $guarded = [];
    public function validator(Request $request, $id = null)
    {
        return Validator::make($request->all(), [
            'name'=> 'required|unique:categories,name,' . ($id ? $id : ''),
            //'slug'=> 'required|unique:categories,slug,' . ($id ? $id : '')
        ]);
		
	}	
public function parent()
    {
        return $this->belongsTo(Self::class, 'parent_id', 'id');
    }
    public function parentName()
    {
      if(!empty($this->parent))
      {
      return $this->parent->name;
      }
      return '';
    }
	
	
	// public function getHomeContent()
	// {
		// $body=strip_tags($this->description);
		// $line=$body;
		// if (preg_match('/^.{1,200}\b/s', $body, $match))
		// {
		// $line=$match[0];
		// }
		// return $line;
	// }
	public static function getProduct($id)
    {
        return Product::where('categeory_id',$id)->where('status',1)->get();
    }
}

