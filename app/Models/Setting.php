<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\BotList;
class Setting extends Model
{
	use HasFactory;
    protected $fillable = ['data_name','data_value'];
	
	public static function getFieldVal($dataName)
	{
		$obje=self::where('data_name','=',$dataName)->first();
		if(!empty($obje))
		{
			return $obje->data_value;
		}
		return '';
	}
	
	public static function getFieldValSession($dataName)
	{
		/*$sVale=session($dataName);
		if($sVale=="")
		{*/
			$sVale=self::getFieldVal($dataName);
			/*session($dataName,$sVale);
		}*/
		
		return $sVale;
	}
	public function botList()
    {
        return $this->hasMany(BotList::class, 'id', 'bot_listing_id');
    }
	public static function getPaymentStatus($type,$key)
	{
		$rest=[];		
		$rest['status']='test';
			$status=self::where('data_name',$type.'_mode')->first();
			if(!empty($status))
			{
			$rest['status'] =$status->data_value;
			}
			if($rest['status']!='disable')
			{
				//dd($type.'_'.$key.'_'.$rest['status'],$type,$key,$rest['status']);
				$emailVal=self::where('data_name',$type.'_'.$key.'_'.$rest['status'])->first();
				if(!empty($status))
					{
					$rest[$key] =$emailVal->data_value;
					}
					else
					{
						$rest['status']='disable';
					}
			}
			return $rest;
	}
}
