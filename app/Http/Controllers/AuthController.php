<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Address;
use App\Models\ShippingArea;
use App\Models\Setting;

class AuthController extends Controller
{
	public function register(Request $request) {
		
		
			$user = User::where('phone',$request->phone)->first();
			if(!empty($user))
			{
				return response(['error' => 'Phone number allrady exist']);
			}
			$email='';
			if (isset($request->email) !="" || isset($request->email)!= null) {
				$email= $request->email;
				$user = User::where('email',$request->email)->first();
				if(!empty($uesr))
				{
					return response(['error' => 'Email allrady exist']);
				}
			}
			
			if(empty($user)) {
				$user = new User;
				$user->name = $request->name;
				$user->phone = $request->phone;
				$user->email = $request->email;
				$user->status = 1;
				$user->role = 3;
				$user->password=Hash::make($request->password);
				$user->save();
				$credentials = $request->only('phone','password');
			   if($this->guard()->attempt($credentials))
				 {		
			      $auth=Auth::User();
		         }
				$auth->tokens()->delete();
				$token = $auth->createToken('api_token');
				return response(['success' => 'Phone number allrady exist']);
				
		}
		
   }
    private function guard()
    {
        return Auth::guard();
    }
   	public function login(Request $request) {
		
		 $requestData = $request->all();
		 $credentials = $request->only('password');
		 if(isset($request->phone) && $request->phone!="")
		 {
		 $user = User::where('phone',$request->phone)->first();
			if(empty($user))
			{
			return response(['error' => 'Phone number is wrong.']);
			}
			$credentials['phone']=$request->phone;
		 }
		 if(isset($request->email) && $request->email!="")
		 {
		   $user = User::where('email',$request->email)->first();
			if(empty($user))
			{
			return response(['error' => 'This Email address is not exits.']);
			}
			$credentials['email']=$request->email;
		 }
		
        if ($this->guard()->attempt($credentials)) {
			$user=Auth::User();
			$user->tokens()->delete();
			$accessToken = $user->createToken('api_token');
			return response(['success'=>'Login successfully.']);
		}else{
			     Auth::logout();
				 return response(['error' => 'Password is wrong.']);
		}	
    }
	
	public function updateUser(Request $request) {
		$user = Auth::user();
		$user->name = $request->name;
		if($request->phone!="")
		{			
			$userPhone=User::where('phone',$request->phone_no)->where('id','!=',$user->id)->first();
			if(!empty($userPhone)) {
				return response()->json([
					'message' => 'Phone number is already exist',
					'data' => []
				],400);
			}		
		}
		if($request->email!="")
		{			
			$userEmail=User::where('email',$request->email)->where('id','!=',$user->id)->first();
			if(!empty($userEmail)) {
				return response()->json([
					'message' => 'Email is already exist',
					'data' => []
				],400);
			}		
		}
		$user->phone = $request->phone_no;
		$user->email = $request->email;
		if(!empty($request->password) && $request->password !=""){
		  $user->password = Hash::make($request->password);
		}else{
		  unset($user['password']);
		}
		if($user->save()) {
			return response()->json([
				'message' => 'saved successfully',
				'data' => [
					'name'=> $user->name,
					'phone_no'=> $user->phone,
					'email'=> $user->email,
				]
			],200);
		}
		return response()->json([
			'message' => 'Something went wrong',
			'data' => []
		],500);
	}
	
	
}












