<?php

namespace App\Http\Controllers;

use App\Models\FrontImage;
use Illuminate\Http\Request;
use File;

class FrontImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$models=FrontImage::orderBy('id','desc')->get();
      return view('admin.frontimage.index')->with(['model'=>$models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.frontimage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		//dd($request->all());
		  $models = new FrontImage;
		  $imageName="";
			if($request->image!='')
			{
     		  $imageName=$request->image;
              File::move(public_path('temp/'.$imageName), public_path('uploads/sliders/'.$imageName));
			 $models->image = asset('uploads/sliders/'.$imageName);
			}
		  $models->slider_type = $request->slider_type;
		  $models->status = $request->status;
		  $models->position = $request->position;
		  $models->url = $request->url;
		  $models->small_title = $request->small_title;
		  $models->large_title = $request->large_title;
		  $models->image_type = $request->image_type;
		  $models->description = $request->description;
		//  $models->slider_type = $request->remove_image;
		
		  
        if ($models->save()) {
            $request->session()->flash('success', 'Slider added successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('frontimage.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FrontImage  $frontImage
     * @return \Illuminate\Http\Response
     */
    public function show(FrontImage $frontImage)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FrontImage  $frontImage
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $model=FrontImage::find($id);
         return view('admin.frontimage.edit')->with(['model'=>$model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FrontImage  $frontImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     // dd($request->all());
		$frontImage=FrontImage::find($id);
          $data=$request->except('_token');
		   if($request->image!='')
			{
				$filePath=public_path('uploads/sliders/'.$frontImage->image);
				if (File::exists($filePath)) {
				File::delete($filePath);
				}	
     		  $imageName=$request->image;
              File::move(public_path('temp/'.$imageName), public_path('uploads/sliders/'.$imageName));
			  $frontImage->image=asset('uploads/sliders/'.$imageName);
			}
			elseif($request->remove_image && $request->remove_image=="y")
			{
				$filePath=public_path('uploads/sliders/'.$frontImage->image);
				if (File::exists($filePath)) {
				File::delete($filePath);
				}
			   $frontImage->image="";
			}
		  $frontImage->slider_type = $request->slider_type;
		  $frontImage->status = $request->status;
		  $frontImage->position = $request->position;
		  $frontImage->url = $request->url;
		  $frontImage->small_title = $request->small_title;
		  $frontImage->large_title = $request->large_title;
		  $frontImage->image_type = $request->image_type;
		  $frontImage->description = $request->description;
        if ($frontImage->save()) {
            $request->session()->flash('success', 'Slider updated successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('frontimage.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FrontImage  $frontImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
		$frontImage=FrontImage::find($id);
       try 
        {
           if ($frontImage->delete()) {
                $request->session()->flash('success', 'Slider deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('frontimage.index');
    }
}
