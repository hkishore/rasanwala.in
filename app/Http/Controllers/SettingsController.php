<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Common;
use App\Models\BotList;
use App\Models\AdminPayout;
use App\Models\Payout;
use Auth;
use DB;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$id=Auth::user()->id;
		$settings = Setting::all()->toArray();
        return view('admin.settings')->with(['settings'=>$settings]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$user = Auth::user();
      $allDat=$request->all();
	  	unset($allDat['_token']);
		if(count($allDat)>0)
		{
			foreach($allDat as $key=>$dta)
			{
				$setting = Setting::updateOrCreate(['data_name' => $key],['data_value' =>$dta ]);
			}
			$msg=" Setting Updated user_id= ".$user->id;
					Common::teligramErrorBot($msg);
			$request->session()->flash('success', 'Settings updated succesfully');
		 return redirect()->back();   
		}
		}
	public function payoutuUpdate(Request $request)
    { 
	//dd($request->all());
		//$id=Auth::user()->id;
		$payout=$request->payout;
		$status=$request->status?$request->status:[];
		$first_name=$request->first_name;
		$last_name=$request->last_name;
		$email=$request->email;
		$phone=$request->phone;
		
		if(count($payout)>0){
			foreach($payout as $key=>$val){
				$data_val=['first_name' =>$first_name[$key],'last_name' => $last_name[$key],'email'=>$email[$key],'phone'=>$phone[$key], 'status'=>0];
				if(in_array($val,$status)){ 
					$data_val['status'] = 1;
				}
				AdminPayout::updateOrCreate(['payout' => $val], $data_val);
			}
			$request->session()->flash('success', 'Settings updated succesfully');
		}
		return redirect()->back(); 
	}
		
}
