<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Brand;
use App\Models\Common;
use App\Models\Category;
use App\Models\Faq;
use App\Mail\SendMail;
use Mail;
use App\Models\Query;
use App\Models\Setting;
use App\Models\FavoriteItem;
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderLine;
use Auth;

class BrandController extends Controller
{
	public function brands(){
		$brands = Brand::select('id', 'name', 'image')->get();
		if(!empty($brands))
		{
			return response()->json([
			'message' => 'Fetched successfully',
			'data' => $brands,
		   ],200);
		}else{
			return response()->json([
			'message' => 'Not Found',
			'data' => [],
		   ],404);
		}
	}
	
	public function search(Request $request)
	{
	$catObj= Category::select('id', 'name', 'image')->whereNotNull('parent_id')->where('name', 'LIKE', '%'.$request->searchTxt.'%')->where('status',1)->get();
	$app_maintenance=Setting::getFieldVal('app_maintenance');
	if(!empty($catObj))
		  {
		  return response()->json([
			'message' => 'Fetched successfully',
			'data' => [
			    'categories' => $catObj,
				'app_maintenance' => $app_maintenance,
			],
		   ],200);  
		  }else
		  {
		  return response()->json([
			'message' => 'Not Found',
			'data' => [
			    'categories' => [],
				'app_maintenance' => $app_maintenance,
			],
		   ],404); 
		  }
	}
	
	public function faqs(Request $request){
		$faqs= Faq::where('status', 1)->get();
		if(!empty($faqs))
			  {
			  return response()->json([
				'message' => 'Fetched successfully',
				'data' => $faqs,
			   ],200);  
			  }else
			  {
			  return response()->json([
				'message' => 'Not Found',
				'data' => [],
			   ],404); 
			  }
		}
		
	public function submitQuery(Request $request){
		
		$model= new Query;
		$model->name= $request->name;
		$model->email= $request->email;
		$model->subject= $request->subject;
		$model->message= $request->message;
		$model->status = 0;
		if($model->save()){
        $msg="customer_name=>".$request->name."\n customer_email=".$request->email ."\n subject=".$request->subject ."\n message=".$request->message;
         Common::teligramErrorBot($msg);
		  return response()->json([
				'message' => 'Submitted successfully',
			   ],200); 
		}
        return response()->json([
				'message' => 'Bad Request',
			   ],500); 
		
	}	
	
	public function addFavorite(Request $request)
	{
		$favorite=FavoriteItem::where('user_id',$request->user_id)->where('product_id',$request->proId)->first();
		if(empty($favorite)) 
		{
			$model = new FavoriteItem;
			$model->user_id = $request->user_id;
			$model->product_id = $request->proId;
			$model->save();			
		}else{
			$favorite->delete();
		}
		if(isset($request->type) && $request->type != ""){
		 return	$this->favSpecialproducts($request);
		}
		return $this->favProducts($request);		
	}
	
	public function favProducts($request)
	{
		 $orderId=0;
		 if(isset($request->user_id) && $request->user_id != "" && $request->user_id != null){
          $order=Order::where('user_id',$request->user_id)->where('status',0)->first();
		  if($order){
		  $orderId = $order->id;
		  }
		 }
		 $products = Product::select('id','name','image')->with(['variant','isFavorite'])->where('categeory_id',$request->catId)->where('status',1)->get();
		 $orderLine = OrderLine::select('variant_id','id','qty')->where('order_id',$orderId)->where('status',0)->get()->keyBy('variant_id');
		 $favorite=FavoriteItem::where('user_id',$request->user_id)->count();
		 if(!empty($products))
		 {
			return response()->json([
				'message' => 'Data Fetched',
				'data' =>[
				    'products'=>$products,
				    'orderLine'=>$orderLine,
				    'favCnt'=>$favorite,
				],						
			],200);
		 }else{
			return response()->json([
			'message' => 'Not Found',
			'data' => [],
		   ],404);
		 }		
	}
	
    public function favSpecialproducts($request)
	{
	    $orderId=0;
	    $order=Order::where('user_id',$request->user_id)->where('status',0)->first();
	    if($order){
	     $orderId = $order->id;
	    }
		$productsObj=Product::select('id', 'name', 'image');
		if(isset($request->type) && $request->type == "1") {    //type 1 for whats'new
			$productsObj->orderBy('id', 'DESC')->limit(30);
		}	
		if(isset($request->type) && $request->type == "2") {     // type 2 for popular
			$productsObj->where('best_sell',1);
		}	
		if(isset($request->type) && $request->type == "3") {     // type 3 for product 
			$productsObj->limit(50);
		}		
        if(isset($request->type) && $request->type == "4" && $request->brand_id != "0"){      // type 4 for product by brand 
			$productsObj->where('brand_id',$request->brand_id);
		}	
        if(isset($request->type) && $request->type == "5") {     // type 5 for favoffer 
			return $this->favOffers($request);
		}
        if(isset($request->type) && $request->type == "6") {     // type 6 for only favorite 
			return $this->onlyFavorite();
		}		
		$products=$productsObj->with(['variant','isFavorite'])->where('status',1)->get();
		$orderLine = OrderLine::select('variant_id','id','qty')->where('order_id',$orderId)->where('status',0)->get()->keyBy('variant_id');
		$favorite=FavoriteItem::where('user_id',$request->user_id)->count();
		if(!empty($products))
		{
			return response()->json([
				'message' => 'Data Fetched',
				'data' =>[
				    'products'=>$products,
				    'orderLine'=>$orderLine,
				    'favCnt'=>$favorite,
				],	
			],200);
		}else{
			return response()->json([
			'message' => 'Not Found',
			'data' => [],
		   ],404);
		}
	}
	
	public function favOffers($request){
	    $orderId=0;
	    $order=Order::where('user_id',$request->user_id)->where('status',0)->first();
	    if($order){
	     $orderId = $order->id;
	    }
		 $productIds = Product::getProducts();
		 $products = Product::with(['variant','isFavorite'])->whereHas('variant', function($query) use($productIds){
          $query->whereIn('product_id',$productIds);
         })->whereIn('id',$productIds)->get();
		 $orderLine = OrderLine::select('variant_id','id','qty')->where('order_id',$orderId)->where('status',0)->get()->keyBy('variant_id');
		 $favorite=FavoriteItem::where('user_id',$request->user_id)->count();
		 return response()->json([
				'message' => 'Data Fecthed',
				'data' =>[
				    'products'=>$products,
				    'orderLine'=>$orderLine,
				    'favCnt'=>$favorite,
				],	
			],200);
	}
	
	public function onlyFavorite()
	{
		 $user = Auth::user();
		  $orderId=0;
          $order=Order::where('user_id',$user->id)->where('status',0)->first();
		  if($order){
		  $orderId = $order->id;
		  }
		 $favoriteIds=FavoriteItem::where('user_id',$user->id)->pluck('product_id'); 
		 $products = Product::select('id','name','image')->with(['variant','isFavorite'])->whereIn('id',$favoriteIds)->where('status',1)->get();
		 $orderLine = OrderLine::select('variant_id','id','qty')->where('order_id',$orderId)->where('status',0)->get()->keyBy('variant_id');
		 $favorite=FavoriteItem::where('user_id',$user->id)->count();
		 if(!empty($products))
		 {
			return response()->json([
				'message' => 'Data Fetched',
				'data' =>[
				    'products'=>$products,
				    'orderLine'=>$orderLine,
				    'favCnt'=>$favorite,
				],						
			],200);
		 }else{
			return response()->json([
			'message' => 'Not Found',
			'data' => [],
		   ],404);
		 }		
	}
	
}



 












