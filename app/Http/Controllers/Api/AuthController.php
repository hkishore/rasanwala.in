<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Address;
use App\Models\ShippingArea;
use App\Models\Setting;
use App\Models\OrderLine;
use App\Models\Order;
use App\Models\FavoriteItem;
use App\Models\Common;
use Auth;

class AuthController extends Controller
{
	public function register(Request $request) {
		$code = 400;
		$infos = [
			'message' => 'Bad Request',
			'data' => []
		];
		if (isset($request->name) && $request->name != "" && isset($request->phone_no) && $request->phone_no != "" && isset($request->password) && $request->password != "") {
			$user = User::where('phone',$request->phone_no)->first();
			$code = 422;
			$infos['message'] = 'Phone number already exist';
			if(empty($user)) {
				if(isset($request->email) && $request->email != ""){
				  $userE = User::where('email',$request->email)->first();
				  $code = 422;
				  $infos['message'] = 'Email already exist';
				  if(!empty($userE)){
				   return response()->json($infos, $code); 
				  }
				}
				$user = new User;
				$user->name = $request->name;
				$user->phone = $request->phone_no;
				$user->email = $request->email;
				$user->device_unique_id = $request->device_id;
				$user->device_type = $request->device_type;
				$user->status = 1;
				$user->role = 3;
				$user->password=Hash::make($request->password);
				$user->save();
				$user->tokens()->delete();
				$token = $user->createToken('api_token');
				$infos['api_token'] = $token->plainTextToken;
				$code = 201;
		}
	 }
	 return response()->json($infos, $code);
   }
   
   	public function login(Request $request) {
		$code = 400;
		$infos = [
			'message' => 'Bad Request',
			'data' => []
		];
		if (isset($request->email) && $request->email != "" && $request->password != "") {
			$keyN = "phone_no";
			if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
			  $keyN = "email";
			}
			$user = User::where($keyN, '=', $request->email)->first();
			$infos['message'] = 'User does not exist';
			$code = 404;
			if(!empty($user)) {
				$infos['message'] = 'Password does not match';
				$code = 401;	
				if (Hash::check($request->get('password'), $user->password)) {
					$user->tokens()->delete();
					$token = $user->createToken('api_token');				
					$user->device_unique_id = $request->device_id;
				    $user->device_type = $request->device_type;
					$user->save();
					$infos['api_token'] = $token->plainTextToken;
					$code = 200;
				}
		    }
		}
		return response()->json($infos, $code);
	}

    public function getUserInfo(){
		$user = Auth::user();
		return response()->json($user, 200);
	}

	
	public function logout(Request $request) {
		$user = User::where('id', $request->user_id)->first();
		if(!empty($user)) {
		     $user->device_token = null;
             $user->save();
		 }
		return response()->json([
			'message' => 'Logout Successfully'
		],200);
	}
	
	public function checkLogin(Request $request) {
        return response()->json([
            'message' => 'Ok'
        ],200);
    }
	
	public function SetDeviceToken(Request $request) {
		$user = Auth::user();
		$user->device_token = $request->token;
		$user->save();
		return response()->json([
			'message' => 'saved successfully',
			'data' => []
		],200);
	}
	
	public function updateUser(Request $request) {
		$user = Auth::user();
		$user->name = $request->name;
		if($request->phone!="")
		{			
			$userPhone=User::where('phone',$request->phone_no)->where('id','!=',$user->id)->first();
			if(!empty($userPhone)) {
				return response()->json([
					'message' => 'Phone number is already exist',
					'data' => []
				],400);
			}		
		}
		if($request->email!="")
		{			
			$userEmail=User::where('email',$request->email)->where('id','!=',$user->id)->first();
			if(!empty($userEmail)) {
				return response()->json([
					'message' => 'Email is already exist',
					'data' => []
				],400);
			}		
		}
		$user->phone = $request->phone_no;
		$user->email = $request->email;
		if(!empty($request->password) && $request->password !=""){
		  $user->password = Hash::make($request->password);
		}else{
		  unset($user['password']);
		}
		if($user->save()) {
			return response()->json([
				'message' => 'saved successfully',
				'data' => [
					'name'=> $user->name,
					'phone_no'=> $user->phone,
					'email'=> $user->email,
				]
			],200);
		}
		return response()->json([
			'message' => 'Something went wrong',
			'data' => []
		],500);
	}
	
	public function getUserAddress(Request $request){
		return $this->getCommonUserAddress($request->user_id);
	}
	
	public function deleteUserAddress(Request $request){
		$address = Address::where('id',$request->address_id)->first();
		if($address->delete())
		{
			return $this->getCommonUserAddress($request->user_id);
		}
	}
	
	public function addAddress(Request $request)
	{
		$existing = Address::where('user_id',$request->user_id)->where('last_selected',1)->first();
		$models = new Address;
		$models->first_name = $request->name;
		$models->phone_no = $request->phone;
		//$models->email_id = $request->email;
		$models->address_1 = $request->address;
		$models->area_id = 0;
		if(isset($request->areaId) && $request->areaId != 0){
			$models->area_id = $request->areaId;
		}
		$models->other = $request->others;
		$models->user_id = $request->user_id;
		if(!$existing){
			$models->last_selected = 1;
		}
		if($models->save()){
			return response()->json([
				'message' => 'area saved successfully',
				'data' => $models->id,
			],200);
		}
		return response()->json([
			'message' => 'Something went wrong',
			'data' => []
		],500);
		
	}
	
	public function getCommonUserAddress($userId){
		$address = Address::select('id','email_id','phone_no','address_1','first_name','last_selected','area_id')->where('user_id',$userId)->get();
		if(count($address)>0) {
			return response()->json([
				'message' => 'address fetched successfully',
				'data' => $address,
			],200);
		}
		return response()->json([
			'message' => 'Something went wrong',
			'data' => []
		],500);
	}
	
	public function getShippingArea(){
		$shipArea = ShippingArea::select('id','area')->get();
		if(!empty($shipArea)){
			return response()->json([
				'message' => 'area fetched successfully',
				'data' => $shipArea,
			],200);
		}
		return response()->json([
			'message' => 'Something went wrong',
			'data' => []
		],500);
	}
	
	public function setLastAdd(Request $request){
		$address = Address::where('user_id',$request->user_id)->get();
		if(count($address)>0)
		{
			foreach($address as $add)
			{
				if($add->id == $request->addressId){
					$add->last_selected = 1;
				}else{
					$add->last_selected = null;
				}
			    $add->save();
			}
			return response()->json([
				     'message' => 'updated',
				     'data' => [],
			         ],200);
		}
		
	}
	
	public function getShippingCharge(Request $request){
		$user = Auth::user();
		if(empty($user)){
			return response()->json([
				     'message' => 'Unauthorised Access',
			],401);
		}
		//$shipCharge = ShippingArea::select('id','charge')->where('id',$request->area_id)->first();
		$id = $request->area_id;
		$user = Auth::user();
        $getshippingCharge = 0;
		$order = Order::getUserOrders(true);
		$amount = $order->order_amount;
		$minOrderAmountFreeDelivery = Setting::getFieldVal('mini_order_amt_free_shiping');
		if ((Auth::check() && $user->freeshipping == 'on') || $id == 0 || ($amount>=$minOrderAmountFreeDelivery && $minOrderAmountFreeDelivery>0)) {
			$getshippingCharge = "0";
		} else {		
			
			$loction = ShippingArea::find($id);
            if(!empty($loction))
			{
			$getshippingCharge = $loction->charge;		     
			}
			else{
				$getshippingCharge = Setting::getFieldVal('sp_charge');
			}
		}
		$totalAmount = $amount + $getshippingCharge;
		$order->shipping_charge = $getshippingCharge;
		$order->total_amount = $totalAmount;
		 
		$order->save();
		$homeDelivery = Setting::getFieldVal('sp_enble_today');
		$minOrdAmt = Setting::getFieldVal('mini_order_amt');
		$payDetails=Common::getRazorpayApi();
		$selfPickupTime = Setting::getFieldVal('open_close_time');
		$freeShipEnable = $user->freeshipping;
		$payOnDelivery = $user->cod;
		 return response()->json([
		 'message' => 'updated',
		 'data' =>[
		       'shippingCharge'=>$getshippingCharge,
		       'payMode'=> $payDetails['payMode'],
		       'homeDelivery'=> $homeDelivery,
		       'minOrdAmt'=> $minOrdAmt,
		       'razorpayKey'=> $payDetails['razorpayKey'],
		       'razorpaySecret'=> $payDetails['razorpaySecret'],
		       'selfPickupTime'=> $selfPickupTime,
		       'freeShipEnable'=> $freeShipEnable,
		       'payOnDelivery'=> $payOnDelivery,
			    'totalAmount' =>$totalAmount,
		 ],
		 
		 ],200);
		
	}
	
	
}












