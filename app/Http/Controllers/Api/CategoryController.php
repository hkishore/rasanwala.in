<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Category;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Variant;
use App\Models\Order;
use App\Models\User;
use App\Models\Notification;
use App\Models\OrderLine;
use App\Models\Setting;
use App\Models\FavoriteItem;
use Auth,DB;


class CategoryController extends Controller
{
	public function categories(){
		$productIds = Product::where('status',1)->pluck('categeory_id')->toArray();
		$categories = Category::select('id', 'name', 'image')->whereNotNull('parent_id')->where('status',1)->whereIn('id',$productIds)->limit(8)->get();
		return response()->json($categories,200);
	}
	
	public function getproducts(Request $request)
	{
		 $products = Product::select('id','name','image')->with(['variant'])->where('status',1)->limit(10)->inRandomOrder()->get();
		 return response()->json($products,200);
	}

	public function getTopSellingProducts(Request $request)
	{
		 $orderLineProductIds = OrderLine::select('product_id', DB::raw('count(*) as order_count'))->groupBy('product_id')->orderByDesc('order_count')->limit(10)->pluck('product_id')->toArray();
		 $products = Product::select('id','name','image')->with('variant')->whereIn('id',$orderLineProductIds)->get();
		 return response()->json($products,200);		
	}

	public function getNewpProducts(Request $request)
	{
		 $products = Product::select('id','name','image')->with('variant')->where('status',1)->orderBy('id','DESC')->limit(10)->get();
		 return response()->json($products,200);		
	}

    public function getProductByTypeId(Request $request){
		 $offset = 0;
		 $productsObj = Product::select('id','name','image')->with(['variant'])->where('status',1);
		 if(isset($request->page) && (int)$request->page > 1){
			$offset = (int)$request->page*10;
		 }
		 if(isset($request->category) && $request->category != "" && $request->category != null){
			 $productsObj = $productsObj->where('categeory_id',$request->category);
		 }
		 if(isset($request->type) && $request->type == "new_arrivals"){
			 $productsObj = $productsObj->orderBy('id','DESC');
		 }
		 if(isset($request->type) && $request->type == "best_selling"){
			 $orderLineProductIds = OrderLine::select('product_id', DB::raw('count(*) as order_count'))->groupBy('product_id')->orderByDesc('order_count')->skip($offset)->limit(10)->pluck('product_id')->toArray();
			 $productsObj = $productsObj->whereIn('id',$orderLineProductIds);
		 }
		 $products = $productsObj->skip($offset)->limit(10)->get();
		 return response()->json($products,200);
	}	
	
	public function singleProduct(Request $request){
		$product= Product::where('id',$request->product_id)->where('status',1)->first();
		$brand= Brand::where('id',$product->brand_id)->first();
		$varients= Variant::where('product_id',$product->id)->orderBy('sale_price','ASC')->get();
		if(!empty($brand)){
			$product->brandName=$brand->name;
		}
		if(!empty($varients)){
		   foreach($varients as $key=>$varient){
				$per = new Product;
				$varients[$key]->offerPercentage=$per->getPercentage($varient);
			   }
		}
		$product->varients=$varients;	 
		return response()->json([
				'message' => 'Data Fetched',
				'data' => $product,
			],200);
	}
	
	public function addToCart(Request $request){
		if(isset($request->user_id) && $request->user_id != "" && $request->user_id != null){
		  $order = Order::where('user_id',$request->user_id)->where('status',0)->first();
		  if(empty($order))
		   {
			$order = new Order;
			$order->user_id = $request->user_id;		
			$order->status = 0;		
			$order->save();
		   }
		}
	    $orderLine= OrderLine::where('order_id',$order->id)->where("id",$request->order_line_id)->where("variant_id",$request->variant_id)->first();
		 if(!empty($orderLine)){
			return $this->updateQuantity($request->order_line_id,$request->quantity,$request->amount);
		  }
			$orderLine = new OrderLine;
			$orderLine->order_id = $order->id;
			$orderLine->product_id = $request->product_id;
			$orderLine->variant_id = $request->variant_id;
			$orderLine->amount = $request->amount;
			$orderLine->qty = $request->quantity;
			$orderLine->status = 0;
			if($orderLine->save()){
				$order->reCalculateFee();
				return response()->json([
				'message' => 'Added into Cart',
				'data' => [
					   'order_line_id'=> $orderLine->id,
				],
			  ],201);
		  }
		return response()->json([
			'message' => 'Somthing went wrong',
			'data' => [],
		   ],500);		
	}
	
	public function cartProductQty(Request $request)
	{
		if(isset($request->type) && $request->type == "product")
		{
		   return $this->updateQuantity($request->orderLineId,$request->quantity,$request->amount);
		}
		$saved = $this->updateQuantity($request->orderLineId,$request->quantity,$request->amount);
		if($saved){
         return $this->commonCart($request->user_id);
		}else{
			return response()->json([
			 'message' => 'Somthing went wrong',
			 'data' => [],
		  ],500);
		}
	}
	
	public function updateQuantity($order_line_id,$quantity,$amount){
		$orderLine = OrderLine::where('id',$order_line_id)->first();
		if($orderLine){
			$order = Order::where('id',$orderLine->order_id)->first();
			$orderLine->qty = $quantity;
            $orderLine->amount = $amount;
		    if($orderLine->save()){
			   $order->reCalculateFee();
			    	return response()->json([
			    	 'message' => 'Updated into Cart',
				   'data' => [
				       'order_line_id'=> $orderLine->id,
			    	 ],
			    ],200);
		    }
		}
		return response()->json([
			'message' => 'Somthing went wrong',
			'data' => [],
		  ],500);		
		
	}
	
	public function getCartProducts(Request $request)
	{
		return $this->commonCart($request->user_id);
	}
	
	public function removeCartProducts(Request $request){
		$orderLine = OrderLine::where('id',$request->orderLid)->first();
		$order = Order::where('id',$orderLine->order_id)->where('status',0)->first();
		$order->reCalculateFee();
		if($orderLine->delete())
		{
		     return $this->commonCart($request->user_id);
		}
		return response()->json([
				'message' => 'Something went wrong',
				'data' => [],
			],500);
	}
	
	public function commonCart($user_id){
		$offerListObj=DB::table('orders')->where('user_id',$user_id)->where("status",0)->first();
		$app_maintenance=Setting::getFieldVal('app_maintenance');
		 if($offerListObj)
	      {
			$orderLine=OrderLine::where('order_id',$offerListObj->id)->get();
			 foreach($orderLine as $key=>$cart){
		       $product=Product::where('id',$cart->product_id)->first();
		       $varients=Variant::where('id',$cart->variant_id)->first();
				$orderLine[$key]->variantSize=$varients->size;
				$orderLine[$key]->variantPrice=$varients->sale_price;
				$orderLine[$key]->orderLimit=$varients->order_limit;
				$orderLine[$key]->proName=$product->name;
				$orderLine[$key]->ProImage=$product->image;
			   }  
			return response()->json([
				'message' => 'Data Fecthed',
				'data' => [
			    'cart' => $orderLine,
				'app_maintenance' => $app_maintenance,
			     ],
			],200);       
		  } 
		return response()->json([
				'message' => 'not avilable',
				'data' => [
			      'cart' => [],
				  'app_maintenance' => $app_maintenance,
			     ],
			],404);   
	}
	
	public function getSpecialproducts(Request $request)
	{
		$user = Auth::user();
	    $orderId=0;
	    $order=Order::where('user_id',$user->id)->where('status',0)->first();
	    if($order){
	     $orderId = $order->id;
	    }
	
		$productsObj=Product::select('id', 'name', 'image');
		if(isset($request->type) && $request->type == "1") {    //type 1 for whats'new
			$productsObj->orderBy('id', 'DESC')->limit(30);
		}	
		if(isset($request->type) && $request->type == "2") {     // type 2 for popular
			$productsObj->where('best_sell',1);
		}	
		if(isset($request->type) && $request->type == "3") {     // type 3 for product 
			$productsObj->limit(50);
		}	
        if(isset($request->type) && $request->type == "4" && $request->brand_id != "0"){      // type 4 for product by brand 
			$productsObj->where('brand_id',$request->brand_id);
		}			
		$products=$productsObj->with(['variant','isFavorite'])->where('status',1)->get();
		$orderLine = OrderLine::select('variant_id','id','qty')->where('order_id',$orderId)->where('status',0)->get()->keyBy('variant_id');
		$favorite=FavoriteItem::where('user_id',$user->id)->count(); 
		if(!empty($products))
		{
			return response()->json([
				'message' => 'Data Fetched',
				'data' =>[
				    'products'=>$products,
				    'orderLine'=>$orderLine,
				    'favCnt'=>$favorite,
				],	
			],200);
		}else{
			return response()->json([
			'message' => 'Not Found',
			'data' => [],
		   ],404);
		}
		
	}
	
	public function getOrders(Request $request){	
		$orderList=DB::table('orders')->where('user_id',$request->user_id)->where('status','>', 0)->get();	
		 if(!empty($orderList))
	     {
			 foreach($orderList as $key=>$cart){
		       $orderLine=OrderLine::where('order_id',$cart->id)->count();
			   if(!empty($orderLine))
			   {
				$orderList[$key]->itemCount=$orderLine;
			   }
			   $orderList[$key]->ordStatus="Preparing";
			   if($cart->status == 2){
				  $orderList[$key]->ordStatus="Out of Delvery";
			   }else if($cart->status == 3){
				  $orderList[$key]->ordStatus="Delivered";
			   }else if($cart->status == 4){
				  $orderList[$key]->ordStatus="Canceled";
			   }
			 }
			 return response()->json([
				'message' => 'Data Fecthed',
				'data' => $orderList,
			],200);
		  }
	}
	
	public function getOffers(Request $request){
		$user = Auth::user();
	    $orderId=0;
	    $order=Order::where('user_id',$user->id)->where('status',0)->first();
	    if($order){
	     $orderId = $order->id;
	    }
		 $productIds = Product::getProducts();
		 $products = Product::with(['variant','isFavorite'])->whereHas('variant', function($query) use($productIds){
          $query->whereIn('product_id',$productIds);
         })->whereIn('id',$productIds)->get();
		 $orderLine = OrderLine::select('variant_id','id','qty')->where('order_id',$orderId)->where('status',0)->get()->keyBy('variant_id');
		 $favorite=FavoriteItem::where('user_id',$user->id)->count(); 
		 return response()->json([
				'message' => 'Data Fecthed',
				'data' =>[
				    'products'=>$products,
				    'orderLine'=>$orderLine,
				    'favCnt'=>$favorite,
				],	
			],200);
	}
	
	public function getOrdersItems(Request $request)
	{
		$items = OrderLine::where('order_id',$request->order_id)->get();
		if(!empty($items))
		{
		  foreach($items as $key=>$item)
		  {
			$orders =Order::where('id',$item->order_id)->first();
		    $product =Product::where('id',$item->product_id)->first();  
		    $varients =Variant::where('id',$item->variant_id)->first();  
			$items[$key]->shipCharge = $orders->shipping_charge;
			$items[$key]->proName = $product->name;
			$items[$key]->variantSize=$varients->size;
		  }	
		 return response()->json([
				'message' => 'Data Fecthed',
				'data' => $items,
			],200);	
		}
		return response()->json([
				'message' => 'Not Found',
				'data' => [],
			],404);
		
	}
	
	public function getAllNotification(){
		$user = Auth::user();
		//$user = User::find(36);
		if(empty($user)){
			return response()->json([
				'message' => 'UnAuthorised Access.',
				'data' => [],
			],401);
		}
		$allNotification=Notification::where('status',1)->where('id','>',$user->clear_msg_id)->where(function($query) use($user){
		$query->where('user_type','all_user')->OrWhere(function($query1) use($user){
			$query1->where('user_type','specify')->where('user_id',$user->id);
		});
		})->get();
		
		
		if(count($allNotification)>0)
		{
		 foreach($allNotification as $key=>$notification)
		 {
		 $allNotification[$key]->format = date('Y-m-d', strtotime($notification->created_at)); 
		 }
	    }
			return response()->json([
			'message' => 'Not Found',
			'data' => $allNotification,
		   ],200);
		}
			
	public function getSettingValue(){
		$home_test = Setting::getFieldVal('home_test');
		
		if(!empty($home_test))
		{
		return response()->json([
			'message' => 'Fetched successfully',
			'data' => strip_tags($home_test),
		   ],200);
		}else{
			return response()->json([
			'message' => 'Not Found',
			'data' => [],
		   ],404);
		}
	}
	
	public function getSettingValueAmount(){
		$mini_order_amt_free_shiping = Setting::getFieldVal('mini_order_amt_free_shiping');
		
		if(!empty($mini_order_amt_free_shiping))
		{
		return response()->json([
			'message' => 'Fetched successfully',
			'data' => strip_tags($mini_order_amt_free_shiping),
		   ],200);
		}else{
			return response()->json([
			'message' => 'Not Found',
			'data' => [],
		   ],404);
		}
	}
	}













