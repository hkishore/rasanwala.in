<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Order;
use App\Models\Common;
use App\Models\OrderLine;
use App\Models\User;
use App\Models\Address;
use App\Models\Payment;
use App\Models\PaymentGateway;
use App\Mail\SendMail;
use Mail;
use DB,Auth;

class CheckoutController extends Controller
{
	public function checkout(Request $request)
	{
		$userId = 0;
		$authUser = Auth::user();
		if(empty($authUser)){
			return response()->json([
				'message' => 'UnAuthorised Access.',
				'data' => [],
			],401);
		}
		if($request->delivery_type == "home" && $request->payment_type == "cod" && $authUser->cod != "on"){
           return response()->json([
				'message' => 'Something went wrong. please refresh the page and try again.',
				'data' => [],
			],500);
		}
		if(isset($request->user_id) && $request->user_id != ""){
		  $userId = $request->user_id;
		  $order = Order::where('user_id',$request->user_id)->where('status',0)->first();
		}
		else if(empty($order))
		{
			$order = new Order;
			$order->user_id = $userId;		
			$order->status = 0;		
			$order->save();
		}
		if(count($order->getOrderLines) > 0){
			$user=User::where('id',$request->user_id)->first();
            if($request->payment_type == "razorpay"){
				$info = json_encode(array('razorpay_payment_id'=>$request->razorpay_payment_id,'razorpay_order_id'=>$request->razorpay_order_id,'razorpay_signature'=>$request->razorpay_signature));
				$payVerify = new PaymentGateway;
				$verifivation =$payVerify->verifyPayment($request);
				if($verifivation == null){
				$payment = new Payment;
				$payment->order_id = $order->id;
				$payment->txn_id = $request->razorpay_payment_id;
				$payment->user_id = $user->id;
				$payment->data = $info;
				$payment->email = $request->shipemail;
				$payment->amount = $request->totalamount;
				$payment->payment_mode = $request->payment_type;
				$payment->payment_currency = "INR";
				$payment->payment_status = "PAID";
				$payment->save();
				}
			}              
			if(count($user->userAddress)==0 && $request->delivery_type == "home")
			{
				$address=Address::createNewMobileAddress($request);				
				$address_id=$address->id;
			}
			$address_id=$request->address_id;
			$order->user_id=$user->id;
			$order->address_id= $address_id;
			$order->area_id= $request->area_id;
			$order->notes= $request->note;
			$order->payment_type= $request->payment_type;
			$order->status=1;
			$order->order_date=date('Y-m-d');
			$order->delivery_type=$request->delivery_type;
			$order->shipping_charge= $request->shipCharge;
				
			if($request->payment_type == "razorpay"){
			$order->payment_status=1;
			$order->save();
			}
			else{
				$order->payment_status=0;
				$order->save();
			}	
		    DB::table('order_lines')->where('order_id', $order->id)->update(['status' => 1]);
		    Common::sendemail($order,$user);
			return response()->json([
				'message' => 'Order placed successfully.',
				'data' => [],
			],201);
			}
			return response()->json([
			  'message' => 'Something went wrong. please try again in few minutes.',
			  'data' => [],
		    ],500);
	}
}

 












