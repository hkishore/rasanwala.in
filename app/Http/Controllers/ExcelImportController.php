<?php

namespace App\Http\Controllers;

use App\Imports\ProductImport;
use Excel;

use Illuminate\Http\Request;

class ExcelImportController extends Controller
{
    public function import(Request $request)
    {
        // dd($request->all());
        // Validate the uploaded file
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);
 
        // Get the uploaded file
        $file = $request->file('file');
 
        // Process the Excel file
        Excel::import(new ProductImport, $file);
 
        return redirect()->back()->with('success', 'Excel file imported successfully!');
    }
}
