<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\Order;
use Auth,DB;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('address.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		//dd($request->all());
		$userId=Auth::user()->id;
       $model = new Address;
        $validator = $model->validator($request);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
		 $model->user_id=$userId;
		$model->first_name=$request->first_name;
		$model->email_id=$request->email_id;
		$model->phone_no=$request->phone_no;
		$model->address_1=$request->address_1;
		if($request->add_id != "other")
		{
			$model->area_id=$request->add_id;
		}else{
			$model->area_id=0;
		}
		$model->other=$request->add_location;
        if ($model->save()) {
            $request->session()->flash('success', 'Address added successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('myprofile.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
       // return view("admin.users.edit")->with(['model'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {//dd($request->all());
        $validator = $address->validator($request);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
	    $data=$request->all();		
		 $data['user_id']=Auth::user()->id;		
        if ($address->fill($data)->push()) {
            $request->session()->flash('success', 'Address Update successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('myprofile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function addressEdit()
	{
		return view('front.addressedit');
	}
	public function Editaddress($id)
	{ $user=Auth::user();
		$models=Address::find($id);
		return view('front.addressedit')->with(['model'=>$models]);
	}
	public function DeleteAddress(Request $request,$id)
	{	
		$Address=Address::find($id);
		$order=Order::where('address_id',$id)->first();
		if(empty($order))
		{
			$Address->delete();
		}else{
			$Address->status=3;
			$Address->save();
		}
		$request->session()->flash('success', 'Address delete successfully.');
		return redirect()->back();
	}
	Public function addressAdd(Request $request)
	{
		//dd($request->all());
		$user=Auth::user();
		$model = new Address;
		$model->user_id=$user->id;
		$model->first_name=$request->first_name1;
		$model->phone_no=$request->phone_no1;
		$model->address_1=$request->address_11;
		if($request->add_id1 != "other")
		{
			$model->area_id=$request->add_id1;
		}else{
			$model->area_id=0;
		}
		$model->other=$request->add_location1;
        $model->save();
        return redirect()->route('checkout');
	}
}
