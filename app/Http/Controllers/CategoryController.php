<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Common;
use Auth,File,DB;



class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Category::orderBy('id','desc')->get();
        return view('admin.categories.index')->with(['models' => $models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
		
    {
        $categories=Common::getParentIdList();
        return view('admin.categories.create')->with(['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $models = new Category;
		$url=Common::slugify($request->name);
		$request->merge(['slug' => $url]);
        $validator = $models->validator($request);
        
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput(); 
		$id = Auth::user()->id;
        $data=$request->all();
		    $imageName="";
			if($request->image!='')
			{
     		  $imageName=$request->image;
              File::move(public_path('temp/'.$imageName), public_path('uploads/categories/'.$imageName));
			  $path=asset('uploads/categories/'.$imageName);
			//  dd($path);
			 $models['image ']= $path;
			}
			
        if ($models->create($data)) {
            $request->session()->flash('success', 'Category added successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories=Common::getParentIdList();
        return view('admin.categories.edit')->with(['model' => $category,'categories'=>$categories  ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $validator = $category->validator($request, $category->id);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
          $data=$request->except('_token');
		  if($request->image!='')
			{
				$filePath=public_path('uploads/categories/'.$category->image);
				if (File::exists($filePath)) {
				File::delete($filePath);
				}	
     		  $imageName=$request->image;
              File::move(public_path('temp/'.$imageName), public_path('uploads/categories/'.$imageName));
			  $path=asset('uploads/categories/'.$imageName);
			  $data['image']= $path;
			}
			elseif($request->remove_image && $request->remove_image=="y")
			{
				$filePath=public_path('uploads/categories/'.$category->image);
				if (File::exists($filePath)) {
				File::delete($filePath);
				}
			   $data['image']="";
			}
		
        if ($category->fill($data)->push()) {
            $request->session()->flash('success', 'Categories updated successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Category $category)
    {
        try 
        {
           if ($category->delete()) {
                $request->session()->flash('success', 'Category deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('category.index');
	}
	public function dataTable(Category $category)
    {
		   $offset=0;
	       $limit=10;
           $user=Auth::user();
			$userID=$user->id;
			$where="where 1";
				$catId=$_GET['cat_id'];
				if($catId >0)
				{
					$where= "where parent_id=".$catId;
				}
			
           $serachStrText=stripslashes($_GET['search']['value']); 	
           $order=" ORDER BY `id` DESC";		   
	       if(isset($_GET['start']) && $_GET['start']!='')
		   {
			   $offset=$_GET['start'];
		   }
		   if(isset($_GET['length']) && $_GET['length']!='')
		   {
			   $limit=$_GET['length'];
		   }
            if($serachStrText!='')
			{
			  $alTextA=explode(' ',$serachStrText);
			  foreach($alTextA as $serachStr)
			   {				
				$serachStr=addslashes($serachStr);
				$whereArray=[];                   
				$whereArray[]="name like '%".$serachStr."%'";		
				$where.=" and (".implode(' or ',$whereArray).")";
			   }
            }						
			$slectQuery="SELECT * FROM categories ".$where;
		     $contractsCnt=DB::select($slectQuery);
			 $total_records=count($contractsCnt);			 
			 $category=DB::select($slectQuery." order by id desc limit ".$offset.",".$limit);//." limit ".$offset.",".$limit
			 $AllData=[];
			$category=Category::hydrate($category);				 
			 foreach($category as $cat)
			 {
				$data=[];
				if(!empty($cat->image) || $cat->image != null )
				{
				$data[]='<img height="50px" src=" '.$cat->image.' ">';
				}else{
					$data[]='<div class="red">No Image</div>';
				}
				if($cat->parent_id>0)
				{
				$data[]= ucwords($cat->parentName());
				$data[]=ucwords($cat->name);
				}else{
				$data[]=ucwords($cat->name);
				$data[]="-";
				}
				$data[]=$cat->slug;
				$data[]=$cat->status== 1?"Active":"InActive";
                $delete=route('category.destroy',$cat->id);
                $edit=route('category.edit',$cat->id);
				$editimage=asset('img/datatable-edit-plus.svg');
				$deleteImage=asset('img/datatable-delete.svg');
				
                $btn='<a href="'.$edit.'" class="btn me-2 dataActionBtn btnEdit"  title="Edit"><img src="'.$editimage.'" alt="" /></a> <a href="javascript:void(0);" class="btn me-2 dataActionBtn btnDelete" title="Delete" onclick="deleteCategory(this)" data-href="'.$delete.'"><img src="'.$deleteImage.'" alt="" /></a> ';
	
                $data[]='<div class="d-flex justify-content-start">'.$btn.'</div>';	
				$AllData[]=$data;
			}
			 $draw=(isset($_GET['draw'])?$_GET['draw']:0);
		return json_encode(array('draw'=>$draw,"recordsTotal"=>$total_records,"recordsFiltered"=> $total_records,"data"=>$AllData));
    }
    
}
