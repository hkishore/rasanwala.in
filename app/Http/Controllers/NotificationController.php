<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;
use Auth, DB,File;
use App\Models\UserNotification;
use App\Models\User;
use App\Models\Common;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $user=Auth::user();
        // // $permision = Permision::where('user_id',$user->id)->first();
        // if(($user->role==2 && $permision->notification_view == 1) || $user->role==1)
        // {
        return view('admin.notifications.index');
        // }
        // return redirect()->route('dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.notifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
		 $title = $request->subject;
		 $message = $request->notification;
		 $notiImage="";
		 if(isset($request->noti_icon) && $request->noti_icon != "" && $request->noti_icon !=null  ){
            $imageName=$request->noti_icon;
            File::move(public_path('temp/'.$imageName), public_path('uploads/notification/'.$imageName));
			$notiImage = asset('uploads/notification/'.$imageName);
		 }
		 
			  $tokens =[];
			    if (isset($request->multiple_user)) {
							$users = $request->multiple_user;

							$countuser = count($request->multiple_user);
							if($countuser>0){
							for ($i = 0; $i < $countuser; $i++) {
								  $models = new Notification;
							$models->notification = $request->notification;
							$models->user_type = $request->user_type;
							$models->subject = $request->subject;
							$models->user_id = $users[$i];
							$models->status  = 1;
							$models->save();
							}
							$tokens =User :: where('status',1)->whereIn('id',$users)->whereNotNull('device_token')->pluck('device_token')->toArray();
							}
							
                        
                  }else{
						$models = new Notification;
						$models->notification = $request->notification;
						$models->user_type = $request->user_type;
						$models->subject = $request->subject;
						$models->status  = 1;
						$models->save();
				
				$tokens = User :: where('status',1)->whereNotNull('device_token')->pluck('device_token')->toArray();
			          }
		 if(count( $tokens)>0){
			 
				  Common::sendFcmAndroidNotification($tokens, $title, $message,$notiImage);
				  
			  }
			 
		 $request->session()->flash('success', 'Notification send successfully.');
		 return redirect()->route('notification.index');
        // $models = new Notification;
        // $validator = $models->validator($request);
        // if ($validator->fails()) {
            // return redirect()->back()->withErrors($validator)->withInput();
        // }
        // $models->status = $request->status;
        // $models->notification = $request->notification;
        // $models->subject = $request->subject;
        // $models->user_type = $request->user_type;
        // if ($models->save()) {

            // if (isset($request->multiple_user)) {
                // $users = $request->multiple_user;
                // $countuser = count($request->multiple_user);
                // for ($i = 0; $i < $countuser; $i++) {
                    // $userN = new UserNotification;
                    // $userN->user_id = $users[$i];
                    // $userN->notification_id = $models->id;
                    // $userN->save();
                // }
            // }

            // $request->session()->flash('success', 'Notification added successfully.');
        // } else {
            // $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        // }
        // return redirect()->route('notification.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        $user = Auth::user();
        return view('admin.notifications.edit')->with(['model' => $notification]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        // dd($request->all(),$notification);
        $validator = $notification->validator($request, $notification->id);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
        $notification->status = $request->status;
        $notification->notification = $request->notification;
        $notification->subject = $request->subject;
        $notification->user_type = $request->user_type;
        if ($notification->save()) {


            if ($request->user_type == 'specify') {

                $id = $notification->id;
                //dd($id);
                UserNotification::where('notification_id', $id)->delete();

                if (isset($request->multiple_user)) {
                    $users = $request->multiple_user;
                    $countuser = count($request->multiple_user);
                    for ($i = 0; $i < $countuser; $i++) {
                        $userN = new UserNotification;
                        $userN->user_id = $users[$i];
                        $userN->notification_id = $notification->id;
                        $userN->save();
                    }
                }
            }


            $request->session()->flash('success', 'Notification updated successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('notification.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Notification $notification)
    {
        try 
        {
            if ($notification->delete()) {
                $request->session()->flash('success', 'Notification deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('notification.index');
    }
    public function dataTable(Request $request)
    {

        $offset = 0;
        $limit = 10;
        $where = " where user_id IS NULL";
        $userId = Auth::user();
        //dd($userId);
        $serachStrText = stripslashes($_GET['search']['value']);
        $order = " ORDER BY `id` DESC";
        if (isset($_GET['start']) && $_GET['start'] != '') {
            $offset = $_GET['start'];
        }
        if (isset($_GET['length']) && $_GET['length'] != '') {
            $limit = $_GET['length'];
        }

        if ($serachStrText != '') {
            $alTextA = explode(' ', $serachStrText);
            foreach ($alTextA as $serachStr) {
                $serachStr = addslashes($serachStr);
                $whereArray = [];
                $whereArray[] = "notification like '%" . $serachStr . "%'";
                $where .= " and (" . implode(' or ', $whereArray) . ")";
            }
        }
        $contractsCnt = DB::select("SELECT count(*) as cnt FROM notifications " . $where . $order);
        $total_records = $contractsCnt[0]->cnt;
        $slectQuery = "SELECT * from notifications " . $where . $order;

        $notifications = DB::select($slectQuery . " limit " . $offset . "," . $limit);    //." limit ".$offset.",".$limit
        $AllData = [];
        $notifications = Notification::hydrate($notifications);
        foreach ($notifications as $notify) {
            $data = [];
            $data[] = $notify->notification;
            $data[] = ($notify->status == 1 ? 'Active' : 'InActive');
            $edit = route('notification.edit', $notify->id);
            $delete = route('notification.destroy', $notify->id);
            $editimage = asset('img/datatable-edit-plus.svg');
            $deleteImage = asset('img/datatable-delete.svg');
            $btn = "";
            $btn = '<a href="' . $edit . '" class="btn me-2 dataActionBtn btnEdit"  title="Edit"><img src="'.$editimage. '" alt="" /></a> <a href="javascript:void(0);" class="btn me-2 dataActionBtn btnDelete" title="Delete" onclick="deleteProduct(this)" data-href="' . $delete . '"><img src="' . $deleteImage . '" alt="" /></a>';
            $data[] = '<div class="d-flex justify-content-start">' . $btn . '</div>';
            $AllData[] = $data;
            // dd($AllData);
        }
        $draw = (isset($_GET['draw']) ? $_GET['draw'] : 0);
        return json_encode(array('draw' => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $total_records, "data" => $AllData));
    }
    // public function getNotification(Request $request)
    // {
    //     $user = Auth::user();
    //     $msg = Notification::where('status', 1);
    //     // if ($user->role == 3) {
    //     //     $msg->where('user_type', 'customer');
    //     // }
    //     // if ($user->msg_id > 0) {
    //     //     $msg->where('id', '>', $user->msg_id);
    //     // }
    //     $Notification = $msg->orderBy('id', 'DESC')->limit(4)->get();
    //     return view('noti-popup')->with(['notification'=>$Notification]);
    //     // return $Notification;
    // }
    public function allNoti(){
		return view('all-notification');
	}

    // public function userAllNotification(Request $request)
    // {
    //     $user = Auth::user();
    //     $userNotificaton = DB::table('user_notifications')->where('user_id', $user->id)->pluck('notification_id')->toArray();
    //     if ($user->role == 2) {
    //         $allNotification = Notification::where('status', 0)->where('id', '>', $user->msg_id)->where('id', '>', $user->clear_msg_id)->where(function ($query) use ($userNotificaton) {
    //             $query->whereIn('id', $userNotificaton)->orWhere('user_type', 'all_user')->orWhere('user_type', 'staff');
    //         });
    //     } else {
    //         $allNotification = Notification::where('status', 0)->where('id', '>', $user->msg_id)->where('id', '>', $user->clear_msg_id)->where(function ($query) use ($userNotificaton) {
    //             $query->whereIn('id', $userNotificaton)->orWhere('user_type', 'all_user');
    //         });
    //     }
    //     $Notification = $allNotification->orderBy('id', 'DESC')->limit(5)->get();
    //     $NotiIds = [];
    //     $NotiIds[] = 0;
    //     if (count($Notification) > 0) {
    //         foreach ($Notification as $key => $not) {
    //             $NotiIds[] = $not->id;
    //             $Notification[$key]->createdAt = date('M, d, Y \\a\\t h:i A', strtotime($not->created_at));
    //             $Notification[$key]->msg_color = 1;   // for unread
    //         }
    //     }

    //     $unreadNotis = count($Notification);

    //     if (isset($request->operation) && $request->operation == "click" && count($Notification) > 0) {
    //         $notiId = $Notification[count($Notification) - 1]->id;
    //         $user->update(['msg_id' => $notiId]);
    //         $unreadNotis = $allNotification->where('id', '>', $notiId)->count();
    //     }

    //     if (count($Notification) < 5) {
    //         $notisCnt = count($Notification);
    //         $notisCnt = 5 - $notisCnt;
    //         $readNotis = Notification::where('status', 0)->where('id', '>', $user->clear_msg_id)->whereNotIn('id', $NotiIds)->where(function ($query) use ($userNotificaton) {
    //             $query->whereIn('id', $userNotificaton)->orWhere('user_type', 'all_user');
    //         })->orderBy('id', 'DESC')->limit($notisCnt)->get();

    //         foreach ($readNotis as $key => $not) {
    //             $readNotis[$key]->createdAt = date('M, d, Y \\a\\t h:i A', strtotime($not->created_at));
    //             $readNotis[$key]->msg_type = 2;   // for read
    //         }

    //         $Notification = array_merge($Notification->toArray(), $readNotis->toArray());
    //     }

    //     return json_encode(['notification' => $Notification, 'unread' => $unreadNotis]);
    // }
    // public function adminSeenNotification(Request $request){
	// 	$user=Auth::user();
	// 	$msg=Notification::where('status',0)->orderBy('id','DESC')->first();
	// 	if(!empty($msg))
	// 	{
	// 	$user->update(['msg_id'=>$msg->id]);
	// 	return response()->json(['msg' => 'Notification Cleared'], 200);
	// 	}
	// 	return response()->json(['error' => 'Something went wrong'], 422);
	// }
}
