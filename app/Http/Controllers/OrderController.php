<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Mail\InvoiceMail;
use App\Mail\OrderDilevered;
use App\Models\Common;
use App\Models\Address;
use App\Models\OrderLine;
use App\Models\OrderListController;
use App\Models\Xpressbees;
use App\Models\Product;
use App\Models\Variant;
use App\Models\TrackOrder;
use App\Models\PickerPickup;
use App\Models\User;
use Auth,DB;
use PDF;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
     {
        return view('admin.orders.order-list');
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('admin.orders.add-order-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->all());
		$models = new Order;
		$validator = $models->validatorAddOrder($request);
		if($validator->fails()) return redirect()->route('order.create')->withErrors($validator)->withInput();
		$userObj = User::find($request->user_id);
		if(empty($userObj))
		{
		$user = new User;
		$user->name = $request->name;
		$user->phone = $request->user_id;
		$user->email = "info@rasanwala.in";
		$user->save();
		}
		if(empty($userObj))
		{
			$models-> user_id = $user->id;
		}else{
			$models->user_id = $request->user_id;
		}
		$models->total_amount = number_format($request->totalAmt);
		$models->payment_status = $request->payment_status;
		$models->order_date = date('Y-m-d');
		$models->status = 1;
		if($models->save())
		{
			if($request->product_id != "" && $request->product_id != null)
			{
				
				$productId = $request->product_id;
				$vairiatId = $request->size;
				$qty = $request->quantity;
				$amount = $request->price;
				$totalAmount = $request->amount;
				$orderId  =$models->id;
				$total = count($productId);
				for($i = 0; $i<$total; $i++)
				{
					$orderLine = new OrderLine;
					$orderLine->product_id = $productId[$i];
					$orderLine->variant_id = $vairiatId[$i];
					$orderLine->qty = $qty[$i];
					$orderLine->amount = $amount[$i];
					//$orderLine->total_amount = $totalAmount[$i];
					$orderLine-> order_id = $orderId;
					$orderLine->status =1;
					$orderLine->save();
				}

			}
			$request->session()->flash('success','Order created successfully.');
		}else{
			$request->session()->flash('error','Something went wrong. Please try again in a few minutes.');
		}
		return redirect()->route('order.index');
    }

    /**
     * Display the specified resource
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order=Order::find($id);
		return view('admin.orders.edit')->with(['model' => $order,]);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    { 
	//dd($request->all());
		$models = Order::find($id);
		$validator = $models->userOrderUpdate($request,$models->id);
		if($validator->fails()) return redirect()->route(order.edit)->withErrors($validator)->withInput();
		$userObj = User::find($request->user_id);
		if(empty($userObj))
		{
		$user = new User;
		$user->name = $request->name;
		$user->phone = $request->user_id;
		$user->email = "info@rasanwala.in";
		$user->save();
		}
		if(empty($userObj))
		{
			$models-> user_id = $user->id;
		}else{
			$models->user_id = $request->user_id;
		}
		$models->total_amount = number_format($request->totalAmt);
		$models->payment_status = $request->payment_status;
		$models->order_date = date('Y-m-d');
		$models->status = 1;
		if($models->save())
		{
			if($request->product_id != "" && $request->product_id != null)
			{
				
				$productId = $request->product_id;
				$vairiatId = $request->size;
				$qty = $request->quantity;
				$amount = $request->price;
				$totalAmount =$request->amount;
				$orderId  =$models->id;
				$total = count($productId);
				$deletVariant =OrderLine::where('order_id',$models->id)->delete();
				for($i = 0; $i<$total; $i++)
				{
					$orderLine = new OrderLine;
					$orderLine->product_id = $productId[$i];
					$orderLine->variant_id = $vairiatId[$i];
					$orderLine->qty = $qty[$i];
					$orderLine->amount = $amount[$i];
					$orderLine-> order_id = $orderId;
					$orderLine->status =1;
					$orderLine->save();
				}

			}
			$request->session()->flash('success','Order update successfully.');
		}else{
			$request->session()->flash('error','Something went wrong. Please try again in a few minutes.');
		}
		return redirect()->route('order.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order,Request $request)
    {
         try 
        {
            if ($order->delete()) {
                $request->session()->flash('success', 'Order deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('order.index');
    }
    public function dataTable(Request $request)
	{
		   $user=Auth::user();
		   $date=date("Y-m-d 00:00:00");
		   $dateend=date("Y-m-d 23:59:59");
		   $where='where updated_at > "'.$date.'" and updated_at < "'.$dateend.'" and status>0';
			$userID=$user->id;
           $serachStrText=stripslashes($_GET['search']['value']); 	
           $order=" ORDER BY `id` DESC";
			$offset=0;
	       if(isset($_GET['start']) && $_GET['start']!='')
		   {
			   $offset=$_GET['start'];
		   }
			$limit=10;
		   if(isset($_GET['length']) && $_GET['length']!='')
		   {
			   $limit=$_GET['length'];
		   }
			 $slectQuery="SELECT * FROM orders ".$where.$order;//" GROUP BY order_id";
		     $contractsCnt=DB::select($slectQuery);
			 $total_records=count($contractsCnt);			 
			 $orders=DB::select($slectQuery." limit ".$offset.",".$limit);
			 $AllData=[];
			 $orders=Order::hydrate($orders);
             foreach($orders as $order)
			 {
				$data=[];
				$shiipingAdd=$order->Address;
				$shipping="";
				$nameState="";
					 
					 if(!empty($shiipingAdd))
					 { 
						$state= $shiipingAdd->region;
						if(!empty($state))
						{
						$nameState=$state->name;
						}

						$shipping=$shiipingAdd->phone_no. ' ' .$shiipingAdd->address_1. ' ' .$shiipingAdd->city. ' ' .$nameState. ' ' .$shiipingAdd->zip_code ; 
					 }
						$data[]=$order->id;
						$product=$order->ManyProduct($order->id);
						$pName=[];
						foreach($product as $prt)
						{
							// $url=route('detail.product',$prt->product->slug);
							 // $pName[]='<a href="' . $url  . '"  title="Product Details" target="_blank" style="text-decoration: none;">'.$prt->product->name.'</a>';
							$pName[]=$prt->product->name;
						}
						$data[]=implode(',',$pName);
						
						if(!empty($order->user)){
						$data[]=$order->user->name;
						}else{
							$data[]="";
						}
						$data[]=$shipping;
						$data[]=$order->payment_status== 1 ? "Paid" : "Pending";
						$data[]=$order->payment_type;
						$data[]=$order->payment_order_id;
						$data[]=$order->getStatus($order->id);
						$data[]=$order->total_amount;
						$data[]=date('Y-m-d H:i:s',strtotime($order->created_at));
						$data[]=date('Y-m-d H:i:s',strtotime($order->updated_at));
						$edit=route('order.edit',$order->id);
                         $invoice=route('order.invoice',$order->id);
						$editimage=asset('img/datatable-edit-plus.svg');
                        $invoiceimage=asset('img/invoice-svgrepo-com.svg');
						$printImage=asset('img/icons8-print-64.png');
						$printPdf=asset('img/icons8-pdf-48.png');
						$statusUpdate=asset('img/icons8-update-48.png');
						if($order->status ==3  ||  $order->status == 4)
						{
						$btn="";
						}else{
					$btn= '<a href="javascript:void(0);"  title="status update" onclick="updateStatus(\''.$order->id.'\')"><img height="25px" src="'.$statusUpdate.'" alt="" /> </a>    <a href="'.$edit.'" class="btn me-2 dataActionBtn btnEdit"  title="Edit"><img src="'.$editimage.'" alt="" /></a>';
                     $btn.= '   <a href="'.$invoice.'" class="fas fa-file-invoice"  title="invoice"><img src="'.$invoiceimage.'" alt="" height="25"/></a>';
						}
					$data[]='<div class="d-flex justify-content-start">'.$btn.'</div>';
					$AllData[]=$data;
				 
			 }
			 $draw=(isset($_GET['draw'])?$_GET['draw']:0);
		return json_encode(array('draw'=>$draw,"recordsTotal"=>$total_records,"recordsFiltered"=> $total_records,"data"=>$AllData));

    }
	public function allOrderdataTable(Request $request)
	{
		  $user=Auth::user();
		 
		   $where='where status>0';
			$userID=$user->id;
           $serachStrText=stripslashes($_GET['search']['value']); 	
           $order=" ORDER BY `id` DESC";
			$offset=0;
	       if(isset($_GET['start']) && $_GET['start']!='')
		   {
			   $offset=$_GET['start'];
		   }
			$limit=10;
		   if(isset($_GET['length']) && $_GET['length']!='')
		   {
			   $limit=$_GET['length'];
		   }
			if (isset($_GET['start_date']) && $_GET['start_date'] != '') {
			$start_date = $_GET['start_date'];
			$where .= " and created_at>='" . date('Y-m-d', strtotime($start_date)) . "'";
			}
			if (isset($_GET['end_date']) && $_GET['end_date'] != '') {
				$end_date = $_GET['end_date'];
				$where .= " and created_at<='" . date('Y-m-d', strtotime($end_date)) . "'";
			}		
			 $slectQuery="SELECT * FROM orders ".$where.$order;//." GROUP BY order_id";
		     $contractsCnt=DB::select($slectQuery);
			 $total_records=count($contractsCnt);			 
			 $orders=DB::select($slectQuery." limit ".$offset.",".$limit);
			 $AllData=[];
			 $orders=Order::hydrate($orders);
             foreach($orders as $order)
			 {
				
				$data=[];
				$shiipingAdd=$order->Address;
				$shipping="";
				$nameState="";
					 
					 if(!empty($shiipingAdd))
					 { 
						$state= $shiipingAdd->region;
						if(!empty($state))
						{
						$nameState=$state->name;
						}

						$shipping=$shiipingAdd->phone_no. ' ' .$shiipingAdd->address_1. ' ' .$shiipingAdd->city. ' ' .$nameState. ' ' .$shiipingAdd->zip_code ; 
					 }
						$data[]=$order->id;
						$product=$order->ManyProduct($order->id);
						$pName=[];
						foreach($product as $prt)
						{
							// $url=route('detail.product',$prt->product->slug);
							 // $pName[]='<a href="' . $url  . '"  title="Product Details" target="_blank" style="text-decoration: none;">'.$prt->product->name.'</a>';
							$pName[]=$prt->product->name;
						}
						$data[]=implode(',',$pName);
						
						if(!empty($order->user)){
						$data[]=$order->user->name;
						}else{
							$data[]="";
						}
						$data[]=$shipping;
						$data[]=$order->getStatus($order->id);
						$data[]=$order->total_amount;
						$data[]=date('Y-m-d H:i:s',strtotime($order->created_at));
						$data[]=date('Y-m-d H:i:s',strtotime($order->updated_at));
						$edit=route('order.edit',$order->id);
						$invoice=route('order.invoice',$order->id);
						$editimage=asset('img/datatable-edit-plus.svg');
						$invoiceimage=asset('img/invoice-svgrepo-com.svg');
						$printImage=asset('img/icons8-print-64.png');
						$printPdf=asset('img/icons8-pdf-48.png');
						$statusUpdate=asset('img/icons8-update-48.png');
						if($order->status ==3  ||  $order->status == 4)
						{
						$btn="";
						}else{
					$btn= '<a href="javascript:void(0);"  title="status update" onclick="updateStatus(\''.$order->id.'\')"><img height="25px" src="'.$statusUpdate.'" alt="" /> </a>    <a href="'.$edit.'" class="btn me-2 dataActionBtn btnEdit"  title="Edit"><img src="'.$editimage.'" alt="" /></a>';
                     $btn.= '   <a href="'.$invoice.'" class="fas fa-file-invoice-dollar"  title="invoice"><img src="'.$invoiceimage.'" alt="" height="25"/></a>';
						}
						$data[]='<div class="d-flex justify-content-start">'.$btn.'</div>';
					$AllData[]=$data;
				 
			 }
			 $draw=(isset($_GET['draw'])?$_GET['draw']:0);
		return json_encode(array('draw'=>$draw,"recordsTotal"=>$total_records,"recordsFiltered"=> $total_records,"data"=>$AllData));
	}
   
    public function shippingStatusUpdate(Request $request){
		
		$order=Order::find($request->order_id);
		if(!empty($order))
		{
			$user_id=$order->user_id;
			$user=User::find($user_id);
			$UpdateOrder=OrderLine::where('order_id',$request->order_id)->update(['status'=>$request->status]);
			 $order->update(['status'=>$request->status]);
			if($request->status==3)//Delivered order
			{
				$order->update(['delivery_date'=>date('Y-m-d')]);
				\Mail::to($user->email)->send(new \App\Mail\OrderCreated($order,'Your order is delivered',' ( Order Number #'.$order->id.' )'));
			}
			if($request->status==4)//Cancel order
			{
				\Mail::to($user->email)->send(new \App\Mail\OrderCreated($order,'Your order hash been canceled',' ( Order Number #'.$order->id.' )'));
			}
		 
		 $request->session()->flash('success', 'Status Updated Suceessfully.');
		}else{
			$request->session()->flash('error', 'Order List not existed.');
			
		}
		return redirect()->route('order.index');
    } 

	public function userOderList()
	{
		return view('front.user_order_list');
	}

     public function orderinvoice($id)
	{
       $orderData = [];
       $order=Order::find($id);
        $shippingcharges=number_format($order->shipping_charge,2, '.', '');
       if(!empty($order)){
          $orderLine= OrderLine::where('order_id',$id)->get();
          if(count($orderLine)>0){
            foreach($orderLine as $key=>$orderL){
              $orderData[$key]['price']=$orderL->amount;
              $orderData[$key]['qty']=$orderL->qty;
              $orderData[$key]['proname']=$orderL->product->name	;
              $orderData[$key]['prosize']=$orderL->variant->size	;
              $orderData[$key]['grandamt']=$orderL->amount;
            }
          }
       }
       return view('admin.orders.invoice')->with(['ordProd' => $orderData,'totalAmount'=>$order->order_amount,'shippingcharges'=>$shippingcharges,'order'=>$order]);
	}
	
	// public function sendInvoice(Request $request)
	// {
		// ini_set('memory_limit','50M');
		// $vendor=Vendor::where('user_id',$request->vendorId)->first();
		// $orderCom=OrderLine::where('vender_id',$request->vendorId)->where('delivery_date','>',$request->startDate)->where('delivery_date','<',$request->endDate)->get();
		
		// if(count($orderCom)>0)
		// {
	    // try{
            // $data["name"]      = $vendor->vendor_name;
            // $data["message"]   =  "You Invoice";
            
			// if($request->shipping!="")
			// {
			  // $orderdata=$orderCom->sum('shipping_charges');
			  // $Invoice="shipping";
			// }else{
			 // $orderdata=$orderCom->sum('commition_amount');
			// $Invoice="comision";
			// }
			   // $pdfDownload=$this->pdfInvoice($orderdata,$Invoice,$vendor);
			  // $pdfinvoice = PDF::loadView('admin.invoice-pdf',['orderdata'=>$orderdata, 'Invoice'=>$Invoice,'vendor'=>$vendor]);
			  
			  // $message = new InvoiceMail($data);
              // $message->attachData($pdfinvoice->output(), $Invoice.'.pdf');
			  // if(!empty($vendor->vendor_email))
			   // \Mail::to($vendor->vendor_email)->send($message);
			  // return $pdfDownload;
	        // }catch(Exception $e){
				
			// }
		// }
		// return 1;
	// }
	// public function pdfInvoice($orderdata, $Invoice, $vendor)
    // {	
        // $url=Common::slugify($vendor->vendor_name);
        // $pdfinvoice = PDF::loadView('admin.invoice-pdf',['orderdata'=>$orderdata, 'Invoice'=>$Invoice,'vendor'=>$vendor]);
		// $foderPath=public_path('uploads/pdf');
		// $nameSavePdf=$Invoice.'-'.$url.'-'.time().'.pdf';
        // $pdfinvoice->save($foderPath.'/'.$nameSavePdf)->stream($nameSavePdf);
		// return '/uploads/pdf/'.$nameSavePdf;
    // }
	public function variantName($name){
			$proName= Product::where('name',$name)->first();
			$varianName = Variant::where('product_id',$proName->id)->get();
			return json_encode($varianName);
	}
	public function getVariant($request){
		$variants=Variant::where('product_id',$request->product)->where('visible','<>',0)->get();
       return json_encode($variants);
	}
	 
}
