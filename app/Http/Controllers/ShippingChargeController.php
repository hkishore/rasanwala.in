<?php

namespace App\Http\Controllers;

use App\Models\ShippingCharge;
use App\Models\ShippingArea;
use Illuminate\Http\Request;

class ShippingChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $models=ShippingCharge::orderBy('id','desc')->get();
       return view('admin.shipcharge.index')->with(['model'=>$models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.shipcharge.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
		$models = new ShippingCharge;
		$validator = $models->validator($request);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();       
       
		  $models->area_id =$request->area_id;
		  $models->start_time =$request->start_time;
		  $models->end_time =$request->end_time;
		  $models->charge =$request->charge;
		  
        if ($models->save()) {
            $request->session()->flash('success', 'Shipping Charge add successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('charge.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShippingCharge  $shippingCharge
     * @return \Illuminate\Http\Response
     */
    public function show(ShippingCharge $shippingCharge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShippingCharge  $shippingCharge
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $shippingCharge=ShippingCharge::find($id);
        return view('admin.shipcharge.edit')->with(['model'=>$shippingCharge]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShippingCharge  $shippingCharge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $shippingCharge=ShippingCharge::find($id);
         $validator = $shippingCharge->validator($request,$shippingCharge->id);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();       
          $data=$request->except('_token');
		  $shippingCharge->area_id =$request->area_id;
		  $shippingCharge->start_time =$request->start_time;
		  $shippingCharge->end_time =$request->end_time;
		  $shippingCharge->charge =$request->charge;
		  
        if ($shippingCharge->save()) {
            $request->session()->flash('success', 'Shipping Area Charge updated successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('charge.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShippingCharge  $shippingCharge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
       $shippingCharge=ShippingCharge::find($id);
        try 
        {
           if ($shippingCharge->delete()) {
                $request->session()->flash('success', 'Area Ship Charge deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('charge.index');
    }
	public function timeCheck(Request $request)
		{
			$shippingCharge=ShippingCharge::where('area_id',$request->areaId);
			$shippingAre=ShippingArea::where('id',$request->areaId);
			if($request->day != "tomarow")
				{
				 $shippingCharge->where('start_time','>',$request->day)->get();
				 $shippingAre->where('start_time','<=',$request->day)->where('end_time','>',$request->day);
				}
			$shippingCharges=$shippingCharge->get();
			$shippingArea = $shippingAre->first();
			if(count($shippingCharges)>0)
			{
			return json_encode($shippingCharges);
			}
			else if(!empty($shippingArea))
			{
			return json_encode(array($shippingArea));
			}
				return json_encode(array());
			
		}
}
