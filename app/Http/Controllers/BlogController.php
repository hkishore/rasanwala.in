<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Common;
use Auth,DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.blogs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //dd($request->all());
        $models = new Blog;
		$url=Common::slugify($request->title);
		$request->merge(['url' => $url]);
        $validator = $models->validator($request);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();       
            
			$id = Auth::user()->id; 		 
		    $imageName="";
			if($request->hasfile('image_name'))
			{
		         $imageName = time().'.'.$request->image_name->getClientOriginalExtension();
                 $request->image_name->move(public_path('uploads/blogs'), $imageName);
				  $data['image_name']=asset('uploads/blogs/'.$imageName);
			}
		   $data=$request->all();		  
		   $data['user_id'] = $id;
		   $data['publish_at'] = date('Y-m-d',strtotime($request->publish_at));
		  
		  
        if ($models->create($data)) {
            $request->session()->flash('success', 'Blog added successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('blog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
       return view('admin.blogs.edit')->with(['model' => $blog]); 
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        $validator = $blog->validator($request, $blog->id);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
		
          $data=$request->except('_token');
		  if($request->hasfile('image_name'))
			{
		         $imageName = time().'.'.$request->file('image_name')->getClientOriginalExtension();
                 $request->file('image_name')->move(public_path('uploads/blogs'), $imageName);
				 $data['image_name']=asset('uploads/blogs/'.$imageName);
			} 
			$data['publish_at'] = date('Y-m-d',strtotime($request->publish_at));
        if ($blog->fill($data)->push()) {
            $request->session()->flash('success', 'Blog updated successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('blog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Blog $blog)
    {
       try 
        {
            if ($blog->delete()) {
                $request->session()->flash('success', 'Blog deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('blog.index');
    }
	
	public function dataTable(Request $request)
	{
		   //$id = Auth::user()->id;
		   $offset=0;
	       $limit=10;
		   $where=" where 1";
           $serachStrText=stripslashes($_GET['search']['value']); 	
           $order=" ORDER BY `id` DESC";		   
	       if(isset($_GET['start']) && $_GET['start']!='')
		   {
			   $offset=$_GET['start'];
		   }
		   if(isset($_GET['length']) && $_GET['length']!='')
		   {
			   $limit=$_GET['length'];
		   }
		   
			if($serachStrText!='')
			{
				$serachStr=addslashes($serachStrText);
				$whereArray=[];                   
				$whereArray[]="title like '".$serachStr."'";				
				$where.=" and (".implode(' or ',$whereArray).")";
			    $order=" ORDER BY `title` ASC";
			}			
			$slectQuery="SELECT * from blogs ".$where.$order;
				
			 $contractsCnt=DB::select($slectQuery);
			 $total_records=count($contractsCnt);			 
			 $blogs=DB::select($slectQuery." limit ".$offset.",".$limit);//." limit ".$offset.",".$limit
			 $AllData=[];
			 $blogs=Blog::hydrate($blogs);
			 foreach($blogs as $blog)
			 {
				 $data=[];
				 $image =$blog->image_name;
				 $data[]='<img height="100px" src=" '.$image.' ">';
				 $data[]=$blog->title;
				 $data[]=date('d-m-Y',strtotime($blog->created_at));
				 $data[]=($blog->status==0?date('d-m-Y',strtotime($blog->publish_at)):'');
				 $data[]=($blog->status==0?'Active':'InActive');	 
				 $edit=route('blog.edit',$blog->id);				 
                 $delete=route('blog.destroy',$blog->id);
				 $editimage=asset('img/datatable-edit-plus.svg');
				 $deleteImage=asset('img/datatable-delete.svg');
				
                 $btn='<a href="'.$edit.'" class="btn me-2 dataActionBtn btnEdit"  title="Edit"><img src="'.$editimage.'" alt="" /> <a href="javascript:void(0);" class="btn me-2 dataActionBtn btnDelete" title="Delete" onclick="deleteBlog(this)" data-href="'.$delete.'"><img src="'.$deleteImage.'" alt="" /></a>';
				 $data[]='<div class="d-flex justify-content-start">'.$btn.'</div>';	
				 $AllData[]=$data;
			 }
			 $draw=(isset($_GET['draw'])?$_GET['draw']:0);
		return json_encode(array('draw'=>$draw,"recordsTotal"=>$total_records,"recordsFiltered"=> $total_records,"data"=>$AllData));
      
	}
}
