<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Common;
use Illuminate\Http\Request;
use Auth,DB;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models=Brand::orderBy('id','desc')->get(); 
        return view('admin.brand.index')->with(['model'=>$models]);
		//($models);
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $models = new Brand;
		 
        $validator = $models->validator($request);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();       
          
		  $user = Auth::user(); 
          $id=$user->id;		  
		  $imageName="";
			if($request->hasfile('image'))
			{
		         $imageName = time().'.'.$request->image->getClientOriginalExtension();
                 $request->image->move(public_path('uploads/brand'), $imageName);
			}
		
		   $data=$request->all();
		   $data['slug']=Common::slugify($request->name);
		  //$data['user_id'] = $id;
		 $data['image']=asset('uploads/brand/'.$imageName);
		  
        if ($models->create($data)) {
            $request->session()->flash('success', 'Brand added successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('brand.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('admin.brand.edit')->with(['model'=>$brand]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
         $validator = $brand->validator($request, $brand->id);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
          $data=$request->except('_token');
		  if($request->hasfile('image'))
			{
		         $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
                 $request->file('image')->move(public_path('uploads/brand'), $imageName);
					$data['image']=asset('uploads/brand/'.$imageName);
			} 
            
        if ($brand->fill($data)->push()) {
            $request->session()->flash('success', 'Brand updated successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('brand.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Brand $brand)
    {
		try 
        {
           if ($brand->delete()) {
                $request->session()->flash('success', 'Brand deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('brand.index');
	}
}
