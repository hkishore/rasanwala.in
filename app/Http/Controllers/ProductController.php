<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Variant;
use App\Models\Brand;
use App\Models\Common;
use App\Models\Category;
use App\Models\OrderLine;
use Illuminate\Http\Request;
use Excel, DB, Auth, File;
use Image;
use App\Exports\ProductsExport;
//use \Illuminate\Support\Facades\Image;
class ProductController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view("admin.product.index");
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view("admin.product.create");
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//dd($request->all());
		$user = Auth::user();
		$model = new Product;
		$validator = $model->validator($request);
		if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
		$url = Common::slugify($request->name . '-' . $request->sku);
		$imageName = "";
		if ($request->image != '') {
			$imageName = $request->image;
			File::move(public_path('temp/' . $imageName), public_path('uploads/product/' . $imageName));
			$model->image = asset('uploads/product/' . $imageName);
		}
		$model->name = $request->name;
		//$model->sku = $request->sku;
		$model->status = $request->status;
		$model->brand_id = $request->brand_id;
		$model->gst_percent = $request->gst_percent;
		$model->categeory_id = $request->categeory_id;
		if (isset($request->description) && $request->description != "") {
			$model->description = $request->description;
		}
		$model->meta_title = $request->meta_title;
		$model->meta_tag = $request->meta_keyword;
		$model->meta_description = $request->meta_description;
		$model->best_sell = $request->best_sell;
		$model->slug = $url;
		$user = Auth::User();
		$model->user_id = $user->id;

		if ($model->save()) {
			$msg = "\n New Product= " . $request->name . " \n user_id= " . $user->id;
			Common::teligramErrorBot($msg);
			$size = $request->size;
			$regular_price = $request->regular_price;
			$sale_price = $request->sale_price;
			$visible = $request->visible;
			$order_limit=$request->order_limit;
			$stock_limit=$request->stock_limit;
			$shiping_charge=$request->charge;
			$total = count($size);

			for ($i = 0; $i < $total; $i++) {
				$modelVariant = new Variant;
				$modelVariant->size = $size[$i];
				$modelVariant->regular_price = $regular_price[$i];
				$modelVariant->order_limit = $order_limit[$i];
				$modelVariant->stock_limit = $stock_limit[$i];
				$modelVariant->shiping_charge = $shiping_charge[$i];
				$modelVariant->sale_price = $sale_price[$i];
				$modelVariant->visible = $visible[$i];
				$modelVariant->product_id = $model->id;
				$modelVariant->save();
			}



			$request->session()->flash('success', 'Product added successfully.');
		} else {
			$request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
		}
		return redirect()->route('product.index');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Product  $product
	 * @return \Illuminate\Http\Response
	 */
	public function show(Product $product)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\Product  $product
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Product $product)
	{
		$variant = Variant::where('product_id', $product->id)->get();
		// dd($images);
		return view("admin.product.edit")->with(['model' => $product, 'variant' => $variant]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Product  $product
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Product $product)
	{
		// dd($request->all(),$product);
		$user = Auth::user();
		$validator = $product->validator($request, $product->id);
		if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
		$url = Common::slugify($request->name . '-' . $request->sku);
		if ($request->image != '') {
			$filePath = public_path('uploads/product/' . $product->image);
			if (File::exists($filePath)) {
				File::delete($filePath);
			}
			$imageName = $request->image;
			File::move(public_path('temp/' . $imageName), public_path('uploads/product/' . $imageName));
			$product->image = asset('uploads/product/' . $imageName);
		} elseif ($request->remove_image && $request->remove_image == "y") {
			$filePath = public_path('uploads/product/' . $product->image);
			if (File::exists($filePath)) {
				File::delete($filePath);
			}
			$product->image = "";
		}
		$product->name = $request->name;
		$product->sku = $request->sku;
		$product->status = $request->status;
		$product->brand_id = $request->brand_id;
		$product->gst_percent = $request->gst_percent;
		$product->categeory_id = $request->categeory_id;
		$product->meta_title = $request->meta_title;
		$product->meta_tag = $request->meta_keyword;
		if (isset($request->description) && $request->description != "") {
			$product->description = $request->description;
		}
		$product->meta_description = $request->meta_description;

		$product->slug = $url;
		//$product->image = $imageName;
		$user = Auth::User();
		$product->user_id = $user->id;
		$product->best_sell = $request->best_sell;
		if ($product->save()) {
			$product->sku = "RW-" . $product->id;
			$product->save();
			$size = $request->size;
			$regular_price = $request->regular_price;
			$sale_price = $request->sale_price;
			$order_limit=$request->order_limit;
			$stock_limit=$request->stock_limit;
			$charge=$request->charge;
			$visible = $request->visible;
			$vids = $request->vids;
			// dd($order_limit,$stock_limit);
			$total = count($size);
			//$deletVariant =Variant::where('product_id',$product->id)->delete();
			$allVids = [];
			for ($i = 0; $i < $total; $i++) {
				$modelVariant = new Variant;
				$vid = $vids[$i];
				if ($vid > 0) {
					$modelVariant = Variant::find($vid);
				}
				$modelVariant->size = $size[$i];
				$modelVariant->regular_price = $regular_price[$i];
				$modelVariant->sale_price = $sale_price[$i];
				$modelVariant->visible = $visible[$i];
				$modelVariant->order_limit = $order_limit[$i];
				$modelVariant->stock_limit = $stock_limit[$i];
				$modelVariant->shiping_charge = $charge[$i];
				$modelVariant->product_id = $product->id;
				$modelVariant->save();
				$allVids[] = $modelVariant->id;

				$msg = "\n Updated Product(product_id)= " . $product->id . " \n user_id= " . $user->id;
				Common::teligramErrorBot($msg);
			}

			if (count($allVids) > 0) {
				$orderCom = OrderLine::whereNotIn('variant_id', $allVids)->where('product_id', '=', $product->id)->where('status', '>', 0)->pluck('variant_id');

				$vList = Variant::whereNotIn('id', $allVids)->where('product_id', '=', $product->id);
				$oList = OrderLine::whereNotIn('variant_id', $allVids)->where('product_id', '=', $product->id)->where('status', '>', 0);
				$msg = 'Product updated successfully.';
				if (count($orderCom) > 0) {
					$vList->whereNotIn('id', $orderCom);
					$oList->whereNotIn('id', $orderCom);
					$msg .= ' Some of them not removed';
				}
				$vList->delete();
				$oList->delete();
			}


			$request->session()->flash('success', $msg);
		} else {
			$request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
		}
		return redirect()->route('product.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Product  $product
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, Product $product)
	{
		$user = Auth::user();
		$id = $product->id;
		try {
			if ($product->delete()) {
				$msg = "\n Deleted Product(product_id)= " . $product->id . " \n user_id= " . $user->id;
				Common::teligramErrorBot($msg);
				$deletimages = Variant::where('product_id', $id)->delete();
				$request->session()->flash('success', 'Product deleted successfully.');
			}
		} catch (Exception $e) {
			$request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
		}
		return redirect()->route('product.index');
	}

	public function dataTable(Product $product)
	{
		$offset = 0;
		$limit = 10;
		$user = Auth::user();
		$userID = $user->id;

		$where = "where 1";
		$catId = $_GET['cat_id'];
		if ($catId > 0) {
			$where .= " and categeory_id=" . $catId;
		}
		$status = $_GET['status_id'];
		if ($status != '') {
			$where .= " and status = " . $status;
		}
		$serachStrText = stripslashes($_GET['search']['value']);
		$order = " ORDER BY `id` DESC";
		if (isset($_GET['start']) && $_GET['start'] != '') {
			$offset = $_GET['start'];
		}
		if (isset($_GET['length']) && $_GET['length'] != '') {
			$limit = $_GET['length'];
		}
		if ($serachStrText != '') {
			$alTextA = explode(' ', $serachStrText);
			foreach ($alTextA as $serachStr) {
				$serachStr = addslashes($serachStr);
				$whereArray = [];
				$whereArray[] = "name like '%" . $serachStr . "%'";
				$where .= " and (" . implode(' or ', $whereArray) . ")";
			}
		}
		$slectQuery = "SELECT * FROM products " . $where;
		$contractsCnt = DB::select($slectQuery);
		$total_records = count($contractsCnt);
		$products = DB::select($slectQuery . " order by id desc limit " . $offset . "," . $limit); //." limit ".$offset.",".$limit
		$AllData = [];
		$products = Product::hydrate($products);
		foreach ($products as $product) {
			$data = [];
			$image = $product->image;
			if (!empty($product->image) || $product->image != null) {
				$data[] = '<img height="50px" src=" ' . $image . ' ">';
			} else {
				$data[] = '<div class="red">No Image</div>';
			}
			$data[] = '<a href="javascript:void(0);" title="Product Detail" onclick="variantShow(\'' . $product->id . '\')">' . $product->name . '</a> ';
			//$data[]='<a href="'.$edit.'" class="btn me-2 dataActionBtn btnEdit"  title="Edit"><img src="'.$editimage.'" alt="" />'.$product->name.'</a>';
			if (!empty($product->brandName)) {
				$data[] = $product->brandName->name;
			} else {
				$data[] = '-';
			}
			if (!empty($product->catName)) {
				$data[] = $product->catName->name;
			} else {
				$data[] = '-';
			}
			// $variant=$product->variant;
			// if(count($variant)>0)
			// {
			// $vri='';
			// foreach($variant as $vr)
			// {
			// $vri.='<tr>
			// <td>'.$vr->size.'</td>
			// <td>'.$vr->regular_price.'</td>
			// <td>'.$vr->sale_price.'</td>
			// <td>'.$vr->visible.'</td>
			// </tr>';


			// }
			// $data[]='
			// <div class="table-responsive">
			// <table id="listtable2">
			// <thead>
			// <tr>
			// <th>Size</th>	
			// <th>Regular Price</th>
			// <th>Sale Price</th>
			// <th>Visible</th>  
			// </tr>
			// </thead>
			// <tbody>'.$vri.'
			// </tbody>
			// </table>
			// </div>';
			// }else{
			// $data[]="";
			// }
			$data[] = '<a href="javascript:void(0);" title="Product Detail" onclick="variantShow(\'' . $product->id . '\')">Variants</a> ';
			$data[] = $product->gst_percent;
			$data[] = $product->status == 1 ? "Active" : "InActive";
			$delete = route('product.destroy', $product->id);
			$edit = route('product.edit', $product->id);
			$editimage = asset('img/datatable-edit-plus.svg');
			$deleteImage = asset('img/datatable-delete.svg');
			$btn = '<a href="' . $edit . '" class="btn me-2 dataActionBtn btnEdit"  title="Edit"><img src="' . $editimage . '" alt="" />
				</a> <a href="javascript:void(0);" class="btn me-2 dataActionBtn btnDelete" title="Delete" onclick="deleteProduct(this)" data-href="' . $delete . '"><img src="' . $deleteImage . '" alt="" /></a>';
			$data[] = '<div class="d-flex justify-content-start">' . $btn . '</div>';
			$AllData[] = $data;
		}
		$draw = (isset($_GET['draw']) ? $_GET['draw'] : 0);
		return json_encode(array('variantDetailsdraw' => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $total_records, "data" => $AllData));
	}
	public function exportProduct(Request $request)
	{
		$template = $request->vendor_id ? $request->vendor_id : '';
		$vendorName = Vendor::where('user_id', $request->vendor_id)->first();
		$name = Common::slugify($vendorName->vendor_name);
		return Excel::download(new ProductsExport($template, $request->vendor_id), $name . '.csv');
	}
	public function importProduct(Request $request)
	{
		$vendorId = $request->vendor;
		$user = Auth::user();
		$userId = $user->id;
		if ($user->role == 1) {
			$userId = $vendorId;
		}
		if ($request->hasfile('import_file')) {
			$file = $request->import_file;
			$imageName = time() . '_file_' . $file->getClientOriginalName();
			$file->move(public_path('temp'), $imageName);
			$filePath = public_path('temp') . '/' . $imageName;
			$file = fopen($filePath, "r");
			$dataErros = [];
			$error_found = false;
			$i = 0;

			while (!feof($file)) {
				$pid = "";
				$row = fgetcsv($file);
				if ($i > 0 && is_array($row) &&  ($row[0] != "" || $row[1] != "" || $row[2] != "" || $row[3] != "" || $row[4] != "" || $row[5] || $row[6] || $row[7] || $row[8] != "" || $row[9] || $row[10] != "" || $row[11] || $row[12] || $row[13] || $row[14] || $row[15] || $row[16] || $row[17] || $row[18] != "" || $row[19] || $row[20] || $row[21] || $row[22] || $row[23] || $row[24] || $row[25] || $row[26] || $row[27] || $row[28] || $row[29] || $row[30] || $row[31] || $row[32] || $row[33] || $row[34] || $row[35] || $row[36] || $row[37] || $row[38] || $row[39] || $row[40] || $row[41] || $row[42] || $row[43] || $row[43] || $row[44] || $row[45])) {

					$brandName = $row[0]; 		//brand_id
					$name = $row[1];      			//product name
					$sku = $row[2];				//sku product
					$regular_price = (float)$row[3]; 		//mrp (regular price)
					$catName = $row[4];			// catgory id
					$sale_price = (float)$row[5];			// sale price
					$SaleStartDate = $row[6];		//sale start date
					$SaleEndDate = $row[7];		//sale end date
					$gst = (float) $row[8];				//gst
					$Cess = (float)$row[9];				//cess
					$HSNcode = $row[10];			//hsn code
					$ShippingCharge = (float)$row[11];			//ShippingCharge
					$stock_quantity = $row[12];	//stock quntity
					$Ingredients = $row[13];		//ingredients 
					$HowToUse = $row[14];			//how to use product 
					$Inclusions = $row[15];   		//inclusions
					$ShippingTimeline = $row[16];	//shipping time line
					$WeightQuantity = (float)$row[17];	//weight/quantity
					$ShelfLife = $row[18];			//shelf life
					$CareInstructions = $row[19];	//care instructions
					$MerchantSKUcode = $row[20];   //merchant sku code
					$RoastType = $row[21];         //roast Type
					$Method = $row[22]; 			//method
					$Description = $row[23];		//description 
					$ShortDescription = $row[24];	//short description
					$CountryOfOrigin = $row[25];	//country of origin
					$ServingType = $row[26];		//serving type
					$WeightKg = (float)$row[27];			// shipping weight kg
					$Length = (float)$row[28];			//product lenth
					$Width = (float)$row[29];				// width
					$Height = (float) $row[30];			//height
					$FirstImage = (float) $row[31];		//first image
					$SecondImage = $row[32];       //second image
					$ThirdImage = $row[33];		//third image
					$FourthImage = $row[34];		//fourth image
					$FifthImage = $row[35];        //fifth image  
					$parentChaild = $row[36];
					$vSKU = $row[37];     			 //variant sku
					$variant_Price = (float)$row[38];		//variant price
					$v_value = $row[39];		//variant price
					$variant_Size = (float)$row[40];		//v size
					$variant_Material = $row[41];	     //v material
					$v_weight = (float)$row[42];			// v weight
					$v_length = (float) $row[43];			//v length
					$v_width = (float)$row[44];			//v width
					$v_height = (float) $row[45];        //v height

					$url = Common::slugify($name . '-' . $sku);
					$CatName = array_map('trim', explode(',', $catName));
					$mantCat = Category::whereIn('name', $CatName)->get();
					$productSku = Product::where('sku', $sku)->where('user_id', $userId)->first();
					$catId = [];
					if (count($mantCat) > 0) {
						foreach ($mantCat as $key => $cat) {
							$catId[$key] = $cat->id;
						}
					}
					if ($brandName != "") {
						$brandId = Brand::where('name', $brandName)->first();
					}
					$imagesProduct = "";
					if ($FirstImage != "") {

						try {
							$path = $FirstImage;
							$imagesProduct = basename($path);
							Image::make($path)->save(public_path('uploads/productimg/' . $imagesProduct));
						} catch (\Exception $e) {
						}
					}
					if ($name == "") {
						$dataErros[] = 'Row ' . $i . "-> Invalid Name";
					} elseif ($sku == "") {
						$dataErros[] = 'Row ' . $i . "-> Invalid SKU";
					} elseif ($gst == "") {
						$dataErros[] = 'Row ' . $i . "-> Invalid GST";
					} elseif ($stock_quantity == "") {
						$dataErros[] = 'Row ' . $i . "-> Invalid Stock Quantity";
					} elseif ($brandId == "") {
						$dataErros[] = 'Row ' . $i . "-> Invalid Brand Name";
					} elseif ($catId == "") {
						$dataErros[] = 'Row ' . $i . "-> Invalid Category Name";
					} else {
						if (empty($productSku)) {
							$product = new Product;
							if ($user->role == 1) {
								$product->user_id = $vendorId;
							} else {
								$product->user_id = $user->id;
							}

							$product->name = $name;
							$product->image = $imagesProduct;
							$product->sku = $sku;
							$product->regular_price = $regular_price;
							$product->sale_price = $sale_price;
							$product->gst = $gst;
							$product->cess = $Cess;
							$product->stock_quantity = $stock_quantity;
							//$product->category_id = $catId->id;
							$product->brand_id = $brandId->id;
							$product->description = $Description;
							$product->hsn = $HSNcode;
							$product->inclusion = $Inclusions;
							$product->quantity_size = $WeightQuantity;
							$product->short_description = $ShortDescription;
							$product->expiredate = $ShelfLife;
							$product->end_date = $SaleEndDate;
							$product->start_date = $SaleStartDate;
							$product->ingredients = $Ingredients;
							$product->care_instructions = $CareInstructions;
							$product->roast_type = $RoastType;
							$product->method = $Method;
							$product->country_origin = $CountryOfOrigin;
							$product->serving_type = $ServingType;
							$product->shipping_box_weight = $WeightKg;
							$product->length = $Length;
							$product->width = $Width;
							$product->height = $Height;
							$product->how_to_use = $HowToUse;
							$product->slug = $url;
							$product->status = 1;
							if ($vSKU != "") {
								$product->product_type = "variable";
							} else {
								$product->product_type = "simple";
							}
							$product->shipping_timelie = $ShippingTimeline;
							$product->shipping_charges = $ShippingCharge;

							if ($vSKU != "") {
								$modelVariant = new Variant;
								$modelVariant->variant_size = $variant_Size;
								$modelVariant->variant_price = $variant_Price;
								$modelVariant->variant_value = $v_value;
								$modelVariant->variant_material = $variant_Material;
								$modelVariant->v_weight = $v_weight;
								$modelVariant->v_length = $v_length;
								$modelVariant->v_width = $v_width;
								$modelVariant->v_height = $v_height;
								$modelVariant->v_sku = $vSKU;
								if ($user->role == 1) {
									$modelVariant->vender_user_id = $vendorId;
								} else {
									$modelVariant->vender_user_id = $user->id;
								}
							}

							DB::beginTransaction();

							try {
								$product->save();

								if ($vSKU != "") {
									$modelVariant->product_id = $product->id;
									$modelVariant->save();
								}

								DB::commit();
								$pid = $product->id;
							} catch (\Exception $e) {
								$pid = 0;
								DB::rollback();
							}

							if ($pid > 0) {
								$modelProduct = new ProductImages;
								$ImageName =  $SecondImage;
								if ($ImageName != "") {
									$modelProduct->productImage($ImageName, $vendorId, $pid);
								}
								$ImageName = $ThirdImage;
								if ($ImageName != "") {
									$modelProduct->productImage($ImageName, $vendorId, $pid);
								}
								$ImageName =  $FourthImage;
								if ($ImageName != "") {
									$modelProduct->productImage($ImageName, $vendorId, $pid);
								}
								$ImageName =  $FifthImage;
								if ($ImageName != "") {
									$modelProduct->productImage($ImageName, $vendorId, $pid);
								}
								$total = count($catId);
								$ids = [];
								$ids[] = 0;
								for ($i = 0; $i < $total; $i++) {
									$CatMultiple = MultipleCategory::updateOrCreate(['product_id' =>  $product->id, 'category_id' => $catId[$i]], ['temp' => 99]);
									$CatMultiple->save();
									$ids[] = $catId[$i];
								}
								$delete = MultipleCategory::where('product_id', $product->id)->whereIn('category_id', $ids)->delete();
							}
						}
					}
				}
				$i++;
			}
			fclose($file);
			if (count($dataErros) == 0) {
				$request->session()->flash('success', 'Entry imported successfully.');
			} else {
				$request->session()->flash('error', implode('\n', $dataErros));
			}
			unlink($filePath);
		} else {
			$request->session()->flash('error', 'Please provide csv file');
		}
		return redirect()->back();
	}
	public function varianDataTable()
	{
		$offset = 0;
		$limit = 10;
		$where = "";
		$order = " ORDER BY `id` DESC";
		if (isset($_GET['product_id']) && $_GET['product_id'] != '') {
			$where .= " where product_id = " . $_GET['product_id'];
		}
		if (isset($_GET['start']) && $_GET['start'] != '') {
			$offset = $_GET['start'];
		}
		if (isset($_GET['length']) && $_GET['length'] != '') {
			$limit = $_GET['length'];
		}

		$slectQuery = "SELECT * FROM variants " . $where;
		$contractsCnt = DB::select($slectQuery);
		$total_records = count($contractsCnt);
		$variants = DB::select($slectQuery . " order by id desc limit " . $offset . "," . $limit); //." limit ".$offset.",".$limit
		$AllData = [];
		$variants = Variant::hydrate($variants);
		foreach ($variants as $variant) {
			$data = [];
			$data[] = $variant->size;
			$data[] = $variant->regular_price;
			$data[] = $variant->sale_price;
			$data[] = $variant->visible;

			$AllData[] = $data;
		}
		$draw = (isset($_GET['draw']) ? $_GET['draw'] : 0);
		return json_encode(array('variantDetailsdraw' => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $total_records, "data" => $AllData));
	}
	public function variantData(Request $request)
	{
		$product = Product::find($request->product_id);
		if (!empty($product)) {
			$variant = Variant::where('product_id', $product->id)->get();
			if (count($variant) > 0) {
				return json_decode($variant);
			}
		}
		return json_decode(array(''));
	}
}
