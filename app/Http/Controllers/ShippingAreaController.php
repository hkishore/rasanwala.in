<?php

namespace App\Http\Controllers;

use App\Models\ShippingArea;
use Illuminate\Http\Request;
use App\Models\Common;

class ShippingAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$models=ShippingArea::orderBy('id','desc')->get();
       return view('admin.area.index')->with(['model'=>$models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.area.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $models = new ShippingArea;
        $validator = $models->validator($request);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();       
       
		  $models->area =$request->area;
		  $models->charge =$request->charge;
		  $models->status =$request->status;
		  $models->start_time =$request->start_time;
		  $models->end_time =$request->end_time;
		  
        if ($models->save()) {
            $request->session()->flash('success', 'Area added successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('area.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShippingArea  $shippingArea
     * @return \Illuminate\Http\Response
     */
    public function show(ShippingArea $shippingArea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShippingArea  $shippingArea
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$shippingArea=ShippingArea::find($id);
        return view('admin.area.edit')->with(['model'=>$shippingArea]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShippingArea  $shippingArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$shippingArea=ShippingArea::find($id);
         $validator = $shippingArea->validator($request,$shippingArea->id);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();       
          $data=$request->except('_token');
		  $shippingArea->area =$request->area;
		  $shippingArea->charge =$request->charge;
		  $shippingArea->status =$request->status;
		   $shippingArea->start_time =$request->start_time;
		  $shippingArea->end_time =$request->end_time;
		  
        if ($shippingArea->save()) {
            $request->session()->flash('success', 'Area updated successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('area.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShippingArea  $shippingArea
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
		$shippingArea=ShippingArea::find($id);
        try 
        {
           if ($shippingArea->delete()) {
                $request->session()->flash('success', 'Area deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('area.index');
    }
}
