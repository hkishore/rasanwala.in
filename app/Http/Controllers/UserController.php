<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Address;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Auth,DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
		
        return view("admin.users.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view("admin.users.create");
    }
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	
        $validator = $models->validator($request);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput(); 
        $models->name=$request->name;
        $models->email=$request->email;
        $models->password=Hash::make($request->password);
        $models->role=$request->role;
        $models->status=$request->status;
		if($models->save()){
		 
		 $request->session()->flash('success', 'user added successfully.');
		}else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        
		return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
		
        return view("admin.users.edit")->with(['model'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = $user->validator($request, $user->id);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
           $data=$request->except('_token');
		   $data['name']=$request->name;
           $data['email']=$request->email;
           if(isset($request->password) && $request->password!="")            	
			{
				$data['password']=Hash::make($request->password);
			}
            else
			{
				unset($data['password']);
			}
			 if(isset($request->cod) && $request->cod!="")            	
			{
				$data['cod']=$request->cod;
			}else{
				$data['cod']="";
			}
			if(isset($request->freeshipping) && $request->freeshipping!="")            	
			{
				$data['freeshipping']=$request->freeshipping;
			}else{
				$data['freeshipping']="";
			}
			$data['role']=$request->role;
           $data['status']=$request->status;
        if ($user->fill($data)->push()) {
           $request->session()->flash('success', 'user updated successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user,Request $request)
    {
       try 
        {
            if ($user->delete()) {
                $request->session()->flash('success', 'User deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('user.index');
    }
	
	public function dataTable (Request $request ,User $user)
	{
		   $userId=Auth::user()->id;
		   $offset=0;
	       $limit=10;
			$key="id";
			$direction="desc";
			$colum=[0=>'name', 1=>'email'];
			if($request->draw > 0 && isset($request->order))
			{
				$key = $colum[$request->order[0]['column']];
				$direction = $request->order[0]['dir'];
			}
		   $where="where role != 1";
           $serachStrText=stripslashes($_GET['search']['value']);
	       if(isset($_GET['start']) && $_GET['start']!='')
		   {
			   $offset=$_GET['start'];
		   }
		   if(isset($_GET['length']) && $_GET['length']!='')
		   {
			   $limit=$_GET['length'];
		   }
            if($serachStrText!='')
			{
			  $alTextA=explode(' ',$serachStrText);
			  foreach($alTextA as $serachStr)
			   {				
				$serachStr=addslashes($serachStr);
				$whereArray=[];                   
				$whereArray[]="name like '%".$serachStr."%'";		
				$whereArray[]="email like '%".$serachStr."%'";		
				$where.=" and (".implode(' or ',$whereArray).")";
			   }
            }						
			$slectQuery="SELECT * FROM users ".$where;
		     $contractsCnt=DB::select($slectQuery);
			 $total_records=count($contractsCnt);
			 
			 $users=DB::select($slectQuery." order by id desc limit ".$offset.",".$limit);
			 $AllData=[];
			 $users=User::hydrate($users);
			$users=$users->sortBy([[$key,$direction]]);
			 foreach($users as $user)
			 {
				
				$data=[];
                $data[]=$user->name;
				$data[]=$user->email;
				$data[]=$user->cod  == 'on'?"Enable":"Disable";
				$data[]=$user->freeshipping  == 'on'?"Enable":"Disable";
				$data[]=$user->role  == 3?"Customer":"Store keeper";
				$data[]=$user->status == 1?"Active":"InActive";
                $delete=route('user.destroy',$user->id);
                $edit=route('user.edit',$user->id);
				$editimage=asset('img/datatable-edit-plus.svg');
				$deleteImage=asset('img/datatable-delete.svg');
				
                $btn='<a href="'.$edit.'" class="btn me-2 dataActionBtn btnEdit"  title="Edit"><img src="'.$editimage.'" alt="" />
				</a>';
				if($user->id != $userId){
				$btn.='<a href="javascript:void(0);" class="btn me-2 dataActionBtn btnDelete" title="Delete" onclick="deleteUser(this)" data-href="'.$delete.'"><img src="'.$deleteImage.'" alt="" /></a>';
				}
	
                $data[]='<div class="d-flex justify-content-start">'.$btn.'</div>';	
				
				$AllData[]=$data;
			}
			 $draw=(isset($_GET['draw'])?$_GET['draw']:0);
		return json_encode(array('draw'=>$draw,"recordsTotal"=>$total_records,"recordsFiltered"=> $total_records,"data"=>$AllData));
	
	}
	 public function MyProfile()
    {  $user=Auth::user();
		$models=Address::where('user_id',$user->id)->get();
        return view("admin.myprofile.index")->with(['model'=>$models]);
    }
	public function userName($id)
	{
		$proName= User::find($id);
		if(empty($proName))
		{
			return json_encode(array());
		}
		return json_encode(array('name'=>$proName->name));
	}
    public function specificUser(Request $request){
		$searchText = $request->term['term'];
		$users=DB::select("select id,name as text from users where status=1 and role > 2 and phone LIKE '%".$searchText ."%'");
		return json_encode(['results'=>$users]);
	}
}
