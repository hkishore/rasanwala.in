<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Schema;
use Validator;
use App\Models\Category;
use App\Models\Product;
use App\Models\Variant;
use App\Models\Order;
use App\Models\OrderLine;
use App\Models\User;
use App\Models\Brand;
use App\Models\PaymentGateway;
use App\Models\FavoriteItem;
use App\Models\Address;
use App\Models\Cashfree;
use App\Models\Common;
use App\Models\Setting;
use App\Models\Faq;
use App\Models\GiftCard;
use App\Models\Query;
use App\Models\Payment;
use App\Models\Page;
use App\Models\Question;
use App\Models\EmailSubscrib;
use App\Models\ShippingArea;
use App\Exports\ProductExport;
use App\Mail\SendMail;
use Excel, Auth, DB;
use App\Rules\Captcha;
use App\Exports\EmailSubscribExport;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\WelcomeEmail;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{

	public function test(Request $request)
	{
		$getVisible = Common::getCashFreeyApi();
		//dd($getVisible);
	}

	public function mainDashboard()
	{

		$user = Auth::user();
		if ($user->role != 1) {
			return Redirect::to('/');
		}
		return view('admin.admin-dashboard');
	}
	public function cart(Request $request)
	{
		$order = Order::getUserOrders(false);
		if (!empty($order)) {
			$oderLines = $order->getOrderLines;
			$orderLineCount = count($oderLines);
			if ($orderLineCount > 0) {
				foreach ($oderLines as $ol) {
					$variant = $ol->variant;
					if (empty($variant)) {
						$ol->Delete();
					}
				}
			}
			$order->reCalculateFee();
		}
		return view('front.cart')->with(['order' => $order]);
	}

	public function checkout(Request $request)
	{


		$order = Order::getUserOrders(true);
		if (empty($order) || count($order->getOrderLines) == 0) {
			return redirect()->route('shop');
		}

		return view('front.checkout')->with(['order' => $order, 'orderlines' => $order->getOrderLines]);
	}
	public function shop(Request $request)
	{
		$users = Auth::user();
		$produtId = Common::dataVisibleGet();
		$productObj = Product::where('status', '=', 1);

		if (isset($request->brandcheck) && $request->brandcheck != "") {
			$productObj->whereIn('brand_id', $request->brandcheck);
		} else if (isset($request->searchText) && $request->searchText != "") {
			$name = $request->searchText;
			$productObj->where('name', 'LIKE', '%' . $name . '%');
		}
		$data = $this->getData($produtId, $productObj);
		return view('front.offer-product')->with(['products' => $data['products'], 'category' => $data['category'], 'brands' => $data['brands'], 'sbcat' => $data['sbcat']]);
	}
	public function offer(Request $request)
	{
		$getVisible = Common::varintVisibleGet();
		$visible = implode(', ', $getVisible);
		$productId = DB::select('select `product_id` from `variants` where visible in (' . $visible . ') and `sale_price` < `regular_price` group by `product_id`');
		$data = [];
		foreach ($productId as $key => $dt) {
			$data[] = $dt->product_id;
		}
		$products = Product::whereIn('id', $data)->where('status', 1);
		$data = $this->getData($data, $products);
		return view('front.offer-product')->with(['products' => $data['products'], 'category' => $data['category'], 'brands' => $data['brands'], 'sbcat' => $data['sbcat']]);
	}
	public function getData($data, $products)
	{
		$productObj = Product::where('status', 1)->whereIn('id', $data);
		$getCat = $productObj->pluck('categeory_id')->toArray();
		$catObj = Category::whereIn('id', $getCat)->where('status', 1);
		$catId = $catObj->pluck('id')->toArray();
		$catsub = $catObj->pluck('parent_id')->toArray();
		$getbrand = $productObj->pluck('brand_id')->toArray();
		$product = $products->whereIn('categeory_id', $catId)->paginate(20);
		if (count($product) > 0) {
			$Cat = Category::whereIn('id', $catsub)->get();
			$sbcat = Category::whereIn('id', $getCat)->get();
			$brand = Brand::whereIn('id', $getbrand)->get();
		} else {
			$getCat = Product::where('status', 1)->pluck('categeory_id')->toArray();
			$getbrand = Product::where('status', 1)->pluck('brand_id')->toArray();
			$CatSub = Category::whereIn('id', $getCat)->pluck('parent_id')->toArray();
			$Cat = Category::whereIn('id', $CatSub)->where('status', 1)->get();
			$sbcat = Category::whereIn('id', $getCat)->where('status', 1)->get();
			$brand = Brand::whereIn('id', $getbrand)->get();
		}

		return (['products' => $product, 'category' => $Cat, 'brands' => $brand, 'sbcat' => $sbcat]);
	}
	public function addToCart(Request $request)
	{
		$pid = $request->p_id;
		$qty = $request->qty;
		$variantId = $request->variant_id;
		$product = Product::find($pid);
		$order = Order::getUserOrders(true);
		$orderL=Variant::where('id',$variantId)->first();
		if(empty($orderL)){
			return ['status' => false,'msg'=>'InValid Varriant'];
		}
		$orderLimit=$orderL->order_limit;
		if($orderLimit<1){
			$orderLimit = config('custom.maxQuantity');
		}
		$orderLine = OrderLine::where('order_id', $order->id)->where('variant_id', '=', $variantId)->where('product_id', $pid)->first();
		if (empty($orderLine)) {

			$orderLine = new OrderLine;
			$orderLine->product_id = $product->id;
			$orderLine->order_id = $order->id;
			$orderLine->variant_id = $variantId;
						//$orderLine->calculateGst();
			//$orderLine->save();

		}

		if (isset($request->type) && $request->type == 'update') {
			$orderLine->qty = $qty;
		} else {
			$orderLine->qty = $orderLine->qty + $qty;
		}

		if ($orderLine->qty < 1) {
			$orderLine->qty = 1;
		}
		if($orderLine->qty>$orderLimit){
			return ['status' => false,'msg'=>'Maximum Order Quantity '.$orderLimit];
		}

		$orderLine->shiping_charge = $orderLine->qty * $orderL->shiping_charge;
		$orderLine->calculateGst($variantId);
		$gstamount = Common::calculategstamonut($variantId, $product);
		$orderLine->gst_amount = $gstamount;
		$orderLine->gst_percent = $product->gst_percent;
		$orderLine->save();

		$order->reCalculateFee();
		$count_cart = 0;
		if (count($order->getOrderLines) > 0) {
			$count_cart = $order->getOrderLines->count();
		}
		$response = new \Illuminate\Http\Response(['status' => true, 'amount' => $order->amount, 'total_amount' => $order->total_amount, 'count_cart' => $count_cart]);
		$response->withCookie(cookie('device_id', $order->device_id, 525949));

		return $response;
	}
	public function createOrder(Request $request)
	{
		$user = Auth::user();
		$WebMaintenance = Setting::getFieldVal('web_maintenance');
		if($WebMaintenance==1){
			return redirect()->back()->withError('We are not accepting order currently');
		}
		//dd($request->all());
		if ($request->delivery != "selfpickup" && $request->payment != "rozorpay" &&  $user->cod !="on") {
			$request->session()->flash('error', 'Please check payment method');
			return redirect()->back()->withInput();
		}
		$order = Order::getUserOrders(true);
		//if ($request->payment == "cod") {

			if (empty($order) || count($order->getOrderLines) == 0) {
				return redirect()->route('shop');
			}
			if ($order->status > 0) {
				$request->session()->flash('error', 'Cart is empty');
				return redirect()->route('cart');
			}
			$models = new Order;
			if ($request->delivery != "selfpickup") {
				$validator = $models->validator($request);
				if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
			}
			//$order->payment_status=1;
	//	}
		// $orderLine=OrderLine::where('order_id',$order->id)->where('variant_id','=',$variantId)->where('product_id',$pid)->first();
		// if(empty($orderLine))
		// {	

		// $orderLine=new OrderLine;
		// $orderLine->product_id=$product->id;
		// $orderLine->order_id=$order->id;
		// $orderLine->variant_id= $variantId;

		// }
		$user_id = 0;
		$address_id = $request->address_id;
		// $user = Auth::user();
		if (!Auth::check()) {
			$user = User::where('phone', $request->phone_no)->first();
			if (empty($user)) {
				$user = new User;
				$email = $request->email_id;
				//$password = $request->phone_no;
				$user->email = $request->phone_no.'@dummy.com';
				$user->name = $request->first_name;
				$user->role = 3;
				$user->status = 1;
				$user->password = Hash::make($request->phone_no);
				$user->phone = $request->phone_no;
				//$email_data = (['email' => $email, 'password' => $password]);
				//Mail::to($request->email_id)->send(new WelcomeEmail($email_data));
				$user->save();
			}
		}
		try {
			$user_id = $user->id;
		} catch (\Exception $e) {
		}

		if ($user_id < 1) {
			$request->session()->flash('error', 'Unable to create order, Please contact to support team.');
			return redirect()->back()->withInput();
		}


		if (!Auth::check() || (count($user->userAddress) == 0 && $request->delivery != "selfpickup")) {
			$address = Address::createNewAddress($request, $user_id);
			$address_id = $address->id;
		}

		/*$getshippingCharge = Setting::getFieldVal('sp_charge');
		if (Auth::check()) {
			if ($request->address_id != "") {
				$addressFind = Address::find($request->address_id);
				if ($addressFind->area_id != 0) {
					$id = $addressFind->area_id;
					$order->area_id = $addressFind->area_id;
					if ($request->delivery != "selfpickup") {
						$loction = ShippingArea::find($id);
						$getshippingCharge = $loction->charge;
					} else {
						$order->area_id = 0;
					}
				} else {

					$order->area_id = 0;
				}
			} else {
				$order->area_id = $request->add_id;
			}
		} else {
			if ($request->add_id != 'other') {
				$order->area_id = $request->add_id;

				if ($request->delivery != "selfpickup") {
					$id = (int)$request->add_id;
					$loction = ShippingArea::find($id);
					$getshippingCharge = $loction->charge;
				} else {
					$order->area_id = 0;
				}
			} else {
				$order->area_id = 0;
			}
		}
		if ($request->delivery == "selfpickup" || $user->freeshipping=='on') {
			$getshippingCharge = 0;
		}*/
		$order->user_id = $user_id;
		$order->address_id = $address_id;
		$order->notes = $request->notes;
		$order->payment_type = $request->payment;
		// $order->delivery_day = $request->dilevryday;
		$order->order_date = date('Y-m-d');
		$order->delivery_type = $request->delivery;
		// $order->shipping_charge_id = $request->shipping_charge_id;
		//$order->shipping_charge = $getshippingCharge;
		//$order->save();
		if ($request->payment == "rozorpay") {
			//$order->shipping_charge = $getshippingCharge;
			$order->payment_status = 1;
			//$order->save();
		}
		if ($request->payment == "cod") {

			$order->payment_status = 0;
			//$order->save();
		}
		$order->save();
		$is_payment = false;
		if ($request->payment == "rozorpay") {
			//$order =Order::where('payment_order_id',$request->razorpay_order_id)->first();
			$txnid = $request->razorpay_payment_id;
			$razorpay_order_id = $request->razorpay_order_id;
			$razorpay_signature = $request->razorpay_signature;
			$msg = "order=>" . $order->id . "\n razorpay_order_id=" . $request->razorpay_order_id . "\n razorpay_payment_id=" . $txnid;

			if ($order->status > 0 || $order->payment_order_id != $request->razorpay_order_id) {
				$request->session()->flash('error', 'Invalid Order');
				$msg = $msg . "\n Order does not match";
				Common::teligramErrorBot($msg);
				return redirect()->back()->withInput();
			}

			$payment = Payment::where('txn_id', $txnid)->first();
			if (!empty($payment)) {
				$request->session()->flash('error', 'This payment already processed');
				$msg = $msg . "\n Payment already Process for " . $payment->order_id . " \n  Requested Order for= " . $order->id;
				Common::teligramErrorBot($msg);
				return redirect()->back()->withInput();
			}
			$id = $user->id;
			$email = $user->email;

			$amount = $order->total_amount;
			$currency = $request->currency;
			$order_id = $order->id;
			$paymentG = new PaymentGateway;
			$orders = $paymentG->verifyPayment($request);
			$pMode = "razorpay";
			$statusP = "Completed";
			Log::info('user id =' . $user->id . ' ' . 'rozorpay payment id =' . $txnid . ' ' . 'amount =' . $amount . ' ' . 'razorpay payment completed');

			if ($orders == null) {
				$payment = $this->_savePayment($txnid, $order_id, $id, $amount, $email, $statusP, $pMode, $request->all());
				$is_payment = true;
			} else {
				$request->session()->flash('error', 'This payment is not verified');
				$msg = $msg . "\n Payment not verified for " . $payment->order_id . " \n  Requested Order for= " . $order->id;
				Common::teligramErrorBot($msg);
				return redirect()->back()->withInput();
			}
		}
		if (($request->payment == "cod") || ($request->payment == "rozorpay" && $is_payment == true)) {
			DB::table('order_lines')->where('order_id', $order->id)->update(['status' => 1]);
			DB::table('orders')->where('id', $order->id)->update(['status' => 1]);
			//$order->total_amount = $order->total_amount + $getshippingCharge;
			//$order->save();
			$request->session()->flash('success', 'Order added successfully.');
			Common::sendemail($order, $user);
			//\Mail::to($user->email)->send(new \App\Mail\OrderCreated($order,'Your item is ordered seccessfully',' ( Order Number #'.$order->id.' )'));
		}
		/*else if($request->payment == "cashfree")
		{
			 $cashfree=new Cashfree;
			 $paymentLink=$cashfree->cteatePaymentLink($user,$order,$getshippingCharge);
			if($paymentLink==1)
			{
				$request->session()->flash('error', 'User phone number is null.');
		        return redirect()->back()->withInput();
			} else if($paymentLink!="")
			 {
				
				 return Redirect::to($paymentLink);
			 }
			 else
			 {
				 $order->status=0;
				 $order->save();
				 $request->session()->flash('error', 'Unable to create payment url, Please contact to support team.');
		        return redirect()->back()->withInput();
			 }			 
		}*/ else {
			//$order->status=0;
			// $order->save();
			$request->session()->flash('error', 'Unable to create payment url, Please contact to support team.');
		}
		return redirect()->route('cart');
	}
	public function returnCashFree(Request $request)
	{
		$uniq_id = $request->order_id;
		$orderidArray = explode("_", $uniq_id);
		$order_id = $orderidArray[0];
		//dd($uniq_id);
		$order = Order::find($order_id);
		$cashfree = new Cashfree;
		$verifyPayment = $cashfree->verifyPayment($uniq_id);
		$userName = $verifyPayment->customer_details->customer_name;
		$email = Setting::getFieldVal('email');
		$dataEmail = array(
			'name'      =>  $userName,
			'subject'      =>  "",
			'message'   =>  "order id = " . $order_id,
		);
		if (empty($order)) {
			$dataEmail['subject'] = "Invalid Order";
			\Mail::to($email)->send(new SendMail($dataEmail));
		} else if ($order->status > 0) {
			$dataEmail['subject'] = " Order already processed";
			\Mail::to($email)->send(new SendMail($dataEmail));
		} else if ($order->user_id != $verifyPayment->customer_details->customer_id) {
			$dataEmail['subject'] = " The Order user does not match";
			\Mail::to($email)->send(new SendMail($dataEmail));
		}
		$user_id = $order->user_id;
		$user = User::find($user_id);
		if (!empty($order) && $verifyPayment->order_status == "PAID") {
			$txnid = $verifyPayment->cf_order_id;
			$id = $verifyPayment->customer_details->customer_id;
			$amount = $verifyPayment->order_amount;
			$email = $verifyPayment->customer_details->customer_email;
			$statusP = $verifyPayment->order_status;
			$pMode = "cashfree";
			$data = $verifyPayment;
			$order->status = 1;
			$order->payment_status = 0;
			$order->total_amount = $order->total_amount + $order->shipping_charge;
			$order->order_date = date("Y-m-d h-m-s");
			if ($order->save()) {
				DB::table('order_lines')->where('order_id', $order->id)->update(['status' => 1]);
			}
			$this->_savePayment($txnid, $order_id, $id, $amount, $email, $statusP, $pMode, $data, $uniq_id);
			\Mail::to($user->email)->send(new \App\Mail\OrderCreated($order, 'Your item is ordered seccessfully', ' ( Order Number #' . $order->id . ' )'));
			$request->session()->flash('success', 'Order added successfully.');
		} else {
			$order->address_id = null;
			$order->notes = null;
			$order->payment_type = null;
			$order->delivery_day = null;
			$order->order_date = null;
			$order->delivery_type = null;
			$order->shipping_charge_id = null;
			$order->shipping_charge = null;
			$order->status = 0;
			$order->save();
			$request->session()->flash('error', 'Unable to payment process, Please contact to support team.');
		}

		return redirect()->route('cart');
	}
	public function notifyPayment(Request $request)
	{
		$request = json_encode($request->all());
		Log::info($request);
		return $request;
	}
	public function addDelete(Request $request)
	{
		$id = $request->id;
		$Address = Address::find($id);
		if ($Address->delete()) {
			return json_encode(['status' => true]);
		}
		return json_encode(['status' => false,]);
	}
	public function getRazorpayGateway(Request $request)
	{
		$order = Order::find($request->order_id);

		if (empty($order) || count($order->getOrderLines) == 0) {
			return 1;
		}
		//if($order->status>0 || ($order->payment_order_id!="" && $order->payment_order_id!=NULL))
		if ($order->status > 0) {
			return 2;
		}
		$striepP = new PaymentGateway;
		$sParams = $striepP->createRazorpayRequest($order);
		return json_encode($sParams);
	}
	public function removeCart(Request $request)
	{
		$id = $request->id;
		$orderLine = OrderLine::find($id);
		$order = $orderLine->order;
		if ($orderLine->delete()) {
			$order->reCalculateFee();
			return json_encode(['status' => true]);
		}
		return json_encode(['status' => false,]);
	}
	public function sendEmail(Request $request)
	{
		$model = new Query;
		$validator = $model->validator($request);
		if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
		$model['name'] = $request->name;
		$model['email'] = $request->email;
		$model['subject'] = $request->subject;
		$model['message'] = $request->message;
		$model['status'] = 0;
		$model->save();
		$msg = "customer_name=>" . $request->name . "\n customer_email=" . $request->email . "\n subject=" . $request->subject . "\n message=" . $request->message;
		Common::teligramErrorBot($msg);
		return back()->with('success', 'Thanks for contacting us!');
	}
	public function queryDestroy(Request $request, $id)
	{
		$model = Query::find($id);
		try {
			if ($model->delete()) {
				$request->session()->flash('success', 'Query deleted successfully.');
			}
		} catch (Exception $e) {
			$request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
		}
		return redirect()->route('query.index');
	}
	public function updateStatus(Request $request, $id)
	{
		$model = Query::find($id);
		if ($model->status == 1) {
			return back()->with('error', 'This query already seen!');
		}
		if (!empty($model)) {
			Query::where('id', $model->id)->update(['status' => 1]);
			$request->session()->flash('success', 'Query update successfully.');
		} else {
			$request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
		}
		return redirect()->route('query.index');
	}
	public function queryIndex()
	{
		//return view('admin.quries-index');
		$models = Query::orderBy('id', 'desc')->get();
		return view('admin.quries-index')->with(['model' => $models]);
	}
	public function contactUser(Request $request, $url)
	{
		//dd($id);
		return view('front.contact')->with(['model' => $url]);
	}
	public function selfProfile(Request $request, $id)
	{
		$user = User::where('id', $id)->first();

		$data = $request->except('_token');
		if (isset($request->password) && $request->password != "") {
			$data['password'] = Hash::make($request->password);
		} else {
			unset($data['password']);
		}
		if ($user->fill($data)->push()) {
			$request->session()->flash('success', 'Profile updated successfully.');
		} else {
			$request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
		}
		return redirect()->route('myprofile.index');
	}
	public function categoryCat(Request $request, $url)
	{
		$prd = Category::where('slug', $url)->where('status', 1)->first();
		if (empty($prd)) {
			return redirect()->route('shop');
		}
		$productObj = Product::where('categeory_id', $prd->id)->where('status', '=', 1);
		$categeory = Category::where('parent_id', $prd->parent_id)->where('status', 1)->orderBy('position')->get();
		if (empty($prd->parent_id)) {
			$productObj = Category::where('parent_id', $prd->id)->where('status', 1)->orderBy('position');
		}

		$product1 = $productObj;
		if (isset($request->searchText) && $request->searchText != "") {
			$name = $request->searchText;
			$productObj->where('name', 'LIKE', '%' . $name . '%');
		}
		if (isset($request->checkBrand) && $request->checkBrand != "") {

			$productObj->whereIn('brand_id', $request->checkBrand);
		}
		$products = $productObj->paginate(20);
		$pb = Product::getBrand($prd->id);
		$brands = Brand::whereIn('id', $pb)->get();
		/*dd($product);
		$variant=Common::dataVisibleGet();
		$catgoryObj=Category::where('status',1)->where('parent_id',$prd->id);
		
		if($prd->parent_id != "" || $prd->parent_id !=null)
		{
			//$catProduct=$catgoryObj->where('id',$prd->id)->first();
			$productObj->where('categeory_id',$catProduct->id);
			
		}else{
			$sCp=$catgoryObj->where('parent_id',$prd->id)->pluck('id')->toArray();
			
			$productObj->whereIn('categeory_id',$sCp);
			
		}
		
		// else{
		  // $products= Product::whereIn('id',$variant);
		// }	
		
		//dd($productObj->get());
		//$productObj1=$productObj;
		
		//$cat=$productObj->groupBy('categeory_id')->pluck('categeory_id')->toArray();
		
		$catSub= Category::whereIn('id',$cat)->where('status',1)->get();
		*/
		return view('front.all-category')->with(['products' => $products, 'categeory' => $categeory, 'brands' => $brands, 'url' => $url]);
	}

	public function ProductDetail(Request $request, $slu)
	{
		$productfine = Product::where('slug', $slu)->first();
		return view('front.product-details')->with(['product' => $productfine]);
	}
	public function brandProducts(Request $request, $pid)
	{
		$prd = Brand::where('slug', $pid)->first();
		if (empty($prd)) {
			return redirect()->route('shop');
		}
		$product_id = Product::where('brand_id', $prd->id)->where('status', 1)->pluck('id')->toArray();
		$products = Product::whereIn('id', $product_id);
		$data = $this->getData($product_id, $products);
		return view('front.offer-product')->with(['products' => $data['products'], 'category' => $data['category'], 'brands' => $data['brands']]);
	}
	public function subscribe(Request $request)
	{
		$user = 0;
		$email = new EmailSubscrib;
		$existingEmail = EmailSubscrib::where('email_sub', $request->email)->count();
		if ($existingEmail == 0) {
			if (Auth::check()) {
				$user = Auth::user()->id;
				$email->user_id = $user;
			}
			$email->email_sub = $request->email;
			$email->save();
		} else {
			return false;
		}
		return true;
	}
	public function emailData(Request $request)
	{
		return view('admin.email-sub');
	}
	public function emaildatatable(Request $request)
	{
		//dd($request->all());
		$offset = 0;
		$limit = 10;
		$where = "where 1";

		$serachStrText = stripslashes($_GET['search']['value']);
		$order = " ORDER BY `id` DESC";
		if (isset($_GET['start']) && $_GET['start'] != '') {
			$offset = $_GET['start'];
		}
		if (isset($_GET['length']) && $_GET['length'] != '') {
			$limit = $_GET['length'];
		}
		if ($serachStrText != '') {
			$alTextA = explode(' ', $serachStrText);
			foreach ($alTextA as $serachStr) {
				$serachStr = addslashes($serachStr);
				$whereArray = [];
				$whereArray[] = "email_sub like '%" . $serachStr . "%'";
				$where .= " and (" . implode(' or ', $whereArray) . ")";
			}
		}
		$slectQuery = "SELECT * FROM email_subscribs " . $where . $order;
		$contractsCnt = DB::select($slectQuery);
		$total_records = count($contractsCnt);
		$emails = DB::select($slectQuery);
		$AllData = [];
		$emails = EmailSubscrib::hydrate($emails);

		foreach ($emails as $email) {
			$data = [];
			$data[] = $email->id;
			$data[] = $email->email_sub;
			$delete = route('email.destroy', $email->id);
			$deleteImage = asset('img/datatable-delete.svg');
			$btn = '</a> <a href="javascript:void(0);" class="btn me-2 dataActionBtn btnDelete" title="Delete" onclick="deleteEmail(this)" data-href="' . $delete . '"><img src="' . $deleteImage . '" alt="" /></a>';
			$data[] = '<div class="d-flex justify-content-start">' . $btn . '</div>';
			$AllData[] = $data;
		}
		$draw = (isset($_GET['draw']) ? $_GET['draw'] : 0);
		return json_encode(array('draw' => $draw, "recordsTotal" => $total_records, "recordsFiltered" => $total_records, "data" => $AllData));
	}
	public function emailDestroy(Request $request, $id)
	{
		$email = EmailSubscrib::where('id', $id)->first();
		try {
			if ($email != null) {
				$email->delete();
				$request->session()->flash('success', 'Email deleted successfully.');
			}
		} catch (Exception $e) {
			$request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
		}
		return redirect()->route('email_sub');
	}

	public function emailExport(Request $request)
	{
		$template = $request->t ? $request->t : '';
		return Excel::download(new EmailSubscribExport($template), 'email-export.csv');
	}
	public function staticPage($url)
	{
		$page = Page::where('url', '=', $url)->first();
		if (!empty($page)) {
			return view('front.page')->with(['model' => $page]);
		}
		abort(404);
	}
	public function faq(Request $request)
	{
		$faq = Faq::where('status', '=', '1')->get();
		if (!empty($faq)) {
			return view('faq')->with(['model' => $faq]);
		}
		abort(404);
	}
	public function inquiryVP(Request $request)
	{
		$save = DB::table('queries')->insert([
			'name' => $request->name,
			'email' => $request->email,
			'phone_no' => $request->phone,
			'product_id' => $request->product_id,
			'message' => $request->question,
		]);
		if ($save) {
			$inquiry = $request->question;
			$request->session()->flash('success', 'Your question send secessfully');
			\Mail::to(config('constant.mail'))->send(new \App\Mail\Inquiry($request));
			return true;
		} else {
			$request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
			return false;
		}
	}

	private function _savePayment($txnid, $order_id, $id, $amount, $email, $statusP, $pMode, $data)
	{
		$payment = Payment::where('txn_id', $txnid)->first();
		if (empty($payment)) {
			$payment = new Payment;
			$payment->order_id = $order_id;
			$payment->txn_id = $txnid;
			$payment->email = $email;
			$payment->user_id = $id;
			$payment->amount = $amount;
			$payment->data = json_encode($data);
			$payment->payment_status = $statusP;
			$payment->payment_mode = $pMode;
			$payment->payment_currency = "INR";
			$payment->save();
		}

		return $payment;
	}
	public function uplaodFile(Request $request)
	{
		$image = $request->file('file');
		$fileInfo = $image->getClientOriginalName();
		$mimeType = $image->getClientMimeType();
		$filename = pathinfo($fileInfo, PATHINFO_FILENAME);
		$extension = pathinfo($fileInfo, PATHINFO_EXTENSION);
		$random = substr(number_format(time() * rand(), 0, '', ''), 0, 10);
		$filename = Common::slugify($filename);
		$file_name = $filename . '-' . $random . '.' . $extension;
		$destinationPath = public_path('/uploads/temp');
		$typeArr = explode('/', $mimeType);
		// if($typeArr[0]=="image")
		// {

		// $img = Image::make($image->path());
		// $img->resize(500, 600, function ($constraint) {
		// $constraint->aspectRatio();
		// })->save($destinationPath.'/'.$file_name);


		// }
		// else
		// {
		$image->move(public_path('temp'), $file_name);
		//}
		return json_encode(['success' => $file_name]);
	}

	public function refreshCaptcha()
	{
		return response()->json(['captcha' => captcha_img()]);
	}
	public function viewItems(Request $request, $id)
	{
		$user = Auth::user();
		$order = Order::where('id', $id)->where('user_id', $user->id)->first();

		if (empty($order)) {
			return redirect()->back();
		}

		$orderLine = OrderLine::where('order_id', $order->id)->get();
		if (count($orderLine) == 0) {
			return redirect()->back();
		}
		return view('front.view-order-product')->with(['order' => $order, 'orderLine' => $orderLine]);
	}
	public function addFavorite(Request $request)
	{
		$user = Auth::user();
		$productId = $request->p_id;

		$favoriteList = FavoriteItem::where('product_id', $productId)->where('user_id', $user->id)->first();
		if (empty($favoriteList)) {
			$favoriteList = new FavoriteItem;
			$favoriteList->user_id = $user->id;
			$favoriteList->product_id = $productId;
			$favoriteList->save();
			$countFavoriteList = FavoriteItem::where('user_id', $user->id)->count();
			return response()->json(['data' => 'save', 'count' => $countFavoriteList]);
		}
		$favoriteList->delete();
		$countFavoriteList = FavoriteItem::where('user_id', $user->id)->count();
		return response()->json(['data' => 'delete', 'count' => $countFavoriteList]);
	}
	public function favoriteProduct(Request $request)
	{
		//dd(12345);
		$user = Auth::user();
		$produtId = FavoriteItem::where('user_id', $user->id)->pluck('product_id')->toArray();
		//$produtId=Common::dataVisibleGet();
		$productObj = Product::where('status', '=', 1)->whereIn('id', $produtId);

		if (isset($request->brandcheck) && $request->brandcheck != "") {
			$productObj->whereIn('brand_id', $request->brandcheck);
		} else if (isset($request->searchText) && $request->searchText != "") {
			$name = $request->searchText;
			$productObj->where('name', 'LIKE', '%' . $name . '%');
		}
		$data = $this->getData($produtId, $productObj);
		return view('front.offer-product')->with(['products' => $data['products'], 'category' => $data['category'], 'brands' => $data['brands'], 'sbcat' => $data['sbcat']]);
	}

	public function exportProduct()
	{
		return Excel::download(new ProductExport, 'product.xlsx');
	}
	public function getShiping(Request $request)
	{			
		$id = (int)$request->areaid;
		$user = Auth::user();
        $getshippingCharge = 0;
		$order = Order::getUserOrders(true);
		$amount = $order->order_amount;
		$minOrderAmountFreeDelivery = Setting::getFieldVal('mini_order_amt_free_shiping');
		if ((Auth::check() && $user->freeshipping == 'on') || $id == 0 || $amount>=$minOrderAmountFreeDelivery) {
			$getshippingCharge = 0;
		} else {		
			
			$loction = ShippingArea::find($id);
            if(!empty($loction))
			{
			$getshippingCharge = $loction->charge;		     
			}
			else{
				$getshippingCharge = Setting::getFieldVal('sp_charge');
			}
		}
		$totalAmount = $amount + $getshippingCharge;
		$order->shipping_charge = $getshippingCharge;
		$order->total_amount = $totalAmount;
		 
		$order->save();
		$data = json_encode(['shiping' => (float)$getshippingCharge, 'totalAmount' =>(float) $totalAmount]);
		return $data;
	}
}
