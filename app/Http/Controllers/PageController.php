<?php


namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Auth,DB;
use App\Models\Common;
use File;

class PageController extends Controller
{
	
    private $modelObj;
    
    private $viewDir;
	function __construct()
    {
        $this->modelObj = new Page;
        $this->viewDir = 'pages';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $models = $this->modelObj->orderBy('created_at','desc');

        return view('admin.'.$this->viewDir.'.index')->with(['models' => $models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.'.$this->viewDir.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		//dd($request->all());
		 $models = $this->modelObj;
		 $url=Common::slugify($request->title);
         $validator = $models->validator($request);

        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();       
          
			  $imageName="";
			if($request->image_name!='')
			{
     		  $imageName=$request->image_name;
              File::move(public_path('temp/'.$imageName), public_path('uploads/pages/'.$imageName));
			 $models->image_name = $imageName;
			}
		  $models->title = $request->title;
		  $models->status = $request->status;
		  $models->url = $url;
		  $models->meta_title = $request->meta_title;
		  $models->meta_tag = $request->meta_tag;
		  $models->meta_description = $request->meta_description;
		  $models->description = $request->description;
        if ($models->save()) {
            $request->session()->flash('success', 'Page added successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('page.index');
	   
	   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
         return view('admin.'.$this->viewDir.'.edit')->with([
            'model' => $page     
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $validator = $page->validator($request, $page->id);
		$url=Common::slugify($request->title);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
		    $data=$request->except('_token');
			
			 if($request->image_name!='')
			{
				$filePath=public_path('uploads/sliders/'.$page->image_name);
				if (File::exists($filePath)) {
				File::delete($filePath);
				}	
     		  $imageName=$request->image_name;
              File::move(public_path('temp/'.$imageName), public_path('uploads/pages/'.$imageName));
			  $page->image_name=$imageName;
			}
			elseif($request->remove_image_name && $request->remove_image_name=="y")
			{
				$filePath=public_path('uploads/pages/'.$page->image_name);
				if (File::exists($filePath)) {
				File::delete($filePath);
				}
			   $page->image_name="";
			}
		  $page->title = $request->title;
		  $page->status = $request->status;
		  $page->url = $url;
		  $page->meta_title = $request->meta_title;
		  $page->meta_tag = $request->meta_tag;
		  $page->meta_description = $request->meta_description;
		  $page->description = $request->description;
        if ($page->save()) {
            $request->session()->flash('success', 'Page updated successfully.');
        } else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
        return redirect()->route('page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Page $page)	
    {
      try 
        {
            if ($page->delete()) {				
                $request->session()->flash('success', 'Page deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('page.index');   
    }	
    public function dataTable(Request $request)
	{
		   $id = Auth::user()->id; 
		   $offset=0;
	       $limit=10;
		   //$where=" where p.user_id=".$id." and cp.status!='inprocess' ";
		   $where=" where 1";
           $serachStrText=stripslashes($_GET['search']['value']); 	
           $order=" ORDER BY `id` DESC";		   
	       if(isset($_GET['start']) && $_GET['start']!='')
		   {
			   $offset=$_GET['start'];
		   }
		   if(isset($_GET['length']) && $_GET['length']!='')
		   {
			   $limit=$_GET['length'];
		   }
		   
			if($serachStrText!='')
			{
			$alTextA=explode(' ',$serachStrText);
			foreach($alTextA as $serachStr)
			{				
				$serachStr=addslashes($serachStr);
				$whereArray=[];                   
				$whereArray[]="p.title like '%".$serachStr."%'";		
				$whereArray[]="p.description like '%".$serachStr."%'";			
				$where.=" and (".implode(' or ',$whereArray).")";
			}
			$order=" ORDER BY p.`title` ASC";
			}			
			$slectQuery="SELECT p.* FROM pages p ".$where.$order;
				
			 $contractsCnt=DB::select($slectQuery);
			 $total_records=count($contractsCnt);			 
			 $blogs=DB::select($slectQuery." limit ".$offset.",".$limit);//." limit ".$offset.",".$limit
			 $AllData=[];
			 $pages=Page::hydrate($blogs);
			 
			 
			 foreach($pages as $page)
			 {
				 $data=[];
				 $view=route('static',[$page->url]);
				 $data[]='<a href="'. $view.'" title="View" target="_blank">'.$page->title.'</a>';
				 //$data[]=$page->title;				 
				 $data[]=date('d-m-Y',strtotime($page->created_at));
				 $data[]=($page->status==1?'Active':'InActive');
			
                 $edit=route('page.edit',$page->id);				 
                 $delete=route('page.destroy',$page->id);
				$editimage=asset('img/datatable-edit-plus.svg');	
				$deleteImage=asset('img/datatable-delete.svg');				
				 $btn='<a href="'.$edit.'" title="Edit"  class="btn me-2 dataActionBtn btnEdit"><img src="'.$editimage.'" alt="" /></a> <a href="javascript:void(0);"  title="Delete" onclick="deletePage(this)" data-href="'.$delete.'" class="btn me-2 dataActionBtn btnDelete"><img src="'.$deleteImage.'" alt="" /></a>';
				 $data[]='<div class="d-flex justify-content-start">'.$btn.'</div>';
				 $AllData[]=$data;
			 }
			 $draw=(isset($_GET['draw'])?$_GET['draw']:0);
		return json_encode(array('draw'=>$draw,"recordsTotal"=>$total_records,"recordsFiltered"=> $total_records,"data"=>$AllData));
	}
	function uploadImages($foldername)
	{
		
		   $accepted_origins = array("http://dc.com", "https://www.dclessons.com");
		   $imageFolder =public_path('uploads/').$foldername.'/';
		   //echo $imageFolder;exit;
			//$imageFolder = "images/";

			reset ($_FILES);
			$temp = current($_FILES);
			if (is_uploaded_file($temp['tmp_name'])){
			if (isset($_SERVER['HTTP_ORIGIN'])) {
			// same-origin requests won't set an origin. If the origin is set, it must be valid.
			if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
			header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
			} else {
			header("HTTP/1.1 403 Origin Denied");
			return;
			}
			}

			/*
			If your script needs to receive cookies, set images_upload_credentials : true in
			the configuration and enable the following two headers.
			*/
			// header('Access-Control-Allow-Credentials: true');
			// header('P3P: CP="There is no P3P policy."');

			// Sanitize input
			if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
			header("HTTP/1.1 400 Invalid file name.");
			return;
			}

			// Verify extension
			if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
			header("HTTP/1.1 400 Invalid extension.");
			return;
			}

			// Accept upload if there was no origin, or if it is an accepted origin
			$filetowrite = $imageFolder . $temp['name'];
			move_uploaded_file($temp['tmp_name'], $filetowrite);

			// Respond to the successful upload with JSON.
			// Use a location key to specify the path to the saved image resource.
			// { location : '/your/uploaded/image/file'}
			$url=url('/uploads/'.$foldername.'/'.$temp['name']);
			echo json_encode(array('location' => $url));
			} else {
			// Notify editor that the upload failed
			header("HTTP/1.1 500 Server Error");
			}
		exit;
	}
}
