<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use Illuminate\Http\Request;
use Auth;
use DB;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('admin.faq.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $models= new Faq;
        $validator = $models->validator($request);
		if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
        $models->question=$request->question;
        $models->answer=$request->answer;
        $models->status=$request->status;
		if($models->save()){
		 
		 $request->session()->flash('success', 'Faq added successfully.');
		}else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
		return redirect()->route('faqs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        return view('admin.faq.edit')->with(['model'=>$faq]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        
        $validator = $faq->validator($request,$faq->id);
        if ($validator->fails()) return redirect()->back()->withErrors($validator)->withInput();
        $faq->question=$request->question;
        $faq->answer=$request->answer;
        $faq->status=$request->status;
		if($faq->save()){
		 
		 $request->session()->flash('success', 'Faq update successfully.');
		}else {
            $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
		return redirect()->route('faqs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Faq $faq)
    {
        try 
        {
            if ($faq->delete()) {
                $request->session()->flash('success', 'Faq deleted successfully.');
            }

        } catch (Exception $e) {
          $request->session()->flash('error', 'Something went wrong. Please try again in a few minutes.');
        }
       return redirect()->route('faqs.index');
    }
	public function faqdataTable(Request $request)
	
	{
		$userId=Auth::user()->id;
		$userName = config('constant.sendnotfiction');
		   $offset=0;
	       $limit=10;
		   $where="where 1";
           $serachStrText=stripslashes($_GET['search']['value']); 	
           $order=" ORDER BY `id` DESC";		   
	       if(isset($_GET['start']) && $_GET['start']!='')
		   {
			   $offset=$_GET['start'];
		   }
		   if(isset($_GET['length']) && $_GET['length']!='')
		   {
			   $limit=$_GET['length'];
		   }
            if($serachStrText!='')
			{
			  $alTextA=explode(' ',$serachStrText);
			  foreach($alTextA as $serachStr)
			   {				
				$serachStr=addslashes($serachStr);
				$whereArray=[];                   
				$whereArray[]="question like '%".$serachStr."%'";		
				$whereArray[]="answer like '%".$serachStr."%'";		
				$where.=" and (".implode(' or ',$whereArray).")";
			   }
            }						
			$slectQuery="SELECT * FROM faqs ".$where;
		     $contractsCnt=DB::select($slectQuery);
			 $total_records=count($contractsCnt);			 
			 $faqs=DB::select($slectQuery." order by id desc limit ".$offset.",".$limit);//." limit ".$offset.",".$limit
			 $AllData=[];
			 $faqs=Faq::hydrate($faqs);
			 foreach($faqs as $faq)
			 {
				
				$data=[];
               
                $data[]=$faq->question;
                $data[]=$faq->answer;
				$data[]=$faq->status==1 ? "Active":"InActive";
                $delete=route('faqs.destroy',$faq->id);
                $edit=route('faqs.edit',$faq->id);
				$editimage=asset('img/datatable-edit-plus.svg');
				$deleteImage=asset('img/datatable-delete.svg');
                $btn='<a href="'.$edit.'" class="btn me-2 dataActionBtn btnEdit"  title="Edit"><img src="'.$editimage.'" alt="" />
				</a>';
				$btn.='<a href="javascript:void(0);" class="btn me-2 dataActionBtn btnDelete" title="Delete" onclick="deleteUser(this)" data-href="'.$delete.'"><img src="'.$deleteImage.'" alt="" /></a>';
			
                $data[]='<div class="d-flex justify-content-start">'.$btn.'</div>';	
				
				$AllData[]=$data;
			}
			 $draw=(isset($_GET['draw'])?$_GET['draw']:0);
		return json_encode(array('draw'=>$draw,"recordsTotal"=>$total_records,"recordsFiltered"=> $total_records,"data"=>$AllData));
    }
}
